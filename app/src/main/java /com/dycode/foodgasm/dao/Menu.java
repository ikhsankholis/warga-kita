package com.dycode.foodgasm.dao;

// THIS CODE IS GENERATED BY greenDAO, EDIT ONLY INSIDE THE "KEEP"-SECTIONS

// KEEP INCLUDES - put your custom includes here
// KEEP INCLUDES END
/**
 * Entity mapped to table Menu.
 */
public class Menu {

    private String _id;
    private java.util.Date createdAt;
    private java.util.Date updatedAt;
    private String name;
    private Integer price;
    private Integer discount;
    private String phone;
    private String categoryId;
    private String categoryName;
    private String description;
    private String pic;
    private String restoId;
    private Integer amount;
    private Integer subtotalPrice;

    // KEEP FIELDS - put your custom fields here
    // KEEP FIELDS END

    public Menu() {
    }

    public Menu(String _id) {
        this._id = _id;
    }

    public Menu(String _id, java.util.Date createdAt, java.util.Date updatedAt, String name, Integer price, Integer discount, String phone, String categoryId, String categoryName, String description, String pic, String restoId, Integer amount, Integer subtotalPrice) {
        this._id = _id;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.name = name;
        this.price = price;
        this.discount = discount;
        this.phone = phone;
        this.categoryId = categoryId;
        this.categoryName = categoryName;
        this.description = description;
        this.pic = pic;
        this.restoId = restoId;
        this.amount = amount;
        this.subtotalPrice = subtotalPrice;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public java.util.Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(java.util.Date createdAt) {
        this.createdAt = createdAt;
    }

    public java.util.Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(java.util.Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getRestoId() {
        return restoId;
    }

    public void setRestoId(String restoId) {
        this.restoId = restoId;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Integer getSubtotalPrice() {
        return subtotalPrice;
    }

    public void setSubtotalPrice(Integer subtotalPrice) {
        this.subtotalPrice = subtotalPrice;
    }

    // KEEP METHODS - put your custom methods here
    // KEEP METHODS END

}
