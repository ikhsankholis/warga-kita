package com.dycode.foodgasm.dao;

// THIS CODE IS GENERATED BY greenDAO, EDIT ONLY INSIDE THE "KEEP"-SECTIONS

// KEEP INCLUDES - put your custom includes here
// KEEP INCLUDES END
/**
 * Entity mapped to table RESTO_ADRESS.
 */
public class RestoAdress {

    private String _id;
    private String street;
    private String district;
    private String city;
    private String province;
    private String postalCode;

    // KEEP FIELDS - put your custom fields here
    // KEEP FIELDS END

    public RestoAdress() {
    }

    public RestoAdress(String _id) {
        this._id = _id;
    }

    public RestoAdress(String _id, String street, String district, String city, String province, String postalCode) {
        this._id = _id;
        this.street = street;
        this.district = district;
        this.city = city;
        this.province = province;
        this.postalCode = postalCode;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    // KEEP METHODS - put your custom methods here
    // KEEP METHODS END

}
