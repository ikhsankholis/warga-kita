package com.dycode.foodgasm.data.model

import android.content.Intent

data class LoginSocialEvent(val requestCode : Int, val resultCode : Int, val data: Intent?)