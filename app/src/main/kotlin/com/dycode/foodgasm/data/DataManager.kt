package com.dycode.foodgasm.feature.data

import com.dycode.foodgasm.model.*
import com.dycode.foodgasm.rest.ApiV2Service
import com.dycode.foodgasm.rest.ApiVeritransService
import com.dycode.foodgasm.utils.Constants
import rx.Observable
import javax.inject.Inject

/**
 * Created by USER on 5/19/2017.
 */

class DataManager @Inject constructor(val apiV2Service: ApiV2Service, val apiVeritransService: ApiVeritransService){
    fun  login(email: String, password: String): Observable<AccessToken> {
        val postLogin = PostLogin(email, password)
        return apiV2Service.login(postLogin)
    }

    fun getOrderDetail(orderId : String): Observable<ApiResponse<OrderDetail>>{
        return apiV2Service.getOrderDetail(orderId)
    }

    fun postRequestBalanceDoku(accountId: String) : Observable<ApiResponse<DokuWalletBalanceResponse>>{
        val dokuBalance = DokuWalletBalance(Constants.DOKU_BALANCE_ENDPOINT, accountId)
        return apiV2Service.postBalanceDokuWallet(dokuBalance)
    }

    fun postPayDokuWalet(orderId: String, accountId: String) : Observable<ApiResponse<DokuWalletResponse>>{
        val dokuWallet = DokuWallet(accountId)
        return apiV2Service.postPayDokuWallet(orderId, dokuWallet)
    }

    fun postEcashBalance(orderId: String, maskedCard: String) : Observable<ApiResponse<Any>>{
        val postBalanceEcash = EcashBalance(orderId, maskedCard)
        return apiV2Service.postBalanceEcash(postBalanceEcash)
    }

    fun postPayEcash(orderId: String, msisdn : String) : Observable<ApiResponse<EcashPaymentModel>>{
        val postEcash = EcashPayment(msisdn)
        return apiV2Service.postPaymentEcash(orderId, postEcash)
    }

    fun getVeritransToken(tokenId : String , cardCvv: String , twoClick : Boolean , clientKey : String, grossAmount : String , secure :  Boolean , paymentType : String) : Observable<CreditCard>{
        return apiVeritransService.getVeritransToken(tokenId, cardCvv, twoClick, clientKey, grossAmount, secure, paymentType)
    }

    fun getCreditCardToken(creditCardId: String) : Observable<ApiResponse<CreditCardOrder>> {
        return apiV2Service.getCreditCardToken(creditCardId)
    }

    fun postVeritransPayment(orderId: String, tokenId: String, bank: String) : Observable<ApiResponse<Payment>> {
        return apiV2Service.postVeritransPayment(orderId, PostPayment(tokenId, bank))
    }

    fun getVeritransStatus(orderId: String): Observable<ApiResponse<Any>> {
        return apiV2Service.getStatusVeritrans(orderId)
    }

    fun patchOrder(id: String, action: String, reason: String): Observable<Order> {
        return apiV2Service.patchOrder(id, PatchOrder(action, reason))
    }

    fun getPhoneVerification(phone: String): Observable<ApiResponse<ReqPhoneVerify>> {
        return apiV2Service.getPhoneVerification(phone)
    }

    fun postVerifyPhone(code: String, tokenId: String) : Observable<ApiResponse<Any>>{
        val body = ReqPhoneVerify(code, tokenId)
        return apiV2Service.postPhoneVerification(body)
    }

    fun postRegister(fullname : String? = "", username : String? = "", email : String? = "", phone : String? = "" , password : String? = "", passwordConfirmation : String? = "", photo: String? = "",socialId : String? = "", isPhoneVerified : Boolean= true, provider: String? = ""): Observable<Register> {
        val body = PostRegister(fullname, username, email, phone, password, passwordConfirmation, null, provider, socialId, photo, isPhoneVerified)
        return apiV2Service.postRegister(body)
    }

    fun putProfileData(fullname : String? = "", username : String? = "", email : String? = "", phone : String? = "" , isPhoneVerified : Boolean= false) : Observable<ApiResponse<Profile>>{
        val body = PostRegister(fullname, username, email, phone, null, null, null, null, null, null, isPhoneVerified)
        return apiV2Service.putProfile(body)
    }





}