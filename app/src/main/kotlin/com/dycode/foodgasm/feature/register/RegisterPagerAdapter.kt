package com.dycode.foodgasm.feature.register

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.dycode.foodgasm.feature.register.step1.PhoneNumberFragment
import com.dycode.foodgasm.feature.register.step2.VerificationPhoneFragment
import com.dycode.foodgasm.feature.register.step3.FormFragment


class RegisterPagerAdapter : FragmentPagerAdapter {

    constructor(fragmentManager: FragmentManager): super(fragmentManager){}

    override fun getItem(position: Int): Fragment = when(position){
        0 -> PhoneNumberFragment.newInstance()
        1 -> VerificationPhoneFragment.newInstance()
        2 -> FormFragment.newInstance()
        else -> PhoneNumberFragment.newInstance()
    }


    override fun getCount(): Int = 3

}