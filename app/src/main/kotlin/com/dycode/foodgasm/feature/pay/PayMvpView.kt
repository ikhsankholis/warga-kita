package com.dycode.foodgasm.feature.pay

import com.dycode.foodgasm.feature.base.MvpView
import com.dycode.foodgasm.model.OrderDetail


interface PayMvpView : MvpView{
    fun showDetail(data: OrderDetail?)
    fun showError(appErrorMessage: String?)
    fun showLoadingBalance()
    fun showBalance(lastBalance: String?)
    fun showFailedLoadBalance()
    fun showLoadingPay()
    fun showSuccess()
    fun showInputCvv()
    fun  showWebview(redirect_url: String?, bank: String?, tokenId: String?)
    fun showDeclinedOrderDialog()
    fun hideLoading()
    fun navigateToCancelled()


}