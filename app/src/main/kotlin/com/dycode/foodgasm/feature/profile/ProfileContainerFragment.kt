package com.dycode.foodgasm.feature.profile

import com.dycode.foodgasm.R
import com.dycode.foodgasm.fragment.BaseFragment
import com.dycode.foodgasm.fragment.ProfileFragment
import com.dycode.foodgasm.utils.Utils


class ProfileContainerFragment : BaseFragment() {

    var mode = MODE_LOGIN
    var profileLoginFragment = ProfileLoginFragment()

    companion object {
        val MODE_LOGIN = 1
        val MODE_PROFILE = 2
    }

    override fun getLayout(): Int = R.layout.fragment_profile_container

    override fun onResume() {
        super.onResume()
        if (Utils.getToken(mContext) != null) {
            childFragmentManager.beginTransaction()
                    .replace(R.id.frameContainer, ProfileFragment())
                    .commit()
            mode = MODE_PROFILE
        } else {
            childFragmentManager.beginTransaction()
                    .replace(R.id.frameContainer, profileLoginFragment)
                    .commit()
            mode = MODE_LOGIN

        }
    }

}