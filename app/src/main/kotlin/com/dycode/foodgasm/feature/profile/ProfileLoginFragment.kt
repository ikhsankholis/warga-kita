package com.dycode.foodgasm.feature.profile

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import com.dycode.foodgasm.FoodGasmApp
import com.dycode.foodgasm.R
import com.dycode.foodgasm.activity.LoginAct
import com.dycode.foodgasm.activity.MainAct
import com.dycode.foodgasm.activity.ReviewOrderAct
import com.dycode.foodgasm.data.model.LoginSocialEvent
import com.dycode.foodgasm.data.model.LoginEvent
import com.dycode.foodgasm.feature.register.RegisterActivity
import com.dycode.foodgasm.feature.skiplogin.SkipLoginActivity
import com.dycode.foodgasm.fragment.BaseFragment
import com.dycode.foodgasm.model.Menu
import com.dycode.foodgasm.model.Register
import com.dycode.foodgasm.model.Restaurant
import com.dycode.foodgasm.utils.SPManager
import com.dycode.foodgasm.utils.Utils
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.sromku.simple.fb.Permission
import com.sromku.simple.fb.SimpleFacebook
import com.sromku.simple.fb.entities.Profile
import com.sromku.simple.fb.listeners.OnLoginListener
import com.sromku.simple.fb.listeners.OnProfileListener
import kotlinx.android.synthetic.main.fragment_profile_login.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import javax.inject.Inject

class ProfileLoginFragment : BaseFragment(), ProfileLoginMvpView, GoogleApiClient.OnConnectionFailedListener {


    @Inject
    lateinit var presenter: ProfileLoginPresenter

    companion object {
        fun newInstance(bundle: Bundle): ProfileLoginFragment {
            val fragment = ProfileLoginFragment()
            fragment.arguments = bundle
            return fragment
        }

    }

    lateinit var simpleFacebook: SimpleFacebook

    internal var properties: Profile.Properties = Profile.Properties.Builder()
            .add(Profile.Properties.ID)
            .add(Profile.Properties.NAME)
            .add(Profile.Properties.EMAIL)
            .add(Profile.Properties.PICTURE)
            .build()

    lateinit var mGoogleApiClient: GoogleApiClient

    var fullName: String? = null
    var email: String? = null
    var socialId: String? = null
    var photo: String? = null
    var provider: String? = null

    override fun onStart() {
        super.onStart()
        mGoogleApiClient.connect()
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this)

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestId()
                .requestProfile()
                .build()

        mGoogleApiClient = GoogleApiClient.Builder(activity)
                .addApi<GoogleSignInOptions>(Auth.GOOGLE_SIGN_IN_API, gso)
                .build()
    }

    override fun getLayout(): Int = R.layout.fragment_profile_login

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        applicationComponent.inject(this)
        presenter.attachView(this)

        if (arguments != null) {
            tvCaption.setText(R.string.please_login_to_start_order)
        }


        btnSignIn.setOnClickListener {
            val intent = Intent(mContext, LoginAct::class.java)
            intent.putExtra("EXTRA_ORDER", arguments)
            startActivity(intent)
        }
        btnSignUp.setOnClickListener {
            RegisterActivity.start(mContext)
        }
        btnConnectToFb.setOnClickListener {
            loginFb()
        }

        btnConnectToGoogle.setOnClickListener {
            val signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient)
            startActivityForResult(signInIntent, 2233)
        }

    }

    override fun onStop() {
        super.onStop()
        mGoogleApiClient.disconnect()
        EventBus.getDefault().unregister(this)
    }

    override fun onConnectionFailed(p0: ConnectionResult) {

    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onLoginSocialEvent(loginSocialEvent: LoginSocialEvent) {
        val result = Auth.GoogleSignInApi.getSignInResultFromIntent(loginSocialEvent.data)
        if (result != null) {
            if (result.isSuccess) {
                val acct = result.signInAccount
                if (acct != null) {
                    socialId = acct.id
                    email = acct.email
                    fullName = acct.displayName

                    if (acct.photoUrl != null)
                        photo = acct.photoUrl?.toString()
                    provider = "google"
                    email?.let { socialId?.let { it1 -> presenter.register(it, it1, provider!!)  } }
                    if (mGoogleApiClient.isConnected) {
                        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback { status ->

                        }
                    }
                }
            }
        } else {
            simpleFacebook.onActivityResult(loginSocialEvent.requestCode, loginSocialEvent.resultCode, loginSocialEvent.data)
        }
    }

    fun loginFb() {
        simpleFacebook.login(object : OnLoginListener {
            override fun onFail(reason: String?) {
                Toast.makeText(mContext, reason, Toast.LENGTH_SHORT).show()
            }

            override fun onCancel() {

            }

            override fun onException(throwable: Throwable?) {
                Toast.makeText(mContext, throwable?.message, Toast.LENGTH_SHORT).show()

            }

            override fun onLogin(accessToken: String?, acceptedPermissions: MutableList<Permission>?, declinedPermissions: MutableList<Permission>?) {
                getProfileFb()
            }

        })
    }

    fun getProfileFb() {
        simpleFacebook.getProfile(properties, object : OnProfileListener() {
            override fun onComplete(profile: Profile?) {
                fullName = profile?.name
                email = profile?.email
                socialId = profile?.id
                provider = "facebook"
                email?.let { socialId?.let { it1 -> presenter.register(it, it1, provider!!)  } }


            }

            override fun onFail(reason: String?) {
                super.onFail(reason)
                Toast.makeText(mContext, reason, Toast.LENGTH_SHORT).show()
            }

            override fun onException(throwable: Throwable?) {
                super.onException(throwable)
                Toast.makeText(mContext, throwable?.message, Toast.LENGTH_SHORT).show()
            }

        })
    }

    override fun onResume() {
        super.onResume()
        simpleFacebook = SimpleFacebook.getInstance(activity)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView()
    }

    override fun showLoading() {
        showProgressDialog("Loading...")
    }

    override fun showError(appErrorMessage: String) {
        showAlertDialog("Alert", appErrorMessage)
    }

    override fun hideLoading() {
        progressDialog?.hide()
    }

    override fun moveToRegister(register: Register?) {
        if (register != null) {
            Utils.saveTokenFromFb(mContext, register)
            fullName?.let { it1 -> email?.let { it2 -> socialId?.let { it3 -> RegisterActivity.start(mContext, it1, it2, "", it3) } } }
            activity.overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out)
        }

    }

    override fun moveToMain(register: Register?) {
        if (register != null) {
            Utils.saveTokenFromFb(mContext, register)
            val foodGasmApp = activity.application as FoodGasmApp
            foodGasmApp.registerWithNotificationHubs()
            SPManager.setIsRegister(mContext, true)
            if (arguments != null) {
                val intent = Intent(mContext, ReviewOrderAct::class.java)
                intent.putExtra(SkipLoginActivity.EXTRA_ID, arguments.getInt(SkipLoginActivity.EXTRA_ID))
                intent.putExtra(ReviewOrderAct.FLAG, arguments.getInt(ReviewOrderAct.FLAG))
                intent.putExtra(Restaurant.RESTAURANT, arguments.getParcelable<Restaurant>(Restaurant.RESTAURANT) as Restaurant)
                intent.putExtra(Menu.MENU, arguments.getParcelableArrayList<Menu>(Menu.MENU))
                intent.putExtra(SkipLoginActivity.EXTRA_PERSON, arguments.getString(SkipLoginActivity.EXTRA_PERSON))
                intent.putExtra(SkipLoginActivity.EXTRA_DAY, arguments.getString(SkipLoginActivity.EXTRA_DAY))
                intent.putExtra(SkipLoginActivity.EXTRA_MINUTE, arguments.getString(SkipLoginActivity.EXTRA_MINUTE))
                intent.putExtra(SkipLoginActivity.EXTRA_FLAG, arguments.getInt(SkipLoginActivity.EXTRA_FLAG))
                intent.putExtra(SkipLoginActivity.EXTRA_BOOKTYPE, arguments.getInt(SkipLoginActivity.EXTRA_BOOKTYPE))
                intent.putExtra(SkipLoginActivity.EXTRA_COMMISION, arguments.getString(SkipLoginActivity.EXTRA_COMMISION))
                intent.putExtra(SkipLoginActivity.EXTRA_CLOSE_TIME, arguments.getString(SkipLoginActivity.EXTRA_CLOSE_TIME))
                intent.putExtra(SkipLoginActivity.EXTRA_OPEN_TIME, arguments.getString(SkipLoginActivity.EXTRA_OPEN_TIME))
                startActivity(intent)
                bus.post(LoginEvent())
            } else {
                val intent = Intent(activity, MainAct::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
                activity.overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out)
            }
        }

    }


}