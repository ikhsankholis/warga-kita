package com.dycode.foodgasm.feature.register.step1

import com.dycode.foodgasm.feature.base.LeMvpView
import com.dycode.foodgasm.model.ReqPhoneVerify

/**
 * Created by USER on 5/31/2017.
 */
interface PhoneNumberMvpView : LeMvpView{
    fun showSuccess(data: ReqPhoneVerify)
}