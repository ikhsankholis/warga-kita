package com.dycode.foodgasm.feature.profile

import com.dycode.foodgasm.feature.base.BasePresenter
import com.dycode.foodgasm.feature.data.DataManager
import com.dycode.foodgasm.rest.NetworkError
import rx.Subscription
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import javax.inject.Inject

class ProfileLoginPresenter @Inject constructor(val dataManager: DataManager) : BasePresenter<ProfileLoginMvpView>() {

    var sub : Subscription? = null

    fun register(email: String, socialId: String, provider: String) {
        mvpView?.showLoading()
        sub = dataManager.postRegister(provider = provider, username = socialId, socialId = socialId, email = email, isPhoneVerified = false)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        {
                            apiResponse -> mvpView?.hideLoading()
                            if (apiResponse.isFirstTime) {
                                mvpView?.moveToRegister(apiResponse)
                            } else {
                                mvpView?.moveToMain(apiResponse)
                            }
                        },
                        {
                            error -> mvpView?.hideLoading()
                            mvpView?.showError(NetworkError(error).appErrorMessage)
                        })
    }

    override fun detachView() {
        super.detachView()
        sub?.unsubscribe()
    }
}
