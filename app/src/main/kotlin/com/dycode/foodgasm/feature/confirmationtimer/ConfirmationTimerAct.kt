package com.dycode.foodgasm.feature.confirmationtimer

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import com.dycode.foodgasm.R
import com.dycode.foodgasm.activity.BaseActivity
import com.dycode.foodgasm.activity.BookOrderCompleteAct
import com.dycode.foodgasm.activity.MainAct
import com.dycode.foodgasm.activity.ReorderAct
import com.dycode.foodgasm.feature.pay.PayActivity
import com.dycode.foodgasm.model.Order
import com.dycode.foodgasm.receiver.FoodgasmNotificationHandler
import com.dycode.foodgasm.utils.Constants
import com.dycode.foodgasm.utils.FoodgasmDateUtil
import com.dycode.foodgasm.utils.SPManager
import kotlinx.android.synthetic.main.activity_confirmation_timer.*
import javax.inject.Inject

/**
 * Created by USER on 5/23/2017.
 */

class ConfirmationTimerAct : BaseActivity(), ConfirmationTimerMvpView {


    val handler = Handler()
    var timer: Float = 0f
    val max = 900f

    var orderId: String? = null

    @Inject
    lateinit var presenter: ConfirmationTimerPresenter

    companion object {
        val EXTRA_ORDER_ID = "ConfirmationTimerAct.EXTRA_ORDER_ID"

        fun start(context: Context, orderId: String) {
            val intent = Intent(context, ConfirmationTimerAct::class.java)
            intent.putExtra(EXTRA_ORDER_ID, orderId)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            context.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_confirmation_timer)
        setToolbar(false)
        applicationComponent.inject(this)
        presenter.attachView(this)


        orderId = intent.extras.getString(EXTRA_ORDER_ID)
        val milisCreatedDate = FoodgasmDateUtil.getMilisDate(SPManager.getString(mActivity, SPManager.ORDER_CREATED_TIME))
        val milisNow = FoodgasmDateUtil.getMilisDate(FoodgasmDateUtil.getTimestampNow())
        val endMilisDate = milisCreatedDate + 900000
        if(milisNow > endMilisDate){
            SPManager.saveString(mActivity, SPManager.ORDER_CREATED_TIME, null)
            SPManager.saveString(mActivity, Order.ORDER_ID, null)
            MainAct.start(this)
        }else{
            setupTimer((endMilisDate - milisNow) / 1000)
            autoCheckStatusOrder()
        }
    }

    private fun autoCheckStatusOrder() {
        val runnable = object : Runnable {
            override fun run() {
                if (isDestroyed) {
                    return
                }

                orderId?.let {
                    presenter.checkStatusOrder(it)
                }

                handler.postDelayed(this, 5000)
            }
        }
        handler.post(runnable)
    }

    private fun setupTimer(startingPoint: Long) {
        timer= 900 - startingPoint.toFloat()
        picker.isEnable = false
        val runnable = object : Runnable {
            override fun run() {

                if (isDestroyed) {
                    return
                }
                picker.setValue(timer)
                if (timer < max) {
                    timer += 1f
                    handler.postDelayed(this, 1000)
                } else {
                    SPManager.saveString(mActivity, SPManager.ORDER_CREATED_TIME, null)
                    SPManager.saveString(mActivity, Order.ORDER_ID, null)
                    val i = Intent(mActivity, BookOrderCompleteAct::class.java)
                    i.putExtra(BookOrderCompleteAct.FLAG, BookOrderCompleteAct.FLAG_ORDER_EXPIRED)
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                    startActivity(i)
                }
            }
        }
        handler.post(runnable)
    }

    override fun getToolbarTitle(): Int {
        return R.string.order_process
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView()

    }

    override fun showStatus(orderStatus: Int?) {
        if (orderStatus != null) {
            if (orderStatus == 0) {

            } else if (orderStatus == 2) {
                SPManager.saveString(mActivity, SPManager.ORDER_CREATED_TIME, null)
                SPManager.saveString(mActivity, Order.ORDER_ID, null)
                val i = Intent(this, BookOrderCompleteAct::class.java)
                i.putExtra(BookOrderCompleteAct.FLAG, BookOrderCompleteAct.FLAG_ORDER_DECLINED)
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                startActivity(i)
            } else if (orderStatus == 3) {
                SPManager.saveString(mActivity, SPManager.ORDER_CREATED_TIME, null)
                SPManager.saveString(mActivity, Order.ORDER_ID, null)

                val i = Intent(this, BookOrderCompleteAct::class.java)
                i.putExtra(BookOrderCompleteAct.FLAG, BookOrderCompleteAct.FLAG_ORDER_CANCELLED)
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                startActivity(i)
            } else if (orderStatus == 1 || orderStatus == 4) {
                SPManager.saveString(mActivity, SPManager.ORDER_CREATED_TIME, null)
                orderId?.let {
                    PayActivity.start(this, it)
                }
            } else if(orderStatus == 5){
                SPManager.saveString(mActivity, Constants.Inbox.STATUS_INBOX, null)
                val intent = Intent(mActivity, ReorderAct::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                intent.putExtra(FoodgasmNotificationHandler.NOTIF_ID, 0)
                intent.putExtra(FoodgasmNotificationHandler.NOTIF_ORDER_ID, orderId)
                intent.putExtra(FoodgasmNotificationHandler.NOTIF_TYPE, FoodgasmNotificationHandler.ORDER_TYPE_REORDER)
                startActivity(intent)
            }
        }

    }


}