package com.dycode.foodgasm.feature.login

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.method.PasswordTransformationMethod
import com.dycode.foodgasm.FoodGasmApp
import com.dycode.foodgasm.R
import com.dycode.foodgasm.activity.BaseActivity
import com.dycode.foodgasm.activity.ForgetPasswordAct
import com.dycode.foodgasm.activity.MainAct
import com.dycode.foodgasm.model.AccessToken
import com.dycode.foodgasm.utils.SPManager
import com.dycode.foodgasm.utils.Utils
import io.fabric.sdk.android.services.common.CommonUtils
import kotlinx.android.synthetic.main.activity_login.*
import javax.inject.Inject


class LoginAct : BaseActivity(), LoginMvpView {


    @Inject
    lateinit var presenter : LoginPresenter

    companion object {
        fun start(context: Context) {
            val intent = Intent(context, LoginAct::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            context.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        applicationComponent.inject(this)
        window.setBackgroundDrawableResource(R.mipmap.img_highlightblur)
        presenter.attachView(this)
        setToolbar(true, R.mipmap.btn_back)

        linContainer.requestFocus()
        linContainer.setOnClickListener {
            CommonUtils.hideKeyboard(this, inpUsername)
            inpPassword.clearFocus()
            inpUsername.clearFocus()
        }



        btnSignIn.setOnClickListener{
            presenter.login(inpUsername.text.toString(), inpPassword.text.toString())
        }

        tvForgot.setOnClickListener {
            val intent = Intent(this, ForgetPasswordAct::class.java)
            startActivity(intent)
            overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out)
        }


    }

    override fun onDestroy() {
        presenter.detachView()
        super.onDestroy()
    }

    override fun getToolbarTitle(): Int {
        return R.string.empty
    }

    override fun showLoading(isShow: Boolean) {
        if(isShow)
            showProgressDialog("Loading...")
        else
            progressDialog?.hide()
    }

    override fun showError(strMessage: String) {
        showAlertDialog("Alert", strMessage)
    }

    override fun showSuccess(accessToken: AccessToken) {
        Utils.saveToken(mActivity, accessToken)
        val foodGasmApp = application as FoodGasmApp
        foodGasmApp.registerWithNotificationHubs()
        SPManager.setIsRegister(mActivity, true)
        val intent = Intent(mActivity, MainAct::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
        finish()

    }
}