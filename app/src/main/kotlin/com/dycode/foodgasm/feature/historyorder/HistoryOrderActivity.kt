package com.dycode.foodgasm.feature.historyorder

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.dycode.foodgasm.R
import com.dycode.foodgasm.activity.BaseActivity
import com.dycode.foodgasm.fragment.OrderFragment


class HistoryOrderActivity : BaseActivity(){

    companion object{
        fun start(context: Context){
            context.startActivity(Intent(context, HistoryOrderActivity::class.java))
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_history_order)
        setToolbar(true)

        supportFragmentManager.beginTransaction()
                .replace(R.id.frameContainer, OrderFragment.newInstance())
                .commit()


    }

    override fun getToolbarTitle(): Int {
        return R.string.title_order
    }
}