package com.dycode.foodgasm.feature.skiplogin

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.dycode.foodgasm.R
import com.dycode.foodgasm.activity.BaseActivity
import com.dycode.foodgasm.activity.ReviewOrderAct
import com.dycode.foodgasm.dao.Menu
import com.dycode.foodgasm.data.model.LoginEvent
import com.dycode.foodgasm.data.model.LoginSocialEvent
import com.dycode.foodgasm.feature.profile.ProfileLoginFragment
import com.dycode.foodgasm.model.Restaurant
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode


class SkipLoginActivity : BaseActivity() {

    companion object {
        val EXTRA_ID = "id"
        val EXTRA_PERSON = "personAmount"
        val EXTRA_DAY = "day"
        val EXTRA_MINUTE = "minute"
        val EXTRA_FLAG = "flag"
        val EXTRA_BOOKTYPE = "bookType"
        val EXTRA_CLOSE_TIME = "getClosetime"
        val EXTRA_COMMISION = "commission"
        val EXTRA_OPEN_TIME = "getOpentime"



        fun start(context: Context, id: Int, flag: Int, restaurant: Restaurant?, menuList: ArrayList<Menu>?, person: String?, dayBook: String?, timeBook: String?, bookType: Int?, commission: String?, getClosetime: String?, getOpentime: String?) {
            val intent = Intent(context, SkipLoginActivity::class.java)
            intent.putExtra(EXTRA_ID, id)
            intent.putExtra(ReviewOrderAct.FLAG, flag)
            intent.putExtra(Restaurant.RESTAURANT, restaurant)
            intent.putExtra(com.dycode.foodgasm.dao.Menu.MENU, menuList)
            intent.putExtra(EXTRA_PERSON, person)
            intent.putExtra(EXTRA_DAY, dayBook)
            intent.putExtra(EXTRA_MINUTE, timeBook)
            intent.putExtra(EXTRA_FLAG, flag)
            intent.putExtra(EXTRA_BOOKTYPE, bookType)
            intent.putExtra(EXTRA_COMMISION, commission)
            intent.putExtra(EXTRA_CLOSE_TIME, getClosetime)
            intent.putExtra(EXTRA_OPEN_TIME, getOpentime)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            context.startActivity(intent)
        }


    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onLoginEvent(event: LoginEvent){
        finish()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_skip_login)
        setToolbar(true)
        supportFragmentManager.beginTransaction()
                .replace(R.id.frameContainer, ProfileLoginFragment.newInstance(intent.extras))
                .commit()


    }


    override fun getToolbarTitle(): Int {
        return R.string.empty
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        EventBus.getDefault().post(LoginSocialEvent(requestCode, resultCode, data))
    }

}