package com.dycode.foodgasm.feature.login

import com.dycode.foodgasm.feature.base.BasePresenter
import com.dycode.foodgasm.feature.data.DataManager
import com.dycode.foodgasm.rest.NetworkError
import rx.Subscription
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import javax.inject.Inject

/**
 * Created by USER on 5/19/2017.
 */

class LoginPresenter @Inject constructor(val mDataManager: DataManager) : BasePresenter<LoginMvpView>() {

    var subLogin: Subscription? = null

    fun login(email: String, password: String) {
        mvpView?.showLoading(true)
        subLogin = mDataManager.login(email, password)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        {
                            apiResponse ->
                            mvpView?.showLoading(false)
                            mvpView?.showSuccess(apiResponse)
                        },
                        {
                            e ->
                            mvpView?.showLoading(false)
                            mvpView?.showError(NetworkError(e).loginErrorMessage)
                        }
                )

    }

    override fun detachView() {
        super.detachView()
        subLogin?.unsubscribe()
    }
}