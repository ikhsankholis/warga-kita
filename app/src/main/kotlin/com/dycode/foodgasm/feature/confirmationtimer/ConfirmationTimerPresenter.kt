package com.dycode.foodgasm.feature.confirmationtimer

import com.dycode.foodgasm.feature.base.BasePresenter
import com.dycode.foodgasm.feature.data.DataManager
import rx.Subscription
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import javax.inject.Inject


class ConfirmationTimerPresenter @Inject constructor(val dataManager: DataManager): BasePresenter<ConfirmationTimerMvpView>(){

    var subOrderDetail : Subscription? = null

    fun checkStatusOrder(id : String){
        checkViewAttached()
        subOrderDetail = dataManager.getOrderDetail(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe (
                    {
                        apiResponse  -> mvpView?.showStatus(apiResponse.data.orderStatus)
                    },
                    {
                        e  ->
                    }
                )
    }

    override fun detachView() {
        super.detachView()
        subOrderDetail?.unsubscribe()
    }
}