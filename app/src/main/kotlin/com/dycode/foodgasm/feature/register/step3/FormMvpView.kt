package com.dycode.foodgasm.feature.register.step3

import com.dycode.foodgasm.feature.base.LeMvpView
import com.dycode.foodgasm.model.AccessToken
import com.dycode.foodgasm.model.Register


interface FormMvpView : LeMvpView {
    fun showSuccess()
    fun navigateToMain()
    fun saveToken(apiResponse: Register?)
}