package com.dycode.foodgasm.feature.login

import com.dycode.foodgasm.feature.base.MvpView
import com.dycode.foodgasm.model.AccessToken

/**
 * Created by USER on 5/19/2017.
 */

interface LoginMvpView : MvpView{

    fun showLoading(isShow : Boolean)

    fun showError(strMessage : String)

    fun showSuccess(accessToken: AccessToken)

}