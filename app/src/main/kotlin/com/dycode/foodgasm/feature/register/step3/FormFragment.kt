package com.dycode.foodgasm.feature.register.step3

import android.content.Intent
import android.os.Bundle
import android.text.method.PasswordTransformationMethod
import android.view.View
import com.afollestad.materialdialogs.MaterialDialog
import com.dycode.foodgasm.FoodGasmApp
import com.dycode.foodgasm.R
import com.dycode.foodgasm.activity.HighLightAct
import com.dycode.foodgasm.activity.MainAct
import com.dycode.foodgasm.feature.register.RegisterActivity
import com.dycode.foodgasm.fragment.BaseFragment
import com.dycode.foodgasm.model.AccessToken
import com.dycode.foodgasm.model.Register
import com.dycode.foodgasm.utils.SPManager
import com.dycode.foodgasm.utils.Utils
import kotlinx.android.synthetic.main.fragment_form_register.*
import javax.inject.Inject

class FormFragment: BaseFragment(), FormMvpView {

    @Inject
    lateinit var presenter : FormPresenter

    var isFirstTime :Boolean = true

    companion object {
        fun newInstance(): FormFragment {
            return FormFragment()
        }
    }

    override fun getLayout(): Int = R.layout.fragment_form_register

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        applicationComponent.inject(this)
        presenter.attachView(this)
        btnHide.setOnClickListener {
            if (inpPassword.transformationMethod != null) {
                inpPassword.transformationMethod = null
                btnHide.setImageResource(R.mipmap.btn_unhide)
            } else {
                inpPassword.transformationMethod = PasswordTransformationMethod()
                btnHide.setImageResource(R.mipmap.btn_hide)
            }
        }

        btnRegister.setOnClickListener{
            val registerAct = activity as RegisterActivity
            registerAct.phone?.let {
                presenter.register(inpFullName.text.toString(), inpUsername.text.toString(), inpEmail.text.toString(), it, inpPassword.text.toString(), registerAct.photo, registerAct.socialId)
            }
        }


    }

    override fun onResume() {
        super.onResume()
        if(isFirstTime){
            val registerAct = activity as RegisterActivity
            inpEmail.setText(registerAct.email)
            inpFullName.setText(registerAct.fullname)
            if(registerAct.fullname != null){
                inpPassword.visibility = View.GONE
                inpUsername.visibility = View.GONE
                btnHide.visibility = View.GONE
            }
            isFirstTime = false

        }
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView()
    }

    override fun showLoading() {
        showProgressDialog("Registering..")
    }

    override fun showSuccess() {
        MaterialDialog.Builder(activity)
                .title("Success")
                .content("Registration success! Please check your email to verify")
                .positiveText(R.string.ok)
                .onPositive { _, _ ->
                    val intent = Intent(activity, HighLightAct::class.java)
                    startActivity(intent)
                    activity.overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out)
                    activity.finish()
                }
                .cancelable(false)
                .show()
    }

    override fun showError(appErrorMessage: String) {
        showAlertDialog("Alert", appErrorMessage)
    }

    override fun hideLoading() {
        progressDialog?.dismiss()
    }

    override fun navigateToMain() {
        val foodGasmApp = activity.application as FoodGasmApp
        foodGasmApp.registerWithNotificationHubs()
        SPManager.setIsRegister(activity, true)
        val intent = Intent(activity, MainAct::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)

    }

    override fun saveToken(apiResponse: Register?) {
        Utils.saveTokenFromFb(activity, apiResponse)

    }


}
