package com.dycode.foodgasm.feature.register.step2

import com.dycode.foodgasm.feature.base.BasePresenter
import com.dycode.foodgasm.feature.data.DataManager
import com.dycode.foodgasm.rest.NetworkError
import rx.Subscription
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import javax.inject.Inject

class VerificationPhonePresenter @Inject constructor(val dataManager: DataManager) : BasePresenter<VerificationPhoneMvpView>() {

    var subVerify: Subscription? = null

    fun verifyPhone(code: String, tokenId: String) {
        if(code.isBlank()){
            mvpView?.showError("Please enter verification code.")
            return
        }
        mvpView?.showLoading()
        subVerify = dataManager.postVerifyPhone(code, tokenId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        {
                            apiResponse ->
                            mvpView?.hideLoading()
                            mvpView?.showSuccess()
                        },
                        {
                            error ->
                            mvpView?.showError(NetworkError(error).appErrorMessage)
                            mvpView?.hideLoading()
                        })
    }
}