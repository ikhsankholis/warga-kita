package com.dycode.foodgasm.feature.register.step2

import android.os.Bundle
import com.dycode.foodgasm.R
import com.dycode.foodgasm.feature.register.RegisterActivity
import com.dycode.foodgasm.fragment.BaseFragment
import kotlinx.android.synthetic.main.fragment_verification_phone.*
import javax.inject.Inject

/**
 * Created by USER on 5/31/2017.
 */
class VerificationPhoneFragment : BaseFragment(), VerificationPhoneMvpView {


    @Inject
    lateinit var presenter : VerificationPhonePresenter

    companion object {
        fun newInstance() : VerificationPhoneFragment {
            return VerificationPhoneFragment()
        }
    }


    override fun getLayout(): Int = R.layout.fragment_verification_phone

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        applicationComponent.inject(this)

        presenter.attachView(this)

        btnNext.setOnClickListener{
            val registerAct = activity as RegisterActivity
            registerAct.tokenId?.let {
                presenter.verifyPhone(inpVerifyCode.text.toString(), it)
            }
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView()
    }

    override fun showLoading() {
        showProgressDialog("Verifying..")
    }

    override fun showError(appErrorMessage: String) {
         showAlertDialog("Alert", appErrorMessage)
    }

    override fun showSuccess() {
        val registerAct = activity as RegisterActivity
        registerAct.changeCurrentPage(2)
    }

    override fun hideLoading() {
        progressDialog?.hide()
    }



}