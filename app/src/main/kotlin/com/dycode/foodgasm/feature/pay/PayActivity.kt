package com.dycode.foodgasm.feature.pay

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.MotionEvent
import android.view.View
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.webkit.WebViewClient
import com.afollestad.materialdialogs.MaterialDialog
import com.dycode.foodgasm.R
import com.dycode.foodgasm.activity.BaseActivity
import com.dycode.foodgasm.activity.BookOrderCompleteAct
import com.dycode.foodgasm.model.Order
import com.dycode.foodgasm.model.OrderDetail
import com.dycode.foodgasm.receiver.FoodgasmNotificationHandler
import com.dycode.foodgasm.utils.CCUtils
import com.dycode.foodgasm.utils.Constants
import com.dycode.foodgasm.utils.FoodgasmDateUtil
import com.dycode.foodgasm.utils.SPManager
import id.co.veritrans.android.api.VTUtil.VTConfig
import kotlinx.android.synthetic.main.activity_pay.*
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class PayActivity : BaseActivity(), PayMvpView {


    @Inject
    lateinit var presenter: PayPresenter

    var orderDetail: OrderDetail? = null

    companion object {
        val EXTRA_ORDER_ID = "PayActivity.EXTRA_ORDER_ID"
        val EXTRA_NOTIF_ID = "PayActivity.EXTRA_NOTIF_ID"

        fun start(context: Context, orderId: String, notifId: Int = 0) {
            val intent = Intent(context, PayActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.putExtra(EXTRA_ORDER_ID, orderId)
            intent.putExtra(EXTRA_NOTIF_ID, notifId)
            context.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pay)
        setToolbar(false)
        applicationComponent.inject(this)
        presenter.attachView(this)
        presenter.getDetailOrder(intent.extras.getString(EXTRA_ORDER_ID))
        val notifId: Int? = intent.extras.getInt(EXTRA_NOTIF_ID)
        if (notifId != null)
            clearNotif(notifId)


        linConfirm.setOnClickListener {
            if (orderDetail != null) {
                //Pay with CC need special treatment such as config VT and webviews
                if (orderDetail?.paymentWith == ("ecash")  ||  orderDetail?.paymentWith == "dokuwallet") {
                    presenter.confirmPay(orderDetail!!.id, orderDetail!!.paymentWith, orderDetail!!.maskedCard)
                } else {
                    paywithCc()
                }
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_cancel, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.action_cancel ->
                MaterialDialog.Builder(mActivity)
                        .content("Are you sure want to cancel this order?")
                        .positiveText("Yes")
                        .negativeText("No")
                        .onPositive(MaterialDialog.SingleButtonCallback { dialog, which ->
                            orderDetail?.let { presenter.cancel(it.id) }
                        }).onNegative(MaterialDialog.SingleButtonCallback { dialog, which -> dialog.dismiss() }).build().show()


        }
        return super.onOptionsItemSelected(item)
    }

    private fun paywithCc() {
        if (inpCvv.text.toString().isNotEmpty() && inpCvv.text.toString().length == 3) {
            VTConfig.VT_IsProduction = true
            VTConfig.CLIENT_KEY = Constants.VERITRANS_CLIENT_KEY
            val client_key = Constants.VERITRANS_CLIENT_KEY
            val total: Int = orderDetail?.price?.total!! + 2500
            orderDetail?.paymentCredential?.let {
                presenter.getVeritransToken(it, inpCvv.text.toString(), true, client_key, total.toString(), true, "credit_card")
            }
        } else {
            showAlertDialog("CVV is required", "Please input valid CVV to continue")
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView()
    }

    override fun getToolbarTitle(): Int {
        return R.string.payment_order
    }

    override fun showError(appErrorMessage: String?) {
        progressDialog?.dismiss()
        showAlertDialog("Alert", appErrorMessage)
    }

    override fun showDetail(data: OrderDetail?) {
        if (data != null) {
            orderDetail = data
            progressBar.visibility = View.GONE
            scrollView.visibility = View.VISIBLE

            val subTotalValue = String.format("%,d", data.price.subtotal).replace(',', ',')
            val convenienceChargeValue = String.format("%,d", data.price.convenience).replace(',', ',')
            val totalValue = String.format("%,d", data.price.total).replace(',', ',')


            tvSubtotalValue.text = subTotalValue
            tvConvenienceChargeValue.text = convenienceChargeValue
            tvTotalValue.text = totalValue
            if (data.paymentWith == "dokuwallet") {
                tvCardNumber.text = data.maskedCard
                imgCcDoku.setImageResource(R.mipmap.doku_wallet)
            } else if (data.paymentWith == "ecash") {
                tvCardNumber.text = data.maskedCard
                imgCcDoku.setImageResource(R.mipmap.image_logo_cc_ecash)
            } else {
                tvCardNumber.text = data.maskedCard.substring(data.maskedCard.length - 4, data.maskedCard.length)
                imgCcDoku.setImageResource(CCUtils.getCardLogo(data.maskedCard.replace("\\s".toRegex(), "")))
            }

        }
    }

    override fun showLoadingBalance() {
        linBalance.visibility = View.VISIBLE
        textBalance.text = "Getting your balance.."
    }

    override fun showBalance(lastBalance: String?) {
        textBalance.text = lastBalance
    }

    override fun showFailedLoadBalance() {
        textBalance.text = "Failed to load balance"
    }

    override fun showLoadingPay() {
        progressDialog = MaterialDialog.Builder(this)
                .content("Loading...")
                .cancelable(false)
                .progress(true, 0).build()
        progressDialog.show()
    }

    override fun showInputCvv() {
        linCvv.visibility = View.VISIBLE
    }

    override fun showSuccess() {
        progressDialog?.dismiss()
        SPManager.saveString(this, SPManager.ORDER_CREATED_TIME, null)
        SPManager.saveString(this, Order.ORDER_ID, null)
        SPManager.saveString(this, SPManager.REORDER, null)
        val calendar = Calendar.getInstance()
        val today = calendar.time
        val bookDay = FoodgasmDateUtil.getStringDate(orderDetail?.visitDate).toString()
        val dateFormat = SimpleDateFormat("MMM dd, yyy", Locale.getDefault())
        val todayAsString = dateFormat.format(today)
        var bookingDay = ""
        if (bookDay != todayAsString) {
            bookingDay = "tomorrow"
        } else {
            bookingDay = "today"
        }
        val bookingTime = FoodgasmDateUtil.getHourAndMinute(orderDetail?.visitDate).toString()
        SPManager.saveString(this, Order.ORDER_ID, null)
        val intent = Intent(this, BookOrderCompleteAct::class.java)
        intent.putExtra(BookOrderCompleteAct.FLAG, BookOrderCompleteAct.FLAG_ORDER_ACCEPTED)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        intent.putExtra(FoodgasmNotificationHandler.NOTIF_ORDER_ID, orderDetail?.id)
        intent.putExtra("bookingTime", bookingDay)
        intent.putExtra("bookingDay", bookingTime)
        intent.putExtra("bookingCode", orderDetail?.code)
        val isTakeAway = orderDetail?.preorder?.isOrder!! && !orderDetail?.booking?.isBooking!!
        intent.putExtra("isTakeAway", isTakeAway)
        intent.putExtra("restoName", orderDetail?.resto?.name)
        startActivity(intent)
    }


    override fun showWebview(redirect_url: String?, bank: String?, tokenId: String?) {
        progressDialog?.dismiss()
        wvPayment.visibility = View.VISIBLE

        wvPayment.settings.javaScriptEnabled = true
        wvPayment.setOnTouchListener { v, event ->
            when (event.action) {
                MotionEvent.ACTION_DOWN, MotionEvent.ACTION_UP -> if (!v.hasFocus()) {
                    v.requestFocus()
                }
            }
            false
        }
        wvPayment.setWebChromeClient(WebChromeClient())
        if (tokenId != null && bank != null)
            wvPayment.setWebViewClient(VtWebViewClient(tokenId, bank))

        wvPayment.loadUrl(redirect_url)
    }

    private inner class VtWebViewClient(internal var tokenId: String, internal var bank: String) : WebViewClient() {

        override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
            view.loadUrl(url)
            return true
        }

        override fun onPageFinished(view: WebView, url: String) {
            super.onPageFinished(view, url)

            Log.d("VtLog", url)

            if (url.startsWith(Constants.getPaymentApiUrl() + "/callback/")) {
                //send token to server
                orderDetail?.id?.let {
                    presenter.postVeritransPayment(it, tokenId, bank)
                }

            } else if (url.startsWith(Constants.getPaymentApiUrl() + "/redirect/") || url.contains("3dsecure")) {
                /* Do nothing */
            }
        }

    }

    override fun showDeclinedOrderDialog() {
        SPManager.saveString(this, SPManager.ORDER_CREATED_TIME, null)
        SPManager.saveString(this, Order.ORDER_ID, null)
        SPManager.saveString(this, SPManager.REORDER, null)
        val materialDialog = MaterialDialog.Builder(mActivity)
                .content("Transaction has been decline. Please contact your bank")
                .cancelable(false)
                .positiveText("OK")
                .onPositive { dialog, which ->
                    val intent = Intent(mActivity, BookOrderCompleteAct::class.java)
                    intent.putExtra(BookOrderCompleteAct.FLAG, BookOrderCompleteAct.FLAG_ORDER_PAYMENT_DECLINE)
                    intent.putExtra(FoodgasmNotificationHandler.NOTIF_ORDER_ID, orderDetail?.id)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    intent.putExtra("bookingTime", "bookingTime")
                    intent.putExtra("bookingDay", "bookingDay")
                    intent.putExtra("bookingCode", "bookingCode")
                    intent.putExtra("isTakeAway", "isTakeAway")
                    intent.putExtra("restoName", "restoName")
                    startActivity(intent)
                }
                .build()
        materialDialog.show()
    }

    override fun hideLoading() {
        progressDialog?.hide()
    }

    override fun navigateToCancelled() {
        SPManager.saveString(this, Order.ORDER_ID, null)
        SPManager.saveString(applicationContext, Constants.Inbox.STATUS_INBOX, null)
        val intent = Intent(mActivity, BookOrderCompleteAct::class.java)
        intent.putExtra(BookOrderCompleteAct.FLAG, BookOrderCompleteAct.FLAG_ORDER_CANCELLED)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        intent.putExtra(FoodgasmNotificationHandler.NOTIF_ORDER_ID, orderDetail?.id)
        intent.putExtra("bookingTime", "bookingTime")
        intent.putExtra("bookingDay", "bookingDay")
        intent.putExtra("bookingCode", "bookingCode")
        intent.putExtra("isTakeAway", "isTakeAway")
        intent.putExtra("restoName", "restoName")
        startActivity(intent)
    }


}