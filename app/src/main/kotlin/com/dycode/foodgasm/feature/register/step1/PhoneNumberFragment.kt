package com.dycode.foodgasm.feature.register.step1

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import com.dycode.foodgasm.R
import com.dycode.foodgasm.feature.register.RegisterActivity
import com.dycode.foodgasm.fragment.BaseFragment
import com.dycode.foodgasm.model.ReqPhoneVerify
import kotlinx.android.synthetic.main.fragment_phone.*
import javax.inject.Inject

/**
 * Created by USER on 5/31/2017.
 */
class PhoneNumberFragment : BaseFragment(), PhoneNumberMvpView {


    @Inject
    lateinit var presenter: PhoneNumberPresenter

    companion object {
        fun newInstance() : PhoneNumberFragment {
            return PhoneNumberFragment()
        }
    }

    override fun getLayout(): Int = R.layout.fragment_phone

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        applicationComponent.inject(this)
        presenter.attachView(this)

        inpPhoneNumber.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

            }

            override fun afterTextChanged(editable: Editable) {

            }
        })

        btnNext.setOnClickListener{
            view ->
            var phoneNumber = inpPhoneNumber.text.toString()
            if(phoneNumber.isBlank()){
                showAlertDialog(null, "Please enter your phone number.")
            }else{
                if (phoneNumber.startsWith("62")) {
                    phoneNumber = phoneNumber.replaceFirst("62", "")
                } else if (phoneNumber.startsWith("+62")) {
                    phoneNumber = phoneNumber.replaceFirst("+62", "")
                } else if (phoneNumber.startsWith("0")) {
                    phoneNumber = phoneNumber.replaceFirst("0", "")
                }
                presenter.verifyPhoneNumber(countryPicker.selectedCountryCode + phoneNumber)

            }

        }

    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView()
    }

    override fun showLoading() {
        showProgressDialog("Loading...")
    }

    override fun showError(appErrorMessage: String) {
        showAlertDialog("Alert", appErrorMessage)
    }

    override fun hideLoading() {
        progressDialog?.dismiss()
    }

    override fun showSuccess(data: ReqPhoneVerify) {
        val regActivity = activity as RegisterActivity
        regActivity.tokenId = data.token_id

        regActivity.phone = countryPicker.selectedCountryCode + inpPhoneNumber.text.toString()
        regActivity.changeCurrentPage(1)
    }


}