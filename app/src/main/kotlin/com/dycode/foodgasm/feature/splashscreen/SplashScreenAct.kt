package com.dycode.foodgasm.feature.splashscreen

import android.Manifest
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.util.Log
import android.widget.Toast
import butterknife.ButterKnife
import com.dycode.foodgasm.R
import com.dycode.foodgasm.activity.BaseActivity
import com.dycode.foodgasm.activity.HighLightAct
import com.dycode.foodgasm.activity.MainAct
import com.dycode.foodgasm.utils.SPManager
import com.facebook.appevents.AppEventsLogger
import kotlinx.android.synthetic.main.activity_splash_screen.*
import java.util.*

class SplashScreenAct : BaseActivity() {

    private var tt: TimerTask? = null
    private var timer: Timer? = null
    private var isCancelled = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)
        ButterKnife.bind(this)
        val isRegister = SPManager.getIsRegister(this)
        ivLogo.setOnClickListener {
            if (isRegister!!) {
                timer?.cancel()
                runApp()
            } else {
                timer?.cancel()
                runNotLoggedInApp()
            }
        }
    }

    override fun onStart() {
        //set Permission
        if (checkAndRequestPermissions()) {
            // carry on the normal flow, as the case of  permissions  granted.
            setupTimer()
        }
        super.onStart()
    }

    private fun setupTimer() {
        timer = Timer()
        tt = object : TimerTask() {
            override fun run() {
                runNotLoggedInApp()
            }
        }
        timer?.schedule(tt, SPLASH_TIME.toLong())
    }

    override fun onDestroy() {
        timer?.cancel()
        super.onDestroy()
    }

    private fun runNotLoggedInApp() {
        if (!isCancelled) {
            val i = Intent(this, HighLightAct::class.java)
            i.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT)
            startActivity(i)
            overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out)
            finish()
        }
    }

    private fun runApp() {
        if (!isCancelled) {
            val i = Intent(this, MainAct::class.java)
            i.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT)
            startActivity(i)
            overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out)
            finish()
        }
    }


    override fun onBackPressed() {
        isCancelled = true
        if (timer != null) {
            timer?.cancel()
        }
        finish()
    }

    override fun onResume() {
        super.onResume()

        // Logs 'install' and 'app activate' App Events.
        AppEventsLogger.activateApp(this)
    }


    //Checking Permission
    private fun checkAndRequestPermissions(): Boolean {
        val permissionExternalStorage = ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
        val locationPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
        val permissionPhone = ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
        val listPermissionsNeeded = ArrayList<String>()
        if (locationPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION)
        }
        if (permissionExternalStorage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        }
        if (permissionPhone != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_PHONE_STATE)
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toTypedArray(), REQUEST_ID_MULTIPLE_PERMISSIONS)
            return false
        }
        return true
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        Log.d(TAG, "Permission callback called-------")
        when (requestCode) {
            REQUEST_ID_MULTIPLE_PERMISSIONS -> {
                val perms = HashMap<String, Int>()
                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED)
                perms.put(Manifest.permission.ACCESS_FINE_LOCATION, PackageManager.PERMISSION_GRANTED)
                perms.put(Manifest.permission.READ_PHONE_STATE, PackageManager.PERMISSION_GRANTED)
                if (grantResults.size > 0) {
                    for (i in permissions.indices)
                        perms.put(permissions[i], grantResults[i])
                    if (perms[Manifest.permission.WRITE_EXTERNAL_STORAGE] == PackageManager.PERMISSION_GRANTED
                            && perms[Manifest.permission.ACCESS_FINE_LOCATION] == PackageManager.PERMISSION_GRANTED
                            && perms[Manifest.permission.READ_PHONE_STATE] == PackageManager.PERMISSION_GRANTED) {
                        Log.d(TAG, "sms & location services permission granted")
                        setupTimer()
                    } else {
                        Log.d(TAG, "Some permissions are not granted ask again ")
                        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION) || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_PHONE_STATE)) {
                            AlertDialog.Builder(this)
                                    .setMessage("SMS and Location Services Permission required for this app")
                                    .setPositiveButton("OK") { _, _ -> checkAndRequestPermissions() }
                                    .setNegativeButton("Cancel") { _, _ -> finish() }
                                    .setOnCancelListener { dialogInterface ->
                                        dialogInterface.dismiss()
                                        finish()
                                    }
                                    .create()
                                    .show()
                        } else {
                            Toast.makeText(this, "Go to settings and enable permissions", Toast.LENGTH_LONG)
                                    .show()
                        }
                    }
                }
            }
        }

    }

    private fun showDialogOK(message: String, okListener: DialogInterface.OnClickListener) {
        AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", okListener)
                .create()
                .show()
    }

    companion object {
        private val TAG = "SplashScreenAct"
        private val REQUEST_ID_MULTIPLE_PERMISSIONS = 1
        private val SPLASH_TIME = 850
    }
}
