package com.dycode.foodgasm.feature.base

/**
 * Created by USER on 12/8/2016.
 */

interface LeMvpView : MvpView {

    fun showLoading()

    fun showError(appErrorMessage: String)

    fun hideLoading()

}
