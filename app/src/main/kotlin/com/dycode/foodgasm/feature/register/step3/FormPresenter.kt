package com.dycode.foodgasm.feature.register.step3

import com.dycode.foodgasm.feature.base.BasePresenter
import com.dycode.foodgasm.feature.data.DataManager
import com.dycode.foodgasm.rest.NetworkError
import rx.Subscription
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import javax.inject.Inject

class FormPresenter @Inject constructor(val dataManager: DataManager) : BasePresenter<FormMvpView>() {

    var subRegister: Subscription? = null
    var subUpdateProfile: Subscription? = null

    fun register(fullname: String, username: String, email: String, phone: String, password: String, photo: String?, socialId: String?) {
        if (fullname.isBlank()) {
            mvpView?.showError("Please fill your fullname")
            return
        }

        if (socialId == null && username.isBlank()) {
            mvpView?.showError("Please fill your username")
            return
        }
        if (email.isBlank()) {
            mvpView?.showError("Please fill your email")
            return
        }

        if (socialId == null) {
            if (password.isBlank()) {
                mvpView?.showError("Please fill your password")
                return
            }
        }

        val provider = if (socialId == null) "" else "facebook"

        mvpView?.showLoading()
        subRegister = dataManager.postRegister(fullname, username, email, phone, password, password, photo, socialId, provider = provider, isPhoneVerified = true)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        {
                            apiResponse ->
                            mvpView?.hideLoading()
                            if (provider != "") {
                                mvpView?.saveToken(apiResponse)
                                updateProfileDataRegisterFromFb(email, fullname, phone)
                            } else {
                                mvpView?.showSuccess()
                            }
                        },
                        {
                            error ->
                            mvpView?.hideLoading()
                            mvpView?.showError(NetworkError(error).appErrorMessage)
                        })


    }

    fun updateProfileDataRegisterFromFb(email: String?, fullname: String?, phone: String?) {
        mvpView?.showLoading()
        subUpdateProfile = dataManager.putProfileData(email = email, fullname = fullname, phone = phone, isPhoneVerified = true)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        {
                            apiResponse ->
                            mvpView?.hideLoading()
                            mvpView?.navigateToMain()
                        },
                        {
                            error ->
                            mvpView?.hideLoading()
                            mvpView?.showError(NetworkError(error).appErrorMessage)
                        })
    }


    override fun detachView() {
        super.detachView()
        subRegister?.unsubscribe()
    }

}
