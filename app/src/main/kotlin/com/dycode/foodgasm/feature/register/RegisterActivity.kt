package com.dycode.foodgasm.feature.register

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v4.view.ViewPager
import android.widget.ImageView
import android.widget.LinearLayout
import com.dycode.foodgasm.R
import com.dycode.foodgasm.activity.BaseActivity
import com.dycode.foodgasm.data.model.LoginEvent
import com.dycode.foodgasm.utils.DensityPixelUtil
import kotlinx.android.synthetic.main.activity_register.*
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode


class RegisterActivity : BaseActivity(), ViewPager.OnPageChangeListener {


    //Pager
    val pagerAdapter : RegisterPagerAdapter by lazy {
        RegisterPagerAdapter(supportFragmentManager)
    }
    //Pager Indicator
    var dots = arrayOfNulls<ImageView>(0)
    //Position of active pager indicator
    var prevPosition = 0


    //userData
    var tokenId : String? = null
    var phone : String? = null
    var email : String? = null
    var fullname : String? = null
    var photo : String? = null
    var socialId : String? = null



    companion object{
        private val  EXTRA_USERNAME: String = "RegisterActivity.EXTRA_USERNAME"
        private val  EXTRA_FULLNAME: String = "RegisterActivity.EXTRA_FULLNAME"
        private val  EXTRA_EMAIL: String = "RegisterActivity.EXTRA_EMAIL"
        private val  EXTRA_PHOTO: String = "RegisterActivity.EXTRA_PHOTO"
        private val  EXTRA_SOCIAL_ID: String = "RegisterActivity.EXTRA_SOCIAL_ID"


        fun start(context: Context){
            context.startActivity(Intent(context, RegisterActivity::class.java))
        }

        fun start(context: Context,  fullName : String?, email : String?, photo : String?, socialId: String){
            val intent = Intent(context, RegisterActivity::class.java)
            intent.putExtra(EXTRA_FULLNAME, fullName)
            intent.putExtra(EXTRA_EMAIL, email)
            intent.putExtra(EXTRA_PHOTO, photo)
            intent.putExtra(EXTRA_SOCIAL_ID, socialId)
            context.startActivity(intent)
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onLoginEvent(event: LoginEvent){
        finish()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        setToolbar(true)

        if(intent.extras != null){
            email = intent.extras.getString(EXTRA_EMAIL)
            fullname = intent.extras.getString(EXTRA_FULLNAME)
            photo = intent.extras.getString(EXTRA_PHOTO)
            socialId = intent.extras.getString(EXTRA_SOCIAL_ID)
        }
        viewpager.adapter = pagerAdapter
        viewpager.addOnPageChangeListener(this)
        setUiPageViewController()

    }


    fun changeCurrentPage(position: Int){
        if(position in 0..2)
            viewpager.currentItem = position
    }


    override fun onPageScrollStateChanged(state: Int) {
    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
    }

    override fun onPageSelected(position: Int) {
        changePageViewIndicator(prevPosition , position)
        prevPosition = position

    }

    private fun setUiPageViewController() {
        viewPagerCountDots.removeAllViews()
        val dotsCount = pagerAdapter.count
        dots = arrayOfNulls<ImageView>(dotsCount)

        for (i in 0..dotsCount - 1) {
            dots[i] = ImageView(this)
            dots[i]?.setImageDrawable(resources.getDrawable(R.drawable.nonselecteditem_dot))

            val params = LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            )

            val margin4 = Math.round(DensityPixelUtil.convertDpToPixel(4f, this))
            params.setMargins(margin4, 0, margin4, 0)

            viewPagerCountDots.addView(dots[i], params)
        }

        dots[0]?.setImageDrawable(resources.getDrawable(R.drawable.selecteditem_dot))

    }

    private fun changePageViewIndicator(prevPos: Int, pos: Int) {
        if (prevPos != pos)
            dots[prevPos]?.setImageDrawable(resources.getDrawable(R.drawable.nonselecteditem_dot))
        dots[pos]?.setImageDrawable(resources.getDrawable(R.drawable.selecteditem_dot))
    }

    override fun getToolbarTitle(): Int {
        return R.string.registration
    }

    override fun onBackPressed() {
        if(viewpager.currentItem != 0){
            changeCurrentPage(viewpager.currentItem - 1)
        }else{
            super.onBackPressed()
        }
    }

}