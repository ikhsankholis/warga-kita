package com.dycode.foodgasm.feature.pay

import android.content.Intent
import android.os.Handler
import android.util.Log
import com.afollestad.materialdialogs.DialogAction
import com.afollestad.materialdialogs.MaterialDialog
import com.dycode.foodgasm.R
import com.dycode.foodgasm.activity.BookOrderCompleteAct
import com.dycode.foodgasm.feature.base.BasePresenter
import com.dycode.foodgasm.feature.data.DataManager
import com.dycode.foodgasm.model.CreditCard
import com.dycode.foodgasm.rest.NetworkError
import com.dycode.foodgasm.utils.CTools
import org.json.JSONException
import org.json.JSONObject
import rx.Subscription
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.util.concurrent.TimeUnit
import javax.inject.Inject


class PayPresenter @Inject constructor(val dataManager: DataManager) : BasePresenter<PayMvpView>() {

    var subDetailOrder: Subscription? = null
    var subDokuBalance: Subscription? = null
    var subPay: Subscription? = null
    var subEcashBalance: Subscription? = null
    var subVeritransToken: Subscription? = null
    var subVeritransPayment: Subscription? = null
    var subCancelOrder: Subscription? = null

    fun getDetailOrder(id: String) {
        subDetailOrder = dataManager.getOrderDetail(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        {
                            apiResponse ->
                            mvpView?.showDetail(apiResponse.data)
                            if (apiResponse.data.paymentWith == "dokuwallet") {
                                getDokuWalletBalance(apiResponse.data.maskedCard)
                            } else if (apiResponse.data.paymentWith == "ecash") {
                                getEcashBalance(apiResponse.data.id, apiResponse.data.maskedCard)
                            } else {
                                mvpView?.showInputCvv()
                            }
                        },
                        {
                            error ->
                            mvpView?.showError(NetworkError(error).appErrorMessage)
                        }
                )

    }

    private fun getEcashBalance(orderId: String, accountId: String) {
        mvpView?.showLoadingBalance()
        subEcashBalance = dataManager.postEcashBalance(orderId, accountId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    apiResponse ->
                    try {
                        val data = JSONObject(apiResponse.data.toString())
                        val status = data.getString("status")
                        if (status == "PROCESSED") {
                            val accountBalance = data.getInt("accountBalance")
                            mvpView?.showBalance(CTools.getRP(accountBalance.toString(), false))
                        }

                    } catch (e: JSONException) {
                        mvpView?.showFailedLoadBalance()
                        e.printStackTrace()
                    }

                }, {
                    error ->
                    mvpView?.showFailedLoadBalance()
                })
    }

    private fun getDokuWalletBalance(accountId: String) {
        mvpView?.showLoadingBalance()
        subDokuBalance = dataManager.postRequestBalanceDoku(accountId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        {
                            apiResponse ->
                            mvpView?.showBalance(CTools.getRP(apiResponse.data.lastBalance, false))
                        },
                        {
                            error ->
                            mvpView?.showFailedLoadBalance()
                        }
                )

    }

    override fun detachView() {
        super.detachView()
        subDetailOrder?.unsubscribe()
        subDokuBalance?.unsubscribe()
        subPay?.unsubscribe()
        subEcashBalance?.unsubscribe()
        subVeritransToken?.unsubscribe()
        subVeritransPayment?.unsubscribe()
    }

    fun confirmPay(orderId: String, payWith: String, accountId: String) {
        mvpView?.showLoadingPay()

        if (payWith == "dokuwallet") {
            subPay = dataManager.postPayDokuWalet(orderId, accountId)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            {
                                apiResponse ->
                                if (apiResponse.data.responseCode != "0000") {
                                    mvpView?.showError("Your Balance is not Enough")
                                } else {
                                    mvpView?.showSuccess()
                                }

                            }
                            ,
                            {
                                error ->
                                mvpView?.showError(NetworkError(error).appErrorMessage)
                            }
                    )
        } else if (payWith == "ecash") {
            subPay = dataManager.postPayEcash(orderId, accountId)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            {
                                apiResponse ->
                                if (apiResponse.data.status == "PROCESSED") {
                                    mvpView?.showSuccess()
                                } else {
                                    mvpView?.showError(apiResponse.data.description)
                                }

                            }
                            ,
                            {
                                error ->
                                mvpView?.showError(NetworkError(error).appErrorMessage)
                            }
                    )
        }


    }

    fun getVeritransToken(paymentCredential: String, cardCvv: String, twoClick: Boolean, clientKey: String, grossAmount: String, secure: Boolean, paymentType: String) {
        mvpView?.showLoadingPay()
        subVeritransToken = dataManager.getCreditCardToken(paymentCredential)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .flatMap { creditCard ->
                    dataManager.getVeritransToken(creditCard.data.savedTokenId, cardCvv, twoClick, clientKey, grossAmount, secure, paymentType)
                }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        {
                            apiResponse ->
                            mvpView?.hideLoading()
                            mvpView?.showWebview(apiResponse.redirect_url, apiResponse.bank, apiResponse.tokenId)
                        },
                        {
                            error ->
                            mvpView?.showError(NetworkError(error).appErrorMessage)
                        }
                )
    }

    fun postVeritransPayment(orderId: String, tokenId: String, bank: String) {
        checkViewAttached()
            subVeritransPayment = dataManager.postVeritransPayment(orderId, tokenId, bank)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        {
                            apiResponse ->
                            val handler = Handler()
                            handler.postDelayed({
                                checkStatus(orderId)
                            }, 5000)
                        }, {
                    error ->
                    postVeritransPayment(orderId, tokenId, bank)
                }
                )

    }

    private fun checkStatus(orderId: String) {
        dataManager.getVeritransStatus(orderId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        {
                            apiResponse ->
                            try {
                                val data = JSONObject(apiResponse.data.toString())
                                val _id = data.getString("_id")
                                val state = data.getInt("state")
                                val stateMessage = data.getString("stateMessage")
                                val vtstate = data.getString("vtstate")
                                Log.d("state > ", state.toString())

                                if (vtstate == "DONE" && state == 4) {
                                    mvpView?.showDeclinedOrderDialog()
                                } else if (state == 1) {
                                    mvpView?.showSuccess()
                                } else if (state == 2) {
                                    mvpView?.showDeclinedOrderDialog()
                                } else {
                                    val handler = Handler()
                                    handler.postDelayed({
                                        checkStatus(orderId)
                                    }, 5000)
                                }
                            } catch (e: JSONException) {
                                mvpView?.showError("Internal error occurred")
                            }

                        },
                        {
                            error ->
                            mvpView?.showError(NetworkError(error).appErrorMessage)
                        }
                )
    }

    fun cancel(id: String?) {
        if (id != null) {
            mvpView?.showLoadingPay()
            subCancelOrder = dataManager.patchOrder(id, "cancel", "User cancelled the order")
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            {
                                apiResponse ->
                                mvpView?.hideLoading()
                                mvpView?.navigateToCancelled()
                            },
                            {
                                error ->
                                mvpView?.hideLoading()
                                mvpView?.showError(NetworkError(error).appErrorMessage)
                            }
                    )
        }
    }

}