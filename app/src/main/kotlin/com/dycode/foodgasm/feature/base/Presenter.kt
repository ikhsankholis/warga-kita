package com.dycode.foodgasm.feature.base

/**
 * Created by fahmi on 11/01/2016.
 * Every presenter in the app must either implement this interface or extend BasePresenter
 * indicating the MvpView type that wants to be attached with.
 */
interface Presenter<V : MvpView> {

    fun attachView(mvpView: V)

    fun detachView()

}
