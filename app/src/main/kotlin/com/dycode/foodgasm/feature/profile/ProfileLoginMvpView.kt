package com.dycode.foodgasm.feature.profile

import com.dycode.foodgasm.feature.base.LeMvpView
import com.dycode.foodgasm.model.Register

interface ProfileLoginMvpView : LeMvpView{
    fun moveToRegister(register: Register?)
    fun moveToMain(register: Register?)

}