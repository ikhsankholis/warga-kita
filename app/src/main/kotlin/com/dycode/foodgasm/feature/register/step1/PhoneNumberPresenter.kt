package com.dycode.foodgasm.feature.register.step1

import com.dycode.foodgasm.feature.base.BasePresenter
import com.dycode.foodgasm.feature.data.DataManager
import com.dycode.foodgasm.rest.NetworkError
import rx.Subscription
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import javax.inject.Inject

/**
 * Created by USER on 5/31/2017.
 */
class PhoneNumberPresenter @Inject constructor(val dataManager: DataManager) : BasePresenter<PhoneNumberMvpView>() {

    var subPhone: Subscription? = null

    fun verifyPhoneNumber(phoneNumber: String) {
        mvpView?.showLoading()
        subPhone = dataManager.getPhoneVerification(phoneNumber)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        {
                            apiResponse ->
                            mvpView?.hideLoading()
                            mvpView?.showSuccess(apiResponse.data)
                        },
                        {
                            error ->
                            mvpView?.hideLoading()
                            mvpView?.showError(NetworkError(error).appErrorMessage)
                        })


    }

    override fun detachView() {
        super.detachView()
        subPhone?.unsubscribe()
    }
}