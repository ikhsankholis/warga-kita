package com.dycode.foodgasm.receiver;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.dycode.foodgasm.R;
import com.dycode.foodgasm.activity.BaseActivity;
import com.dycode.foodgasm.activity.BookOrderCompleteAct;
import com.dycode.foodgasm.activity.MainAct;
import com.dycode.foodgasm.activity.ReorderAct;
import com.dycode.foodgasm.feature.pay.PayActivity;
import com.dycode.foodgasm.model.Order;
import com.dycode.foodgasm.service.GCMService;
import com.dycode.foodgasm.utils.Constants;
import com.dycode.foodgasm.utils.SPManager;
import com.microsoft.windowsazure.notifications.NotificationsHandler;

//import com.dycode.foodgasm.activity.PaymentAct;

/**
 * Created by fahmi on 22/12/2015.
 */
public class FoodgasmNotificationHandler extends NotificationsHandler {

    Context mContext;


    public static String NOTIF_TYPE = "type";
    public static String NOTIF_ORDER_ID = "orderId";
    public static String NOTIF_TITLE = "title";
    public static String NOTIF_TEXT = "text";


    public static String NOTIF_ID = "id_notifications";

    public static String ORDER_TYPE_ACCEPTED   = "order_accepted";
    public static String ORDER_TYPE_DECLINED   = "order_declined";
    public static String ORDER_TYPE_CANCELLED   = "order_canceled";
    public static String ORDER_TYPE_WAITING    = "order_waiting";
    public static String ORDER_TYPE_REORDER    = "order_reorder";
    public static String ORDER_TYPE_BOOKING    = "booking_accepted";
    public static String ORDER_TYPE_EXPIRED    = "order_expired";

    @Override
    public void onReceive(Context context, Bundle bundle) {
        mContext = context;

        boolean sendNotif = true;

        //prevent push notif triggered after install
        String from = bundle.getString("from");
        if ("google.com/iid".equals(from))
            return;


//        int notifId = (int) System.currentTimeMillis();
        int notifId = 0;
        String orderId = bundle.getString(NOTIF_ORDER_ID);
        String title = bundle.getString(NOTIF_TITLE);
        String caption = bundle.getString(NOTIF_TEXT);
        String type = bundle.getString(NOTIF_TYPE);
        Log.d("Notification", "Push received: " + orderId + ", " + title + ", " + caption + ", " + type);

        if(type == null)
            return;

        Boolean isActivityVisible = BaseActivity.isViewVisible();
        Boolean isActivityInForeground = BaseActivity.isInForeground();

        //When app is active, open the next page spontaneously according to notif type
        if (isActivityVisible || isActivityInForeground) {
            if (type.equals(ORDER_TYPE_DECLINED)) {
                navigateToDeclined(notifId, orderId, type);
            } else if (type.equals(ORDER_TYPE_CANCELLED)) {
                navigateToCancelled(notifId, orderId, type);
            } else if (type.equals(ORDER_TYPE_WAITING)) {
                navigateToPayment(orderId, notifId);
            } else if (type.equals(ORDER_TYPE_REORDER)) {
                navigateToReorder(notifId, orderId);
            } else if (type.equals(ORDER_TYPE_BOOKING)) {
                updateMyActivity(mContext, "0", type);
                GCMService.cancelAll(mContext);
                SPManager.saveString(mContext, SPManager.ORDER_CREATED_TIME, null);
                SPManager.saveString(mContext, Order.ORDER_ID, null);
                SPManager.saveString(mContext, SPManager.REORDER, null);
                Intent i = new Intent(mContext, MainAct.class);
                i.putExtra(BookOrderCompleteAct.FLAG, BookOrderCompleteAct.FLAG_BOOKING_ACCEPTED);
                i.putExtra(NOTIF_ID, notifId);
                i.putExtra(NOTIF_ORDER_ID, orderId);
                i.putExtra(NOTIF_TITLE, title);
                i.putExtra(NOTIF_TYPE, type);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                mContext.startActivity(i);
            } else if (type.equals(ORDER_TYPE_EXPIRED)) {
                navigateToExpired(notifId, orderId, type);
            } else if (type.equals(ORDER_TYPE_ACCEPTED)) {
                navigateToAccepted(notifId, orderId, type);
            } else {
                sendNotif = false;
            }
        }

        //if orderType is verified, send push notif with selected pending intent
        if (sendNotif) {
            sendNotification(notifId, orderId, title, caption, type);
        }


    }


    private void sendNotification(int notifId, String id, String title, String text, String type) {
        NotificationManager mNotificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);

        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Intent i = new Intent();
        boolean sendNotif = true;

        if(type == null)
            return;

        if (type.equals(ORDER_TYPE_DECLINED)) {
            //Open BookOrderComplete with declined order message
            GCMService.cancelAll(mContext);
            SPManager.saveString(mContext, SPManager.ORDER_CREATED_TIME, null);
            SPManager.saveString(mContext, Order.ORDER_ID, null);
            SPManager.saveString(mContext, SPManager.REORDER, null);
            i = new Intent(mContext, BookOrderCompleteAct.class);
            i.putExtra(BookOrderCompleteAct.FLAG, BookOrderCompleteAct.FLAG_ORDER_DECLINED);
        } else if (type.equals("order_canceled")) {
            GCMService.cancelAll(mContext);
            SPManager.saveString(mContext, SPManager.ORDER_CREATED_TIME, null);
            SPManager.saveString(mContext, Order.ORDER_ID, null);
            SPManager.saveString(mContext, SPManager.REORDER, null);
            i = new Intent(mContext, BookOrderCompleteAct.class);
            i.putExtra(BookOrderCompleteAct.FLAG, BookOrderCompleteAct.FLAG_ORDER_CANCELLED);
        } else if (type.equals("order_waiting")) {
            SPManager.saveString(mContext, SPManager.ORDER_CREATED_TIME, null);
            i = new Intent(mContext, PayActivity.class);
            i.putExtra(PayActivity.Companion.getEXTRA_ORDER_ID(), id);
        } else if (type.equals("order_reorder")) {
            SPManager.saveString(mContext, SPManager.ORDER_CREATED_TIME, null);
            SPManager.saveString(mContext, Order.ORDER_ID, id);
            SPManager.saveString(mContext, SPManager.REORDER, id);
            i = new Intent(mContext, ReorderAct.class);
        } else if (type.equals("booking_accepted")) {
            SPManager.saveString(mContext, Constants.Inbox.STATUS_INBOX, type);
            //Open BookOrderComplete with accepted booking message
            GCMService.cancelAll(mContext);
            i = new Intent(mContext, BookOrderCompleteAct.class);
            i.putExtra(BookOrderCompleteAct.FLAG, BookOrderCompleteAct.FLAG_BOOKING_ACCEPTED);
        } else if (type.equals("order_expired")) {
            //Open BookOrderComplete with expired booking message
            SPManager.saveString(mContext, "stack", "0");
            GCMService.cancelAll(mContext);
            SPManager.saveString(mContext, SPManager.REORDER, null);
            SPManager.saveString(mContext, SPManager.ORDER_CREATED_TIME, null);
            SPManager.saveString(mContext, Order.ORDER_ID, null);
//            i = new Intent(mContext, MainAct.class);
            i = new Intent(mContext, BookOrderCompleteAct.class);
            i.putExtra(BookOrderCompleteAct.FLAG, BookOrderCompleteAct.FLAG_ORDER_EXPIRED);
        } else if (type.equals("order_accepted")) {
            SPManager.saveString(mContext, "stack", "0");
            GCMService.cancelAll(mContext);
            SPManager.saveString(mContext, SPManager.ORDER_CREATED_TIME, null);
            SPManager.saveString(mContext, SPManager.REORDER, null);
            SPManager.saveString(mContext, Order.ORDER_ID, null);
            i = new Intent(mContext, BookOrderCompleteAct.class);
            i.putExtra(BookOrderCompleteAct.FLAG, BookOrderCompleteAct.FLAG_ORDER_ACCEPTED);
        } else {
            sendNotif = false;
//            throw new IllegalArgumentException("Notification Type not supported");
        }

        if (sendNotif) {
            i.putExtra("id_notifications", notifId);
            i.putExtra(NOTIF_ORDER_ID, id);
            i.putExtra(NOTIF_TITLE, title);
            i.putExtra(NOTIF_TYPE, type);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

            //EventBus.getDefault().post(new EventBusMessage("Hello everyone!" ));


            PendingIntent contentIntent = PendingIntent.getActivity(mContext, 0, i, PendingIntent.FLAG_UPDATE_CURRENT);
            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(mContext)
                    .setContentTitle(title)
                    .setAutoCancel(true)
                    .setSmallIcon(getNotificationIcon())
                    .setLargeIcon(BitmapFactory.decodeResource(mContext.getResources(),
                            R.mipmap.ic_launcher))
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(text))
                    .setContentText(text)
                    .setContentIntent(contentIntent)
                    .setVibrate(new long[]{1000})
                    .setLights(Color.CYAN, 3000, 3000)
                    .setSound(uri);

            mNotificationManager.notify(notifId, mBuilder.build());
        }

    }

    private void navigateToDeclined(int notifId, String orderId, String type){
        SPManager.saveString(mContext, SPManager.ORDER_CREATED_TIME, null);
        SPManager.saveString(mContext, Order.ORDER_ID, null);
        SPManager.saveString(mContext, SPManager.REORDER, null);
        GCMService.cancelAll(mContext);
        Intent i = new Intent(mContext, BookOrderCompleteAct.class);
        i.putExtra(BookOrderCompleteAct.FLAG, BookOrderCompleteAct.FLAG_ORDER_DECLINED);
        i.putExtra(NOTIF_ID, notifId);
        i.putExtra(NOTIF_ORDER_ID, orderId);
        i.putExtra(NOTIF_TYPE, type);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        mContext.startActivity(i);
    }

    private void navigateToCancelled(int notifId, String orderId, String type) {
        GCMService.cancelAll(mContext);
        SPManager.saveString(mContext, SPManager.ORDER_CREATED_TIME, null);
        SPManager.saveString(mContext, Order.ORDER_ID, null);
        SPManager.saveString(mContext, SPManager.REORDER, null);
        Intent i = new Intent(mContext, BookOrderCompleteAct.class);
        i.putExtra(BookOrderCompleteAct.FLAG, BookOrderCompleteAct.FLAG_ORDER_CANCELLED);
        i.putExtra(NOTIF_ID, notifId);
        i.putExtra(NOTIF_ORDER_ID, orderId);
        i.putExtra(NOTIF_TYPE, type);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        mContext.startActivity(i);
    }

    private void navigateToPayment(String orderId, int notifId) {
        SPManager.saveString(mContext, SPManager.ORDER_CREATED_TIME, null);
        if (orderId != null)
            PayActivity.Companion.start(mContext, orderId, notifId);
    }

    private void navigateToReorder(int notifId, String orderId) {
        SPManager.saveString(mContext, SPManager.ORDER_CREATED_TIME, null);
        SPManager.saveString(mContext, Order.ORDER_ID, orderId);
        SPManager.saveString(mContext, SPManager.REORDER, orderId);
        Intent i = new Intent(mContext, ReorderAct.class);
        i.putExtra("id_notifications", notifId);
        i.putExtra(NOTIF_ORDER_ID, orderId);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mContext.startActivity(i);
    }

    private void navigateToExpired(int notifId, String orderId, String type) {
        GCMService.cancelAll(mContext);
        SPManager.saveString(mContext, SPManager.ORDER_CREATED_TIME, null);
        SPManager.saveString(mContext, Order.ORDER_ID, null);
        Intent i = new Intent(mContext, BookOrderCompleteAct.class);
        i.putExtra(BookOrderCompleteAct.FLAG, BookOrderCompleteAct.FLAG_ORDER_EXPIRED);
        i.putExtra(NOTIF_ID, notifId);
        i.putExtra(NOTIF_ORDER_ID, orderId);
        i.putExtra(NOTIF_TYPE, type);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        mContext.startActivity(i);
    }

    private void navigateToAccepted(int notifId, String orderId, String type) {
        SPManager.saveString(mContext, SPManager.ORDER_CREATED_TIME, null);
        SPManager.saveString(mContext, Order.ORDER_ID, null);
        GCMService.cancelAll(mContext);
        Intent i = new Intent(mContext, BookOrderCompleteAct.class);
        i.putExtra(BookOrderCompleteAct.FLAG, BookOrderCompleteAct.FLAG_ORDER_ACCEPTED);
        i.putExtra(NOTIF_ID, notifId);
        i.putExtra(NOTIF_ORDER_ID, orderId);
        i.putExtra(NOTIF_TYPE, type);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        mContext.startActivity(i);
    }


    private int getNotificationIcon() {
        boolean useWhiteIcon = (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP);
        return useWhiteIcon ? R.mipmap.ic_notification_user : R.mipmap.ic_launcher;
    }


    static void updateMyActivity(Context context, String message, String type) {
        Intent intent = new Intent("unique_name");
        //put whatever data you want to send, if any
        intent.putExtra("message", message);
        intent.putExtra("type", type);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        context.sendBroadcast(intent);
    }
}
