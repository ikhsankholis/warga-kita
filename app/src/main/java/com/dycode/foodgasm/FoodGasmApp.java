package com.dycode.foodgasm;

import android.app.Application;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.support.multidex.MultiDex;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.dycode.foodgasm.dao.DaoMaster;
import com.dycode.foodgasm.dao.DaoSession;
import com.dycode.foodgasm.inject.ApplicationComponent;
import com.dycode.foodgasm.inject.ApplicationModule;
import com.dycode.foodgasm.inject.DaggerApplicationComponent;
import com.dycode.foodgasm.listener.UnregisterNotificationListener;
import com.dycode.foodgasm.receiver.FoodgasmNotificationHandler;
import com.dycode.foodgasm.utils.Constants;
import com.dycode.foodgasm.utils.SPManager;
import com.dycode.foodgasm.utils.Utils;
import com.facebook.applinks.AppLinkData;
import com.facebook.login.LoginBehavior;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.microsoft.windowsazure.messaging.NotificationHub;
import com.microsoft.windowsazure.notifications.NotificationsManager;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;
import com.sromku.simple.fb.Permission;
import com.sromku.simple.fb.SimpleFacebook;
import com.sromku.simple.fb.SimpleFacebookConfiguration;

import java.io.File;

import io.fabric.sdk.android.Fabric;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by asus pc on 03/12/2015.
 */
public class FoodGasmApp extends Application {

    private static final String APP_ID = "1027406327309606";
    private static final String APP_NAMESPACE = "foodgasmuserapp";
    private static final String TAG = FoodGasmApp.class.getSimpleName();
    private String SENDER_ID = "831869922975";
    private GoogleCloudMessaging gcm;
    private NotificationHub hub;
    private String HubName = "foodgasm";
//    private String HubName = "foodgasm-ditto";
    private String HubListenConnectionString = "Endpoint=sb://foodgasm-ns.servicebus.windows.net/;SharedAccessKeyName=DefaultListenSharedAccessSignature;SharedAccessKey=6KRZL4nVlBxWCgXiQnjcZCLYQV0LnU1n91u1hbyypK0=";
//    private String HubListenConnectionString = "Endpoint=sb://foodgasm-ditto.servicebus.windows.net/;SharedAccessKeyName=DefaultListenSharedAccessSignature;SharedAccessKey=7hS9B1B2pGJrz+k16s2/Ngt8ebBZH0ZSC4YfIrFUvIU=";

    private DaoSession mDaoSession;

    Permission[] permissions = new Permission[]{
            Permission.USER_PHOTOS,
            Permission.EMAIL,
            Permission.PUBLISH_ACTION,
            Permission.USER_STATUS
    };


    ApplicationComponent mApplicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        Fabric.with(this, new Crashlytics());
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/S_Core_CoreSansR35Regular.otf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
//        Dexter.initialize(this);



        Picasso.Builder builder = new Picasso.Builder(this);
        builder.downloader(new OkHttpDownloader(this, Integer.MAX_VALUE));
        Picasso built = builder.build();
//        built.setIndicatorsEnabled(true);
        built.setLoggingEnabled(true);
        Picasso.setSingletonInstance(built);

        //Simple Facebook
        SimpleFacebookConfiguration configuration = new SimpleFacebookConfiguration.Builder()
                .setAppId(APP_ID)
                .setNamespace(APP_NAMESPACE)
                .setPermissions(permissions)
                .setLoginBehavior(LoginBehavior.WEB_ONLY)
                .build();
        SimpleFacebook.setConfiguration(configuration);

        // Facebook ADS
        AppLinkData.fetchDeferredAppLinkData(this,
                new AppLinkData.CompletionHandler() {
                    @Override
                    public void onDeferredAppLinkDataFetched(AppLinkData appLinkData) {
                    }
                }
        );

        // Generate Facebook SHA-1
//        try {
//            PackageInfo info = getPackageManager().getPackageInfo("com.dycode.foodgasm", PackageManager.GET_SIGNATURES);
//            for (Signature signature : info.signatures) {
//                MessageDigest md;
//                md = MessageDigest.getInstance("SHA");
//                md.update(signature.toByteArray());
//                String something = new String(Base64.encode(md.digest(), 0));
//                //String something = new String(Base64.encodeBytes(md.digest()));
//                Log.e("hash key", something);
//            }
//        } catch (PackageManager.NameNotFoundException e1) {
//            Log.e("name not found", e1.toString());
//        } catch (NoSuchAlgorithmException e) {
//            Log.e("no such an algorithm", e.toString());
//        } catch (Exception e) {
//            Log.e("exception", e.toString());
//        }

        //Azure notification
        NotificationsManager.handleNotifications(this, SENDER_ID, FoodgasmNotificationHandler.class);
        gcm = GoogleCloudMessaging.getInstance(this);
        hub = new NotificationHub(HubName, HubListenConnectionString, this);

        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, "foodgasm-db", null);
        SQLiteDatabase db = helper.getWritableDatabase();
        DaoMaster daoMaster = new DaoMaster(db);
        mDaoSession = daoMaster.newSession();

    }

    @SuppressWarnings("unchecked")
    public void registerWithNotificationHubs() {
        new AsyncTask() {
            @Override
            protected Object doInBackground(Object... params) {
                try {
                    String regid = gcm.register(SENDER_ID);
                    String username = Utils.getToken(getApplicationContext()).getBody().getUsername();
                    hub.register(regid, username);
                    SPManager.saveString(getApplicationContext(), Constants.SP.GCM_REGID, regid);
                    Log.i(TAG, "Register with TAG " + username);

                } catch (Exception e) {

                    Log.e(TAG, "GCM Ex: " + e.getMessage());
                    Log.i(TAG, "GCM retry register");
                    registerWithNotificationHubs();
                    return e;
                }
                return null;
            }
        }.execute(null, null, null);
    }

    @SuppressWarnings("unchecked")
    public void unregisterNotificationHub(final UnregisterNotificationListener listener) {
        new AsyncTask() {
            @Override
            protected Object doInBackground(Object... params) {
                try {
                    Log.d(TAG, "Unregistering");
                    hub.unregisterAll(SPManager.getString(getApplicationContext(), Constants.SP.GCM_REGID));
                    SPManager.saveString(getApplicationContext(), Constants.SP.GCM_REGID, null);
                    Log.d(TAG, "Unregister success");
                    listener.onSuccess();
                } catch (Exception e) {
                    Log.d(TAG, "Unregister failed");
                    Log.i(TAG, "GCM retry unregister");
                    if (e instanceof IllegalArgumentException) {
                        Log.d(TAG, "No need to unregister");
                        listener.onSuccess();
                    } else if (Utils.isOnline(getApplicationContext())) {
                        // connected to the internet
                        unregisterNotificationHub(listener);
                    } else {
                        listener.onFailed();
                        Log.d(TAG, "No internet connection, stop unregister");

                    }
                    e.printStackTrace();

                }
                return null;
            }
        }.execute(null, null, null);
    }

    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public DaoSession getDaoSession() {
        return mDaoSession;
    }

    Tracker mTracker = null;

    public synchronized Tracker getTracker() {
        if (mTracker == null) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
//             To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
            Tracker t = analytics.newTracker(R.xml.global_tracker);

            mTracker = t;
        }
        return mTracker;
    }

    public ApplicationComponent getComponent() {
        if (mApplicationComponent == null) {
            mApplicationComponent = DaggerApplicationComponent.builder()
                    .applicationModule(new ApplicationModule(this))
                    .build();
        }
        return mApplicationComponent;
    }

}
