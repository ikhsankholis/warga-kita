package com.dycode.foodgasm.rest;

import android.util.Base64;

import com.dycode.foodgasm.utils.Constants;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.OkHttpClient;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;

/**
 * Created by asus pc on 21/01/2016.
 */
public class RestClientForUploadFile {
    private ApiService apiService;
    private static RestClientForUploadFile instance = null;

    public static RestClientForUploadFile getInstance() {
        if (instance == null)
            instance = new RestClientForUploadFile();

        return instance;
    }

    private RestClientForUploadFile() {
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'SSS'Z'")
                .create();

        String username = Constants.BASIC_USERNAME;
        String password = Constants.BASIC_PASSWORD;

        String credentials = username + ":" + password;
        final String basic = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);


        RestAdapter restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(Constants.Api.BASE_URL)
                .setClient(new OkClient(new OkHttpClient()))
                .setConverter(new GsonConverter(gson))
                .setRequestInterceptor(new RequestInterceptor() {
                    @Override
                    public void intercept(RequestFacade request) {
                        request.addHeader("Authorization", basic);
                        request.addHeader("Accept", "application/json");
                    }
                })
                .build();


        apiService = restAdapter.create(ApiService.class);
    }

    public ApiService getApiService() {
        return apiService;
    }
}
