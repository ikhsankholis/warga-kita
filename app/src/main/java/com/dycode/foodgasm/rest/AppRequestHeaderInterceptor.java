package com.dycode.foodgasm.rest;

import android.content.Context;

import com.dycode.foodgasm.utils.Constants;
import com.dycode.foodgasm.utils.Utils;

import java.io.IOException;

import javax.inject.Inject;

import okhttp3.Credentials;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by fahmi on 30/06/2016.
 */
public class AppRequestHeaderInterceptor implements Interceptor {

    private final Context context;

    @Inject
    public AppRequestHeaderInterceptor(Context context) {
        this.context = context;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {

        String basicAuth = Credentials.basic(Constants.BASIC_USERNAME, Constants.BASIC_PASSWORD);


        Request request = chain.request().newBuilder()
                .removeHeader("Content-Type")
                .addHeader("Accept", "application/json")
                .addHeader("Content-Type", "application/json")
                .addHeader("Authorization", basicAuth)
                .build();

        if (Utils.getToken(context) != null) {
            HttpUrl url = request.url().newBuilder()
                    .addQueryParameter("access_token", Utils.getToken(context).getAccessToken())
                    .build();
            request = request.newBuilder()
                    .url(url)
                    .build();
        }

        return chain.proceed(request);
    }
}
