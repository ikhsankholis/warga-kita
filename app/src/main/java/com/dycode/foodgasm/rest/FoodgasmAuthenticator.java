package com.dycode.foodgasm.rest;

import android.content.Context;

import com.dycode.foodgasm.model.EventUnauthorized;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;

import javax.inject.Inject;

import okhttp3.Authenticator;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.Route;

/**
 * Created by USER on 2/20/2017.
 */

public class FoodgasmAuthenticator implements Authenticator {

    private Context mContext;

    @Inject
    public FoodgasmAuthenticator(Context mContext) {
        this.mContext = mContext;
    }

    @Override
    public Request authenticate(Route route, Response response) throws IOException {
        if(!response.request().url().toString().contains("login")){
            EventBus.getDefault().post(new EventUnauthorized());
        }
        return null;
}


}
