package com.dycode.foodgasm.rest;

import com.dycode.foodgasm.utils.Constants;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.OkHttpClient;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;

/**
 * Created by asus pc on 28/01/2016.
 */
public class RestClientUberProduct {
    private ApiService apiService;
    private static RestClientUberProduct instance = null;

    public static RestClientUberProduct getInstance() {
        if (instance == null)
            instance = new RestClientUberProduct();

        return instance;
    }

    private RestClientUberProduct() {
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'SSS'Z'")
                .create();

        final String basic = "Token " + Constants.Uber.SERVER_TOKEN;

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(Constants.Uber.BASE_URL)
                .setClient(new OkClient(new OkHttpClient()))
                .setConverter(new GsonConverter(gson))
                .setRequestInterceptor(new RequestInterceptor() {
                    @Override
                    public void intercept(RequestFacade request) {
                        request.addHeader("Authorization", basic);
                    }
                })
                .build();


        apiService = restAdapter.create(ApiService.class);
    }

    public ApiService getApiService() {
        return apiService;
    }
}
