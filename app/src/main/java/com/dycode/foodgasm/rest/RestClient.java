package com.dycode.foodgasm.rest;

import android.util.Base64;

import com.dycode.foodgasm.utils.Constants;
import com.dycode.foodgasm.model.EventUnauthorized;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.Authenticator;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;
import java.net.Proxy;
import java.util.concurrent.TimeUnit;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;

/**
 * Created by Okta Dwi Priatna on 02/10/2015.
 */
public class RestClient {
    private ApiService apiService;
    private static RestClient instance = null;

    public static RestClient getInstance() {
        if (instance == null)
            instance = new RestClient();

        return instance;
    }

    private RestClient() {
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'SSS'Z'")
                .create();

        String username = Constants.BASIC_USERNAME;
        String password = Constants.BASIC_PASSWORD;

        String credentials = username + ":" + password;
        final String basic = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);


        RestAdapter restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(Constants.Api.BASE_URL)
                .setClient(new OkClient( getOkhttp()))
                .setConverter(new GsonConverter(gson))
                .setRequestInterceptor(new RequestInterceptor() {
                    @Override
                    public void intercept(RequestFacade request) {
                        request.addHeader("Authorization", basic);
                        request.addHeader("Content-Type", "application/json");
                        request.addHeader("Accept", "application/json");
                    }
                })
                .build();


        apiService = restAdapter.create(ApiService.class);
    }

    public OkHttpClient getOkhttp(){
        OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setAuthenticator(new Authenticator() {
            @Override
            public Request authenticate(Proxy proxy, Response response) throws IOException {
                if(!response.request().url().toString().contains("login")){
                    EventBus.getDefault().post(new EventUnauthorized());
                }
                return null;
            }

            @Override
            public Request authenticateProxy(Proxy proxy, Response response) throws IOException {
                return null;
            }
        });

        return okHttpClient;

    }

    public ApiService getApiService() {
        return apiService;
    }
}
