package com.dycode.foodgasm.rest;

import com.dycode.foodgasm.model.AccessToken;
import com.dycode.foodgasm.model.ApiResponse;
import com.dycode.foodgasm.model.ApiResponseList;
import com.dycode.foodgasm.model.AppVersion;
import com.dycode.foodgasm.model.Category;
import com.dycode.foodgasm.model.ChangePassword;
import com.dycode.foodgasm.model.Comment;
import com.dycode.foodgasm.model.CommentDetail;
import com.dycode.foodgasm.model.CreditCard;
import com.dycode.foodgasm.model.CreditCardOrder;
import com.dycode.foodgasm.model.Deals;
import com.dycode.foodgasm.model.DealsDetail;
import com.dycode.foodgasm.model.DirectPay;
import com.dycode.foodgasm.model.DokuResponse;
import com.dycode.foodgasm.model.DokuSample;
import com.dycode.foodgasm.model.DokuWallet;
import com.dycode.foodgasm.model.DokuWalletBalance;
import com.dycode.foodgasm.model.DokuWalletBalanceResponse;
import com.dycode.foodgasm.model.DokuWalletRegData;
import com.dycode.foodgasm.model.DokuWalletRegEmail;
import com.dycode.foodgasm.model.DokuWalletRegister;
import com.dycode.foodgasm.model.DokuWalletResponse;
import com.dycode.foodgasm.model.DokuWalletResponseNew;
import com.dycode.foodgasm.model.EcashBalance;
import com.dycode.foodgasm.model.EcashOTP;
import com.dycode.foodgasm.model.EcashPayment;
import com.dycode.foodgasm.model.EcashPaymentModel;
import com.dycode.foodgasm.model.EcashRegister;
import com.dycode.foodgasm.model.EcashValidate;
import com.dycode.foodgasm.model.Gallery;
import com.dycode.foodgasm.model.Inbox;
import com.dycode.foodgasm.model.Menu;
import com.dycode.foodgasm.model.Order;
import com.dycode.foodgasm.model.OrderDetail;
import com.dycode.foodgasm.model.OrderHistory;
import com.dycode.foodgasm.model.PatchOrder;
import com.dycode.foodgasm.model.Payment;
import com.dycode.foodgasm.model.PostComment;
import com.dycode.foodgasm.model.PostCreditCard;
import com.dycode.foodgasm.model.PostEmail;
import com.dycode.foodgasm.model.PostLogin;
import com.dycode.foodgasm.model.PostOrder;
import com.dycode.foodgasm.model.PostPayment;
import com.dycode.foodgasm.model.PostRegister;
import com.dycode.foodgasm.model.Profile;
import com.dycode.foodgasm.model.Promo;
import com.dycode.foodgasm.model.Register;
import com.dycode.foodgasm.model.ReqPhoneVerify;
import com.dycode.foodgasm.model.Restaurant;
import com.dycode.foodgasm.model.SearchCategory;
import com.dycode.foodgasm.model.Times;
import com.dycode.foodgasm.model.UberPrices;
import com.dycode.foodgasm.model.User;
import com.dycode.foodgasm.model.Voucher;

import org.json.JSONObject;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.Headers;
import retrofit.http.Multipart;
import retrofit.http.PATCH;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Part;
import retrofit.http.Path;
import retrofit.http.Query;
import retrofit.mime.TypedFile;

/**
 * Created by Okta Dwi Priatna on 02/10/2015.
 */
public interface ApiService {

    // Login
    @POST("/login")
    void postLogin(@Body PostLogin postLogin, Callback<AccessToken> callback);

    // Register
    @POST("/register")
    void postRegister(@Body PostRegister postRegister, Callback<Register> callback);

    // Restaurant List
    @GET("/restaurants")
    void getListRestaurant(@Query("access_token") String accessToken, @Query("lat") String lat, @Query("long") String longitude, @Query("name") String name, @Query("category") String category, @Query("location") String location, @Query("skip") int skip, @Query("limit") int limit, Callback<ApiResponseList<Restaurant>> callback);

    // Restaurant List
    @GET("/restaurants")
    void getListRestaurantAll(@Query("access_token") String accessToken, @Query("lat") String lat, @Query("long") String longitude, Callback<ApiResponseList<Restaurant>> callback);

    // Restaurant List
    @GET("/restaurants")
    void getListRestaurantNoLimit(@Query("lat") String lat, @Query("long") String longitude, @Query("name") String name, @Query("category") String category, @Query("location") String location, @Query("skip") int skip, Callback<ApiResponseList<Restaurant>> callback);

    // Restaurant List
    @GET("/restaurants")
    void getListRestaurantDistance(@Query("lat") String lat, @Query("long") String longitude, @Query("name") String name, @Query("skip") int skip, @Query("limit") int limit, @Query("maxdistance") int maxdistances, @Query("firstdistance") Double firstdistance, Callback<ApiResponseList<Restaurant>> callback);

    // Restaurant Detail
    @GET("/restaurants/{id}")
    void getRestaurantDetail(@Path("id") String id, @Query("lat") String lat, @Query("long") String longitude, Callback<ApiResponse<Restaurant>> callback);

    // Menu List
    @GET("/menus")
    void getMenu(@Query("resto") String resto, @Query("access_token") String accessToken, @Query("name") String name, @Query("category") String categoryID, Callback<ApiResponseList<Menu>> callback);

    // Menu Category List
    @GET("/menu_categories")
    void getListMenuCategories(@Query("resto") String resto, Callback<ApiResponseList<Category>> callback);

    // Promo List
    @GET("/promos")
    void getListPromo(@Query("category") String category, @Query("resto") String restoId, Callback<ApiResponseList<Promo>> callback);

    // Promo Detail
    @GET("/promo/{id}")
    void getPromoDetail(@Path("id") String id, Callback<ApiResponse<Promo>> callback);

    // Deals List
    @GET("/deals")
    void getListDeals(@Query("access_token") String accessToken, @Query("skip") int skip, @Query("limit") int limit, Callback<ApiResponseList<Deals>> callback);

    //Deals detail
    @GET("/deal/{id}")
    void getDetailDeals(@Path("id") String id, @Query("lat") String lat, @Query("long") String longitude, @Query("skip") int skip, @Query("limit") int limit, Callback<ApiResponse<DealsDetail>> callback);

    // Comment List
    @GET("/comments")
    void getListComment(@Query("resto") String restoID, @Query("publish") String publish, @Query("order_desc") String order_desc, @Query("order_by") String order_by, Callback<ApiResponseList<Comment>> callback);

    // Comment Detail
    @GET("/comment/{id}")
    void getCommentDetail(@Path("id") String id, @Query("access_token") String accessToken, Callback<ApiResponse<CommentDetail>> callback);

    // Add Comment
    @POST("/comment")
    void postComment(@Body PostComment postComment, @Query("access_token") String accessToken, Callback<ApiResponse<Comment>> callback);

    //Gallery list
    @GET("/gallery")
    void getListGallery(@Query("resto") String restoID, Callback<ApiResponseList<Gallery>> callback);

    // Profile Data
    @GET("/user/me")
    void getProfileData(@Query("access_token") String accessToken, Callback<ApiResponse<Profile>> callback);

    //Put Profile
    @PUT("/user")
    void putProfileData(@Query("access_token") String accessToken, @Body PostRegister postRegister, Callback<ApiResponse<Profile>> callback);

    //Put Profile
    @PUT("/user")
    void changePassword(@Query("access_token") String accessToken, @Body ChangePassword changePassword, Callback<ApiResponse<Profile>> callback);

    //Put Profile Picture
    @PUT("/user")
    void putProfilePictureRegistration(@Query("access_token") String accessToken, @Body JSONObject photo, Callback<ApiResponse<Profile>> callback);

    @Multipart
    @PUT("/user")
    void uploadPhoto(@Query("access_token") String accessToken, @Part("image") TypedFile image, Callback<ApiResponse<Profile>> callback);

    // Add Order
    @Headers({
            "kHeaderID: wrwcjwczPYEZW7tZ5UVR8Sikd9TmnjrO",
            "kHeaderSecret:Ktuo8ausgJdxNmm9YQh4sKCCAmtQNR0M"
    })
    @POST("/order")
    void postOrder(@Body PostOrder postOrder, @Query("access_token") String accessToken, @Query("paymentWith") String paymentMethod, Callback<ApiResponse<Order>> callback);
    //doku pay implement


    @POST("/order")
    void postOrderNew(@Body PostOrder postOrder, @Query("access_token") String accessToken, @Query("paymentWith") String paymentMethod, @Query("paymentCredential") String paymentCredential, Callback<ApiResponse<Order>> callback);

    @Headers({
            "kHeaderID: wrwcjwczPYEZW7tZ5UVR8Sikd9TmnjrO",
            "kHeaderSecret:Ktuo8ausgJdxNmm9YQh4sKCCAmtQNR0M"
    })
    @POST("/order")
    void postOrderTotalPay(Callback<ApiResponseList<String>> callback);


    // Get Order List
//    @GET("/orders")
//    void getListOrder(@Query("access_token") String accessToken, @Query("user") String user, @Query("order_desc") String order_desc, @Query("order_by") String order_by, @Query("state") String state, @Query("state") String state2, @Query("state") String stat3, @Query("state") String stat4, Callback<ApiResponseList<OrderHistory>> callback);
    @GET("/orders")
    void getListOrder(@Query("access_token") String accessToken, @Query("user") String user, @Query("order_desc") String order_desc, @Query("order_by") String order_by, @Query("state") String state, @Query("state") String stat4, Callback<ApiResponseList<OrderHistory>> callback);

    // Get Order Detail
    @GET("/order/{id}")
    void getOrderDetail(@Path("id") String id, @Query("access_token") String accessToken, Callback<ApiResponse<OrderDetail>> callback);

    // Payment order
    @POST("/order/{id}")
    void postPayment(@Path("id") String id, @Body PostPayment postPayment, @Query("access_token") String accessToken, Callback<ApiResponse<Payment>> callback);

    // Payment  Doku
    @GET("/payment/doku/{id}")
    void postOrderNew(@Path("id") String id, @Query("access_token") String accessToken, Callback<ApiResponse<Payment>> callback);


    // Patch order
    @PATCH("/order/{id}")
    void patchOrder(@Path("id") String id, @Query("access_token") String accessToken, @Body PatchOrder patchOrder, Callback<Order> callback);

    //Validate voucher
    @GET("/voucher/{code}")
    void validateVoucher(@Path("code") String code, @Query("resto") String restoID, @Query("gross_amount") int grossAmount, @Query("access_token") String accessToken, @Query("paymentWith") String paymentWith, Callback<ApiResponse<Voucher>> callback);

    //UBER estimate time
    @GET("/estimates/time")
    void getUberEstimatesTime(@Query("start_latitude") Float start_latitude, @Query("start_longitude") Float start_longitude, @Query("customer_uuid") String customer_uuid, @Query("product_id") String product_id, Callback<Times> callback);

    //UBER estimate time
    @GET("/estimates/price")
    void getUberEstimatesPrice(@Query("start_latitude") Float start_latitude, @Query("start_longitude") Float start_longitude, @Query("end_latitude") Float end_latitude, @Query("end_longitude") Float end_longitude, Callback<UberPrices> callback);

    //Restart Password
    @POST("/forgot")
    void postEmailResetPassword(@Body PostEmail postEmail, Callback<ApiResponse<User>> callback);

    //App Version
    @GET("/appversion")
    void getAppVersion(@Query("access_token") String accessToken, Callback<ApiResponse<AppVersion>> callback);

    //Register Credit Card
    @POST("/creditcard")
    void postCreditCard(@Query("access_token") String accessToken, @Body PostCreditCard postCreditCard, Callback<ApiResponse<CreditCard>> callback);

    //Register Credit Card to VT
    @GET("/card/register")
    void postCreditCardToVeritrans(@Query("card_number") String card_number, @Query("card_exp_month") String card_exp_month, @Query("card_exp_year") String card_exp_year, @Query("client_key") String client_key, Callback<CreditCard> callback);

    //Get Credit Card List
    @GET("/creditcards")
    void getCreditCardList(@Query("access_token") String accessToken, Callback<ApiResponseList<CreditCard>> callback);

    //Remove Credit Card List
    @DELETE("/creditcard/{creditcard_id}")
    void removeCreditCard(@Path("creditcard_id") String creditcard_id, @Query("access_token") String accessToken, Callback<CreditCard> callback);

    //Set Active Credit Card
    @PATCH("/creditcard/{creditcard_id}")
    void setCreditCardActive(@Path("creditcard_id") String creditcard_id, @Query("access_token") String accessToken, Callback<CreditCard> callback);

    //GET Detail Saved Token
    @GET("/creditcard/{creditcard_id}")
    void setCreditCardActivePay(@Path("creditcard_id") String creditcard_id, @Query("access_token") String accessToken, Callback<ApiResponse<CreditCardOrder>> callback);

    //Get Token from VT
    @GET("/token")
    void getVeritransToken(@Query("token_id") String token_id, @Query("card_cvv") String card_cvv, @Query("two_click") Boolean two_click, @Query("client_key") String client_key, @Query("gross_amount") String gross_amount, @Query("secure") boolean secure, @Query("payment_type") String payment_type, Callback<CreditCard> callback);

    // Inbox List
    @GET("/inbox")
    void getListInbox(@Query("access_token") String accessToken, @Query("skip") int skip, @Query("limit") int limit, Callback<ApiResponseList<Inbox>> callback);

    // Restaurant Search Category
    @GET("/restaurant_categories")
    void getListCategory(Callback<ApiResponseList<SearchCategory>> callback);

    // Restaurant Search Location
    @GET("/restaurant_cities")
    void getListLocation(Callback<ApiResponseList<String>> callback);

    @POST("/direct_payment")
    void getDokuSample(@Query("access_token") String token_id, @Body DirectPay directpay, Callback<ApiResponse<DokuSample>> callback);

    //PostDokuResponse
    @POST("/payment/withdokucc/57fb56b5298abdbc1641252e?access_token=u1jDK3T0eJNd6NuFn9cFGLcfQgocPMwuOgBelEUr9yqzMngYZXgrKjLGSYqA8KpZPt1am6qfdbpPg1WyX8SwWhLTiUDhuR0Tnz3pWBkvUaSEim56JCvqmMEbiu9J7KVF")
    void postDokuResponse(@Body DokuResponse dataDoku, Callback<ApiResponse<String>> callback);

    @GET("/payment/withdokucc/{id}")
    void getDokuSampleSandBox(@Path("id") String id, @Query("access_token") String token_id, Callback<ApiResponse<DokuSample>> callback);

    @POST("/payment/withdokucc/{id}")
    void postDokuResponseSandbox(@Path("id") String id, @Query("access_token") String token_id, @Body DokuResponse dataDoku, Callback<ApiResponse<String>> callback);

    @POST("/payment/withdokuwl/{orderId}")
    void postDokuResponseWallet(@Path("orderId") String orderId, @Query("access_token") String token_id, @Body DokuWallet dokuwallet, Callback<ApiResponse<DokuWalletResponse>> callback);

    @POST("/payment/withdokuwl")
    void postRegisterDokuWallet(@Query("access_token") String token_id, @Body DokuWalletRegister dokuwalletregis, Callback<ApiResponse<DokuWalletRegData>> callback);

    @GET("/payment/withdokuwl")
    void postRegWalletEmail(@Query("access_token") String token_id, @Query("email") String email, Callback<ApiResponse<DokuWalletRegEmail>> callback);

    @POST("/payment/withdokuwl/{orderId}")
    void postDokuResponseWalletNew(@Path("orderId") String orderId, @Query("access_token") String token_id, @Body DokuWallet dokuwallet, Callback<ApiResponse<DokuWalletResponseNew>> callback);

    @POST("/dokuhub")
    void postBalanceDokuWallet(@Query("access_token") String token_id, @Body DokuWalletBalance dokuWalletBalance, Callback<ApiResponse<DokuWalletBalanceResponse>> callback);

    // Request Phone Verification
    @GET("/verification/phone")
    void getPhoneVerify(@Query("access_token") String accessToken, @Query("to") String phoneNumber, Callback<ApiResponse<ReqPhoneVerify>> callback);

    // Phone Verification
    @POST("/verification/phone")
    void postPhoneVerify(@Query("access_token") String accessToken, @Body ReqPhoneVerify reqBodyPhone, Callback<ApiResponse> callback);

    // OTP Verification E-CASH
    @POST("/ecashhub/OTPRequest")
    void postPhoneVerifyEcash(@Query("access_token") String accessToken, @Body EcashOTP ecashOTP, Callback<ApiResponse> callback);

    // OTP Verification E-CASH
    @POST("/payment/withecash")
    void postEcashRegister(@Query("access_token") String accessToken, @Body EcashRegister ecashRegister, Callback<ApiResponse> callback);

    // Checkout Payment E-CASH
    @POST("/payment/withecash/{orderId}")
    void postEcashPayment(@Path("orderId") String orderId, @Query("access_token") String token_id, @Body EcashPayment ecashPayment, Callback<ApiResponse<EcashPaymentModel>> callback);

    //Validate Account e-cash /api/v1/payment/withecash
    @GET("/payment/withecash")
    void getAccountValidate(@Query("access_token") String accessToken, @Query("msisdn") String msisdn, Callback<ApiResponse<EcashValidate>> callback);

    @GET("/dummy/ecashpaymentsuccess")
    void getDummyPaymentEcash(Callback<ApiResponse<EcashPaymentModel>> callback);

    @POST("/ecashhub/balanceInquiry")
    void postBalanceEcash(@Query("access_token") String token_id, @Body EcashBalance ecashBalance, Callback<ApiResponse> callback);

    @GET("/order/state/{orderId}")
    void getStatusVeritrans(@Path("orderId") String orderId, @Query("access_token") String token_id, Callback<ApiResponse> callback);


}

