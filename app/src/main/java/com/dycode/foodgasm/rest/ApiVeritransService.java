package com.dycode.foodgasm.rest;

import com.dycode.foodgasm.model.AccessToken;
import com.dycode.foodgasm.model.ApiResponse;
import com.dycode.foodgasm.model.CreditCard;
import com.dycode.foodgasm.model.CreditCardOrder;
import com.dycode.foodgasm.model.DokuWallet;
import com.dycode.foodgasm.model.DokuWalletBalance;
import com.dycode.foodgasm.model.DokuWalletBalanceResponse;
import com.dycode.foodgasm.model.DokuWalletResponse;
import com.dycode.foodgasm.model.EcashBalance;
import com.dycode.foodgasm.model.EcashPayment;
import com.dycode.foodgasm.model.EcashPaymentModel;
import com.dycode.foodgasm.model.OrderDetail;
import com.dycode.foodgasm.model.PostLogin;

import org.jetbrains.annotations.NotNull;

import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by USER on 5/17/2017.
 */

public interface ApiVeritransService {

    @GET("/v2/token")
    Observable<CreditCard> getVeritransToken(@Query("token_id") String token_id, @Query("card_cvv") String card_cvv, @Query("two_click") Boolean two_click, @Query("client_key") String client_key, @Query("gross_amount") String gross_amount, @Query("secure") boolean secure, @Query("payment_type") String payment_type);

}
