package com.dycode.foodgasm.rest;

import com.dycode.foodgasm.model.AccessToken;
import com.dycode.foodgasm.model.ApiResponse;
import com.dycode.foodgasm.model.CreditCard;
import com.dycode.foodgasm.model.CreditCardOrder;
import com.dycode.foodgasm.model.DokuWallet;
import com.dycode.foodgasm.model.DokuWalletBalance;
import com.dycode.foodgasm.model.DokuWalletBalanceResponse;
import com.dycode.foodgasm.model.DokuWalletResponse;
import com.dycode.foodgasm.model.EcashBalance;
import com.dycode.foodgasm.model.EcashPayment;
import com.dycode.foodgasm.model.EcashPaymentModel;
import com.dycode.foodgasm.model.Order;
import com.dycode.foodgasm.model.OrderDetail;
import com.dycode.foodgasm.model.PatchOrder;
import com.dycode.foodgasm.model.Payment;
import com.dycode.foodgasm.model.PostLogin;
import com.dycode.foodgasm.model.PostPayment;
import com.dycode.foodgasm.model.PostRegister;
import com.dycode.foodgasm.model.Profile;
import com.dycode.foodgasm.model.Register;
import com.dycode.foodgasm.model.ReqPhoneVerify;

import org.jetbrains.annotations.NotNull;

import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by USER on 5/17/2017.
 */

public interface ApiV2Service {

    @POST("/api/v1/login")
    Observable<AccessToken> login(@NotNull @Body PostLogin postLogin);

    @GET("/api/v1/order/{id}")
    Observable<ApiResponse<OrderDetail>> getOrderDetail(@Path("id") String id);

    @POST("/api/v1/dokuhub")
    Observable<ApiResponse<DokuWalletBalanceResponse>> postBalanceDokuWallet(@Body DokuWalletBalance dokuWalletBalance);

    @POST("/api/v1/payment/withdokuwl/{orderId}")
    Observable<ApiResponse<DokuWalletResponse>> postPayDokuWallet(@Path("orderId") String orderId, @Body DokuWallet dokuWallet);

    @POST("/api/v1/ecashhub/balanceInquiry")
    Observable<ApiResponse> postBalanceEcash(@Body EcashBalance ecashBalance);

    @POST("/api/v1/payment/withecash/{orderId}")
    Observable<ApiResponse<EcashPaymentModel>> postPaymentEcash(@Path("orderId") String orderId, @Body EcashPayment ecashPayment);

    @GET("/api/v1/token")
    Observable<CreditCard> getVeritransToken(@Query("token_id") String token_id, @Query("card_cvv") String card_cvv, @Query("two_click") Boolean two_click, @Query("client_key") String client_key, @Query("gross_amount") String gross_amount, @Query("secure") boolean secure, @Query("payment_type") String payment_type);

    //GET Detail Saved Token
    @GET("/api/v1/creditcard/{creditcard_id}")
    Observable<ApiResponse<CreditCardOrder>> getCreditCardToken(@Path("creditcard_id") String creditcardId);

    @POST("/api/v1/order/{id}")
    Observable<ApiResponse<Payment>> postVeritransPayment(@Path("id") String id, @Body PostPayment postPayment);

    @GET("/api/v1/order/state/{orderId}")
    Observable<ApiResponse> getStatusVeritrans(@Path("orderId") String orderId);

    // Patch order
    @PATCH("/api/v1/order/{id}")
    Observable<Order> patchOrder(@Path("id") String id, @Body PatchOrder patchOrder);

    //Verify Phone Regiter
    @GET("/api/v1/verification/phone")
    Observable<ApiResponse<ReqPhoneVerify>> getPhoneVerification(@Query("to") String phoneNumber);

    @POST("/api/v1/verification/phone/check")
    Observable<ApiResponse> postPhoneVerification(@Body ReqPhoneVerify reqBodyPhone);

    @POST("/api/v1/register")
    Observable<Register> postRegister(@Body PostRegister postRegister);

    @PUT("/api/v1/user")
    Observable<ApiResponse<Profile>> putProfile(@Body PostRegister postRegister);
}
