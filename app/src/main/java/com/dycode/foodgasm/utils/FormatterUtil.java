package com.dycode.foodgasm.utils;

import android.util.Log;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by fahmi on 27/10/2015.
 */
public class FormatterUtil {

    private static char[] c = new char[]{'K', 'M', 'B', 'T'};
    private static String dataB;
    private static DecimalFormat formatter;

    public static String convertToIDRFormat(double n) {
        if(n < 1000){
            return "IDR " + (long) n ;
        }
        return convertToIDRFormat(n, 0);
    }

    private static String convertToIDRFormat(double n, int iteration) {
        double d = ((long) n / 100) / 10.0;
        return "IDR " + (d < 1000000000 ? ((d > 99.9 || (d * 10) % 10 == 0 ? (int) d * 10 / 10 : d + "") + "" + c[iteration]) : convertToIDRFormat(d, iteration + 1));
    }


    public static String getGermanCurrencyFormat(double value) {
        Log.d("Value > ", String.valueOf(value));
        double data = 0;
        NumberFormat nf = NumberFormat.getNumberInstance(Locale.getDefault());

        nf.setRoundingMode(RoundingMode.CEILING);
        nf.setMinimumFractionDigits(0);
        nf.setMaximumFractionDigits(1);
        nf.setGroupingUsed(true);

        DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.GERMAN);
        otherSymbols.setDecimalSeparator('.');
        otherSymbols.setGroupingSeparator(',');
        formatter = new DecimalFormat("#.#", otherSymbols);
//        formatter.setRoundingMode(RoundingMode.CEILING);
        formatter.setDecimalFormatSymbols(otherSymbols);
        formatter.setMinimumFractionDigits(0);
        formatter.setMaximumFractionDigits(1);


//        double data = Double.parseDouble(nf.format(value).replaceAll(",", "."));


        if (String.valueOf(value).length() > 8) {
//            String[] dataDo = String.valueOf(value).replace("\\.", ",").split("\\.");
//            dataB = dataDo[0];
//            double valuesB = Double.parseDouble(dataB);
//            Log.d("dataB > ", dataB);
//            nf.setMaximumFractionDigits(1);
//            data = Double.parseDouble(nf.format(valuesB).split("\\.")[0].replace(",", "."));
        } else {
            data = Double.parseDouble(nf.format(value).replaceAll(",", "."));
        }


        if (String.valueOf(value).length() > 5 && String.valueOf(value).length() < 9) {
            return "IDR " + formatter.format(data) + "K";
        } else if (String.valueOf(value).length() > 8) {
            Log.d(" >8", " 88 ");
            formatter = new DecimalFormat("###,###", otherSymbols);
            return "IDR " + formatter.format(value/1000) + "K";
        } else {
            return "IDR " + formatter.format(data);
        }
    }


    public static String CurrencyFormatThree(double value) {
        NumberFormat nf = NumberFormat.getNumberInstance(Locale.GERMAN);
        nf.setRoundingMode(RoundingMode.CEILING);
        nf.setMinimumFractionDigits(0);
        nf.setMaximumFractionDigits(1);
        nf.setGroupingUsed(true);

        DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.GERMAN);
        otherSymbols.setDecimalSeparator('.');
        otherSymbols.setGroupingSeparator('.');
        DecimalFormat formatter = new DecimalFormat("#.##", otherSymbols);
//        formatter.setRoundingMode(RoundingMode.CEILING);
        formatter.setDecimalFormatSymbols(otherSymbols);
        formatter.setMinimumFractionDigits(0);
        formatter.setMaximumFractionDigits(1);

        double data = Double.parseDouble(nf.format(value));
        return "IDR " + formatter.format(data) + "K";
    }

    public String Format(Integer number) {
        String[] suffix = new String[]{"k", "m", "b", "t"};
        int size = (number.intValue() != 0) ? (int) Math.log10(number) : 0;
        if (size >= 3) {
            while (size % 3 != 0) {
                size = size - 1;
            }
        }
        double notation = Math.pow(10, size);
        String result = (size >= 3) ? +(Math.round((number / notation) * 100) / 100.0d) + suffix[(size / 3) - 1] : +number + "";
        return result;
    }

}
