package com.dycode.foodgasm.utils;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.Toast;


public class CommonUtil {

    public static void showAlertDialog(Context context, String title, String message){
        AlertDialog alertDialog = new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create();
        alertDialog.show();
    }


    public static void showLoading() {

    }

    public static ProgressDialog createLoading(Context context) {
        ProgressDialog progressDialog  = new ProgressDialog(context);
        progressDialog.setMessage("Loading....");
        return progressDialog;
    }

    public static void hideLoading(ProgressDialog progressDialog) {
        if(progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
    }

    public static void showToast(Context context, String text) {
        Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
    }


}
