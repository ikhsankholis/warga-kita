package com.dycode.foodgasm.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;

import java.util.Random;
import java.util.Vector;


/**
 * 
 * @author Irvan Rabiullah
 * 
 */
public class CTools {
	
	public CTools()
	{
	}
	
    public int RGBToInt(String r, String g, String b) {
    	int red = Integer.parseInt( r );
    	int gre = Integer.parseInt( g );
    	int blu = Integer.parseInt( b );
    	return RGBToInt( red, gre, blu );
    }
    public int RGBToInt(int r, int g, int b) 
    {
    	int color = 0;
    	
    	if( r < 0 && r > 255 ) return -1;
    	if( g < 0 && g > 255 ) return -1;
    	if( b < 0 && b > 255 ) return -1;
    	
        color = color | b;
        color = color | g << 8;
        color = color | r << 16;   
        
        return color;
    }

	public static byte[] converIntToBytes( int[] convert )
	{
		byte[] bbt = new byte[ convert.length ];
		for( int v=0; v<convert.length; v++ )
		{
			bbt[v] = (byte) convert[v];
		}
		return bbt;
	}
	
	public static int convertToInt( String s )
	{
		try{
			return Integer.parseInt( s );
		}catch (Exception e) {}
		return -1;
	}
	

	public String[] convertVectorToStringArray( Vector vec )
	{
		String[] str = new String[ vec.size() ];
		for( int v=0; v<vec.size(); v++ )
		{
			str[v] = vec.elementAt( v ).toString();
		}
		return str;
	}
	
    private int getIndexData( String[] choices, String dataSelected )
    {
    	int i=0;
    	for( int v=0; v<choices.length; v++ )
    	{
    		if( choices[v].equals( dataSelected ) )
    		{
    			i = v;
    			break;
    		}
    	}
    	return i;
    }
    
	public int getRandom( int min, int max )
	{
	    Random r = new Random();
	    r.setSeed(System.currentTimeMillis());

	    int i = r.nextInt(max - min + 1) + min;
	    return i;
	}
	
	public static int getRandomInteger(int aStart, int aEnd){
        Random generator = new Random();
        generator.setSeed(System.currentTimeMillis());
        if ( aStart > aEnd ) {
          throw new IllegalArgumentException("Start cannot exceed End.");
        }
        //get the range, casting to long to avoid overflow problems
        long range = (long)aEnd - (long)aStart + 1;
        // compute a fraction of the range, 0 <= frac < range
        long fraction = (long)(range * generator.nextDouble());
        int randomNumber =  (int)(fraction + aStart);
        System.out.println("Generated : " + randomNumber);
        return randomNumber;
	}
	
	public static boolean checkAngka( String str )
	{
		return str.startsWith("1") || str.startsWith("2") || str.startsWith("3") ||
				str.startsWith("4") || str.startsWith("5") || str.startsWith("6") ||
				str.startsWith("7") || str.startsWith("8") || str.startsWith("9") ||
				str.startsWith("0");
	}

    
    public static String getRP( String myNumber, boolean showRp )
    {
//    	if( !checkAngka( myNumber ) )
//    	{
//            if( showRp ) myNumber = "Rp " + "0";
//        	return myNumber;
//    	}
//    	else
    	if( myNumber.indexOf( "." ) > 0 ) return myNumber;
    	
    	if( myNumber.length() == 0 )
    	{
            if( showRp ) myNumber = "IDR " + "0";
        	return myNumber;
    	}
    	else
        if( myNumber.length() <= 3 ) 
    	{
            if( showRp ) myNumber = "IDR " + myNumber;
        	return myNumber;
    	}
            
        String rp = "";
        String rp1 ="", rp2 ="";
        if( myNumber.length() > 3 ) rp1 = "."+myNumber.substring( myNumber.length()-3, myNumber.length() );
        if( myNumber.length() > 6 ) rp2 = "."+myNumber.substring( myNumber.length()-6, myNumber.length() );
        int x = myNumber.length()-1;
        int r = 0;
        while( x >= 0 )
        {
                rp = myNumber.substring( x, x+1 ) + rp;
                r++;
                if( r == 3 && x != 0 )
                {
                        r = 0;
                        rp = "," + rp;
                }
                x--;
        }
        
        if( showRp ) rp = "IDR " + rp;
        return rp;
    }
    
	public static int getInt( String str ) 
	{
		try{
			return Integer.parseInt( str );
		}catch( Exception e ){}
		return 0;
	}

    
    public static String[] split(String strString, String strDelimiter)
	{
		int iOccurrences = 0;
		int iIndexOfInnerString = 0;
		int iIndexOfDelimiter = 0;
		int iCounter = 0;

		// Check for null input strings.
		if (strString == null)
		{
			throw new NullPointerException("Input string cannot be null.");
		}
		// Check for null or empty delimiter
		// strings.
		if (strDelimiter.length() <= 0 || strDelimiter == null)
		{
			throw new NullPointerException("Delimeter cannot be null or empty.");
		}

		// If strString begins with delimiter
		// then remove it in
		// order
		// to comply with the desired format.

		if (strString.startsWith(strDelimiter))
		{
			strString = strString.substring(strDelimiter.length());
		}

		// If strString does not end with the
		// delimiter then add it
		// to the string in order to comply with
		// the desired format.
		if (!strString.endsWith(strDelimiter))
		{
			strString += strDelimiter;
		}

		// Count occurrences of the delimiter in
		// the string.
		// Occurrences should be the same amount
		// of inner strings.
		while((iIndexOfDelimiter= strString.indexOf(strDelimiter,iIndexOfInnerString))!=-1)
		{
			iOccurrences += 1;
			iIndexOfInnerString = iIndexOfDelimiter + strDelimiter.length();
		}

		// Declare the array with the correct
		// size.
		String[] strArray = new String[iOccurrences];

		// Reset the indices.
		iIndexOfInnerString = 0;
		iIndexOfDelimiter = 0;

		// Walk across the string again and this
		// time add the
		// strings to the array.
		while((iIndexOfDelimiter= strString.indexOf(strDelimiter,iIndexOfInnerString))!=-1)
		{

			// Add string to
			// array.
			strArray[iCounter] = strString.substring(iIndexOfInnerString, iIndexOfDelimiter);

			// Increment the
			// index to the next
			// character after
			// the next
			// delimiter.
			iIndexOfInnerString = iIndexOfDelimiter + strDelimiter.length();

			// Inc the counter.
			iCounter += 1;
		}
            return strArray;
	}
    
    public static String replace(String source, String pattern, String replacement)
	{	
	
		//If source is null then Stop
		//and return empty String.
		if (source == null)
		{
			return "";
		}

		StringBuffer sb = new StringBuffer();
		//Intialize Index to -1
		//to check against it later 
		int idx = -1;
		//Intialize pattern Index
		int patIdx = 0;
		//Search source from 0 to first occurrence of pattern
		//Set Idx equal to index at which pattern is found.
		idx = source.indexOf(pattern, patIdx);
		//If Pattern is found, idx will not be -1 anymore.
		if (idx != -1)
		{
			//append all the string in source till the pattern starts.
			sb.append(source.substring(patIdx, idx));
			//append replacement of the pattern.
			sb.append(replacement);
			//Increase the value of patIdx
			//till the end of the pattern
			patIdx = idx + pattern.length();
			//Append remaining string to the String Buffer.
			sb.append(source.substring(patIdx));
		}
		//Return StringBuffer as a String

                if ( sb.length() == 0)
                {
                    return source;
                }
                else
                {
                    return sb.toString();
                }
	}
    
    public static String replaceAll(String source, String pattern, String replacement)
    {    

        //If source is null then Stop
        //and retutn empty String.
        if (source == null)
        {
            return "";
        }

        StringBuffer sb = new StringBuffer();
        //Intialize Index to -1
        //to check agaist it later 
        int idx = 0;
        //Search source from 0 to first occurrence of pattern
        //Set Idx equal to index at which pattern is found.

        String workingSource = source;
        
        //Iterate for the Pattern till idx is not be -1.
        while ((idx = workingSource.indexOf(pattern, idx)) != -1)
        {
            //append all the string in source till the pattern starts.
            sb.append(workingSource.substring(0, idx));
            //append replacement of the pattern.
            sb.append(replacement);
            //Append remaining string to the String Buffer.
            sb.append(workingSource.substring(idx + pattern.length()));
            
            //Store the updated String and check again.
            workingSource = sb.toString();
            
            //Reset the StringBuffer.
            sb.delete(0, sb.length());
            
            //Move the index ahead.
            idx += replacement.length();
        }

        return workingSource;
    }

	public static String getImeiId( Context a )
	{
		String imeiNo = "";
		TelephonyManager tel = (TelephonyManager)a.getSystemService(Context.TELEPHONY_SERVICE);
		imeiNo = tel.getDeviceId();
		return imeiNo;
	}
	
	public static void sendEmail( Activity activity, String email, String subject, String message )
	{
		Intent intentEmail = new Intent(Intent.ACTION_SEND);
		intentEmail.putExtra(Intent.EXTRA_EMAIL, new String[]{ email });
		intentEmail.putExtra(Intent.EXTRA_SUBJECT, subject );
		intentEmail.putExtra(Intent.EXTRA_TEXT, message);
		intentEmail.setType("message/rfc822");
		activity.startActivity(Intent.createChooser(intentEmail, "Choose an email provider :"));		
	}

}