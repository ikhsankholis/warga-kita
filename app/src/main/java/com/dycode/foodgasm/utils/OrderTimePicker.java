package com.dycode.foodgasm.utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.dycode.foodgasm.R;
import com.dycode.foodgasm.widget.loopview.LoopListener;
import com.dycode.foodgasm.widget.loopview.LoopView;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;


/**
 * Created by asus pc on 23/11/2015.
 */
public class OrderTimePicker extends PopupWindow implements View.OnClickListener {

    public TextView cancelBtn;
    public TextView confirmBtn;
    public LoopView yearLoopView;
    public LoopView monthLoopView;
    public LoopView dayLoopView;
    public View pickerContainerV;
    public View contentView;//root view

    private int yearPos = 0;
    private int monthPos = 0;
    private int dayPos = 0;
    private Context mContext;

    List<String> yearList = new ArrayList();
    List<String> monthList = new ArrayList();
    List<String> dayList = new ArrayList();

    List<String> todayList = new ArrayList<String>();
    List<String> tomorrowList = new ArrayList<String>();

    String openTime, closeTime, openTimeFormatted, closeTimeFormatted, openTimeHoursFormat, closeTimeHoursFormat, openHoursTimeFormat, closeHoursTimeFormat;
    int open, close, openMinutes, closeMinutes;
    boolean isTakeAway;

    private OnDatePickedListener mListener;


    public OrderTimePicker(Context cxt, String openTime, String closeTime, boolean isTakeAway, OnDatePickedListener l) {

        this.mContext = cxt;
        this.openTime = openTime;
        this.closeTime = closeTime;
        this.isTakeAway = isTakeAway;
        this.mListener = l;

        initView();
    }


    private void initView() {

        contentView = LayoutInflater.from(mContext).inflate(R.layout.layout_date_picker, null);
        cancelBtn = (TextView) contentView.findViewById(R.id.tvCancel);
        confirmBtn = (TextView) contentView.findViewById(R.id.tvConfirm);
        yearLoopView = (LoopView) contentView.findViewById(R.id.picker_year);
        monthLoopView = (LoopView) contentView.findViewById(R.id.picker_month);
        dayLoopView = (LoopView) contentView.findViewById(R.id.picker_day);
        pickerContainerV = contentView.findViewById(R.id.container_picker);

        SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm a", Locale.getDefault());
        SimpleDateFormat dateFormat2 = new SimpleDateFormat("HH:mm", Locale.getDefault());
        try {
            Date dateOpen = dateFormat.parse(openTime);
            Date dateClose = dateFormat.parse(closeTime);

            openTimeFormatted = dateFormat2.format(dateOpen);
            closeTimeFormatted = dateFormat2.format(dateClose);

            openTimeHoursFormat = dateFormat.format(dateOpen);
            closeTimeHoursFormat = dateFormat.format(dateClose);

        } catch (ParseException e) {
        }

        String customOpenTime = openTimeFormatted.substring(0, 2);
        String customCloseTime = closeTimeFormatted.substring(0, 2);
        String openTimeMinutes = openTimeFormatted.substring(3, 5);
        String closeTimeMinutes = closeTimeFormatted.substring(3, 5);
        open = Integer.parseInt(customOpenTime);
        close = Integer.parseInt(customCloseTime);
        openMinutes = Integer.parseInt(openTimeMinutes);
        closeMinutes = Integer.parseInt(closeTimeMinutes);

        openHoursTimeFormat = openTimeHoursFormat.substring(6, 8);
        closeHoursTimeFormat = closeTimeHoursFormat.substring(6, 8);


        //do not loop,default can loop
        yearLoopView.setNotLoop();
        monthLoopView.setNotLoop();
        dayLoopView.setNotLoop();

        //set loopview text size
        yearLoopView.setTextSize(18);
        monthLoopView.setTextSize(18);
        dayLoopView.setTextSize(18);

        //set checked listen
        yearLoopView.setListener(new LoopListener() {
            @Override
            public void onItemSelect(int item) {
                yearPos = item;
                initMonthPickerView(yearPos);
            }
        });

        initPickerViews(); // init day and hour loop view
        if (isTakeAway) dayLoopView.setVisibility(View.GONE);
        else {
            dayLoopView.setVisibility(View.VISIBLE);
            initPersonPickerViews(); //init person loop view
        }

        cancelBtn.setOnClickListener(this);
        confirmBtn.setOnClickListener(this);
        contentView.setOnClickListener(this);

        setTouchable(true);
        setFocusable(true);
        // setOutsideTouchable(true);
        setBackgroundDrawable(new BitmapDrawable());
        setAnimationStyle(R.style.FadeInPopWin);
        setContentView(contentView);
        setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        setHeight(ViewGroup.LayoutParams.MATCH_PARENT);
    }

    /**
     * Init year and month loop view,
     * Let the day loop view be handled separately
     */
    private void initPickerViews() {

        Calendar c = Calendar.getInstance();
        if (close == 0)
            close += 24;
        else if (open > close)
            close += 24;

        int hoursLeft = close - c.get(Calendar.HOUR_OF_DAY);
        setupTodayList();
        setupTomorrowList();

        if (hoursLeft - 1 == 0 || hoursLeft - 1 < 0) {
            yearList.add("Tomorrow");
            initMonthbPickerView();
        } else {
            if (hoursLeft > 0) {
                yearList.add("Today");
                yearList.add("Tomorrow");
                initMonthPickerView(yearPos);
            } else {
                yearList.add("Tomorrow");
                initMonthbPickerView();
            }
        }

        yearLoopView.setArrayList((ArrayList) yearList);
        yearLoopView.setInitPosition(yearPos);

    }

    private void initMonthPickerView(int yearPos) {
        if (yearPos == 0) {
            monthList.clear();
            monthList.addAll(todayList);
            monthLoopView.setArrayList((ArrayList) monthList);
            monthLoopView.setInitPosition(monthPos);
        } else if (yearPos == 1) {
            monthList.clear();
            monthList.addAll(tomorrowList);
            monthLoopView.setArrayList((ArrayList) monthList);
            monthLoopView.setInitPosition(monthPos);
        }
    }

    private void initMonthbPickerView() {
        monthList.addAll(tomorrowList);
        monthLoopView.setArrayList((ArrayList) monthList);
        monthLoopView.setInitPosition(monthPos);
    }

    private void setupTodayList() {
        monthList.clear();
        Calendar c = Calendar.getInstance();
        int minutesCount = (c.get(Calendar.MINUTE) / 15) + 1;
        int minutes = minutesCount * 15;
        int hours = c.get(Calendar.HOUR_OF_DAY);
        int firstHours = hours;
        int minutesAdd = 0;
        if (firstHours < open) {
            firstHours = open;
            minutes = 0;
        } else if (firstHours >= open){
            firstHours += 1;
        }

        String minuteResult;

        int businessHourLeft = close - firstHours;
        for (int indexOfHours = 0; indexOfHours < businessHourLeft - 1; indexOfHours++) {
            int hoursAdd = firstHours + indexOfHours;
            for (int indexOfMinutes = 0; indexOfMinutes < 4; indexOfMinutes++) {
                minutesAdd = minutes + (indexOfMinutes * 15);
                if (hoursAdd == open) {
                    if (minutesAdd >= openMinutes) {
                        if (minutesAdd < 60) {
                            if (hoursAdd + 1 < close) {
                                minuteResult = new DecimalFormat("00").format(minutesAdd);
                                if (hoursAdd < 24)
                                    todayList.add(String.valueOf(hoursAdd + ":" + minuteResult));
                                else
                                    tomorrowList.add(String.valueOf(hoursAdd - 24 + ":" + minuteResult));
                            }
                        } else {
                            if (hoursAdd + 1 < close) {
                                if (hoursAdd + 2 < close) {
                                    minuteResult = new DecimalFormat("00").format(minutesAdd - 60);
                                    if (hoursAdd < 23)
                                        todayList.add(String.valueOf(hoursAdd + 1 + ":" + minuteResult));
                                    else
                                        tomorrowList.add(String.valueOf(hoursAdd + 1 - 24 + ":" + minuteResult));
                                }
                            }
                        }
                    }
                } else {
                    if (minutesAdd < 60) {
                        if (hoursAdd + 1 < close) {
                            minuteResult = new DecimalFormat("00").format(minutesAdd);
                            if (hoursAdd < 24)
                                todayList.add(String.valueOf(hoursAdd + ":" + minuteResult));
                            else
                                tomorrowList.add(String.valueOf(hoursAdd - 24 + ":" + minuteResult));
                        }
                    } else {
                        if (hoursAdd + 1 < close) {
                            if (hoursAdd + 2 < close) {
                                minuteResult = new DecimalFormat("00").format(minutesAdd - 60);
                                if (hoursAdd < 23)
                                    todayList.add(String.valueOf(hoursAdd + 1 + ":" + minuteResult));
                                else
                                    tomorrowList.add(String.valueOf(hoursAdd + 1 - 24 + ":" + minuteResult));
                            }
                        }
                    }
                }

            }
        }
        if (close <= 24)
            todayList.add(String.valueOf(close - 1 + ":" + new DecimalFormat("00").format(0)));
        else todayList.add("24:00");
    }

    private void setupTomorrowList() {
        int minutes = 0;
        int minutesAdd = 0;
        int hoursleft = 0;
        if (close > open) hoursleft = close - open;
        else hoursleft = open - close;
        String minuteResult;

        for (int indexOfHours = 0; indexOfHours < hoursleft - 1; indexOfHours++) {
            int hoursAdd = open + indexOfHours;
            for (int indexOfMinutes = 0; indexOfMinutes < 4; indexOfMinutes++) {
                minutesAdd = minutes + (indexOfMinutes * 15);
                if (hoursAdd == open) {
                    if (minutesAdd >= openMinutes) {
                        if (minutesAdd < 60) {
                            if (hoursAdd + 1 < close) {
                                minuteResult = new DecimalFormat("00").format(minutesAdd);
                                if (hoursAdd < 24)
                                    tomorrowList.add(String.valueOf(hoursAdd + ":" + minuteResult));
                            }
                        } else {
                            if (hoursAdd + 1 < close) {
                                if (hoursAdd + 2 < close) {
                                    minuteResult = new DecimalFormat("00").format(minutesAdd - 60);
                                    if (hoursAdd < 23)
                                        tomorrowList.add(String.valueOf(hoursAdd + 1 + ":" + minuteResult));
                                }
                            }
                        }
                    }
                } else {
                    if (minutesAdd < 60) {
                        if (hoursAdd + 1 < close) {
                            minuteResult = new DecimalFormat("00").format(minutesAdd);
                            if (hoursAdd < 24)
                                tomorrowList.add(String.valueOf(hoursAdd + ":" + minuteResult));
                        }
                    } else {
                        if (hoursAdd + 1 < close) {
                            if (hoursAdd + 2 < close) {
                                minuteResult = new DecimalFormat("00").format(minutesAdd - 60);
                                if (hoursAdd < 23)
                                    tomorrowList.add(String.valueOf(hoursAdd + 1 + ":" + minuteResult));
                            }
                        }
                    }
                }
            }
        }

        if (close <= 24)
            tomorrowList.add(String.valueOf(close - 1 + ":" + new DecimalFormat("00").format(0)));
        else tomorrowList.add("24:00");

        monthLoopView.setArrayList((ArrayList) tomorrowList);
        monthLoopView.setInitPosition(monthPos);
    }

    /**
     * Init day item
     */
    private void initPersonPickerViews() {

        dayList = new ArrayList<String>();
        for (int c = 1; c <= 20; c++) {
            dayList.add(c + " person");
        }

        dayLoopView.setArrayList((ArrayList) dayList);
        dayLoopView.setInitPosition(dayPos);
    }

    /**
     * Show date picker popWindow
     *
     * @param activity
     */
    public void showPopWin(Activity activity) {

        if (null != activity) {

            TranslateAnimation trans = new TranslateAnimation(
                    Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF,
                    0, Animation.RELATIVE_TO_SELF, 1,
                    Animation.RELATIVE_TO_SELF, 0);

            showAtLocation(activity.getWindow().getDecorView(), Gravity.BOTTOM,
                    0, 0);
            trans.setDuration(400);
            trans.setInterpolator(new AccelerateDecelerateInterpolator());

            pickerContainerV.startAnimation(trans);
        }
    }

    /**
     * Dismiss date picker popWindow
     */
    public void dismissPopWin() {

        TranslateAnimation trans = new TranslateAnimation(
                Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, 0,
                Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, 1);

        trans.setDuration(400);
        trans.setInterpolator(new AccelerateInterpolator());
        trans.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                dismiss();
            }
        });

        pickerContainerV.startAnimation(trans);
    }

    @Override
    public void onClick(View v) {

        if (v == contentView || v == cancelBtn) {

            dismissPopWin();
        } else if (v == confirmBtn) {

            if (null != mListener) {
                if (isTakeAway)
                    mListener.onDatePickCompleted(yearList.get(yearLoopView.getSelectedItem()), monthList.get(monthLoopView.getSelectedItem()), "0 person");
                else
                    mListener.onDatePickCompleted(yearList.get(yearLoopView.getSelectedItem()), monthList.get(monthLoopView.getSelectedItem()), dayList.get(dayLoopView.getSelectedItem()));
            }

            dismissPopWin();
        }
    }

    public interface OnDatePickedListener {

        void onDatePickCompleted(String day, String minute, String personAmount);
    }
}
