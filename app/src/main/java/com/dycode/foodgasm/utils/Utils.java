package com.dycode.foodgasm.utils;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import com.afollestad.materialdialogs.MaterialDialog;
import com.dycode.foodgasm.R;
import com.dycode.foodgasm.model.AccessToken;
import com.dycode.foodgasm.model.Deals;
import com.dycode.foodgasm.model.ErrorMessage;
import com.dycode.foodgasm.model.Inbox;
import com.dycode.foodgasm.model.Register;
import com.dycode.foodgasm.model.Restaurant;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import retrofit.RetrofitError;
import retrofit.mime.TypedByteArray;

/**
 * Created by asus pc on 20/11/2015.
 */
public class Utils {

    public static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;

    private static SharedPreferences getSharedPreference(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static void logoutApps(Context ctx) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(ctx);
        sharedPreferences.edit().clear().commit();
    }

    public static void saveToken(Context context, AccessToken dataToken) {
        AccessToken token = getToken(context);
        String strJson;
        if (token == null) {
            strJson = new Gson().toJson(dataToken);
        } else {
            if (dataToken.getAccessToken() != null) {
                strJson = new Gson().toJson(dataToken);
            } else {
                token.setAccessToken(dataToken.getAccessToken());
                token.setRefreshToken(dataToken.getRefreshToken());

                strJson = new Gson().toJson(token);
            }
        }
        SPManager.saveString(context, Constants.SP.DATA_TOKEN, strJson);
    }

    public static void saveTokenFromFb(Context context, Register register) {
        AccessToken token = getToken(context);
        String strJson;
        if (token == null) {
            strJson = new Gson().toJson(register);
        } else {
            if (register.getAccessToken() != null) {
                strJson = new Gson().toJson(register);
            } else {
                token.setAccessToken(register.getAccessToken());
                token.setRefreshToken(register.getRefreshToken());

                strJson = new Gson().toJson(token);
            }
        }
        SPManager.saveString(context, Constants.SP.DATA_TOKEN, strJson);
    }


    public static AccessToken getToken(Context context) {
        String strJson = SPManager.getString(context, Constants.SP.DATA_TOKEN);
        AccessToken dataToken = new Gson().fromJson(strJson, AccessToken.class);
        return dataToken;
    }

    public static String getLatString(Context context) {
        String latitude = SPManager.getString(context, Constants.Location.LATITUDE);
        return latitude;
    }

    public static String getLngString(Context context) {
        String longitude = SPManager.getString(context, Constants.Location.LONGITUDE);
        return longitude;
    }

    public static String getRestoLatString(Context context) {
        String latitude = SPManager.getString(context, Constants.RestaurantLocation.LATITUDE);
        return latitude;
    }

    public static String getRestoLngString(Context context) {
        String longitude = SPManager.getString(context, Constants.RestaurantLocation.LONGITUDE);
        return longitude;
    }

    public static void dialogError(Context context, String message) {
        if (context != null)
            new MaterialDialog.Builder(context).title("Alert")
                    .content(message)
                    .positiveText("OK")
                    .callback(new MaterialDialog.ButtonCallback() {
                        @Override
                        public void onPositive(MaterialDialog dialog) {
                            super.onPositive(dialog);
                            dialog.dismiss();
                        }
                    }).show();
    }

    public static MaterialDialog buildMaterialProgressDialog(Context context) {
        return new MaterialDialog.Builder(context)
                .content(context.getString(R.string.loading))
                .cancelable(false)
                .progress(true, 0).build();

    }

    public static boolean isOnline(Context context) {
        try {
            ConnectivityManager conMgr = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);

            if (conMgr.getActiveNetworkInfo().getState() == NetworkInfo.State.CONNECTED
                    ) {
                return true;
            }
            return false;
        } catch (Exception e) {
            return true;
        }
    }

    public static String getErrorMessage(RetrofitError error) {

        Gson gson = new GsonBuilder().create();
        ErrorMessage errorMessage = gson.fromJson(new String(((TypedByteArray) error.getResponse().getBody()).getBytes()), ErrorMessage.class);
        String message = errorMessage.getMeta().getMessage();
        return message;
    }

    public static void saveString(Context activity, String key, String value) {
        getSharedPreference(activity).edit().putString(key, value).commit();
    }

    public static String getString(Context activity, String key) {
        return getSharedPreference(activity).getString(key, null);
    }

    public static void saveListResto(Context context, List<Restaurant> list, String prefCons) {
        String strJson = new Gson().toJson(list);
        saveString(context, prefCons, strJson);
    }

    public static List<Restaurant> getListResto(Context context, String prefCons) {
        String strJson = getString(context, prefCons);
        Type listType = new TypeToken<ArrayList<Restaurant>>() {
        }.getType();
        return new Gson().fromJson(strJson, listType);
    }

    public static void saveListDeals(Context context, List<Deals> list, String prefCons) {
        String strJson = new Gson().toJson(list);
        saveString(context, prefCons, strJson);
    }

    public static List<Deals> getListDeals(Context context, String prefCons) {
        String strJson = getString(context, prefCons);
        Type listType = new TypeToken<ArrayList<Deals>>() {
        }.getType();
        return new Gson().fromJson(strJson, listType);
    }

    public static void saveListInbox(Context context, List<Inbox> list, String prefCons) {
        String strJson = new Gson().toJson(list);
        saveString(context, prefCons, strJson);
    }

    public static List<Inbox> getListInbox(Context context, String prefCons) {
        String strJson = getString(context, prefCons);
        Type listType = new TypeToken<ArrayList<Inbox>>() {
        }.getType();
        return new Gson().fromJson(strJson, listType);
    }


    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static Drawable getDrawable(Context context, int resource) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            return context.getResources().getDrawable(resource, null);
        }

        return context.getResources().getDrawable(resource);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static final int getColor(Context context, int id) {
        final int version = Build.VERSION.SDK_INT;
        if (version >= 23) {
            return ContextCompat.getColor(context, id);
        } else {
            return context.getResources().getColor(id);
        }
    }

    public static Intent newInstagramProfileIntent(PackageManager pm, String url) {
        final Intent intent = new Intent(Intent.ACTION_VIEW);
        try {
            if (pm.getPackageInfo("com.instagram.android", 0) != null) {
                if (url.endsWith("/")) {
                    url = url.substring(0, url.length() - 1);
                }
                final String username = url.substring(url.lastIndexOf("/") + 1);
                // http://stackoverflow.com/questions/21505941/intent-to-open-instagram-user-profile-on-android
                intent.setData(Uri.parse("http://instagram.com/_u/" + username));
                intent.setPackage("com.instagram.android");
                return intent;
            }
        } catch (PackageManager.NameNotFoundException ignored) {

        }
        intent.setData(Uri.parse(url));
        return intent;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public static boolean checkPermission(final Context context) {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= android.os.Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
                    alertBuilder.setCancelable(true);
                    alertBuilder.setTitle("Permission necessary");
                    alertBuilder.setMessage("External storage permission is necessary");
                    alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                        }
                    });
                    AlertDialog alert = alertBuilder.create();
                    alert.show();
                } else {
                    ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                }
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }
}
