package com.dycode.foodgasm.utils;

import id.co.veritrans.android.api.VTUtil.VTConfig;

/**
 * Created by Okta Dwi Priatna on 02/10/2015.
 */
public class Constants {

    public static final String GRANT_TYPE = "password";
    public static final String BASIC_USERNAME = "PzHc6RnQOzm0lnCTIWvcGPF8WdptM6DF";
    public static final String BASIC_PASSWORD = "kSM8SBnrHucbAHFae2DKXfKHCmHI8ErU";
    public static final String VERITRANS_CLIENT_KEY = "VT-client-Hjg85JWnsz13L-kf";
    public static final String MAPS_API = "AIzaSyCgKpQ9GkoVgAhWkOueqoXFCYV1w73_uyI";

    public final static String PAYMENT_API = "https://api.veritrans.co.id/v2/token";
    public final static String PAYMENT_API_SANDBOX = "https://api.sandbox.veritrans.co.id/v2/token";

    public static final String URL_CHARGING_DOKU_DAN_CC = "http://crm.doku.com/doku-library-staging/example-payment-mobile/merchant-example.php";
    public static final String DOKU_BALANCE_ENDPOINT = "custsourceoffunds";


    public static String getPaymentApiUrl() {
        if (VTConfig.VT_IsProduction) {
            return PAYMENT_API;
        }
        return PAYMENT_API_SANDBOX;
    }

    public class Api {
//        public static final String BASE_URL = "http://foodgasm.cloudapp.net:8000/api/v1";
        //live
        public static final String BASE_URL = "https://api.foodgasm.id/api/v1";
        public static final String BASE_URL2 = "https://api.foodgasm.id/";
//        public static final String BASE_URL = "http://192.168.1.3:8000/api/v1";
        //doku
//        public static final String BASE_URL = "http://192.168.1.195:8000/api/v1";
        //printer
//        public static final String BASE_URL = "http://ditto-api.foodgasm.id/api/v1";
    }

    public class SP {
        public static final String DATA_TOKEN = "data_token";
        public static final String LOGIN = "login";
        public static final String NAME = "name";
        public static final String GCM_REGID = "GCM_REGID";
    }

    public class Location {
        public static final String LONGITUDE = "longitude";
        public static final String LATITUDE = "latitude";
    }

    public class RestaurantLocation {
        public static final String LONGITUDE = "longitude";
        public static final String LATITUDE = "latitude";
    }

    public class Permission {
        public static final int LOCATION = 100;
        public static final int PHONE_CALL = 100;
    }

    public class Uber {
        public static final String BASE_URL = "https://api.uber.com/v1";
        public static final String SERVER_TOKEN = "-jIWnit1gtkUuMotx-8tRF1y8Tym-GlQprzIgS2T";
        public static final String CLIENT_ID = "HrVvoXBlgbyzn6Sf1vg_9dGVL3qaTGpS";
    }

    public class Veritrans {
        public static final String SANDBOX_BASE_URL = "https://api.sandbox.veritrans.co.id/v2/";
        public static final String BASE_URL = "https://api.veritrans.co.id/v2/";
    }

    public class Inbox {
        public static final String STATUS_INBOX = "status_inbox";
    }
}
