package com.dycode.foodgasm.utils;

import android.content.Context;
import android.net.Uri;
import android.os.Environment;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Locale;

/**
 * Created by asus pc on 14/01/2016.
 */
public class FileUtil {
    public static final String IMAGE_DIR = "/FoodGasm/Images";
    public static final String FILE_EXTENSION = "jpg";

    public static File getDownloadDir(Context context) {

        boolean isSDPresent = android.os.Environment.getExternalStorageState().equals(
                android.os.Environment.MEDIA_MOUNTED);
        File parent = isSDPresent ? Environment.getExternalStorageDirectory() : context.getFilesDir();

        File dir = new File(parent, IMAGE_DIR);

        if (!dir.exists())
            dir.mkdirs();
        return dir;
    }

    public static File getFile(Context context, String name) {
        return getFile(context, name, FILE_EXTENSION);
    }

    public static File getFile(Context context, String name, String extension) {

        extension = ".".concat(extension.trim().toLowerCase(Locale.getDefault()));

        File dir = getDownloadDir(context);
        File myPath = new File(dir, name + extension);

        // int modifier = 1;
        // while (myPath.exists()) {
        // myPath = new File(dir, name + "(" + modifier++ + ")" + extension);
        // }
        return myPath;
    }

    public static File getTempFile(Context context) {
        return getFile(context, "temp");
    }

    public static Uri getTempUri(Context context) {
        return Uri.fromFile(getTempFile(context));
    }

    public static void copyFile(File afile, File bfile) {
        InputStream inStream = null;
        OutputStream outStream = null;

        try {

            inStream = new FileInputStream(afile);
            outStream = new FileOutputStream(bfile);

            byte[] buffer = new byte[1024];

            int length;
            while ((length = inStream.read(buffer)) > 0) {
                outStream.write(buffer, 0, length);
            }

            inStream.close();
            outStream.close();

            System.out.println("File successfully copied!");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
