package com.dycode.foodgasm.utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.dycode.foodgasm.R;
import com.dycode.foodgasm.widget.loopview.LoopView;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by asus pc on 23/11/2015.
 */
public class PaymentPicker extends PopupWindow implements View.OnClickListener {

    public TextView cancelBtn;
    public TextView confirmBtn;
    public LoopView yearLoopView;
    public LoopView monthLoopView;
    public LoopView dayLoopView;
    public View pickerContainerV;
    public View contentView;//root view

    private int dayPos = 0;
    private Context mContext;

    List<String> dayList = new ArrayList();
    List<String> dayposition = new ArrayList();
    boolean isTakeAway;

    private OnDatePickedListener mListener;
    private int positionLoop = 0;

    public PaymentPicker(Context cxt, OnDatePickedListener l) {

        this.mContext = cxt;
        this.mListener = l;
        initView();
    }


    private void initView() {

        contentView = LayoutInflater.from(mContext).inflate(R.layout.layout_date_picker, null);
        cancelBtn = (TextView) contentView.findViewById(R.id.tvCancel);
        confirmBtn = (TextView) contentView.findViewById(R.id.tvConfirm);
        yearLoopView = (LoopView) contentView.findViewById(R.id.picker_year);
        monthLoopView = (LoopView) contentView.findViewById(R.id.picker_month);
        dayLoopView = (LoopView) contentView.findViewById(R.id.picker_day);
        pickerContainerV = contentView.findViewById(R.id.container_picker);

        yearLoopView.setVisibility(View.GONE);
        monthLoopView.setVisibility(View.GONE);

        if (isTakeAway) dayLoopView.setVisibility(View.GONE);
        else {
            dayLoopView.setVisibility(View.VISIBLE);
            initPersonPickerViews(); //init person loop view
        }

        cancelBtn.setOnClickListener(this);
        confirmBtn.setOnClickListener(this);
        contentView.setOnClickListener(this);

        setTouchable(true);
        setFocusable(true);
        // setOutsideTouchable(true);
        setBackgroundDrawable(new BitmapDrawable());
        setAnimationStyle(R.style.FadeInPopWin);
        setContentView(contentView);
        setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        setHeight(ViewGroup.LayoutParams.MATCH_PARENT);
    }


    /**
     * Init day item
     */
    private void initPersonPickerViews() {

        dayList = new ArrayList<String>();
//        dayList.add("TBank");
        dayList.add("Doku");
        dayList.add("Creditcard");

        dayLoopView.setArrayList((ArrayList) dayList);
        dayLoopView.setInitPosition(dayPos);
    }

    /**
     * Show date picker popWindow
     *
     * @param activity
     */
    public void showPopWin(Activity activity) {

        if (null != activity) {

            TranslateAnimation trans = new TranslateAnimation(
                    Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF,
                    0, Animation.RELATIVE_TO_SELF, 1,
                    Animation.RELATIVE_TO_SELF, 0);

            showAtLocation(activity.getWindow().getDecorView(), Gravity.BOTTOM,
                    0, 0);
            trans.setDuration(400);
            trans.setInterpolator(new AccelerateDecelerateInterpolator());

            pickerContainerV.startAnimation(trans);
        }
    }

    /**
     * Dismiss date picker popWindow
     */
    public void dismissPopWin() {

        TranslateAnimation trans = new TranslateAnimation(
                Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, 0,
                Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, 1);

        trans.setDuration(400);
        trans.setInterpolator(new AccelerateInterpolator());
        trans.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                dismiss();
            }
        });

        pickerContainerV.startAnimation(trans);
    }

    @Override
    public void onClick(View v) {
        if (v == contentView || v == cancelBtn) {

            dismissPopWin();
        } else if (v == confirmBtn) {
            if (null != mListener) {
                mListener.onDatePickCompleted(dayList.get(dayLoopView.getSelectedItem()));
            }

            dismissPopWin();

        }
    }

    public interface OnDatePickedListener {

        void onDatePickCompleted(String categoryName);
    }
}
