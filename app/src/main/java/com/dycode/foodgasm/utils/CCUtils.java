package com.dycode.foodgasm.utils;

import com.dycode.foodgasm.R;

/**
 * Created by faerulsalamun on 4/4/16.
 */
public class CCUtils {

    public static int getCardLogo(String number) {
        int typeCard = R.mipmap.ic_credit_card_black;

        String digit1 = number.substring(0, 1);

        // Visa
        if (digit1.equals("4")) {
            typeCard = R.mipmap.img_foodgasm_credit_card_visa;
        }

        // Mastercard
        if (number.length() >= 2) {
            String digit2 = number.substring(0, 2);

            if (digit2.compareTo("51") >= 0 && digit2.compareTo("55") <= 0) {
                typeCard = R.mipmap.img_foodgasm_credit_card_mastercard;
            }
        }

        if (digit1.equals("1")) {
            typeCard = R.mipmap.doku_wallet;
        }

        if (digit1.equals("0")) {
            typeCard = R.mipmap.image_logo_cc_ecash;
        }
        return typeCard;
    }

    public static int getCardLogoCc(String number) {
        int typeCard = R.mipmap.ic_credit_card_black;

        String digit1 = number.substring(0, 1);

        // Visa
        if (digit1.equals("4")) {
            typeCard = R.mipmap.img_foodgasm_credit_card_visa;
        }

        // Mastercard
        if (number.length() >= 2) {
            String digit2 = number.substring(0, 2);

            if (digit2.compareTo("51") >= 0 && digit2.compareTo("55") <= 0) {
                typeCard = R.mipmap.img_foodgasm_credit_card_mastercard;
            }
        }

        return typeCard;
    }
    public static int getCardLogo(String number, String provider) {
        int typeCard = R.mipmap.ic_credit_card_black;

        String digit1 = number.substring(0, 1);

        // Visa
        if (digit1.equals("4")) {
            typeCard = R.mipmap.img_foodgasm_credit_card_visa;
        }

        // Mastercard
        if (number.length() >= 2) {
            String digit2 = number.substring(0, 2);

            if (digit2.compareTo("51") >= 0 && digit2.compareTo("55") <= 0) {
                typeCard = R.mipmap.img_foodgasm_credit_card_mastercard;
            }
        }

        if (provider.equals("dokuwallet")){
            if (digit1.equals("1")) {
                typeCard = R.mipmap.doku_wallet;
            }
        }


        return typeCard;
    }
}
