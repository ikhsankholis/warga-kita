package com.dycode.foodgasm.utils;

import com.dycode.foodgasm.model.ApiResponse;

import java.io.InterruptedIOException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import retrofit.RetrofitError;

/**
 * Created by USER on 4/26/2017.
 */

public class NetworkErrorUtil {

    public static String getErrorMessage(RetrofitError retrofitError){

        try {
            ApiResponse apiResponse = (ApiResponse) retrofitError.getBodyAs(ApiResponse.class);
            return apiResponse.getMeta().getMessage();
        }catch (Exception e){
            if(retrofitError.getCause() instanceof SocketTimeoutException || retrofitError.getCause() instanceof InterruptedIOException){
                return "Your request took too long time to process. Please try again.";
            }else if(retrofitError.getCause() instanceof UnknownHostException){
                return "Failed to establish connection.";
            }
            return retrofitError.getMessage();
        }

    }
}
