package com.dycode.foodgasm.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.dycode.foodgasm.R;
import com.dycode.foodgasm.model.ReviewPoints;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by asus pc on 16/02/2016.
 */
public class GridReviewAdapter extends BaseAdapter {
    LayoutInflater mInflater;
    Context mContext;
    List<ReviewPoints> mListData = new ArrayList<ReviewPoints>();

    public GridReviewAdapter(Activity activity, List<ReviewPoints> reviewPointsList) {
        mContext = activity;
        mListData = reviewPointsList;
        mInflater = LayoutInflater.from(mContext);
    }

    @Override
    public int getCount() {
        return mListData.size();
    }

    @Override
    public ReviewPoints getItem(int position) {
        return mListData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.row_review_points, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (!mListData.get(position).getIsActive()) {
            holder.ibReviewItem.setImageDrawable(mContext.getResources().getDrawable(R.drawable.btn_review_problem_normal));
            holder.tvReviewItem.setTextColor(mContext.getResources().getColor(R.color.text_menu));
        }

        holder.tvReviewItem.setText(mListData.get(position).getReviewPoints());
        holder.ibReviewItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                holder.ibReviewItem.setSelected(!holder.ibReviewItem.isSelected());


                if (holder.ibReviewItem.isSelected()) {
                    //Handle selected state change
                    holder.ibReviewItem.setImageDrawable(mContext.getResources().getDrawable(R.drawable.btn_review_problem_pressed));
                    holder.tvReviewItem.setTextColor(mContext.getResources().getColor(R.color.windowBackground));
                    mListData.get(position).setIsActive(true);
                    notifyDataSetChanged();
                } else {
                    //Handle de-select state change
                    holder.ibReviewItem.setImageDrawable(mContext.getResources().getDrawable(R.drawable.btn_review_problem_normal));
                    holder.tvReviewItem.setTextColor(mContext.getResources().getColor(R.color.text_menu));
                    mListData.get(position).setIsActive(false);
                    notifyDataSetChanged();
                }
            }
        });

        return convertView;
    }

    class ViewHolder {
        @BindView(R.id.ibReviewItem)
        ImageButton ibReviewItem;
        @BindView(R.id.tvReviewItem)
        TextView tvReviewItem;

        public ViewHolder(View itemView) {
            ButterKnife.bind(this, itemView);
        }
    }
}
