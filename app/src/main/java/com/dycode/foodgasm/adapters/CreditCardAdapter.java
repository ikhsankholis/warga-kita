package com.dycode.foodgasm.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.dycode.foodgasm.FoodGasmApp;
import com.dycode.foodgasm.R;
import com.dycode.foodgasm.dao.CreditCard;
import com.dycode.foodgasm.dao.CreditCardDao;
import com.dycode.foodgasm.dao.DaoSession;
import com.dycode.foodgasm.utils.CCUtils;

import java.util.List;

import butterknife.ButterKnife;

/**
 * Created by asus pc on 29/01/2016.
 */
public class CreditCardAdapter extends RecyclerView.Adapter<CreditCardAdapter.ViewHolder> {

    Activity activity;
    List<com.dycode.foodgasm.dao.CreditCard> creditCardList;
    FoodGasmApp foodGasmApp;
    DaoSession daoSession;
    private boolean editMode = false;
    StatusChangedListener mListener;
    String selectedId;

    public CreditCardAdapter(Activity activity, List<com.dycode.foodgasm.dao.CreditCard> creditCardList) {
        this.activity = activity;
        this.creditCardList = creditCardList;
        this.mListener = (StatusChangedListener) activity;
        foodGasmApp = (FoodGasmApp) activity.getApplication();
        daoSession = foodGasmApp.getDaoSession();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(activity).inflate(R.layout.row_credit_card, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        String creditCardNumber = creditCardList.get(position).getMasked_card();
        creditCardNumber = creditCardNumber.substring(creditCardNumber.length() - 4, creditCardNumber.length());
        String creditCardDisplay = String.format(activity.getString(R.string.credit_card_display), creditCardNumber);
        holder.tvCardNumber.setText(creditCardDisplay);
        holder.ivCardType.setImageResource(CCUtils.getCardLogo(creditCardList.get(position).getMasked_card().replaceAll("\\s", "")));

        if (creditCardList.get(position).getProvider().equals("dokuwallet")){
            holder.tvCardNumber.setText(creditCardList.get(position).getMasked_card());
        }
        if (creditCardList.get(position).getProvider().equals("ecash")){
            holder.tvCardNumber.setText(creditCardList.get(position).getMasked_card());
        }
//        holder.ivCardType.setCompoundDrawablesWithIntrinsicBounds(0, 0, CCUtils.getCardLogo(etCardNumber.getText().toString().replaceAll("\\s", "")), 0);


        if (editMode) {
            holder.itemView.setOnClickListener(null);
            holder.ivCardSelection.setVisibility(View.VISIBLE);
            holder.ivCardStatus.setVisibility(View.GONE);

            if (creditCardList.get(position).getIsEditSelected())
                holder.ivCardSelection.setImageResource(R.mipmap.ic_circle_fill);
            else holder.ivCardSelection.setImageResource(R.mipmap.ic_circle_outline);

            holder.ivCardSelection.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    holder.ivCardSelection.setImageResource(R.mipmap.ic_circle_fill);

                    if (creditCardList != null) {
                        String id = creditCardList.get(position).get_id();
                        for (int pos = 0; pos < creditCardList.size(); pos++) {
                            creditCardList.get(pos).setIsEditSelected(false);
                        }

                        creditCardList.get(position).setIsEditSelected(true);

                        notifyDataSetChanged();
                        mListener.onStatusChanged();
                        mListener.onSelectedIdChanged(id);
                    }
                }
            });
        } else {
            holder.ivCardSelection.setVisibility(View.GONE);
            holder.ivCardStatus.setVisibility(View.GONE); //Before Visible
            if (creditCardList.get(position).getState())
                holder.ivCardStatus.setVisibility(View.GONE);//Before Visible
            else holder.ivCardStatus.setVisibility(View.GONE);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String id = creditCardList.get(position).get_id();
                    List<CreditCard> creditCardNotSelected = daoSession.getCreditCardDao().queryBuilder().where(CreditCardDao.Properties._id.notEq(id)).list();
                    for (int pos = 0; pos < creditCardNotSelected.size(); pos++) {
                        creditCardNotSelected.get(pos).setState(false);
                        daoSession.getCreditCardDao().insertOrReplace(creditCardNotSelected.get(pos));
                    }
                    List<CreditCard> creditCard = daoSession.getCreditCardDao().queryBuilder().where(CreditCardDao.Properties._id.eq(id)).limit(1).list();
                    creditCard.get(0).setState(true);
                    daoSession.getCreditCardDao().insertOrReplace(creditCard.get(0));

                    mListener.onActiveIdSelected(id);
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return creditCardList.size();
    }

    public void setEditMode(boolean editMode) {
        this.editMode = editMode;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView ivCardType;
        ImageView ivCardStatus;
        TextView tvCardNumber;
        ImageView ivCardSelection;

        public ViewHolder(View itemView) {
            super(itemView);
            ivCardType = ButterKnife.findById(itemView, R.id.ivCardType);
            ivCardStatus = ButterKnife.findById(itemView, R.id.ivCardStatus);
            tvCardNumber = ButterKnife.findById(itemView, R.id.tvCardNumber);
            ivCardSelection = ButterKnife.findById(itemView, R.id.ivCardSelection);
        }
    }

    public interface StatusChangedListener {
        void onStatusChanged();

        void onSelectedIdChanged(String selectedId);

        void onActiveIdSelected(String selectedId);
    }
}
