package com.dycode.foodgasm.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.dycode.foodgasm.R;
import com.dycode.foodgasm.model.GroupOrderHistory;
import com.dycode.foodgasm.utils.FoodgasmDateUtil;
import com.github.curioustechizen.ago.RelativeTimeTextView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by fahmi on 07/01/2016.
 */
public class ExpandableOrderAdapter extends BaseExpandableListAdapter {

    LayoutInflater mInflater;
    Context mContext;
    List<GroupOrderHistory> mListData = new ArrayList<>();

    public ExpandableOrderAdapter(Context mContext, List<GroupOrderHistory> mListData) {
        this.mContext = mContext;
        this.mListData = mListData;
        mInflater = LayoutInflater.from(mContext);
    }

    @Override
    public int getGroupCount() {
        return mListData.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return mListData.get(groupPosition).getOrderHistoryList().size();
    }

    @Override
    public GroupOrderHistory getGroup(int groupPosition) {
        return mListData.get(groupPosition);
    }

    @Override
    public com.dycode.foodgasm.dao.OrderHistory getChild(int groupPosition, int childPosition) {
        return mListData.get(groupPosition).getOrderHistoryList().get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View view, ViewGroup parent) {
        HeaderViewHolder holder;

        if (view == null) {
            view = mInflater.inflate(R.layout.list_header, null);
            holder = new HeaderViewHolder(view);
            view.setTag(holder);
        } else {
            holder = (HeaderViewHolder) view.getTag();
        }
        holder.tvHeader.setText(getGroup(groupPosition).getName());
        holder.tvHeader.setVisibility(View.GONE);
        return view;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.row_order, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final com.dycode.foodgasm.dao.OrderHistory order = getChild(groupPosition, childPosition);

        holder.tvName.setText(order.getRestoName());

        String info;
        if (order.getNumOfPeople() > 0) {
            info = String.format(mContext.getResources().getString(R.string.info_order), FoodgasmDateUtil.getHourAndMinute(order.getVisitDate()), order.getNumOfPeople());
        }else{
            info = FoodgasmDateUtil.getHourAndMinute(order.getVisitDate()) + ", "+ mContext.getString(R.string.take_away);
        }
        holder.tvInfo.setText(info);

//        Picasso.with(mContext).load(order.getRestoPic()).into(holder.ivRestaurant);
        Picasso.with(mContext)
                .load(order.getRestoPic())
                .networkPolicy(NetworkPolicy.OFFLINE)
                .into(holder.ivRestaurant, new Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError() {
                        //Try again online if cache failed
                        Picasso.with(mContext)
                                .load(order.getRestoPic())
                                .error(R.mipmap.img_placeholder_image_load_error)
                                .into(holder.ivRestaurant, new Callback() {
                                    @Override
                                    public void onSuccess() {

                                    }

                                    @Override
                                    public void onError() {
                                        Log.v("Picasso", "Could not fetch image");
                                    }
                                });
                    }
                });
        holder.tvTime.setReferenceTime(order.getBookingDate().getTime());
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    class ViewHolder {
        @BindView(R.id.tvName)
        TextView tvName;
        @BindView(R.id.tvInfo)
        TextView tvInfo;
        @BindView(R.id.tvTime)
        RelativeTimeTextView tvTime;
        @BindView(R.id.ivRestaurant)
        ImageView ivRestaurant;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    class HeaderViewHolder {
        @BindView(R.id.tvHeader)
        TextView tvHeader;

        public HeaderViewHolder(View view) {
            ButterKnife.bind(this, view);
        }

    }
}
