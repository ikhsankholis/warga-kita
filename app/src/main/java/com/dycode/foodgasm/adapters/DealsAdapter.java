package com.dycode.foodgasm.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.dycode.foodgasm.R;
import com.dycode.foodgasm.activity.DealsDetailAct;
import com.dycode.foodgasm.model.Deals;
import com.dycode.foodgasm.model.Promo;
import com.dycode.foodgasm.model.Restaurant;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by asus pc on 04/12/2015.
 */
public class DealsAdapter extends BaseAdapter {

    LayoutInflater mInflater;
    Context mContext;
    List<Deals> mListData = new ArrayList<Deals>();
    String dealsId;

    public DealsAdapter(Context context, List<Deals> dealsList) {
        mContext = context;
        mListData = dealsList;
        mInflater = LayoutInflater.from(mContext);
    }

    @Override
    public int getCount() {
        return mListData.size();
    }

    @Override
    public Deals getItem(int position) {
        return mListData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.row_deals, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Deals deals = getItem(position);
        Picasso.with(mContext)
                .load(deals.getPic())
//                .error(R.mipmap.ic_not_found_restaurant_image)
                .into(holder.ivPromo);
        dealsId = deals.getId();
        holder.ivPromo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, DealsDetailAct.class);
                intent.putExtra("dealsId", dealsId);
                mContext.startActivity(intent);
                ((Activity) mContext).overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
            }
        });

        return convertView;
    }

    class ViewHolder {
        @BindView(R.id.ivPromo)
        ImageView ivPromo;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
