package com.dycode.foodgasm.adapters;

import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dycode.foodgasm.R;
import com.dycode.foodgasm.activity.DetailRestaurantAct;
import com.dycode.foodgasm.model.Restaurant;
import com.dycode.foodgasm.widget.rounded.RoundedImageView;
import com.eftimoff.viewpagertransformers.DefaultTransformer;
import com.github.florent37.beautifulparallax.ParallaxViewController;
import com.lzy.widget.tab.CircleIndicator;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.ButterKnife;

/**
 * Created by asus pc on 30/12/2015.
 */
public class RestaurantWithHeaderAdapter extends HeaderFooterRecyclerViewAdapter {

    Context context;
    List<Restaurant> restaurantList;
    ParallaxViewController parallaxViewController = new ParallaxViewController();
    int footerCount = 1;
    int headerCount = 0;

    public RestaurantWithHeaderAdapter(Context context, List<Restaurant> restaurantList) {
        this.context = context;
        this.restaurantList = restaurantList;

    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        parallaxViewController.registerImageParallax(recyclerView);
    }

    @Override
    protected int getHeaderItemCount() {
        return headerCount;
    }

    @Override
    protected int getFooterItemCount() {
        return footerCount;
    }

    @Override
    protected int getContentItemCount() {
        return restaurantList.size();
    }

    @Override
    protected RecyclerView.ViewHolder onCreateHeaderItemViewHolder(ViewGroup parent, int headerViewType) {
//        View view = LayoutInflater.from(context).inflate(R.layout.header_restaurant, parent, false);
//        return new HeaderViewHolder(view);
        return null;
    }

    @Override
    protected RecyclerView.ViewHolder onCreateFooterItemViewHolder(ViewGroup parent, int footerViewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.view_footer_resto, parent, false);
        return new FooterViewHolder(view);
    }

    @Override
    protected RecyclerView.ViewHolder onCreateContentItemViewHolder(ViewGroup parent, int contentViewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_resto_v2, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        parallaxViewController.imageParallax(viewHolder.ivRestaurant);
        return viewHolder;
    }

    @Override
    protected void onBindHeaderItemViewHolder(RecyclerView.ViewHolder headerViewHolder, int position) {
        final HeaderViewHolder hHolder = (HeaderViewHolder) headerViewHolder;
        hHolder.pagerHeader.setAdapter(new HeaderAdapter());
        hHolder.pagerHeader.setPageTransformer(false, new DefaultTransformer());
        hHolder.ci.setViewPager(hHolder.pagerHeader);

    }

    @Override
    protected void onBindFooterItemViewHolder(RecyclerView.ViewHolder footerViewHolder, int position) {
        FooterViewHolder footerviewHolder = (FooterViewHolder) footerViewHolder;

    }

    @Override
    protected void onBindContentItemViewHolder(RecyclerView.ViewHolder contentViewHolder, final int position) {
        final ViewHolder viewHolder = (ViewHolder) contentViewHolder;
        String distanceFinal;

        if (!TextUtils.isEmpty(restaurantList.get(position).getPic())) {
            Picasso.with(context)
                    .load(restaurantList.get(position).getPic())
//                    .error(R.mipmap.ic_not_found_restaurant_image)

                    .into(viewHolder.ivRestaurant);
            viewHolder.ivRestaurant.setVisibility(View.VISIBLE);
        }
//
//        if (!TextUtils.isEmpty(restaurantList.get(position).getLogo())) {
//            viewHolder.tvRestaurantInitialName.setVisibility(View.GONE);
//            viewHolder.ivLogo.setVisibility(View.VISIBLE);
//            Picasso.with(context).load(restaurantList.get(position).getLogo()).into(viewHolder.ivLogo);
//        } else if (TextUtils.isEmpty(restaurantList.get(position).getPic()) || TextUtils.isEmpty(restaurantList.get(position).getLogo())) {
//            viewHolder.tvRestaurantInitialName.setVisibility(View.VISIBLE);
//            viewHolder.ivLogo.setVisibility(View.GONE);
//        } else {
//            viewHolder.tvRestaurantInitialName.setVisibility(View.GONE);
//            viewHolder.ivLogo.setVisibility(View.VISIBLE);
//            Picasso.with(context).load(restaurantList.get(position).getLogo()).into(viewHolder.ivLogo);
//        }
//
//        final String initial = restaurantList.get(position).getName().length() > 1 ? restaurantList.get(position).getName().substring(0, 1) : "R";
//        viewHolder.tvRestaurantInitialName.setText(initial);
//
//
        String location = String.format(context.getString(R.string.resto_location), restaurantList.get(position).getAddress().getDistrict(), restaurantList.get(position).getAddress().getCity());
        String countLocation = location.length() > 35 ? String.format(context.getString(R.string.resto_location_cut), location.substring(0, 20)) : location;

        viewHolder.tvRestaurantName.setText(restaurantList.get(position).getName());
        viewHolder.tvRestaurantType.setText(restaurantList.get(position).getCategory().getCategoryName() + " | " + countLocation);
        Double distance = restaurantList.get(position).getDistance();
        int countDistance = distance.intValue();
        if (countDistance >= 1) {
            distanceFinal = String.valueOf(countDistance) + " " + "km";
        } else {
            Double roundedDistance = Math.round(100 * distance) / 100.0;
            Double countRoundedDistance = roundedDistance * 1000;
            int countFinalDistance = countRoundedDistance.intValue();

            distanceFinal = String.valueOf(countFinalDistance) + " " + "m";
        }
//        viewHolder.tvLocation.setText(countLocation);
        viewHolder.tvDistance.setText(distanceFinal);
//
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, DetailRestaurantAct.class);
                intent.putExtra(Restaurant.RESTAURANT, restaurantList.get(position));
                //Todo : change initial
                intent.putExtra("restoInitial", "TE");
                ActivityOptions opts = ActivityOptions.makeScaleUpAnimation(viewHolder.itemView, 0, 0,
                        viewHolder.itemView.getWidth(), viewHolder.itemView.getHeight());
                context.startActivity(intent, opts.toBundle());
            }
        });
        viewHolder.itemView.setTag(restaurantList.get(position));
//
//
//        viewHolder.lPay.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(context, TotalPayment.class);
//                intent.putExtra("restoName", restaurantList.get(position).getName());
//                intent.putExtra("restoAddress", restaurantList.get(position).getAddress().getStreet());
//                intent.putExtra("restoCity", restaurantList.get(position).getAddress().getCity());
//                intent.putExtra("restoID", restaurantList.get(position).getId());
//                context.startActivity(intent);
//                ((Activity) context).overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
//            }
//        });

//        viewHolder.lOrder.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                Intent intent = new Intent(context, BookOrderAct.class);
//                intent.putExtra(Restaurant.RESTAURANT, restaurantList.get(position));
//                intent.putExtra("tax", restaurantList.get(position).getTax());
//                intent.putExtra("new", true);
//                intent.putExtra(BookOrderAct.MENU_TYPE, BookOrderAct.FLAG_NOT_PICKED_MENU);
//                context.startActivity(intent);
//                ((Activity) context).overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
//            }
//        });
        ;

    }

    public void showFooter(boolean isShownFooter) {
        if (isShownFooter)
            footerCount = 1;
        else
            footerCount = 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvRestaurantName;
        TextView tvRestaurantType;
        TextView tvDistance;
        ImageView ivRestaurant;

        public ViewHolder(View itemView) {
            super(itemView);
            tvRestaurantName = ButterKnife.findById(itemView, R.id.tvRestaurantName);
            tvRestaurantType = ButterKnife.findById(itemView, R.id.tvRestaurantTypeLocation);
            tvDistance = ButterKnife.findById(itemView, R.id.tvDistance);
            ivRestaurant = ButterKnife.findById(itemView, R.id.ivRestaurant);
        }

    }


    public class HeaderViewHolder extends RecyclerView.ViewHolder {

        LinearLayout l_top_categories;
        ViewPager pagerHeader;
        CircleIndicator ci;

        public HeaderViewHolder(View itemView) {
            super(itemView);
            l_top_categories = ButterKnife.findById(itemView, R.id.l_top_categories);
            pagerHeader = ButterKnife.findById(itemView, R.id.pagerHeader);
            ci = ButterKnife.findById(itemView, R.id.ci);
        }

    }

    public class FooterViewHolder extends RecyclerView.ViewHolder {

        ProgressBar pbLoadMore;

        public FooterViewHolder(View itemView) {
            super(itemView);
            pbLoadMore = ButterKnife.findById(itemView, R.id.pbLoadMore);
        }

    }


    private class HeaderAdapter extends PagerAdapter {
        LayoutInflater inflater;
        public int[] images = new int[]{//
                R.mipmap.img_banner_deals, R.mipmap.img_banner_deals, R.mipmap.img_banner_deals,
                R.mipmap.img_banner_deals, R.mipmap.img_banner_deals};

        @Override
        public int getCount() {
            return images.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((RelativeLayout) object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View itemView = inflater.inflate(R.layout.galery_header, container,
                    false);
            ImageView imgflag = (ImageView) itemView.findViewById(R.id.img_gal);
            imgflag.setImageResource(images[position]);
            container.addView(itemView);

            return itemView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((RelativeLayout) object);

        }

    }

}
