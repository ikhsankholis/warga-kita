package com.dycode.foodgasm.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.doku.sdkocov2.DirectSDK;
import com.doku.sdkocov2.interfaces.iPaymentCallback;
import com.doku.sdkocov2.model.PaymentItems;
import com.dycode.foodgasm.FoodGasmApp;
import com.dycode.foodgasm.R;
import com.dycode.foodgasm.activity.DokuWalletConfirmation;
import com.dycode.foodgasm.activity.DokuWalletLogin;
import com.dycode.foodgasm.activity.EcashPaymentAct;
import com.dycode.foodgasm.activity.OrderDetailAct;
import com.dycode.foodgasm.activity.PaymentAct;
import com.dycode.foodgasm.activity.ReorderAct;
import com.dycode.foodgasm.activity.ResultPaymentAct;
import com.dycode.foodgasm.dao.CreditCardDao;
import com.dycode.foodgasm.dao.DaoSession;
import com.dycode.foodgasm.model.ApiResponse;
import com.dycode.foodgasm.model.DokuResponse;
import com.dycode.foodgasm.model.DokuSample;
import com.dycode.foodgasm.model.Inbox;
import com.dycode.foodgasm.model.Order;
import com.dycode.foodgasm.rest.RestClient;
import com.dycode.foodgasm.utils.AppsUtil;
import com.dycode.foodgasm.utils.Constants;
import com.dycode.foodgasm.utils.NetworkErrorUtil;
import com.dycode.foodgasm.utils.SPManager;
import com.dycode.foodgasm.utils.Utils;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by asus pc on 30/12/2015.
 */
public class InboxAdapter extends RecyclerView.Adapter<InboxAdapter.MyViewHolder> {

    private List<Inbox> inboxList;
    private DokuSample dokuData;
    Context context;
    protected RestClient restClient = RestClient.getInstance();

    //doku
    private DirectSDK directSDK;
    private JSONObject respongetTokenSDK;
    TelephonyManager telephonyManager;
    String invoiceNumber;
    List<com.dycode.foodgasm.dao.CreditCard> creditCardDaoList;
    DaoSession daoSession;
    private String jsonRespon;

    MaterialDialog progressDialog;
    private Inbox inbox;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvName;
        TextView tvInfo;
        TextView tvTime;
        ImageView ivRestaurant;
        RelativeLayout relOrder;

        public MyViewHolder(View view) {
            super(view);
            tvName = (TextView) view.findViewById(R.id.tvName);
            tvInfo = (TextView) view.findViewById(R.id.tvInfo);
            tvTime = (TextView) view.findViewById(R.id.tvTime);
            ivRestaurant = (ImageView) view.findViewById(R.id.ivRestaurant);
            relOrder = (RelativeLayout) view.findViewById(R.id.relOrder);
        }
    }


    public InboxAdapter(Context context, List<Inbox> moviesList) {
        this.inboxList = moviesList;
        this.context = context;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_inbox, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        inbox = inboxList.get(position);
        holder.tvName.setText(inbox.getResto().getNameResto());
        holder.tvName.setText(inbox.getResto().getNameResto());
        holder.tvTime.setText(inbox.getCode().toUpperCase());

        switch (Integer.valueOf(inbox.getOrderStatus())) {
            case 0:
                holder.tvInfo.setText("Waiting For Confirmation");
                holder.tvInfo.setTextColor(Color.parseColor("#0097d5"));
                break;
            case 1:
                holder.tvInfo.setText("accepted".toUpperCase());
                break;
            case 2:
                holder.tvInfo.setText("declined".toUpperCase());
                break;
            case 3:
                holder.tvInfo.setText("canceled".toUpperCase());
                break;
            case 4:
                holder.tvInfo.setText("pending payment".toUpperCase());
                holder.tvInfo.setTextColor(Color.parseColor("#FF8800"));
                break;
            case 5:
                holder.tvInfo.setText("pending payment".toUpperCase());
                holder.tvInfo.setTextColor(Color.parseColor("#FF8800"));
                break;

            default:
                holder.tvInfo.setText("");
        }

        if (!TextUtils.isEmpty(inbox.getResto().getPicResto())) {
            Picasso.with(context)
                    .load(inbox.getResto().getPicResto())
//                    .error(R.mipmap.ic_not_found_restaurant_image)
                    .into(holder.ivRestaurant);
        }

        holder.relOrder.setTag(position);
        holder.relOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int pos = (Integer) v.getTag();
                inbox = inboxList.get(pos);
                Log.d("order position", String.valueOf(pos));

                if (inbox.getOrderStatus().equals("5")) {
                    SPManager.saveString(context, Constants.Inbox.STATUS_INBOX, null);
                    Intent intent = new Intent(context, ReorderAct.class);
                    intent.putExtra("orderId", inbox.getId());
                    intent.putExtra("myObject", new Gson().toJson(inbox));
                    intent.putExtra("position", String.valueOf(position));
                    ((Activity) context).finish();
                    context.startActivity(intent);
                    ((Activity) context).overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                } else if (inbox.getOrderStatus().equals("0")) {
                    SPManager.saveString(context, "orderDetail", "waiting");
                    Intent intent = new Intent(context, OrderDetailAct.class);
                    intent.putExtra(Order.ORDER, inbox.getId());
                    ((Activity) context).startActivityForResult(intent, 0);

                } else {
                    SPManager.saveString(context, Constants.Inbox.STATUS_INBOX, null);
                    if (inbox.getPaymentWith().equals("creditcard")) {
                        Log.d("creditcard", "creditcard");
                        //getDokuSample(position);
                        Intent intent = new Intent(context, PaymentAct.class);
                        intent.putExtra("orderId", inbox.getId());
                        ((Activity) context).finish();
                        context.startActivity(intent);
                        ((Activity) context).overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                    } else if (inbox.getPaymentWith().equals("ecash")) {
                        Intent intent = new Intent(context, EcashPaymentAct.class);
                        intent.putExtra("orderId", inbox.getId());
                        intent.putExtra("accountId", inbox.getMaskedCard());
                        context.startActivity(intent);
                    } else {
                        Log.d("id", inbox.getId());
                        Log.d("dokuwallet", "dokuwallet");

                        if (inbox.getPaymentWith().equals("dokuwallet")) {
                            if (inbox.getPaymentCredential() != null) {
                                Intent intent = new Intent(context, DokuWalletConfirmation.class);
                                intent.putExtra("orderId", inbox.getId());
                                intent.putExtra("accountId", inbox.getMaskedCard());
                                intent.putExtra("walletStatus", "1");
                                context.startActivity(intent);
                            } else {
                                Intent intent = new Intent(context, DokuWalletConfirmation.class);
                                intent.putExtra("walletStatus", "0");
                                intent.putExtra("orderId", inbox.getId());
                                context.startActivity(intent);
                            }
                        } else {
                            if (inbox.getPaymentCredential() != null) {
                                Intent intent = new Intent(context, DokuWalletLogin.class);
                                intent.putExtra("orderId", inbox.getId());
                                intent.putExtra("accountId", inbox.getMaskedCard());
                                intent.putExtra("walletStatus", "1");
                                context.startActivity(intent);
                            } else {
                                Intent intent = new Intent(context, DokuWalletLogin.class);
                                intent.putExtra("walletStatus", "0");
                                intent.putExtra("orderId", inbox.getId());
                                context.startActivity(intent);
                            }
                        }


                    }

                }

            }
        });
    }

    public void getDokuSample(final int pos) {
        restClient = RestClient.getInstance();
        restClient.getApiService().getDokuSampleSandBox(inboxList.get(pos).getId(), Utils.getToken(context).getAccessToken(), new Callback<ApiResponse<DokuSample>>() {
            @Override
            public void success(ApiResponse<DokuSample> categoryApiResponseList, Response response) {
                dokuData = categoryApiResponseList.getData();
                registerDokuToken(pos);
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d("error", error.getMessage());
            }
        });
    }

    public void registerDokuToken(int pos) {
        progressDialog = new MaterialDialog.Builder(context)
                .content("Submitting..")
                .cancelable(true)
                .progress(true, 0).build();

        telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

        PaymentItems cardDetails = null;
        cardDetails = InputCard(pos);

        directSDK = new DirectSDK();
        directSDK.setCart_details(cardDetails);
        directSDK.setPaymentChannel(1);
        directSDK.getResponse(new iPaymentCallback() {
            @Override
            public void onSuccess(final String text) {
                Log.d("cek token DOKU-->", text);
                progressDialog.dismiss();
                jsonRespon = text;
                try {
                    respongetTokenSDK = new JSONObject(text);
                    progressDialog.dismiss();
                    if (respongetTokenSDK.getString("res_response_code").equalsIgnoreCase("0000")) {
                        SPManager.saveString(context, "res_token_id", respongetTokenSDK.getString("res_token_id"));
                        progressDialog.setContent("Silahkan Tunggu");
                        progressDialog.show();
                        restClient.getApiService().postDokuResponseSandbox(inbox.getId(), Utils.getToken(context).getAccessToken(), new DokuResponse(jsonRespon), new Callback<ApiResponse<String>>() {
                            @Override
                            public void success(ApiResponse<String> userApiResponse, Response response) {
                                progressDialog.dismiss();
                                JSONObject json;
                                try {
                                    json = new JSONObject(userApiResponse.getData().toString());
                                    if (json.getString("res_response_code").equalsIgnoreCase("0000") && json != null) {
                                        Intent intent = new Intent(context.getApplicationContext(), ResultPaymentAct.class);
                                        intent.putExtra("data", json.toString());
                                        context.startActivity(intent);
                                        ((Activity) context).finish();
                                        Toast.makeText(context.getApplicationContext(), " PAYMENT SUKSES", Toast.LENGTH_SHORT).show();
                                    } else {
                                        Intent intent = new Intent(context.getApplicationContext(), ResultPaymentAct.class);
                                        intent.putExtra("data", json.toString());
                                        context.startActivity(intent);
                                        ((Activity) context).finish();
                                        Toast.makeText(context.getApplicationContext(), "PAYMENT ERROR", Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void failure(RetrofitError error) {
                                progressDialog.dismiss();
                                new MaterialDialog.Builder(context)
                                        .title(R.string.alert)
                                        .content(NetworkErrorUtil.getErrorMessage(error))
                                        .positiveText(R.string.ok)
                                        .cancelable(false)
                                        .show();
                            }
                        });
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onError(final String text) {
                //showLog(text);
                Log.d("error doku ", text);
            }

            @Override
            public void onException(Exception eSDK) {
                eSDK.printStackTrace();
            }
        }, context.getApplicationContext());
    }

    private PaymentItems InputCard(int pos) {
        daoSession = ((FoodGasmApp) ((Activity) context).getApplication()).getDaoSession();
        creditCardDaoList = daoSession.getCreditCardDao().queryBuilder().orderDesc(CreditCardDao.Properties.State).list();

        String dataWord = AppsUtil.SHA1(dokuData.getReq_amount() + dokuData.getReq_mall_id() + "cCOc42ElK71g" + dokuData.getReq_transaction_id() + 360 + telephonyManager.getDeviceId());
        String customerID = String.valueOf(AppsUtil.nDigitRandomNo(8));
        PaymentItems paymentItems = new PaymentItems();
        paymentItems.setDataAmount(dokuData.getReq_amount());
        paymentItems.setDataBasket(dokuData.getReq_basket());
        paymentItems.setDataCurrency(dokuData.getReq_currency());
        paymentItems.setDataWords(dataWord);
        paymentItems.setDataMerchantChain(dokuData.getReq_chain_merchant());
        paymentItems.setDataSessionID(dokuData.getReq_session_id());
        paymentItems.setDataTransactionID(dokuData.getReq_transaction_id());
        paymentItems.setDataMerchantCode(dokuData.getReq_mall_id());
        paymentItems.setDataImei(telephonyManager.getDeviceId());
        paymentItems.setMobilePhone("");
        paymentItems.setPublicKey("MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAiwz9tMtvtz2Ch2UbvzUr9XX1Olb5nPG7egobwlcyBoblY1aqIUw2LFJeTGtOZ5JMAk7w8AFG36ii6b2lo5K8bORvx3StVcavwT8pD8VMQ2AI9uaEmseS4Gn8AJzPSMV5lw/1lMpTDId2ug0pH5xnAS4StAvrKQ47wyQ1tVE/v7UbApVlX/ok/I24FN7uvKoteEXPGsLrwFRcjseXYYNDZ1OIyqIPc5HftL1PiW6P29Fjn++3aEFtHl+xDyOkBqo95NdHqQAqH9RF3jladh21PsGlQHZeTXSfSUz9/a9ebkbOc41TLvtJt50f5MzwGfoTheBOiGh4xwaX/Y+NHkgSDQIDAQAB");
        paymentItems.isProduction(true);
        paymentItems.setCustomerID(customerID);

        if (inboxList.get(pos).getPaymentCredential() != null && !inboxList.get(pos).getPaymentCredential().isEmpty() && !inboxList.get(pos).getPaymentCredential().equals("null")) {
            if (dokuData.getCustomer_id() != null) {
                Log.d("Hello token", dokuData.getSaved_token_id());
                paymentItems.setTokenPayment(dokuData.getSaved_token_id());
                paymentItems.setCustomerID(dokuData.getCustomer_id());
            }
        }

        return paymentItems;
    }


    @Override
    public int getItemCount() {
        return inboxList.size();
    }


    //Wallet Payment

}
