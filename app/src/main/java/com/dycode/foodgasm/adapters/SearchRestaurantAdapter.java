package com.dycode.foodgasm.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.dycode.foodgasm.FoodGasmApp;
import com.dycode.foodgasm.R;
import com.dycode.foodgasm.dao.DaoSession;
import com.dycode.foodgasm.model.Restaurant;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by asus pc on 15/12/2015.
 */
public class SearchRestaurantAdapter extends BaseAdapter {

    LayoutInflater mInflater;
    Activity mActivity;
    List<Restaurant> mListData = new ArrayList<Restaurant>();

    FoodGasmApp foodGasmApp;
    DaoSession daoSession;

    public SearchRestaurantAdapter(Activity activity, List<Restaurant> restaurantList) {
        mActivity = activity;
        mListData = restaurantList;
        mInflater = LayoutInflater.from(mActivity);

        foodGasmApp = (FoodGasmApp) mActivity.getApplication();
        daoSession = foodGasmApp.getDaoSession();
    }

    @Override
    public int getCount() {
        return mListData.size();
    }

    @Override
    public Restaurant getItem(int position) {
        return mListData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.row_searched_resto, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final Restaurant restaurant = getItem(position);

        holder.tvRestaurantName.setText(restaurant.getName());
        if (restaurant.getAddress() != null)
            holder.tvRestaurantLoc.setText(restaurant.getAddress().getCity());
        else
            holder.tvRestaurantLoc.setText(restaurant.getCity());

        if (restaurant.getIsHistory() == null)
            holder.ivDelete.setVisibility(View.GONE);

        holder.ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                daoSession.getSearchHistoryDao().deleteByKey(restaurant.getName());
                mListData.remove(position);
                notifyDataSetChanged();
            }
        });

//        holder.tvRestaurantName.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(mActivity, DetailRestaurantAct.class);
//                intent.putExtra(Restaurant.RESTAURANT, restaurant);
//                mActivity.startActivity(intent);
//                mActivity.overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
//            }
//        });

        return convertView;
    }

    class ViewHolder {
        @BindView(R.id.tvRestaurantName)
        TextView tvRestaurantName;
        @BindView(R.id.tvRestaurantLoc)
        TextView tvRestaurantLoc;
        @BindView(R.id.ivDelete)
        ImageView ivDelete;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
