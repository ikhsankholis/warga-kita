package com.dycode.foodgasm.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dycode.foodgasm.R;
import com.dycode.foodgasm.utils.CCUtils;

import java.util.List;

/**
 * Created by asus pc on 30/12/2015.
 */
public class DialogDokuAdapter extends RecyclerView.Adapter<DialogDokuAdapter.MyViewHolder> {

    private List<com.dycode.foodgasm.dao.CreditCard> inboxList;
    Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvName;
        ImageView image_logo_cc_doku;
        RelativeLayout r_doku;

        public MyViewHolder(View view) {
            super(view);
            tvName = (TextView) view.findViewById(R.id.tvCardNumberDoku);
            image_logo_cc_doku = (ImageView) view.findViewById(R.id.image_logo_cc_doku);
            r_doku = (RelativeLayout) view.findViewById(R.id.r_doku);
        }
    }


    public DialogDokuAdapter(Context context, List<com.dycode.foodgasm.dao.CreditCard> moviesList) {
        this.inboxList = moviesList;
        this.context = context;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.payment_pick_item, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final com.dycode.foodgasm.dao.CreditCard creditCard = inboxList.get(position);
        holder.image_logo_cc_doku.setImageResource(CCUtils.getCardLogo(inboxList.get(position).getMasked_card().replaceAll("\\s", "")));

        Log.d("inboxList.size()", String.valueOf(inboxList.size()));
        String creditCardNumber = inboxList.get(position).getMasked_card();
        creditCardNumber = creditCardNumber.substring(creditCardNumber.length() - 4, creditCardNumber.length());

        holder.tvName.setText(creditCardNumber);
        if (inboxList.get(position).getProvider().equals("dokuwallet")) {
            holder.tvName.setText(inboxList.get(position).getMasked_card());
        }

        if (inboxList.get(position).getProvider().equals("ecash")) {
            holder.tvName.setText(inboxList.get(position).getMasked_card());
        }
    }

    @Override
    public int getItemCount() {
        return inboxList.size();
    }
}
