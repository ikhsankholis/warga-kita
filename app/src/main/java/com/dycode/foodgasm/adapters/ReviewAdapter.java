package com.dycode.foodgasm.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dycode.foodgasm.R;

import com.dycode.foodgasm.model.Comment;
import com.dycode.foodgasm.model.Menu;
import com.dycode.foodgasm.model.Restaurant;
import com.dycode.foodgasm.utils.FoodgasmDateUtil;
import com.dycode.foodgasm.widget.rounded.RoundedImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;

/**
 * Created by asus pc on 12/10/2015.
 */
public class ReviewAdapter extends RecyclerView.Adapter<ReviewAdapter.ViewHolder> {

    Activity activity;
    Context context;
    List<Comment> mListData = new ArrayList<>();

    public ReviewAdapter(Activity activity, Context context, List<Comment> commentList) {
        this.activity = activity;
        this.context = context;
        mListData = commentList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_review, null);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        Comment comment = mListData.get(position);

        String name = comment.getUser().getFullname() == null ? "" : comment.getUser().getFullname();
        holder.tvName.setText(name);
        if (!TextUtils.isEmpty(comment.getUser().getProfilePicture()))
            Picasso.with(context).load(comment.getUser().getProfilePicture()).fit().into(holder.ivProfile);
        else
            holder.ivProfile.setImageResource(R.mipmap.ic_account_circle_black_48dp);

        holder.tvDesc.setText(comment.getComment());
        holder.tvReviewTime.setText(FoodgasmDateUtil.getPassedTimeFromStamp(comment.getTu()));
        holder.tvReviewRate.setText(String.valueOf(comment.getRating()));

        if (comment.getRating() <= 3) holder.ivReviewRateBackground.setBackground(activity.getResources().getDrawable(R.drawable.bg_rounded_bad_rating));

    }

    @Override
    public int getItemCount() {
        return mListData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvName;
        TextView tvDesc;
        TextView tvReviewTime;
        TextView tvReviewRate;
        ImageView ivReviewRateBackground;
        RoundedImageView ivProfile;
        RelativeLayout relReview;

        public ViewHolder(View itemView) {
            super(itemView);
            tvName = ButterKnife.findById(itemView, R.id.tvName);
            tvDesc = ButterKnife.findById(itemView, R.id.tvDesc);
            tvReviewTime = ButterKnife.findById(itemView, R.id.tvReviewTime);
            tvReviewRate = ButterKnife.findById(itemView, R.id.tvReviewRate);
            ivReviewRateBackground = ButterKnife.findById(itemView, R.id.ivReviewRateBackground);
            ivProfile = ButterKnife.findById(itemView, R.id.ivProfile);
            relReview = ButterKnife.findById(itemView, R.id.relReview);
        }
    }
}
