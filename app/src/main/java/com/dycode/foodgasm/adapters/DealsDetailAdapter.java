package com.dycode.foodgasm.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dycode.foodgasm.R;
import com.dycode.foodgasm.activity.DetailRestaurantAct;
import com.dycode.foodgasm.model.Restaurant;
import com.dycode.foodgasm.widget.rounded.RoundedImageView;
import com.github.florent37.beautifulparallax.ParallaxViewController;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.ButterKnife;

/**
 * Created by asus pc on 23/01/2016.
 */
public class DealsDetailAdapter extends HeaderFooterRecyclerViewAdapter {

    Activity activity;
    List<Restaurant> restaurantList;
    ParallaxViewController parallaxViewController = new ParallaxViewController();
    int footerCount = 1;

    public DealsDetailAdapter(Activity activity, List<Restaurant> restaurantList) {
        this.activity = activity;
        this.restaurantList = restaurantList;

    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        parallaxViewController.registerImageParallax(recyclerView);
    }

    @Override
    protected int getHeaderItemCount() {
        return 0;
    }

    @Override
    protected int getFooterItemCount() {
        return footerCount;
    }

    @Override
    protected int getContentItemCount() {
        return restaurantList.size();
    }

    @Override
    protected RecyclerView.ViewHolder onCreateHeaderItemViewHolder(ViewGroup parent, int headerViewType) {
//        View view = LayoutInflater.from(context).inflate(R.layout.view_ads, parent, false);
//        return new HeaderViewHolder(view);
        return null;
    }

    @Override
    protected RecyclerView.ViewHolder onCreateFooterItemViewHolder(ViewGroup parent, int footerViewType) {
        View view = LayoutInflater.from(activity).inflate(R.layout.view_footer_resto, parent, false);
        return new FooterViewHolder(view);
    }

    @Override
    protected RecyclerView.ViewHolder onCreateContentItemViewHolder(ViewGroup parent, int contentViewType) {
        View view = LayoutInflater.from(activity).inflate(R.layout.row_resto, null);
        ViewHolder viewHolder = new ViewHolder(view);
        parallaxViewController.imageParallax(viewHolder.ivRestaurant);
        return viewHolder;
    }

    @Override
    protected void onBindHeaderItemViewHolder(RecyclerView.ViewHolder headerViewHolder, int position) {

    }

    @Override
    protected void onBindFooterItemViewHolder(RecyclerView.ViewHolder footerViewHolder, int position) {
        FooterViewHolder footerviewHolder = (FooterViewHolder) footerViewHolder;

    }

    @Override
    protected void onBindContentItemViewHolder(RecyclerView.ViewHolder contentViewHolder, final int position) {
        final ViewHolder viewHolder = (ViewHolder) contentViewHolder;
        String distanceFinal;

        Picasso.with(activity).load(restaurantList.get(position).getPic()).into(viewHolder.ivRestaurant);

        if (!TextUtils.isEmpty(restaurantList.get(position).getLogo())) {
            viewHolder.tvRestaurantInitialName.setVisibility(View.GONE);
            viewHolder.ivLogo.setVisibility(View.VISIBLE);
            Picasso.with(activity).load(restaurantList.get(position).getLogo()).into(viewHolder.ivLogo);
        }

        final String initial = restaurantList.get(position).getName().length() > 1 ? restaurantList.get(position).getName().substring(0, 1) : "R";
        viewHolder.tvRestaurantInitialName.setText(initial);


        viewHolder.tvRestaurantName.setText(restaurantList.get(position).getName());
        viewHolder.tvRestaurantType.setText(restaurantList.get(position).getCategoryName());
        Double distance = restaurantList.get(position).getDistance();
        int countDistance = distance.intValue();
        if (countDistance >= 1) {
            distanceFinal = String.valueOf(countDistance) + " " + "km";
        } else {
            Double roundedDistance = Math.round(100 * distance) / 100.0;
            Double countRoundedDistance = roundedDistance * 1000;
            int countFinalDistance = countRoundedDistance.intValue();

            distanceFinal = String.valueOf(countFinalDistance) + " " + "m";
        }
        String location = String.format(activity.getString(R.string.resto_location), restaurantList.get(position).getAddress().getDistrict(), restaurantList.get(position).getAddress().getCity());
        String countLocation = location.length() > 35 ? String.format(activity.getString(R.string.resto_location_cut), location.substring(0, 20)) : location;
        viewHolder.tvLocation.setText(countLocation);
        viewHolder.tvDistance.setText(distanceFinal);

        viewHolder.relContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, DetailRestaurantAct.class);
                intent.putExtra(Restaurant.RESTAURANT, restaurantList.get(position));
                intent.putExtra("restoInitial", initial);
                activity.startActivity(intent);
            }
        });
        viewHolder.itemView.setTag(restaurantList.get(position));


    }

    public void showFooter(boolean isShownFooter){
        if(isShownFooter)
            footerCount = 1;
        else
            footerCount = 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        RelativeLayout relContent;
        TextView tvRestaurantName;
        TextView tvRestaurantType;
        TextView tvLocation;
        TextView tvDistance;
        TextView tvRestaurantInitialName;
        ImageView ivRestaurant;
        ImageView ivProfile;
        RoundedImageView ivLogo;

        public ViewHolder(View itemView) {
            super(itemView);
            relContent = ButterKnife.findById(itemView, R.id.relContent);
            tvRestaurantName = ButterKnife.findById(itemView, R.id.tvRestaurantName);
            tvRestaurantType = ButterKnife.findById(itemView, R.id.tvRestaurantType);
            tvLocation = ButterKnife.findById(itemView, R.id.tvLocation);
            tvDistance = ButterKnife.findById(itemView, R.id.tvDistance);
            tvRestaurantInitialName = ButterKnife.findById(itemView, R.id.tvRestaurantInitialName);
            ivRestaurant = ButterKnife.findById(itemView, R.id.ivRestaurant);
            ivProfile = ButterKnife.findById(itemView, R.id.ivProfile);
            ivLogo = ButterKnife.findById(itemView, R.id.ivLogo);
        }

    }

    public class HeaderViewHolder extends RecyclerView.ViewHolder {

        LinearLayout linAds;

        public HeaderViewHolder(View itemView) {
            super(itemView);
            linAds = ButterKnife.findById(itemView, R.id.linAds);
        }

    }

    public class FooterViewHolder extends RecyclerView.ViewHolder {

        ProgressBar pbLoadMore;

        public FooterViewHolder(View itemView) {
            super(itemView);
            pbLoadMore = ButterKnife.findById(itemView, R.id.pbLoadMore);
        }

    }


}
