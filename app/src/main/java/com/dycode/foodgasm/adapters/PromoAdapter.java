package com.dycode.foodgasm.adapters;

import android.app.Activity;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.dycode.foodgasm.R;
import com.dycode.foodgasm.model.ApiResponse;
import com.dycode.foodgasm.model.Promo;
import com.dycode.foodgasm.rest.RestClient;
import com.dycode.foodgasm.utils.FormatterUtil;
import com.dycode.foodgasm.utils.Utils;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by asus pc on 12/01/2016.
 */
public class PromoAdapter extends RecyclerView.Adapter<PromoAdapter.ViewHolder> {

    LayoutInflater mInflater;
    Activity mActivity;
    List<Promo> mListData = new ArrayList<Promo>();
    PromoItemClickListener mListener;
    String promoValue;

    public PromoAdapter(Activity activity, List<Promo> promoList) {
        mActivity = activity;
        mListData = promoList;
        mListener = (PromoItemClickListener) activity;
        mInflater = LayoutInflater.from(mActivity);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public int getItemCount() {
        return mListData.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_promo, parent, false);
        return new ViewHolder(v);
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        Promo promo = mListData.get(position);

        holder.itemView.setVisibility(View.VISIBLE);
        if (mListData.get(position).getDiscount().getKind().equals("nominal")) {
            promoValue = FormatterUtil.convertToIDRFormat(promo.getDiscount().getValue());
            holder.tvPromoValue.setText(promoValue);
        } else {
            promoValue = String.format(mActivity.getString(R.string.promo_value), promo.getDiscount().getValue(), "%");
            holder.tvPromoValue.setText(promoValue);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onPromoClick(mListData.get(position));
            }
        });

    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvPromoValue;

        public ViewHolder(View itemView) {
            super(itemView);
            tvPromoValue = (TextView) itemView.findViewById(R.id.tvPromoValue);
        }
    }

    public interface PromoItemClickListener {
        void onPromoClick(Promo promo);
    }
}
