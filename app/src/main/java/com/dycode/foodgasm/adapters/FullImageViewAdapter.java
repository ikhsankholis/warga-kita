package com.dycode.foodgasm.adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.dycode.foodgasm.R;
import com.dycode.foodgasm.model.Gallery;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import uk.co.senab.photoview.PhotoViewAttacher;

/**
 * Created by asus pc on 09/12/2015.
 */
public class FullImageViewAdapter extends PagerAdapter {

    PhotoViewAttacher mAttacher;
    Context mContext;
    LayoutInflater mInflater;
    List<Gallery> mListData = new ArrayList<Gallery>();

    public FullImageViewAdapter(Context context, List<Gallery> galleryList) {
        mContext = context;
        mListData = galleryList;
        mInflater = LayoutInflater.from(mContext);
    }

    @Override
    public int getCount() {
        return mListData.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        // Declare Variables
        ImageView ivRestaurantPict;

        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = mInflater.inflate(R.layout.viewpager_image_item, container, false);

        // Locate the ImageView in viewpager_item.xml
        ivRestaurantPict = (ImageView) itemView.findViewById(R.id.ivRestaurantPict);

        // Capture position and set to the ImageView

        mAttacher = new PhotoViewAttacher(ivRestaurantPict);
        Picasso.with(mContext).load(mListData.get(position).getPic()).into(ivRestaurantPict);

        // Add viewpager_item.xml to ViewPager
        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        // Remove viewpager_item.xml from ViewPager
        container.removeView((RelativeLayout) object);

    }
}
