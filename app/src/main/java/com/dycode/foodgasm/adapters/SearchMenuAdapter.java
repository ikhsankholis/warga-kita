package com.dycode.foodgasm.adapters;

import android.app.Activity;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dycode.foodgasm.FoodGasmApp;
import com.dycode.foodgasm.R;
import com.dycode.foodgasm.dao.DaoSession;
import com.dycode.foodgasm.utils.FormatterUtil;
import com.dycode.foodgasm.utils.SPManager;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by asus pc on 14/12/2015.
 */
public class SearchMenuAdapter extends BaseAdapter {

    LayoutInflater mInflater;
    Activity mActivity;
    List<com.dycode.foodgasm.dao.Menu> mListData = new ArrayList<com.dycode.foodgasm.dao.Menu>();
    MenuCountListener mListener;

    FoodGasmApp foodGasmApp;
    DaoSession daoSession;

    public SearchMenuAdapter(Activity activity, List<com.dycode.foodgasm.dao.Menu> menuList) {
        mActivity = activity;
        mListener = (MenuCountListener) activity;
        mListData = menuList;
        mInflater = LayoutInflater.from(mActivity);

        foodGasmApp = (FoodGasmApp) activity.getApplication();
        daoSession = foodGasmApp.getDaoSession();
    }

    public void refreshData(List<com.dycode.foodgasm.dao.Menu> menuList) {
        mListData.clear();
        mListData = menuList;
    }

    @Override
    public int getCount() {
        return mListData.size();
    }

    @Override
    public com.dycode.foodgasm.dao.Menu getItem(int position) {
        return mListData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.row_menu_detail, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
            holder.imgCheck.setVisibility(View.INVISIBLE);
        }

        final com.dycode.foodgasm.dao.Menu menu = getItem(position);


        holder.tvFoodName.setText(menu.getName());
        if (!menu.getDescription().isEmpty()) {
            holder.tvDesc.setVisibility(View.VISIBLE);
            holder.tvDesc.setText(menu.getDescription());

        } else {
            holder.tvDesc.setHeight(0);
        }

        holder.itemView.setOnClickListener(v -> {
            if (holder.imgCheck.getVisibility() == View.INVISIBLE) {
                holder.imgCheck.setVisibility(View.VISIBLE);
                holder.linCounter.setVisibility(View.VISIBLE);
                holder.tvCountValue.setText("1");
                holder.itemView.setBackgroundColor(Color.parseColor("#FFCA43"));
                mListener.onChangeSearchedMenuCount(menu, position, 1);

            }
        });

        holder.imgCountPlus.setOnClickListener(view -> {
            int currentAmount = Integer.parseInt(holder.tvCountValue.getText().toString());
            int count = currentAmount + 1;
            holder.tvCountValue.setText(String.valueOf(count));
            holder.itemView.setBackgroundColor(count > 0 ? Color.parseColor("#FFCA43") : Color.parseColor("#FFFFFF"));
            holder.imgCheck.setVisibility(count > 0 ? View.VISIBLE : View.INVISIBLE);
            holder.linCounter.setVisibility(count > 0 ? View.VISIBLE : View.GONE);
            mListener.onChangeSearchedMenuCount(menu, position, count);
        });

        holder.imgCountMinus.setOnClickListener(view -> {
            int currentAmount = Integer.parseInt(holder.tvCountValue.getText().toString());
            if (currentAmount > 0) {
                int count = currentAmount - 1;
                holder.tvCountValue.setText(String.valueOf(count));
                holder.itemView.setBackgroundColor(count > 0 ? Color.parseColor("#FFCA43") : Color.parseColor("#FFFFFF"));
                holder.imgCheck.setVisibility(count > 0 ? View.VISIBLE : View.INVISIBLE);
                holder.linCounter.setVisibility(count > 0 ? View.VISIBLE : View.GONE);
                mListener.onChangeSearchedMenuCount(menu, position, count);
            }

        });
        String taxResto = SPManager.getString(mActivity, "taxResto");
        String commission = SPManager.getString(mActivity, "commission");


        double taxRestoD = Double.parseDouble(taxResto);
        Double d = new Double(taxRestoD);
        int a = menu.getPrice() + (menu.getPrice() * d.intValue() / 100);
        double c = (int) Math.ceil((double) (a + a * Double.parseDouble(commission) / 100));

        holder.tvPrice.setText(FormatterUtil.getGermanCurrencyFormat(c));



        menu.setAmount(menu.getAmount() == null ? 0 : menu.getAmount());
        holder.itemView.setBackgroundColor(menu.getAmount() > 0 ? Color.parseColor("#FFCA43") : Color.parseColor("#FFFFFF"));
        holder.imgCheck.setVisibility(menu.getAmount() > 0 ? View.VISIBLE : View.INVISIBLE);
        holder.linCounter.setVisibility(menu.getAmount() > 0 ? View.VISIBLE : View.GONE);
        holder.tvCountValue.setText(String.valueOf(menu.getAmount()));
//        holder.viewOrange.setVisibility(menu.getAmount() > 0 ? View.VISIBLE : View.INVISIBLE);
        return convertView;
    }

//    private void populateTvCount(final DetailMenuAdapter.ViewHolder holder, final com.dycode.foodgasm.dao.Menu menu, final int position) {
//        holder.linCountContainer.removeAllViews();
//        for (int pos = 0; pos <= 20; pos++) {
//            final int count = pos;
//            TextView tvCount = (TextView) mInflater.inflate(R.layout.text_view_count, holder.linCountContainer, false);
//            holder.linCountContainer.addView(tvCount);
//            if (count == menu.getAmount()) {
//                tvCount.setBackground(ContextCompat.getDrawable(mContext, R.drawable.bg_circle_orange));
//                tvCount.setTextColor(ContextCompat.getColor(mContext, android.R.color.white));
//            }
//            tvCount.setText(String.valueOf(count));
//            tvCount.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    holder.tvCount.setText(String.valueOf(count));
//                    if (count == 0)
//                        holder.tvCount.setVisibility(View.GONE);
//                    mListener.onChangeCount(menu, position, count);
//                    holder.linCountContainer.setVisibility(View.GONE);
//
//                }
//            });
//        }
//    }


    public class ViewHolder {

        @BindView(R.id.rlRowMenu)
        RelativeLayout rlRowMenu;
        @BindView(R.id.tvFoodName)
        TextView tvFoodName;
        @BindView(R.id.tvDesc)
        TextView tvDesc;
        @BindView(R.id.imgCheck)
        ImageView imgCheck;
        @BindView(R.id.tvPrice)
        TextView tvPrice;
        //        @BindView(R.id.viewOrange)
//        View viewOrange;
        @BindView(R.id.linCounter)
        LinearLayout linCounter;
        @BindView(R.id.tvCountValue)
        TextView tvCountValue;
        @BindView(R.id.imgCountPlus)
        ImageView imgCountPlus;
        @BindView(R.id.imgCountMinus)
        ImageView imgCountMinus;
        View itemView;


        public ViewHolder(View itemView) {
            this.itemView = itemView;
            ButterKnife.bind(this, itemView);
        }
    }

    public interface MenuCountListener {
        void onChangeSearchedMenuCount(com.dycode.foodgasm.dao.Menu menu, int posMenu, int newCount);
    }

}
