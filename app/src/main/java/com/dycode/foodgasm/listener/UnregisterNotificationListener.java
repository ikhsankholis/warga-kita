package com.dycode.foodgasm.listener;

/**
 * Created by asus pc on 29/12/2015.
 */
public interface UnregisterNotificationListener {
    public void onSuccess();

    public void onFailed();
}
