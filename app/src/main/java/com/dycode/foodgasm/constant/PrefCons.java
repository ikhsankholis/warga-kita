package com.dycode.foodgasm.constant;

/**
 * Created by asus pc on 21/01/2016.
 */
public class    PrefCons {
    //URL
    public static final String GCM_REGID = "GCM_REGID";

    public static final String PREF_HISTORY = "PREF_HISTORY";
    public static final String PREF_DEALS = "PREF_DEALS";
    public static final String PREF_DEALS_DETAIL = "PREF_DEALS_DETAIL";
}
