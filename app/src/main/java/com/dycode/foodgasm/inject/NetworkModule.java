package com.dycode.foodgasm.inject;

import android.content.Context;

import com.dycode.foodgasm.BuildConfig;
import com.dycode.foodgasm.rest.ApiV2Service;
import com.dycode.foodgasm.rest.ApiVeritransService;
import com.dycode.foodgasm.rest.AppRequestHeaderInterceptor;
import com.dycode.foodgasm.rest.FoodgasmAuthenticator;
import com.dycode.foodgasm.utils.Constants;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created by fahmi on 30/06/2016.
 */
@Module
public class NetworkModule {

    @Provides
    @Singleton
    @SuppressWarnings("unused")
    public Gson providesGson() {
        return new GsonBuilder().create();

    }

    @Provides
    @Singleton
    @SuppressWarnings("unused")
    public AppRequestHeaderInterceptor provideAppRequestHeaderInterceptor(Context context) {
        return new AppRequestHeaderInterceptor(context);
    }

    @Provides
    @Singleton
    @SuppressWarnings("unused")
    public HttpLoggingInterceptor providesHttpLoggingInterceptor(Context context) {
        if(BuildConfig.DEBUG){
            return new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);
        }else {
            return new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.NONE);
        }
    }

    @Provides
    @Singleton
    @SuppressWarnings("unused")
    public okhttp3.OkHttpClient providesOkHttpClient3(
            HttpLoggingInterceptor httpLoggingInterceptor,
            AppRequestHeaderInterceptor appRequestHeaderInterceptor,
            FoodgasmAuthenticator authenticator) {
        return new okhttp3.OkHttpClient.Builder()
                .readTimeout(15, TimeUnit.SECONDS)
                .writeTimeout(15, TimeUnit.SECONDS)
                .connectTimeout(15, TimeUnit.SECONDS)
                .addInterceptor(appRequestHeaderInterceptor)
                .addInterceptor(httpLoggingInterceptor)
                .authenticator(authenticator)
                .build();
    }


    @Provides
    @Singleton
    @SuppressWarnings("unused")
    public Retrofit.Builder providesRetrofitBuilder(
            okhttp3.OkHttpClient okHttpClient,
            Gson gson) {
        return new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(okHttpClient);
    }

    @Provides
    @Singleton
    @SuppressWarnings("unused")
    public Retrofit providesRetrofit(
            Retrofit.Builder builder) {
        return builder.baseUrl(Constants.Api.BASE_URL2)
                .build();
    }


    @Provides
    @Singleton
    @SuppressWarnings("unused")
    public ApiV2Service provideFoodgasmService(
            Retrofit retrofit) {
        return retrofit.create(ApiV2Service.class);
    }

    @Provides
    @Singleton
    @SuppressWarnings("unused")
    public @Named("veritrans") Retrofit providesRetrofitVeritrans(
            Retrofit.Builder builder) {
        return builder.baseUrl(Constants.Veritrans.BASE_URL)
                .build();
    }


    @Provides
    @Singleton
    @SuppressWarnings("unused")
    public ApiVeritransService provideVeritransService(
            @Named("veritrans") Retrofit retrofit) {
        return retrofit.create(ApiVeritransService.class);
    }


}
