package com.dycode.foodgasm.inject;

import android.app.Application;
import android.content.Context;


import com.dycode.foodgasm.feature.confirmationtimer.ConfirmationTimerAct;
import com.dycode.foodgasm.feature.login.LoginAct;
import com.dycode.foodgasm.feature.pay.PayActivity;
import com.dycode.foodgasm.feature.profile.ProfileLoginFragment;
import com.dycode.foodgasm.feature.register.step1.PhoneNumberFragment;
import com.dycode.foodgasm.feature.register.step2.VerificationPhoneFragment;
import com.dycode.foodgasm.feature.register.step3.FormFragment;
import com.dycode.foodgasm.feature.splashscreen.SplashScreenAct;

import org.jetbrains.annotations.NotNull;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by fahmi on 11/01/2016.
 */
@Singleton
@Component(modules = {ApplicationModule.class, NetworkModule.class})
public interface ApplicationComponent {

    Context context();

    Application application();

    void inject(@NotNull SplashScreenAct screenAct);

    void inject(@NotNull LoginAct loginAct);

    void inject(@NotNull ConfirmationTimerAct confirmationTimerAct);

    void inject(@NotNull PayActivity payActivity);

    void inject(@NotNull PhoneNumberFragment phoneNumberFragment);

    void inject(@NotNull VerificationPhoneFragment verificationPhoneFragment);

    void inject(@NotNull FormFragment formFragment);

    void inject(@NotNull ProfileLoginFragment profileLoginFragment);
}
