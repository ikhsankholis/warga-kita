package com.dycode.foodgasm.activity;

import android.os.Bundle;
import android.view.MenuItem;
import android.webkit.WebView;

import com.dycode.foodgasm.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by asus pc on 27/01/2016.
 */
public class TermsAct extends BaseActivity {

    @BindView(R.id.wvTerms)
    WebView wvTerms;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms);
        ButterKnife.bind(this);
        setToolbar(true);

        // Obtain the shared Tracker instance.
        getTracker("Terms & Privacy Policy");
        wvTerms.loadUrl("file:///android_asset/TermsOfUse.html");

    }
    @Override
    protected int getToolbarTitle() {
        return R.string.terms;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
    }
}
