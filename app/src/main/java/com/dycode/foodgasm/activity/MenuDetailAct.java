package com.dycode.foodgasm.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.dycode.foodgasm.FoodGasmApp;
import com.dycode.foodgasm.R;
import com.dycode.foodgasm.adapters.DetailMenuAdapter;
import com.dycode.foodgasm.adapters.SearchMenuAdapter;
import com.dycode.foodgasm.dao.CategoryDao;
import com.dycode.foodgasm.dao.DaoSession;
import com.dycode.foodgasm.dao.MenuDao;
import com.dycode.foodgasm.dao.MenuVersion;
import com.dycode.foodgasm.dao.MenuVersionDao;
import com.dycode.foodgasm.fragment.DetailMenuFragment;
import com.dycode.foodgasm.model.ApiResponseList;
import com.dycode.foodgasm.model.Category;
import com.dycode.foodgasm.model.EventBusAction;
import com.dycode.foodgasm.model.Menu;
import com.dycode.foodgasm.model.Restaurant;
import com.dycode.foodgasm.utils.DensityPixelUtil;
import com.dycode.foodgasm.utils.FormatterUtil;
import com.dycode.foodgasm.utils.SPManager;
import com.dycode.foodgasm.utils.Utils;
import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.Subscribe;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.greenrobot.dao.query.DeleteQuery;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Okta Dwi Priatna on 12/10/2015.
 * Menu Detail flow dari detail restaurant ke menu detail
 */
public class MenuDetailAct extends BaseActivity implements DetailMenuAdapter.MenuCountListener, SearchMenuAdapter.MenuCountListener, DetailMenuAdapter.MenuScrollListener, View.OnClickListener, SmartTabLayout.OnTabClickListener {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.ivSearch)
    ImageView ivSearch;
    @BindView(R.id.tabs)
    SmartTabLayout mTabs;
    @BindView(R.id.viewpager)
    ViewPager mPager;
    @BindView(R.id.ivRestaurantPict)
    ImageView ivRestaurantPict;
    @BindView(R.id.tvRestaurantName)
    TextView tvRestaurantName;
    @BindView(R.id.linProceed)
    LinearLayout linProceed;
    @BindView(R.id.tvPrice)
    TextView tvPrice;
    @BindView(R.id.linSearch)
    LinearLayout linSearch;
    @BindView(R.id.linStartSearch)
    LinearLayout linStartSearch;
    @BindView(R.id.etSearch)
    EditText etSearch;
    @BindView(R.id.tvCancel)
    TextView tvCancel;
    @BindView(R.id.divider)
    View divider;
    @BindView(R.id.listSearchMenu)
    ListView listSearchMenu;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.linContainer)
    LinearLayout linContainer;
    @BindView(R.id.tvProcess)
    TextView tvProcess;


    //dot viewpager
    @BindView(R.id.viewPagerCountDots)
    LinearLayout pager_indicator;
    private int dotsCount;
    private ImageView[] dots;

    SearchMenuAdapter mAdapter;
    Restaurant restaurant;
    int tax;

    ArrayList<com.dycode.foodgasm.dao.Menu> menuList = new ArrayList<>();
    List<com.dycode.foodgasm.dao.Menu> searchMenuList = new ArrayList<>();
    List<com.dycode.foodgasm.dao.Category> categoryDaoList = new ArrayList<>();
    List<Category> categoryList = new ArrayList<>();
    List<Menu> categoryMenuList = new ArrayList<>();
    int update = 0;
    int id_value;
    boolean isUpdateBook = false;
    String dayBook, timeBook, person, openTimeFormatted, closeTimeFormatted, openTimeHoursFormat, closeTimeHoursFormat, openHoursTimeFormat, closeHoursTimeFormat;


    FoodGasmApp foodGasmApp;
    DaoSession daoSession;
    ViewPagerAdapter adapter;
    private String taxResto;
    private double taxRestoD;
    private String getOpentime, getClosetime;
    private String commission;

    private int pagerPos = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_detail);
        ButterKnife.bind(this);

        // Register as a subscriber
        bus.register(this);


        foodGasmApp = (FoodGasmApp) getApplication();
        daoSession = foodGasmApp.getDaoSession();
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        mToolbar.setNavigationIcon(R.mipmap.ic_navigation_arrow_back);
        setSupportActionBar(mToolbar);
        ivSearch.setColorFilter(Utils.getColor(this, R.color.windowBackground));
        restaurant = getIntent().getExtras().getParcelable(Restaurant.RESTAURANT);
        tax = getIntent().getExtras().getInt("tax");
        getOpentime = getIntent().getExtras().getString("getOpentime");
        getClosetime = getIntent().getExtras().getString("getClosetime");
        commission = getIntent().getExtras().getString("commission");

//        Log.d("getOpentime >", " " + getOpentime);
//        Log.d("CloseTime >", " " + getClosetime);
//        Log.d("tax > ", String.valueOf(tax));
        //setUiPageViewController();

        // Obtain the shared Tracker instance.
        getTracker(restaurant.getName() + " Menu List (" + restaurant.getId() + ")");

        Picasso.with(this).load(restaurant.getPic()).into(ivRestaurantPict);
        tvRestaurantName.setText(restaurant.getName());
        for (int pos = 0; pos < menuList.size(); pos++) {
            if (menuList.get(pos).getAmount() > 0) {
                linProceed.setVisibility(View.VISIBLE);
            } else {
                linProceed.setVisibility(View.GONE);
            }

        }
        mAdapter = new SearchMenuAdapter(mActivity, searchMenuList);
        listSearchMenu.setAdapter(mAdapter);
        linSearch.setOnClickListener(this);

        tvCancel.setOnClickListener(this);
        linProceed.setOnClickListener(this);
        getCategoryList(false, null);

//        Log.d("cek sukses time", compareTime(getOpentime, getClosetime));

        if (restaurant.getClosedState() == 1) {
            linProceed.setEnabled(false);
            linProceed.setBackgroundColor(Color.parseColor("#AAAAAA"));
            tvProcess.setText("WE'RE CLOSED");
        }

        mPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                changePageViewIndicator(pagerPos / 4, position / 4);
                pagerPos = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    private void setUiPageViewController() {
        pager_indicator.removeAllViews();
        dotsCount = (int) Math.ceil((double) adapter.getCount() / 4);
        dots = new ImageView[dotsCount];

        for (int i = 0; i < dotsCount; i++) {
            dots[i] = new ImageView(this);
            dots[i].setImageDrawable(getResources().getDrawable(R.drawable.nonselecteditem_dot));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );

            int margin4 = Math.round(DensityPixelUtil.convertDpToPixel(4, this));
            params.setMargins(margin4, 0, margin4, 0);

            pager_indicator.addView(dots[i], params);
        }

        dots[0].setImageDrawable(getResources().getDrawable(R.drawable.selecteditem_dot));

    }

    private void changePageViewIndicator(int prevPos, int pos) {
        if (prevPos != pos)
            dots[prevPos].setImageDrawable(getResources().getDrawable(R.drawable.nonselecteditem_dot));
        dots[pos].setImageDrawable(getResources().getDrawable(R.drawable.selecteditem_dot));
    }


    void getCategoryList(final boolean update, final ArrayList<com.dycode.foodgasm.dao.Menu> menuList) {
        restClient.getApiService().getListMenuCategories(restaurant.getId(), new Callback<ApiResponseList<Category>>() {
            @Override
            public void success(ApiResponseList<Category> categoryApiResponseList, Response response) {
                if (MenuDetailAct.this.isDestroyed()) {
                    return;
                }


                categoryList.clear();
                categoryList.addAll(categoryApiResponseList.getData());
                if (categoryList.size() == 0) {
                    new MaterialDialog.Builder(mActivity)
                            .content(R.string.no_menu)
                            .positiveText(R.string.ok)
                            .cancelable(false)
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(MaterialDialog materialDialog, DialogAction dialogAction) {
                                    materialDialog.dismiss();
                                }
                            })
                            .show();
                } else {
                    List<MenuVersion> menuVersionList = daoSession.getMenuVersionDao().queryBuilder().where(MenuVersionDao.Properties.RestoId.eq(restaurant.getId()), MenuVersionDao.Properties.MenuVersion.eq(restaurant.getMenuVersion())).limit(1).list();
                    if (menuVersionList.size() == 1) {
                        categoryDaoList = daoSession.getCategoryDao().queryBuilder().orderAsc(CategoryDao.Properties.Position).where(CategoryDao.Properties._id.isNotNull(), CategoryDao.Properties.RestoId.eq(restaurant.getId())).list();

                        if (!update) {
                            for (int cPos = 0; cPos < categoryDaoList.size(); cPos++) {
                                adapter.addFrag(DetailMenuFragment.newInstance(restaurant.getId(), categoryDaoList.get(cPos).get_id(), categoryDaoList.get(cPos).getName(), 0, null), categoryDaoList.get(cPos).getName());
                            }
                        } else if (update) {
                            adapter.removeFragment();

                            for (int cPos = 0; cPos < categoryDaoList.size(); cPos++) {
                                adapter.addFrag(DetailMenuFragment.newInstance(restaurant.getId(), categoryDaoList.get(cPos).get_id(), categoryDaoList.get(cPos).getName(), 1, menuList), categoryDaoList.get(cPos).getName());

                                adapter.notifyDataSetChanged();
                                mPager.getAdapter().notifyDataSetChanged();
                            }

                        }


                    } else {
                        final DeleteQuery<com.dycode.foodgasm.dao.Category> categoryDeleteQuery = daoSession.queryBuilder(com.dycode.foodgasm.dao.Category.class)
                                .where(CategoryDao.Properties.RestoId.eq(restaurant.getId()))
                                .buildDelete();
                        categoryDeleteQuery.executeDeleteWithoutDetachingEntities();
                        daoSession.clear();
                        final DeleteQuery<com.dycode.foodgasm.dao.Menu> menuDeleteQuery = daoSession.queryBuilder(com.dycode.foodgasm.dao.Menu.class)
                                .where(MenuDao.Properties.RestoId.eq(restaurant.getId()))
                                .buildDelete();
                        menuDeleteQuery.executeDeleteWithoutDetachingEntities();
                        daoSession.clear();
                        for (int pos = 0; pos < categoryList.size(); pos++) {
                            Category category = categoryList.get(pos);
                            com.dycode.foodgasm.dao.Category categoryDao = new com.dycode.foodgasm.dao.Category();
                            categoryDao.set_id(category.getId());
                            categoryDao.setRestoId(restaurant.getId());
                            categoryDao.setName(category.getName());
                            categoryDao.setPosition(category.getPosition());
                            daoSession.getCategoryDao().insertOrReplace(categoryDao);

                            categoryMenuList.clear();
                            categoryMenuList.addAll(category.getMenus());
                            for (int x = 0; x < categoryMenuList.size(); x++) {
                                Menu menu = categoryMenuList.get(x);
                                com.dycode.foodgasm.dao.Menu menuDao = new com.dycode.foodgasm.dao.Menu();
                                menuDao.set_id(menu.getId());
                                menuDao.setCategoryId(category.getId());
                                menuDao.setCategoryName(category.getName());
                                daoSession.getMenuDao().insertOrReplace(menuDao);

                                List<com.dycode.foodgasm.dao.Menu> menuDaoList = daoSession.getMenuDao().queryBuilder().where(MenuDao.Properties.CategoryId.eq(category.getId()), MenuDao.Properties._id.eq(menu.getId())).list();
                                for (int i = 0; i < menuDaoList.size(); i++) {
                                    menuDao = menuDaoList.get(i);
                                    menuDao.setName(menu.getName());
                                    menuDao.setPrice(menu.getPrice());
                                    menuDao.setDescription(menu.getDescription());
                                    menuDao.setPic(menu.getImage());
                                    menuDao.setDiscount(menu.getDiscountPrice());
                                    menuDao.setAmount(0);
                                    menuDao.setSubtotalPrice(menu.getSubtotalPrice());
                                    menuDao.setRestoId(restaurant.getId());
                                    daoSession.getMenuDao().insertOrReplace(menuDao);
                                }
                            }
                        }
                        com.dycode.foodgasm.dao.MenuVersion menuVersion = new com.dycode.foodgasm.dao.MenuVersion();
                        menuVersion.setRestoId(restaurant.getId());
                        menuVersion.setMenuVersion(restaurant.getMenuVersion());
                        daoSession.getMenuVersionDao().insertOrReplace(menuVersion);

                        categoryDaoList = daoSession.getCategoryDao().queryBuilder().orderAsc(CategoryDao.Properties.Position).where(CategoryDao.Properties._id.isNotNull(), CategoryDao.Properties.RestoId.eq(restaurant.getId())).list();
                        for (int cPos = 0; cPos < categoryDaoList.size(); cPos++) {
                            adapter.addFrag(DetailMenuFragment.newInstance(restaurant.getId(), categoryDaoList.get(cPos).get_id(), categoryDaoList.get(cPos).getName(), 0, null), categoryDaoList.get(cPos).getName());
                        }
                    }

                    setUiPageViewController();

                }
                progressBar.setVisibility(View.GONE);
                linContainer.setVisibility(View.VISIBLE);
                mPager.setAdapter(adapter);
                mPager.setOffscreenPageLimit(categoryList.size() - 1);
                mTabs.setViewPager(mPager);
//                indicator.setViewPager(mPager);

            }

            @Override
            public void failure(RetrofitError error) {
                if (!MenuDetailAct.this.isDestroyed()) {
                    progressBar.setVisibility(View.GONE);
                    linContainer.setVisibility(View.VISIBLE);
                    showAlertDialog("Failed", error.getMessage());
                }

            }
        });
    }

    void getSearchedMenu(String query) {
        searchMenuList.clear();
        searchMenuList.addAll(daoSession.getMenuDao().queryBuilder().where(MenuDao.Properties.Name.like("%" + query + "%"), MenuDao.Properties.RestoId.eq(restaurant.getId())).list());
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
    }

    @Override
    public void onChangeCount(com.dycode.foodgasm.dao.Menu menu, int posMenu, int newCount) {
        linProceed.setVisibility(View.VISIBLE);

        //change database
        daoSession.getMenuDao().insertOrReplace(menu);
        menuList.clear();
        List<com.dycode.foodgasm.dao.Menu> databaseList = daoSession.getMenuDao().queryBuilder().where(MenuDao.Properties.Amount.gt(0), MenuDao.Properties.RestoId.eq(restaurant.getId())).list();
        menuList.addAll(databaseList);
        int totalPrice = 0;
        for (com.dycode.foodgasm.dao.Menu menuloop : menuList) {
            taxResto = SPManager.getString(MenuDetailAct.this, "taxResto");
            taxRestoD = Double.parseDouble(taxResto);
            totalPrice += menuloop.getPrice() * menuloop.getAmount();
            Log.d("total>>>", String.valueOf(totalPrice));
        }
        if (totalPrice == 0)
            linProceed.setVisibility(View.GONE);

        int a = totalPrice + (totalPrice * Integer.valueOf(tax) / 100);
        int d = (int) Math.ceil((double) (a + a * Double.valueOf(commission) / 100));
        int commissionPrice = (int) Math.ceil((double) (d + d * Integer.valueOf(commission) / 100));

        tvPrice.setText(FormatterUtil.getGermanCurrencyFormat(d));


        // Synchronize scroll
        DetailMenuFragment childFragment = (DetailMenuFragment) adapter.getItem(mPager.getCurrentItem());
        childFragment.synchronizeScroll(posMenu, false);

    }

    @Override
    public void onChangeSearchedMenuCount(com.dycode.foodgasm.dao.Menu menu, int posMenu, int newCount) {
        linProceed.setVisibility(View.VISIBLE);
        searchMenuList.get(posMenu).setAmount(newCount);
        daoSession.getMenuDao().insertOrReplace(searchMenuList.get(posMenu));
        menuList.clear();
        List<com.dycode.foodgasm.dao.Menu> databaseList = daoSession.getMenuDao().queryBuilder().where(MenuDao.Properties.Amount.gt(0), MenuDao.Properties.RestoId.eq(restaurant.getId())).list();
        menuList.addAll(databaseList);
        int totalPrice = 0;

        // Refresh data
        getCategoryList(true, menuList);

        for (com.dycode.foodgasm.dao.Menu menuloop : menuList) {
            totalPrice += menuloop.getPrice() * menuloop.getAmount();
        }
        if (totalPrice == 0)
            linProceed.setVisibility(View.GONE);

        int b = (int) Math.ceil((double) (totalPrice + totalPrice * tax / 100));
        int a = totalPrice + (totalPrice * Integer.valueOf(tax) / 100);
        int d = (int) Math.ceil((double) (a + a * Double.valueOf(commission) / 100));

        tvPrice.setText(FormatterUtil.getGermanCurrencyFormat(d));

    }

    @Override
    public void onClickedMenuAmount(com.dycode.foodgasm.dao.Menu menu, int posMenu) {
        // Synchronize scroll
        DetailMenuFragment childFragment = (DetailMenuFragment) adapter.getItem(mPager.getCurrentItem());
        childFragment.synchronizeScroll(posMenu, true);
    }

    @Override
    public void onClick(View v) {
        if (v == linSearch) {
            pager_indicator.setVisibility(View.GONE);
            divider.setVisibility(View.GONE);
            linSearch.setVisibility(View.GONE);
            mTabs.setVisibility(View.GONE);
            linStartSearch.setVisibility(View.VISIBLE);
            mPager.setVisibility(View.GONE);
            listSearchMenu.setVisibility(View.VISIBLE);
            etSearch.requestFocus();
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(etSearch, InputMethodManager.SHOW_IMPLICIT);
            etSearch.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    getSearchedMenu(s.toString());
                }
            });
        } else if (v == tvCancel) {
//            for (int pos = 0; pos < adapter.getCount(); pos++) {
//                ((DetailMenuFragment) adapter.getItem(pos)).getMenus(false);
//            }

            linSearch.setVisibility(View.VISIBLE);
            linStartSearch.setVisibility(View.GONE);
            mTabs.setVisibility(View.VISIBLE);
            divider.setVisibility(View.VISIBLE);
            mPager.setVisibility(View.VISIBLE);
            listSearchMenu.setVisibility(View.GONE);
            pager_indicator.setVisibility(View.VISIBLE);


            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(etSearch.getWindowToken(), 0);
        } else if (v == linProceed) {
            if (restaurant.getClosedState() == 0) {
                Intent intent = new Intent(MenuDetailAct.this, BookOrderAct.class);
                intent.putExtra(BookOrderAct.MENU_TYPE, BookOrderAct.FLAG_PICKED_MENU);
                intent.putExtra(Restaurant.RESTAURANT, restaurant);
                intent.putExtra("commission", commission);
                if (isUpdateBook) {
                    intent.putExtra("id", id_value);
                    intent.putExtra("update", "update");
                    intent.putExtra("day", dayBook);
                    intent.putExtra("personAmount", person);
                    intent.putExtra("minute", timeBook);
                    intent.putExtra("commission", commission);
                }
                intent.putExtra(com.dycode.foodgasm.dao.Menu.MENU, menuList);
                startActivityForResult(intent, 1);
                overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
            } else if (restaurant.getClosedState() == 1) {
                linProceed.setEnabled(false);
            }


        }

    }


    @Override
    public void onTabClicked(int position) {
        for (int i = 0; i < dotsCount; i++) {
            dots[i].setImageDrawable(getResources().getDrawable(R.drawable.selecteditem_dot));
        }

        dots[position].setImageDrawable(getResources().getDrawable(R.drawable.nonselecteditem_dot));
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        public void removeFragment() {
            mFragmentList.clear();
            mFragmentTitleList.clear();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == 1 && data.getExtras() != null) {

            if (data.getExtras().getBoolean("isUpdateBook")) {
                id_value = data.getExtras().getInt("id");
                isUpdateBook = true;
            }
        }
    }


    @Subscribe
    public void onEvent(EventBusAction event) {
        if (event.getName().equals("updateMenu")) {

            getCategoryList(true, event.getMenuArrayList());
            menuList.clear();
            menuList.addAll(event.getMenuArrayList());
            update = 1;
            int totalPrice = 0;

            for (com.dycode.foodgasm.dao.Menu menuloop : event.getMenuArrayList()) {
                totalPrice += menuloop.getPrice() * menuloop.getAmount();
                int b = (int) Math.ceil((double) (totalPrice + totalPrice * tax / 100));
            }
            if (totalPrice == 0)
                linProceed.setVisibility(View.GONE);


            int a = totalPrice + (totalPrice * 5 / 100);
            int d = (int) Math.ceil((double) (a + a * Double.valueOf(commission) / 100));

            Log.d("totalPrice", String.valueOf(d) + "tax >>" + String.valueOf(taxRestoD));
            tvPrice.setText(FormatterUtil.getGermanCurrencyFormat(d));

        } else if (event.getName().equals("updatePerson")) {
            person = event.getPersonAmount();
            dayBook = event.getDay();
            timeBook = event.getMinute();
            String customCurrentTimeTakeAway = String.format(getString(R.string.custom_current_time_take_away), dayBook, timeBook);
            String customCurrentTime = String.format(getString(R.string.custom_current_time), dayBook, timeBook, person);
        }
    }

    @Override
    protected void onDestroy() {
        // Unregister
        bus.unregister(this);
        super.onDestroy();
    }


}
