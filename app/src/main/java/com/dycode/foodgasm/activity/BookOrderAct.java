package com.dycode.foodgasm.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dycode.foodgasm.FoodGasmApp;
import com.dycode.foodgasm.R;
import com.dycode.foodgasm.dao.DaoSession;
import com.dycode.foodgasm.feature.skiplogin.SkipLoginActivity;
import com.dycode.foodgasm.model.EventBusAction;
import com.dycode.foodgasm.model.Menu;
import com.dycode.foodgasm.model.Restaurant;
import com.dycode.foodgasm.utils.OrderTimePicker;
import com.dycode.foodgasm.utils.Utils;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.Subscribe;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by asus pc on 13/10/2015.
 * flow dari home > detail restaurant > request to book
 */
public class BookOrderAct extends BaseActivity implements View.OnClickListener {
    public static final String MENU_TYPE = "MENU_TYPE";
    public static final int FLAG_PICKED_MENU = 0;
    public static final int FLAG_NOT_PICKED_MENU = 1;
    public static final int FLAG_REVIEW_EDIT = 2;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.tvRestaurantName)
    TextView tvRestaurantName;
    @BindView(R.id.ivRestaurant)
    ImageView ivRestaurant;
    @BindView(R.id.linProceed)
    LinearLayout linProceed;
    @BindView(R.id.ivBookTable)
    ImageView ivBookTable;
    @BindView(R.id.tvBookTable)
    TextView tvBookTable;
    @BindView(R.id.ivBookTablePreOrder)
    ImageView ivBookTablePreOrder;
    @BindView(R.id.tvBookTablePreOrder)
    TextView tvBookTablePreOrder;
    @BindView(R.id.ivTakeAway)
    ImageView ivTakeAway;
    @BindView(R.id.tvTakeAway)
    TextView tvTakeAway;
    @BindView(R.id.linOrderDetail)
    LinearLayout linOrderDetail;
    @BindView(R.id.tvTimeNow)
    TextView tvTimeNow;
    @BindView(R.id.tvOrderTime)
    TextView tvOrderTime;
    @BindView(R.id.linBookTable)
    LinearLayout linBookTable;
    @BindView(R.id.imgArrow)
    ImageView imgArrow;
    @BindView(R.id.txtProceed)
    TextView txtProceed;


    Restaurant restaurant;

    int bookType, flag, open, close, openMinutes, closeMinutes, id_value;
    String dayBook, timeBook, person, openTimeFormatted, closeTimeFormatted, openTimeHoursFormat, closeTimeHoursFormat, openHoursTimeFormat, closeHoursTimeFormat;
    ArrayList<com.dycode.foodgasm.dao.Menu> menuList = new ArrayList<>();
    DaoSession daoSession;
    FoodGasmApp foodGasmApp;
    boolean isUpdateBook = false;
    private String commission;
    private String getOpentime, getClosetime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_order);
        ButterKnife.bind(this);

        // Obtain the shared Tracker instance.
        getTracker("Order Table");

        // Register as a subscriber
        bus.register(this);

        mToolbar.setNavigationIcon(R.mipmap.ic_navigation_arrow_back);
        setSupportActionBar(mToolbar);

        foodGasmApp = (FoodGasmApp) getApplication();
        daoSession = foodGasmApp.getDaoSession();

        restaurant = getIntent().getExtras().getParcelable(Restaurant.RESTAURANT);
        menuList = getIntent().getExtras().getParcelableArrayList(Menu.MENU);
        flag = getIntent().getExtras().getInt(MENU_TYPE);
        commission = getIntent().getExtras().getString("commission");
        getOpentime = getIntent().getExtras().getString("getOpentime");
        getClosetime = getIntent().getExtras().getString("getClosetime");
        Picasso.with(this).load(restaurant.getPic()).into(ivRestaurant);
        tvRestaurantName.setText(restaurant.getName());


        if (!restaurant.getSetting().getOrderType().getRequestTabel()) {
            tvBookTable.setTextColor(ContextCompat.getColor(this, R.color.colorGrey));

        }
        if (!restaurant.getSetting().getOrderType().getRequestTabelPreorder()) {
            tvBookTablePreOrder.setTextColor(ContextCompat.getColor(this, R.color.colorGrey));
        }
        if (!restaurant.getSetting().getOrderType().getTakeAway()) {
            tvTakeAway.setTextColor(ContextCompat.getColor(this, R.color.colorGrey));
        }

        if (!restaurant.getSetting().getOrderType().getTakeAway() && !restaurant.getSetting().getOrderType().getRequestTabelPreorder()
                && !restaurant.getSetting().getOrderType().getTakeAway()) {
            linProceed.setBackgroundColor(Color.parseColor("#CCCCCC"));
            linProceed.setEnabled(false);
            txtProceed.setText("Pre-order not available");

        }


        if (flag == FLAG_NOT_PICKED_MENU) {
            if (getIntent().getExtras().getString("update") != null) {
                id_value = getIntent().getExtras().getInt("id");
                commission = getIntent().getExtras().getString("commission");

                updateCheckMenu();
            }
            addListenerOnButton();
        } else if (flag == FLAG_PICKED_MENU) {
            if (getIntent().getExtras().getString("update") != null) {
                id_value = getIntent().getExtras().getInt("id");
                commission = getIntent().getExtras().getString("commission");
                updateCheckMenu();
            }
            linBookTable.setVisibility(View.GONE);
            addListenerOnButtonToReview(false);
        } else if (flag == FLAG_REVIEW_EDIT) {
            id_value = getIntent().getExtras().getInt("id");
            commission = getIntent().getExtras().getString("commission");
            linBookTable.setVisibility(View.GONE);

            if (id_value == 0) {
                ivBookTable.setImageResource(R.mipmap.ic_circle_fill);
                ivBookTablePreOrder.setImageResource(R.mipmap.ic_circle_outline);
                ivTakeAway.setImageResource(R.mipmap.ic_circle_outline);
                bookType = 1;
                ivBookTablePreOrder.setOnClickListener(null);
                ivBookTablePreOrder.setEnabled(false);
                tvBookTablePreOrder.setOnClickListener(null);
                tvBookTablePreOrder.setEnabled(false);
                ivTakeAway.setOnClickListener(null);
                ivTakeAway.setEnabled(false);
                tvTakeAway.setOnClickListener(null);
                tvTakeAway.setEnabled(false);
                linProceed.setOnClickListener(v -> {
                    Intent intent = new Intent(BookOrderAct.this, ReviewOrderAct.class);
                    intent.putExtra("id", 0);
                    intent.putExtra(ReviewOrderAct.FLAG, ReviewOrderAct.FLAG_BOOK_TABLE);
                    intent.putExtra(Restaurant.RESTAURANT, restaurant);
                    intent.putExtra("day", dayBook);
                    intent.putExtra("personAmount", person);
                    intent.putExtra("minute", timeBook);
                    intent.putExtra("commission", commission);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);

                });
            } else if (id_value == 1) {
                ivBookTable.setImageResource(R.mipmap.ic_circle_outline);
                ivBookTablePreOrder.setImageResource(R.mipmap.ic_circle_fill);
                ivTakeAway.setImageResource(R.mipmap.ic_circle_outline);
                bookType = 2;
                ivBookTable.setOnClickListener(null);
                ivBookTable.setEnabled(false);
                tvBookTable.setOnClickListener(null);
                tvBookTable.setEnabled(false);
                ivTakeAway.setOnClickListener(null);
                ivTakeAway.setEnabled(false);
                tvTakeAway.setOnClickListener(null);
                tvTakeAway.setEnabled(false);
                addListenerOnButtonToReview(true);
            } else {
                ivBookTable.setImageResource(R.mipmap.ic_circle_outline);
                ivBookTablePreOrder.setImageResource(R.mipmap.ic_circle_outline);
                ivTakeAway.setImageResource(R.mipmap.ic_circle_fill);
                bookType = 3;
                ivBookTable.setOnClickListener(null);
                ivBookTable.setEnabled(false);
                tvBookTable.setOnClickListener(null);
                tvBookTable.setEnabled(false);
                ivBookTablePreOrder.setOnClickListener(null);
                ivBookTablePreOrder.setEnabled(false);
                tvBookTablePreOrder.setOnClickListener(null);
                tvBookTablePreOrder.setEnabled(false);
                addListenerOnButtonToReview(true);
            }
        }

        getCustomTime();
        ivBookTable.setOnClickListener(this);
        tvBookTable.setOnClickListener(this);
        ivBookTablePreOrder.setOnClickListener(this);
        tvBookTablePreOrder.setOnClickListener(this);
        ivTakeAway.setOnClickListener(this);
        tvTakeAway.setOnClickListener(this);
        linOrderDetail.setOnClickListener(this);

    }

    private void updateCheckMenu() {
        if (id_value == 0) {
            ivBookTable.setImageResource(R.mipmap.ic_circle_fill);
            ivBookTablePreOrder.setImageResource(R.mipmap.ic_circle_outline);
            ivTakeAway.setImageResource(R.mipmap.ic_circle_outline);
            bookType = 1;
        } else if (id_value == 1) {
            ivBookTable.setImageResource(R.mipmap.ic_circle_outline);
            ivBookTablePreOrder.setImageResource(R.mipmap.ic_circle_fill);
            ivTakeAway.setImageResource(R.mipmap.ic_circle_outline);
            bookType = 2;
        } else {
            ivBookTable.setImageResource(R.mipmap.ic_circle_outline);
            ivBookTablePreOrder.setImageResource(R.mipmap.ic_circle_outline);
            ivTakeAway.setImageResource(R.mipmap.ic_circle_fill);
            bookType = 3;
        }
    }

    public void getCustomTime() {

        if (flag == FLAG_REVIEW_EDIT || getIntent().getExtras().getString("update") != null) {
            person = getIntent().getExtras().getString("personAmount");
            dayBook = getIntent().getExtras().getString("day");
            timeBook = getIntent().getExtras().getString("minute");
            commission = getIntent().getExtras().getString("commission");

            String customCurrentTimeTakeAway = String.format(getString(R.string.custom_current_time_take_away), dayBook, timeBook);
            String customCurrentTime = String.format(getString(R.string.custom_current_time), dayBook, timeBook, person);
            if (id_value == 2) tvTimeNow.setText(customCurrentTimeTakeAway);
            else tvTimeNow.setText(customCurrentTime);
            tvTimeNow.setVisibility(View.VISIBLE);
            tvOrderTime.setVisibility(View.GONE);

        } else {
            Calendar c = Calendar.getInstance();
            Date today = c.getTime();
            c.add(Calendar.DAY_OF_YEAR, 1);
            Date tomorrow = c.getTime();
            DateFormat dateFormat = new SimpleDateFormat("MMM d, yyyy", java.util.Locale.getDefault());
            int minutesCount = (c.get(Calendar.MINUTE) / 15) + 1;
            int minutes = (minutesCount * 15);
            int hoursLeft = c.get(Calendar.HOUR_OF_DAY);
            if (hoursLeft < open) {
                hoursLeft = open;
                minutes = 0;
            } else if (hoursLeft >= open) {
                hoursLeft += 1;
                minutes = 0;
                if (hoursLeft >= close) {
                    hoursLeft = open;
                    minutes = 0;
                }
            }

            SimpleDateFormat dateFormats = new SimpleDateFormat("hh:mm a", Locale.getDefault());
            SimpleDateFormat dateFormat2 = new SimpleDateFormat("HH:mm", Locale.getDefault());

            try {
                Date dateOpen = dateFormats.parse(restaurant.getOpen().getOpentime());
                Date dateClose = dateFormats.parse(restaurant.getOpen().getClosetime());

                openTimeFormatted = dateFormat2.format(dateOpen);
                closeTimeFormatted = dateFormat2.format(dateClose);

                openTimeHoursFormat = dateFormats.format(dateOpen);
                closeTimeHoursFormat = dateFormats.format(dateClose);
                String customOpenTime = openTimeFormatted.substring(0, 2);
                String customCloseTime = closeTimeFormatted.substring(0, 2);
                String openTimeMinutes = openTimeFormatted.substring(3, 5);
                String closeTimeMinutes = closeTimeFormatted.substring(3, 5);
                open = Integer.parseInt(customOpenTime);
                close = Integer.parseInt(customCloseTime);
                openMinutes = Integer.parseInt(openTimeMinutes);
                closeMinutes = Integer.parseInt(closeTimeMinutes);

                openHoursTimeFormat = openTimeHoursFormat.substring(6, 8);
                closeHoursTimeFormat = closeTimeHoursFormat.substring(6, 8);

                if (close == 0)
                    close += 24;
                else if (openHoursTimeFormat.equals("am") && closeHoursTimeFormat.equals("am") && open > close)
                    close += 24;

                int hoursNow = close - c.get(Calendar.HOUR_OF_DAY);
                if (hoursNow - 1 == 0 || hoursNow - 1 < 0) {
                    dayBook = dateFormat.format(tomorrow);
                } else {
                    if (hoursNow > 0) {
                        dayBook = dateFormat.format(today);
                    } else {
                        dayBook = dateFormat.format(tomorrow);
                    }
                }
                String currentCustomTime = (String.valueOf(hoursLeft + ":" + new DecimalFormat("00").format(minutes)));
                if (open > hoursLeft)
                    timeBook = String.valueOf(open + ":" + new DecimalFormat("00").format(openMinutes));
                else if (close < hoursLeft)
                    timeBook = String.valueOf(open + ":" + new DecimalFormat("00").format(openMinutes));
                else timeBook = currentCustomTime;

                person = "1 person";
                tvTimeNow.setVisibility(View.VISIBLE);
                tvOrderTime.setVisibility(View.GONE);
                String customCurrentTime = String.format(getString(R.string.custom_current_time), dayBook, timeBook, person);
                String customCurrentTimeTakeAway = String.format(getString(R.string.custom_current_time_take_away), dayBook, timeBook);
                if (bookType == 3) {
//                tvTimeNow.setText(customCurrentTimeTakeAway);
                    tvTimeNow.setText("Order Now");
                    imgArrow.setVisibility(View.GONE);
                    linOrderDetail.setEnabled(false);
                    linOrderDetail.setClickable(false);
                } else {
                    tvTimeNow.setText(customCurrentTime);
                    imgArrow.setVisibility(View.VISIBLE);
                    linOrderDetail.setClickable(true);
                    linOrderDetail.setEnabled(true);
                }
            } catch (ParseException ignored) {
            }


        }

    }

    public void addListenerOnButton() {

        linProceed.setOnClickListener(v -> {
            if (bookType == 1) {
                if (Utils.getToken(mActivity) == null) {
                    SkipLoginActivity.Companion.start(mActivity, 0, ReviewOrderAct.FLAG_BOOK_TABLE, restaurant, menuList, person, dayBook, timeBook, bookType, commission, getClosetime, getOpentime);
                } else {
                    Intent intent = new Intent(BookOrderAct.this, ReviewOrderAct.class);
                    intent.putExtra("id", 0);
                    intent.putExtra(ReviewOrderAct.FLAG, ReviewOrderAct.FLAG_BOOK_TABLE);
                    intent.putExtra(Restaurant.RESTAURANT, restaurant);
                    intent.putExtra("day", dayBook);
                    intent.putExtra("personAmount", person);
                    intent.putExtra("minute", timeBook);
                    intent.putExtra("commission", commission);
                    intent.putExtra("getClosetime", getClosetime);
                    intent.putExtra("getOpentime", getOpentime);
                    startActivity(intent);
                }
                overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);

            } else if (bookType == 2) {
                Intent intent = new Intent(BookOrderAct.this, MenuDetailOrderAct.class);
                intent.putExtra("id", 1);
                intent.putExtra(MenuDetailOrderAct.FLAG, MenuDetailOrderAct.FLAG_FIRST_TIME_ORDER);
                intent.putExtra(Restaurant.RESTAURANT, restaurant);
                intent.putExtra("day", dayBook);
                intent.putExtra("personAmount", person);
                intent.putExtra("minute", timeBook);
                intent.putExtra("commission", commission);
                intent.putExtra("getClosetime", getClosetime);
                intent.putExtra("getOpentime", getOpentime);

                startActivity(intent);

                overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);

            } else if (bookType == 3) {
                Intent intent = new Intent(BookOrderAct.this, MenuDetailOrderAct.class);
                intent.putExtra("id", 2);
                intent.putExtra(MenuDetailOrderAct.FLAG, MenuDetailOrderAct.FLAG_FIRST_TIME_ORDER);
                intent.putExtra(Restaurant.RESTAURANT, restaurant);
                intent.putExtra("day", dayBook);
                intent.putExtra("personAmount", person);
                intent.putExtra("minute", timeBook);
                intent.putExtra("commission", commission);
                intent.putExtra("getClosetime", getClosetime);
                intent.putExtra("getOpentime", getOpentime);
                startActivity(intent);
                overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);

            } else {
                Toast.makeText(BookOrderAct.this, "Please choose at least 1 option", Toast.LENGTH_SHORT).show();
            }

        });

    }

    public void addListenerOnButtonToReview(final boolean fromEdit) {

        linProceed.setOnClickListener(v -> {

            //TODO look for the intent
            if (bookType == 2) {

                if (Utils.getToken(mActivity) == null) {
                    SkipLoginActivity.Companion.start(mActivity, 1, ReviewOrderAct.FLAG_ORDER, restaurant, menuList, person, dayBook, timeBook, bookType, commission, getClosetime, getOpentime);
                } else {
                    Intent intent = new Intent(BookOrderAct.this, ReviewOrderAct.class);
                    intent.putExtra("id", 1);
                    intent.putExtra(ReviewOrderAct.FLAG, ReviewOrderAct.FLAG_ORDER);
                    intent.putExtra(Restaurant.RESTAURANT, restaurant);
                    intent.putExtra(com.dycode.foodgasm.dao.Menu.MENU, menuList);
                    intent.putExtra("personAmount", person);
                    intent.putExtra("day", dayBook);
                    intent.putExtra("minute", timeBook);
                    intent.putExtra("flag", flag);
                    intent.putExtra("bookType", bookType);
                    intent.putExtra("bookType", bookType);
                    intent.putExtra("commission", commission);
                    intent.putExtra("getClosetime", getClosetime);
                    intent.putExtra("getOpentime", getOpentime);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                    id_value = 1;
                    isUpdateBook = true;
                    bus.post(new EventBusAction("updatePerson", person, dayBook, timeBook));

                    startActivity(intent);

                }

                overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);

            }

            //TODO look for the intent
            else if (bookType == 3) {
                if (Utils.getToken(mActivity) == null) {
                    SkipLoginActivity.Companion.start(mActivity, 2, ReviewOrderAct.FLAG_TAKE_AWAY, restaurant, menuList, person, dayBook, timeBook, bookType, commission, getClosetime, getOpentime);
                } else {
                    Intent intent = new Intent(BookOrderAct.this, ReviewOrderAct.class);
                    intent.putExtra("id", 2);
                    intent.putExtra(ReviewOrderAct.FLAG, ReviewOrderAct.FLAG_TAKE_AWAY);
                    intent.putExtra(Restaurant.RESTAURANT, restaurant);
                    intent.putExtra(com.dycode.foodgasm.dao.Menu.MENU, menuList);
                    intent.putExtra("personAmount", person);
                    intent.putExtra("day", dayBook);
                    intent.putExtra("minute", timeBook);
                    intent.putExtra("flag", flag);
                    intent.putExtra("commission", commission);
                    intent.putExtra("getClosetime", getClosetime);
                    intent.putExtra("getOpentime", getOpentime);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    id_value = 2;
                    isUpdateBook = true;
                    bus.post(new EventBusAction("updatePerson", person, dayBook, timeBook));
                    startActivity(intent);
                }
                overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);

            } else {
                Toast.makeText(BookOrderAct.this, "Please choose at least 1 option", Toast.LENGTH_SHORT).show();
            }

        });

    }

    void showCustomTimePicker(boolean isTakeAway) {
        if (isTakeAway) {
            OrderTimePicker orderTimePicker = new OrderTimePicker(mActivity, restaurant.getOpen().getOpentime(), restaurant.getOpen().getClosetime(), true, new OrderTimePicker.OnDatePickedListener() {
                @Override
                public void onDatePickCompleted(String day, String minute, String personAmount) {
                    Calendar calendar = Calendar.getInstance();
                    Date today = calendar.getTime();
                    calendar.add(Calendar.DAY_OF_YEAR, 1);
                    Date tomorrow = calendar.getTime();
                    DateFormat dateFormat = new SimpleDateFormat("MMM d, yyyy", Locale.ENGLISH);
                    if (day.equals("Today")) {
                        day = dateFormat.format(today);
                    } else if (day.equals("Tomorrow")) {
                        day = dateFormat.format(tomorrow);
                    }
                    dayBook = day;
                    timeBook = minute;
                    person = personAmount;

                    String customCurrentTimeTakeAway = String.format(getString(R.string.custom_current_time_take_away), dayBook, timeBook);
                    tvOrderTime.setText(customCurrentTimeTakeAway);
                    tvTimeNow.setVisibility(View.GONE);
                    tvOrderTime.setVisibility(View.VISIBLE);

                    Log.d("TimePicker >", dayBook + " " + timeBook + " " + person);


                }
            });

            orderTimePicker.showPopWin(mActivity);

//            orderTimePicker.dayLoopView.setInitPosition(1);//today or tommorow
//            orderTimePicker.yearLoopView.setInitPosition(3);//jam 19.00 dll
//            orderTimePicker.monthLoopView.setInitPosition(2);//person


        } else {
            OrderTimePicker orderTimePicker = new OrderTimePicker(mActivity, restaurant.getOpen().getOpentime(), restaurant.getOpen().getClosetime(), false, new OrderTimePicker.OnDatePickedListener() {
                @Override
                public void onDatePickCompleted(String day, String minute, String personAmount) {
                    Calendar calendar = Calendar.getInstance();
                    Date today = calendar.getTime();
                    calendar.add(Calendar.DAY_OF_YEAR, 1);
                    Date tomorrow = calendar.getTime();
                    DateFormat dateFormat = new SimpleDateFormat("MMM d, yyyy", Locale.ENGLISH);
                    if (day.equals("Today")) {
                        day = dateFormat.format(today);
                    } else if (day.equals("Tomorrow")) {
                        day = dateFormat.format(tomorrow);
                    }
                    dayBook = day;
                    timeBook = minute;
                    person = personAmount;

                    String customCurrentTime = String.format(getString(R.string.custom_current_time), dayBook, timeBook, person);
                    tvOrderTime.setText(customCurrentTime);
                    tvTimeNow.setVisibility(View.GONE);
                    tvOrderTime.setVisibility(View.VISIBLE);

                }
            });
            orderTimePicker.showPopWin(mActivity);

//            orderTimePicker.dayLoopView.setInitPosition(1);//today or tommorow
//            orderTimePicker.yearLoopView.setInitPosition(3);//jam 19.00 dll
//            orderTimePicker.monthLoopView.setInitPosition(2);//person


        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        if (isUpdateBook) {
            intent.putExtra("id", id_value);
            intent.putExtra("isUpdateBook", isUpdateBook);

        }
        setResult(1, intent);
        finish();
        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);

    }

    @Override
    public void onClick(View v) {
        if (v == ivBookTable) {
            if (restaurant.getSetting().getOrderType().getRequestTabel()) {
                ivBookTable.setImageResource(R.mipmap.ic_circle_fill);
                ivBookTablePreOrder.setImageResource(R.mipmap.ic_circle_outline);
                ivTakeAway.setImageResource(R.mipmap.ic_circle_outline);
                bookType = 1;
                getCustomTime();
            }
//            else
//                showAlertDialog("Alert", "This feature isn't available for this restaurant");
        } else if (v == tvBookTable) {
            if (restaurant.getSetting().getOrderType().getRequestTabel()) {
                ivBookTable.setImageResource(R.mipmap.ic_circle_fill);
                ivBookTablePreOrder.setImageResource(R.mipmap.ic_circle_outline);
                ivTakeAway.setImageResource(R.mipmap.ic_circle_outline);
                bookType = 1;
                getCustomTime();
            }
//            else
//                showAlertDialog("Alert", "This feature isn't available for this restaurant");
        } else if (v == ivBookTablePreOrder) {
            if (restaurant.getSetting().getOrderType().getRequestTabelPreorder()) {
                ivBookTable.setImageResource(R.mipmap.ic_circle_outline);
                ivBookTablePreOrder.setImageResource(R.mipmap.ic_circle_fill);
                ivTakeAway.setImageResource(R.mipmap.ic_circle_outline);
                linOrderDetail.setClickable(true);
                linOrderDetail.setBackgroundColor(getResources().getColor(R.color.colorDivider));
                bookType = 2;
                getCustomTime();
            }
//            else
//                showAlertDialog("Alert", "This feature isn't available for this restaurant");
        } else if (v == tvBookTablePreOrder) {
            if (restaurant.getSetting().getOrderType().getRequestTabelPreorder()) {
                ivBookTable.setImageResource(R.mipmap.ic_circle_outline);
                ivBookTablePreOrder.setImageResource(R.mipmap.ic_circle_fill);
                ivTakeAway.setImageResource(R.mipmap.ic_circle_outline);
                bookType = 2;
                getCustomTime();
            }
//            else
//                showAlertDialog("Alert", "This feature isn't available for this restaurant");
        } else if (v == ivTakeAway || v == tvTakeAway) {
            if (restaurant.getSetting().getOrderType().getTakeAway()) {
                ivBookTable.setImageResource(R.mipmap.ic_circle_outline);
                ivBookTablePreOrder.setImageResource(R.mipmap.ic_circle_outline);
                ivTakeAway.setImageResource(R.mipmap.ic_circle_fill);
                bookType = 3;
                getCustomTime();
            }
//            else
//                showAlertDialog("Alert", "This feature isn't available for this restaurant");
//        } else if (v == tvTakeAway) {
//            if (restaurant.getSetting().getOrderType().getTakeAway()) {
//                ivBookTable.setImageResource(R.mipmap.ic_circle_outline);
//                ivBookTablePreOrder.setImageResource(R.mipmap.ic_circle_outline);
//                ivTakeAway.setImageResource(R.mipmap.ic_circle_fill);
//                bookType = 3;
//                getCustomTime();
//            }
//            else
//                showAlertDialog("Alert", "This feature isn't available for this restaurant");
        } else if (v == linOrderDetail) {
            if (bookType == 3) {
                showCustomTimePicker(true);
                linOrderDetail.setEnabled(false);
            } else {
                showCustomTimePicker(false);
                linOrderDetail.setEnabled(true);
            }
        }
    }

    @Subscribe
    public void onEvent(EventBusAction event) {
        if (event.getName().equals("updateMenu")) {
            menuList.clear();

            menuList.addAll(event.getMenuArrayList());
        } else if (event.getName().equals("updatePerson")) {
            person = event.getPersonAmount();
            dayBook = event.getDay();
            timeBook = event.getMinute();
            String customCurrentTimeTakeAway = String.format(getString(R.string.custom_current_time_take_away), dayBook, timeBook);
            String customCurrentTime = String.format(getString(R.string.custom_current_time), dayBook, timeBook, person);
            if (id_value == 2) tvTimeNow.setText(customCurrentTimeTakeAway);
            else tvTimeNow.setText(customCurrentTime);
            tvTimeNow.setVisibility(View.VISIBLE);
            tvOrderTime.setVisibility(View.GONE);

            if (bookType == 3) {
//                tvTimeNow.setText(customCurrentTimeTakeAway);
                tvTimeNow.setText("Order Now");
                imgArrow.setVisibility(View.GONE);
                linOrderDetail.setEnabled(false);
                linOrderDetail.setClickable(false);
            } else {
                tvTimeNow.setText(customCurrentTime);
                imgArrow.setVisibility(View.VISIBLE);
                linOrderDetail.setClickable(true);
                linOrderDetail.setEnabled(true);
            }
        }
    }

    @Override
    protected void onDestroy() {
        // Unregister
        bus.unregister(this);
        super.onDestroy();
    }
}
