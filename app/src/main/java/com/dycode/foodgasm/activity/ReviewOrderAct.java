package com.dycode.foodgasm.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.dycode.foodgasm.FoodGasmApp;
import com.dycode.foodgasm.R;
import com.dycode.foodgasm.dao.DaoSession;
import com.dycode.foodgasm.fragment.ReviewOrderType1Fragment;
import com.dycode.foodgasm.fragment.ReviewOrderType2Fragment;
import com.dycode.foodgasm.model.Menu;
import com.dycode.foodgasm.model.Restaurant;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by asus pc on 15/10/2015.
 */
public class ReviewOrderAct extends BaseActivity {
    public static final String FLAG = "FLAG";
    public static final int FLAG_BOOK_TABLE = 1;
    public static final int FLAG_ORDER = 2;
    public static final int FLAG_TAKE_AWAY = 3;

//    @BindView(R.id.btn_book)
//    Button btnBook;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.tvTitle)
    TextView tvTitle;

    int flag, tax;
    String commission;
    String personAmount, day, minute, id_value;

    public String mCurrentTitle;
    private Fragment mCurrentFragment;
    private FragmentManager fragmentManager;

    Restaurant restaurant;
    DaoSession daoSession;
    FoodGasmApp foodGasmApp;
    ArrayList<com.dycode.foodgasm.dao.Menu> menuList = new ArrayList<>();
    private String getOpentime, getClosetime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review_order);
        ButterKnife.bind(this);

        foodGasmApp = (FoodGasmApp) getApplication();
        daoSession = foodGasmApp.getDaoSession();

        mToolbar.setNavigationIcon(R.mipmap.ic_back);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        tvTitle.setText(R.string.review_order);

        fragmentManager = getSupportFragmentManager();
        flag = getIntent().getExtras().getInt(FLAG);
        menuList = getIntent().getExtras().getParcelableArrayList(Menu.MENU);
        restaurant = getIntent().getExtras().getParcelable(Restaurant.RESTAURANT);
        day = getIntent().getExtras().getString("day");
        personAmount = getIntent().getExtras().getString("personAmount");
        minute = getIntent().getExtras().getString("minute");
        id_value = getIntent().getExtras().getString("id_value");
        tax = getIntent().getExtras().getInt("tax");
        commission = getIntent().getExtras().getString("commission");
        getOpentime = getIntent().getExtras().getString("getOpentime");
        getClosetime = getIntent().getExtras().getString("getClosetime");
        Log.d("Commisison > ", commission);


        if (savedInstanceState == null) {
            if (flag == FLAG_BOOK_TABLE) {
                displayView(0);
            } else if (flag == FLAG_ORDER || flag == FLAG_TAKE_AWAY) {
                displayView(1);
            }
        }

    }

    public void displayView(int position) {
        Fragment fragment = null;
        switch (position) {
            case 0:
                fragment = ReviewOrderType1Fragment.newInstance(personAmount, day, minute, commission);
                break;

            case 1:
                fragment = ReviewOrderType2Fragment.newInstance(menuList, restaurant, personAmount, day, minute, flag, tax, commission, getOpentime, getClosetime);
                break;
        }

        mCurrentFragment = fragment;

        if (fragment != null) {
            fragmentManager.beginTransaction().replace(R.id.frameContainer, fragment).commit();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        setResult(RESULT_OK, intent);
        finish();
        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);

    }


    public static void start(Context context) {
        context.startActivity(new Intent(context, ReviewOrderAct.class));
    }

}
