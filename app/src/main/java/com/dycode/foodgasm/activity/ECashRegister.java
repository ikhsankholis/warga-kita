package com.dycode.foodgasm.activity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.dycode.foodgasm.R;
import com.dycode.foodgasm.model.ApiResponse;
import com.dycode.foodgasm.model.EcashOTP;
import com.dycode.foodgasm.model.EcashRegister;
import com.dycode.foodgasm.utils.Utils;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

public class ECashRegister extends BaseActivity implements DatePickerDialog.OnDateSetListener {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.ivLogo)
    ImageView ivLogo;
    @BindView(R.id.ecUserID)
    EditText ecUserID;
    @BindView(R.id.inputLayoutUsername)
    TextInputLayout inputLayoutUsername;
    @BindView(R.id.ecFullName)
    EditText ecFullName;
    @BindView(R.id.inputLayoutFullname)
    TextInputLayout inputLayoutFullname;
    @BindView(R.id.ecDateofBirth)
    EditText ecDateofBirth;
    @BindView(R.id.inputLayoutContact)
    TextInputLayout inputLayoutContact;
    @BindView(R.id.ecCity)
    EditText ecCity;
    @BindView(R.id.inputLayoutPassword)
    TextInputLayout inputLayoutPassword;
    @BindView(R.id.eCEmail)
    EditText eCEmail;
    @BindView(R.id.inputLayoutEmail)
    TextInputLayout inputLayoutEmail;
    @BindView(R.id.ecOTP)
    EditText ecOTP;
    @BindView(R.id.inputLayoutConfirmPassword)
    TextInputLayout inputLayoutConfirmPassword;
    @BindView(R.id.ecPIN)
    EditText ecPIN;
    @BindView(R.id.inputLayoutPIN)
    TextInputLayout inputLayoutPIN;
    @BindView(R.id.linBtnContinue)
    LinearLayout linBtnContinue;
    @BindView(R.id.txtPhoneNumber)
    TextView txtPhoneNumber;
    private String mobilePhone;
    private String dateOfBirthS;
    private String status;
    boolean statusRegistered = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.e_cash_register);
        ButterKnife.bind(this);
        mToolbar.setNavigationIcon(R.mipmap.ic_back);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        tvTitle.setText(R.string.ecash);

        mobilePhone = getIntent().getExtras().getString("ecashID");
        status = getIntent().getExtras().getString("status");
        Log.d("phone number ", mobilePhone);
        Log.d("status ", status);

        if (status.equals("REGISTERED")) {
            statusRegistered = true;
//            inputLayoutContact.setVisibility(View.GONE);
//            inputLayoutEmail.setVisibility(View.GONE);
//            inputLayoutPassword.setVisibility(View.GONE);
//            inputLayoutFullname.setVisibility(View.GONE);
//            inputLayoutUsername.setVisibility(View.GONE);
        }

        txtPhoneNumber.setText(mobilePhone);

    }

    @OnClick(R.id.ecDateofBirth)
    public void datePicker(View view) {
        DatePickerFragment fragment = new DatePickerFragment();
        fragment.show(getSupportFragmentManager(), "");
    }

    @OnClick(R.id.linBtnContinue)
    void registerEcash() {
        String phoneNumber = mobilePhone;
        String userID = ecUserID.getText().toString();
        String fullName = ecFullName.getText().toString();
        String city = ecCity.getText().toString();
        String email = eCEmail.getText().toString();
        String pin = ecPIN.getText().toString();
        String otp = ecOTP.getText().toString();

        if (!statusRegistered) {
            postRegisterEcash(phoneNumber, userID, fullName, dateOfBirthS, city, email, pin, otp, false);
        } else {
            Log.d("registerd", String.valueOf(statusRegistered));
            statusRegistered = true;
            postRegisterEcash(phoneNumber, pin, otp, true);
        }


    }

    void postRegisterEcash(String phonenumber, String idnumber, String fullname, String dateofbirth, final String city, String email, String pin, String otp, boolean statusRegistered) {
        showProgressDialog("Loading...");
        restClient.getApiService().postEcashRegister(Utils.getToken(mActivity).getAccessToken(), new EcashRegister(phonenumber, idnumber, fullname, dateofbirth, city, email, pin, otp, statusRegistered), new Callback<ApiResponse>() {
            @Override
            public void success(ApiResponse data, Response response) {
                progressDialog.dismiss();

                Log.d("data Status > ", data.getMeta().getMessage());
                new MaterialDialog.Builder(mActivity)
                        .content(data.getMeta().getMessage())
                        .positiveText(R.string.ok)
                        .cancelable(true)
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(MaterialDialog dialog, DialogAction which) {
                                setResult(2);
                                finish();
                            }
                        })
                        .show();

            }

            @Override
            public void failure(RetrofitError error) {
                progressDialog.dismiss();
                String json = new String(((TypedByteArray) error.getResponse().getBody()).getBytes());
                Log.v("failure", json.toString());
                try {
                    JSONObject dataError = new JSONObject(json.toString());
                    JSONObject meta = dataError.getJSONObject("meta");
                    String code = meta.getString("code");
                    if (code.equals("400")) {
                        String message = meta.getString("message");
                        new MaterialDialog.Builder(mActivity)
                                .content(message)
                                .positiveText(R.string.ok)
                                .cancelable(true)
                                .onPositive(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(MaterialDialog dialog, DialogAction which) {
                                        dialog.dismiss();
                                    }
                                })
                                .show();
                    } else {

                        showAlertDialog("Error", error.getMessage());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        });
    }

    void postRegisterEcash(String mobilePhone, String pin, String otp, boolean statusRegistered) {
        showProgressDialog("Loading...");
        restClient.getApiService().postEcashRegister(Utils.getToken(mActivity).getAccessToken(), new EcashRegister(mobilePhone, pin, otp, true), new Callback<ApiResponse>() {
            @Override
            public void success(ApiResponse data, Response response) {
                progressDialog.dismiss();

                Log.d("data Status > ", data.getMeta().getMessage());
                new MaterialDialog.Builder(mActivity)
                        .content(data.getMeta().getMessage())
                        .positiveText(R.string.ok)
                        .cancelable(true)
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(MaterialDialog dialog, DialogAction which) {
                                setResult(2);
                                finish();
                            }
                        })
                        .show();

            }

            @Override
            public void failure(RetrofitError error) {
                progressDialog.dismiss();
                String json = new String(((TypedByteArray) error.getResponse().getBody()).getBytes());
                Log.v("failure", json.toString());
                try {
                    JSONObject dataError = new JSONObject(json.toString());
                    JSONObject meta = dataError.getJSONObject("meta");
                    String code = meta.getString("code");
                    if (code.equals("400")) {
                        String message = meta.getString("message");
                        new MaterialDialog.Builder(mActivity)
                                .content(message)
                                .positiveText(R.string.ok)
                                .cancelable(true)
                                .onPositive(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(MaterialDialog dialog, DialogAction which) {
                                        dialog.dismiss();
                                    }
                                })
                                .show();
                    } else {

                        showAlertDialog("Error", error.getMessage());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        });
    }


    private void setDate(final Calendar calendar) {
        final DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.MEDIUM);
        ecDateofBirth.setText(dateFormat.format(calendar.getTime()));
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int day) {
        Calendar cal = new GregorianCalendar(year, month, day);
        setDate(cal);
        String years = String.valueOf(year);
        String months = String.valueOf(month);
        String days = String.valueOf(day);

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String strDate = format.format(cal.getTime());

        dateOfBirthS = strDate;
        Log.d("dateOfbIRrth > ", dateOfBirthS);
    }

    public static class DatePickerFragment extends DialogFragment {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);


            return new DatePickerDialog(getActivity(),
                    (DatePickerDialog.OnDateSetListener)
                            getActivity(), year, month, day);
        }

    }

    @OnClick(R.id.txtResendOtp)
    void onResendOtp() {
        if (status.equals("REGISTERED")) {
            statusRegistered = true;
            EcashOTP reqOTPeCASH = new EcashOTP(mobilePhone, "N02");
            reqOTPeCASH(reqOTPeCASH);
        } else {
            EcashOTP reqOTPeCASH = new EcashOTP(mobilePhone, "N01");
            reqOTPeCASH(reqOTPeCASH);
        }

    }

    private void reqOTPeCASH(EcashOTP ecashOTP) {
        showProgressDialog("Loading...");
        restClient.getApiService().postPhoneVerifyEcash(Utils.getToken(mActivity).getAccessToken(), ecashOTP, new Callback<ApiResponse>() {
            @Override
            public void success(ApiResponse data, Response response) {
                progressDialog.dismiss();
                Log.d("data String> ", new Gson().toJson(data.getData()));
            }

            @Override
            public void failure(RetrofitError error) {
                progressDialog.dismiss();
                showAlertDialog("Error", error.getMessage());
            }
        });
    }
}
