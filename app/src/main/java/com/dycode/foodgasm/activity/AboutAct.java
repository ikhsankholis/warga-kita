package com.dycode.foodgasm.activity;

import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dycode.foodgasm.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by asus pc on 20/10/2015.
 */
public class AboutAct extends BaseActivity implements View.OnClickListener {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.linEmail)
    LinearLayout linEmail;
    @BindView(R.id.linTerms)
    LinearLayout linTerms;
    @BindView(R.id.linRate)
    LinearLayout linRate;
    @BindView(R.id.linFaq)
    LinearLayout linFaq;
    @BindView(R.id.ivToFb)
    ImageView ivToFb;
    @BindView(R.id.ivToTwitter)
    ImageView ivToTwitter;
    @BindView(R.id.ivToInstagram)
    ImageView ivToInstagram;
    @BindView(R.id.tvVersion)
    TextView tvVersion;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        ButterKnife.bind(this);

        // Obtain the shared Tracker instance.
        getTracker("About");
        setToolbar(true);

        linEmail.setOnClickListener(this);
        linTerms.setOnClickListener(this);
        linRate.setOnClickListener(this);
        linFaq.setOnClickListener(this);
        ivToFb.setOnClickListener(this);
        ivToTwitter.setOnClickListener(this);
        ivToInstagram.setOnClickListener(this);


        PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            String version = "Version " + pInfo.versionName;
            tvVersion.setText(version);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

    }

    @Override
    protected int getToolbarTitle() {
        return R.string.about;
    }

    @Override
    public void onClick(View v) {
        if (v == linEmail) {
            Intent intent = new Intent(Intent.ACTION_SEND);
            String[] recipients = {"help@foodgasm.id"};
            intent.putExtra(Intent.EXTRA_EMAIL, recipients);
            intent.setType("text/html");
            startActivity(Intent.createChooser(intent, "Send mail"));
        } else if (v == linTerms) {
            Intent intent = new Intent(mActivity, TermsAct.class);
            startActivity(intent);
            overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
        } else if (v == linRate) {
            final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
            try {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
            } catch (android.content.ActivityNotFoundException anfe) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
            }
        } else if (v == linFaq) {
            Intent intent = new Intent(mActivity, FaqAct.class);
            startActivity(intent);
            overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
        } else if (v == ivToFb) {
            PackageManager pm = mActivity.getPackageManager();
            Uri uri;
            String url = "http://www.facebook.com/foodgasm.id";
            try {
                pm.getPackageInfo("com.facebook.katana", 0);
                uri = Uri.parse("fb://facewebmodal/f?href=" + url);
            } catch (PackageManager.NameNotFoundException e) {
                uri = Uri.parse(url);
            }
            startActivity(new Intent(Intent.ACTION_VIEW, uri));
            overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
        } else if (v == ivToTwitter) {
            try {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("twitter://user?screen_name=foodgasm_id"));
                startActivity(intent);
                overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);

            } catch (Exception e) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/foodgasm_id")));
                overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
            }
        } else if (v == ivToInstagram) {

            Uri uri = Uri.parse("http://instagram.com/_u/foodgasm.indonesia");
            Intent likeIng = new Intent(Intent.ACTION_VIEW, uri);

            likeIng.setPackage("com.instagram.android");

            try {
                startActivity(likeIng);
            } catch (ActivityNotFoundException e) {
                startActivity(new Intent(Intent.ACTION_VIEW,
                        Uri.parse("http://instagram.com/_u/foodgasm.indonesia")));
            }

        }

    }
}
