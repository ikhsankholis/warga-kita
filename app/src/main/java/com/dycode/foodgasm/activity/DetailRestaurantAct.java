package com.dycode.foodgasm.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.dycode.foodgasm.FoodGasmApp;
import com.dycode.foodgasm.R;
import com.dycode.foodgasm.adapters.PromoAdapter;
import com.dycode.foodgasm.dao.DaoSession;
import com.dycode.foodgasm.model.ApiResponse;
import com.dycode.foodgasm.model.ApiResponseList;
import com.dycode.foodgasm.model.Gallery;
import com.dycode.foodgasm.model.Promo;
import com.dycode.foodgasm.model.Restaurant;
import com.dycode.foodgasm.rest.RestClient;
import com.dycode.foodgasm.utils.DensityPixelUtil;
import com.dycode.foodgasm.utils.NetworkErrorUtil;
import com.dycode.foodgasm.utils.SPManager;
import com.dycode.foodgasm.utils.Utils;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;
import com.sromku.simple.fb.SimpleFacebook;
import com.sromku.simple.fb.entities.Feed;
import com.sromku.simple.fb.listeners.OnPublishListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Okta Dwi Priatna on 09/10/2015.
 * flow dari dari home > detail restaurant
 */
public class DetailRestaurantAct extends BaseActivity implements PromoAdapter.PromoItemClickListener, View.OnClickListener, OnMapReadyCallback {

    private static final String TAG = "DetailRestaurantAct";

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.linContainer)
    LinearLayout linContainer;
    @BindView(R.id.linBtnReqest)
    LinearLayout linBtnRequest;
    @BindView(R.id.scrollView)
    ScrollView scrollView;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.linReview)
    LinearLayout linReview;
    @BindView(R.id.linMenu)
    LinearLayout linMenu;
    @BindView(R.id.rvPromo)
    RecyclerView rvPromo;
    @BindView(R.id.relMap)
    RelativeLayout relMap;
    @BindView(R.id.linPhone)
    LinearLayout linPhone;
    @BindView(R.id.linWebsite)
    LinearLayout linWebsite;
    @BindView(R.id.tvRestaurantName)
    TextView tvRestaurantName;
    @BindView(R.id.tvRestaurantType)
    TextView tvRestaurantType;
    @BindView(R.id.tvReview)
    TextView tvReview;
    @BindView(R.id.tvRestaurantLocation)
    TextView tvRestaurantLocation;
    @BindView(R.id.tvUber)
    TextView tvUber;
    @BindView(R.id.tvRestaurantContact)
    EditText tvRestaurantContact;
    @BindView(R.id.tvRestaurantWeb)
    TextView tvRestaurantWeb;
    @BindView(R.id.tvOpenTime)
    TextView tvOpenTime;
    @BindView(R.id.btnPay)
    ImageView btnPay;
    @BindView(R.id.tvProcess)
    TextView tvProcess;

    //Gallery
    @BindView(R.id.lin_gallery)
    LinearLayout linGallery;
    @BindView(R.id.img_gallery1)
    ImageView imgGallery1;
    @BindView(R.id.img_gallery2)
    ImageView imgGallery2;
    @BindView(R.id.img_gallery3)
    ImageView imgGallery3;
    @BindView(R.id.img_overlay)
    ImageView imgOverlay;
    @BindView(R.id.text_see_more_gallery)
    TextView tvSeeMoreGallery;

    //Maps
    private GoogleMap mMap;


    SimpleFacebook mSimpleFacebook;

    Restaurant restaurant, restaurantDetail;
    String url = "", distanceFinal, restoInitial;
    double tax;
    String commission;
    List<Promo> promoList = new ArrayList<Promo>();
    PromoAdapter mAdapter;
    LinearLayoutManager linLayoutManager;
    FoodGasmApp foodGasmApp;
    DaoSession daoSession;
    private String getClosetime;
    private String getOpentime;

    @Override
    public void onResume() {
        super.onResume();
        mSimpleFacebook = SimpleFacebook.getInstance(this);
        setUpMapIfNeeded();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_restaurant_v2);
        ButterKnife.bind(this);

        mToolbar.setNavigationIcon(R.mipmap.btn_back_maps_blue);
        setSupportActionBar(mToolbar);

        restaurant = getIntent().getExtras().getParcelable(Restaurant.RESTAURANT);
        restoInitial = getIntent().getExtras().getString("restoInitial");
        tvRestaurantName.setText(restaurant.getName());

        // Obtain the shared Tracker instance.
        getTracker(restaurant.getName() + " Detail (" + restaurant.getId() + ")");
        foodGasmApp = (FoodGasmApp) getApplication();
        daoSession = foodGasmApp.getDaoSession();
        getDetailRestaurant();
        getGalleryList();


    }

    protected void setUpMapIfNeeded() {
        if (mMap == null) {
            MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);
        }

    }

    void viewOnClickListener() {
        linReview.setOnClickListener(this);
        linMenu.setOnClickListener(this);
        linPhone.setOnClickListener(this);
        relMap.setOnClickListener(this);
        linBtnRequest.setOnClickListener(this);
        btnPay.setOnClickListener(this);
        if (!TextUtils.isEmpty(url)) linWebsite.setOnClickListener(this);
        else {
            linWebsite.setClickable(false);
//            linWebsite.setBackgroundColor(getResources().getColor(R.color.colorDivider));
            tvRestaurantWeb.setTextColor(getResources().getColor(R.color.icons_inactive));
        }
    }

    void getDetailRestaurant() {
        restClient.getApiService().getRestaurantDetail(restaurant.getId(), Utils.getLatString(mActivity), Utils.getLngString(mActivity), new Callback<ApiResponse<Restaurant>>() {
            @Override
            public void success(ApiResponse<Restaurant> restaurantApiResponse, Response response) {
                if (Build.VERSION.SDK_INT >= 21) {
                    getWindow().setStatusBarColor(ContextCompat.getColor(mActivity, R.color.colorAccent));
                }
                mToolbar.setBackgroundColor(ContextCompat.getColor(mActivity, android.R.color.transparent));
                tvTitle.setText(null);

                progressBar.setVisibility(View.GONE);
                mToolbar.setVisibility(View.VISIBLE);
                scrollView.setVisibility(View.VISIBLE);
                linContainer.setVisibility(View.VISIBLE);
                linBtnRequest.setVisibility(View.VISIBLE);

                restaurantDetail = restaurantApiResponse.getData();
                addMarkerToMaps();

                if (restaurantApiResponse.getData().getMenuVersion() <= 0 || restaurantApiResponse.getData().getMenuState() == 0) {
                    tvProcess.setText("COMING SOON");
                    linBtnRequest.setEnabled(false);
                    linBtnRequest.setBackgroundColor(Color.parseColor("#e0e0e0"));
                }

                if (!restaurantDetail.getSetting().getOrderType().getRequestTabel() && !restaurantDetail.getSetting().getOrderType().getRequestTabelPreorder()
                        && !restaurantDetail.getSetting().getOrderType().getTakeAway()) {
                    tvProcess.setText("COMING SOON");
                    linBtnRequest.setEnabled(false);
                    linBtnRequest.setBackgroundColor(Color.parseColor("#e0e0e0"));

                }

                tax = restaurantDetail.getTax();
                commission = restaurantDetail.getCommission();
                getClosetime = restaurantDetail.getOpen().getClosetime();
                getOpentime = restaurantDetail.getOpen().getOpentime();
                String location = String.format(getString(R.string.resto_location), restaurantDetail.getAddress().getDistrict(), restaurantDetail.getAddress().getCity());
                String countLocation = location.length() > 35 ? String.format(getString(R.string.resto_location_cut), location.substring(0, 20)) : location;

                tvRestaurantType.setText(restaurantDetail.getCategory().getCategoryName() + " | " + countLocation);
                tvReview.setText(String.valueOf(restaurantDetail.getNumOfReview()));
                String fullAddress = String.format(getString(R.string.resto_address), restaurantDetail.getAddress().getStreet(), restaurantDetail.getAddress().getDistrict(), restaurantDetail.getAddress().getCity(), restaurantDetail.getAddress().getProvince(), restaurantDetail.getAddress().getPostalCode());
                tvRestaurantLocation.setText(fullAddress);
                tvRestaurantContact.setText(restaurantDetail.getPhone());
                String openTime = String.format(getString(R.string.open_time), restaurantDetail.getOpen().getOpentime(), restaurantDetail.getOpen().getClosetime());
                tvOpenTime.setText(openTime);
                url = restaurantDetail.getWebsites();
                tvRestaurantWeb.setText(getString(R.string.website));
                tvRestaurantWeb.setTextColor(getResources().getColor(R.color.textColorSecondary));
                Double distance = restaurantDetail.getDistance();
                int countDistance = distance.intValue();
                if (countDistance >= 1) {
                    distanceFinal = String.valueOf(countDistance) + " " + "km";
                } else {
                    Double roundedDistance = Math.round(100 * distance) / 100.0;
                    Double countRoundedDistance = roundedDistance * 1000;
                    int countFinalDistance = countRoundedDistance.intValue();

                    distanceFinal = String.valueOf(countFinalDistance) + " " + "m";
                }
                String infoRestoDistance = String.format(mActivity.getResources().getString(R.string.resto_distance), distanceFinal);
                tvUber.setText(infoRestoDistance);
                viewOnClickListener();
                getListPromo();
            }

            @Override
            public void failure(RetrofitError error) {
                progressBar.setVisibility(View.GONE);
                scrollView.setVisibility(View.VISIBLE);
                mToolbar.setVisibility(View.VISIBLE);
                linContainer.setVisibility(View.VISIBLE);
                linBtnRequest.setVisibility(View.VISIBLE);

                if (Build.VERSION.SDK_INT >= 21) {
                    getWindow().setStatusBarColor(ContextCompat.getColor(mActivity, R.color.red_error));
                }
                mToolbar.setBackgroundColor(ContextCompat.getColor(mActivity, R.color.red_error));

                if (error.getKind() == RetrofitError.Kind.NETWORK) {
                    tvTitle.setText(R.string.alert_no_internet_connection);
                } else {
                    tvTitle.setText(R.string.restaurant_detail_error);
                    String message = NetworkErrorUtil.getErrorMessage(error);
                    showToast(message);
                }

            }
        });
    }

    void getListPromo() {
        restClient.getApiService().getListPromo("resto", restaurant.getId(), new Callback<ApiResponseList<Promo>>() {
            @Override
            public void success(ApiResponseList<Promo> promoApiResponseList, Response response) {

                promoList.clear();
                promoList.addAll(promoApiResponseList.getData());
                if (promoList.size() != 0) {
                    setUpRecyclerView();
                    mAdapter.notifyDataSetChanged();
                } else {
                    rvPromo.setVisibility(View.GONE);
                }

            }

            @Override
            public void failure(RetrofitError error) {
                showToast("No Internet Connection");
            }
        });
    }


    private void setUpRecyclerView() {
        linLayoutManager = new LinearLayoutManager(mActivity, LinearLayoutManager.HORIZONTAL, false);
        rvPromo.setLayoutManager(linLayoutManager);
        mAdapter = new PromoAdapter(mActivity, promoList);
        rvPromo.setAdapter(mAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_gallery, menu);
        return true;
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    public static void start(Context context) {
        context.startActivity(new Intent(context, DetailRestaurantAct.class));
    }

    @Override
    protected int getToolbarTitle() {
        return R.string.string_null;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0 && resultCode == 1) {
            getDetailRestaurant();
        }
    }

    @Override
    public void onPromoClick(Promo promo) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.promo_dialog_layout);

        ImageView ivClose = (ImageView) dialog.findViewById(R.id.ivClosePromo);
        final ImageView ivShareFb = (ImageView) dialog.findViewById(R.id.ivShareFb);
        final ImageView ivShareEmail = (ImageView) dialog.findViewById(R.id.ivShareEmail);
        final ImageView ivShareMessage = (ImageView) dialog.findViewById(R.id.ivShareMessage);

        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        final ImageView image = (ImageView) dialog.findViewById(R.id.imgPromo);
        final TextView tvPromoName = (TextView) dialog.findViewById(R.id.tvPromoName);
        final TextView tvPromoDesc = (TextView) dialog.findViewById(R.id.tvPromoDesc);

        restClient.getApiService().getPromoDetail(promo.getId(),  new Callback<ApiResponse<Promo>>() {

            @Override
            public void success(final ApiResponse<Promo> promoApiResponse, Response response) {
                final Promo promoData = promoApiResponse.getData();
                tvPromoName.setText(promoData.getName());
//                String promoDesc = promoData.getDescription().length() > 140 ? String.format(mActivity.getString(R.string.resto_location_cut), promoData.getDescription().substring(0, 140)) : promoData.getDescription();
                tvPromoDesc.setText(promoData.getDescription());
                Picasso.with(mActivity).load(promoData.getPic()).into(image);
                final String promoPic = promoData.getPic();

                ivShareFb.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Feed feed = new Feed.Builder()
                                .setMessage(promoData.getName() + " @ " + restaurant.getName())
                                .setName("FoodGasm")
                                .setCaption(getResources().getString(R.string.highlight))
                                .setDescription(getResources().getString(R.string.highlight))
                                .setLink("http://www.foodgasm.id/?type=promo&id=" + promoData.getId())
                                .setPicture(promoPic)
                                .build();

                        mSimpleFacebook.publish(feed, true, onPublishListener);
                    }
                });

                ivShareEmail.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Uri uri = Uri.parse("mailto:");
                        Intent intent = new Intent(Intent.ACTION_SEND, uri);
                        intent.setType("text/html");
                        intent.putExtra(Intent.EXTRA_TEXT, promoData.getName() + " @ " + restaurant.getName());
                        startActivity(Intent.createChooser(intent, "Send mail"));
                    }
                });

                ivShareMessage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Uri uri = Uri.parse("smsto:");
                        Intent intent = new Intent(Intent.ACTION_SENDTO, uri);
                        intent.putExtra("sms_body", promoData.getName() + " @ " + restaurant.getName());
                        startActivity(intent);
                    }
                });
            }

            @Override
            public void failure(RetrofitError error) {
                error.getMessage();
                ivShareFb.setClickable(false);
                ivShareFb.setEnabled(false);
                ivShareEmail.setClickable(false);
                ivShareEmail.setEnabled(false);
                ivShareMessage.setClickable(false);
                ivShareMessage.setEnabled(false);
            }
        });

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();

    }

    OnPublishListener onPublishListener = new OnPublishListener() {

        @Override
        public void onComplete(String postId) {
            Log.i(TAG, "Published successfully. The new post id = " + postId);
            showAlertDialog("Success", "Message successfully shared");

        }
                /*
                * You can override other methods here:
                * onThinking(), onFail(String reason), onException(Throwable throwable)
                */

        @Override
        public void onFail(String reason) {
            super.onFail(reason);
            Log.i(TAG, "Failed to publish " + reason);
        }

        @Override
        public void onException(Throwable throwable) {
            super.onException(throwable);
            Log.i(TAG, "Failed to publish " + throwable.getMessage());
        }
    };


    @Override
    public void onClick(View v) {
        if (v == linReview) {
            Intent intent = new Intent(DetailRestaurantAct.this, ReviewAct.class);
            intent.putExtra(Restaurant.RESTAURANT, restaurantDetail);
            intent.putExtra("restoInitial", restoInitial);
            intent.putExtra("commission", commission);
            startActivityForResult(intent, 0);
            overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
        } else if (v == linMenu) {
            Intent intent = new Intent(DetailRestaurantAct.this, MenuDetailAct.class);
            intent.putExtra(Restaurant.RESTAURANT, restaurantDetail);
            intent.putExtra("tax", tax);
            intent.putExtra("commission", commission);
            intent.putExtra("getClosetime", getClosetime);
            intent.putExtra("getOpentime", getOpentime);

            SPManager.saveString(DetailRestaurantAct.this, "taxResto", String.valueOf(tax));
            SPManager.saveString(DetailRestaurantAct.this, "commission", String.valueOf(commission));
            startActivity(intent);
            overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
        } else if (v == linPhone) {
            if (!tvRestaurantContact.getText().toString().equals("")) {
                showAlertDialog(tvRestaurantContact.getText().toString(), "Call", "Cancel");
            }
        } else if (v == relMap) {
            Intent intent = new Intent(DetailRestaurantAct.this, MapsAct.class);
            intent.putExtra(Restaurant.RESTAURANT, restaurantDetail);
            startActivity(intent);
            overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
        } else if (v == linWebsite) {
            if (!url.startsWith("http://") && !url.startsWith("https://"))
                url = "http://" + url;
            final Intent intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse(url));
            startActivity(intent);
        } else if (v == linBtnRequest) {
//            Intent intent = new Intent(DetailRestaurantAct.this, BookOrderAct.class);
//            intent.putExtra(Restaurant.RESTAURANT, restaurantDetail);
//            intent.putExtra("tax", tax);
//            intent.putExtra("commission", commission);
//            intent.putExtra("new", true);
//            intent.putExtra("getClosetime", getClosetime);
//            intent.putExtra("getOpentime", getOpentime);
//            intent.putExtra(BookOrderAct.MENU_TYPE, BookOrderAct.FLAG_NOT_PICKED_MENU);
//            startActivity(intent);
//            overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
            Double d = new Double(tax);
            int i = d.intValue();

            Intent intent = new Intent(DetailRestaurantAct.this, MenuDetailAct.class);
            intent.putExtra(Restaurant.RESTAURANT, restaurantDetail);
            intent.putExtra("tax", i);
            intent.putExtra("commission", commission);
            intent.putExtra("getClosetime", getClosetime);
            intent.putExtra("getOpentime", getOpentime);

            SPManager.saveString(DetailRestaurantAct.this, "taxResto", String.valueOf(tax));
            SPManager.saveString(DetailRestaurantAct.this, "commission", String.valueOf(commission));
            startActivity(intent);
            overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
        } else if (v == btnPay) {
            Intent intent = new Intent(this, TotalPayment.class);
            intent.putExtra("restoName", restaurant.getName());
            intent.putExtra("restoAddress", restaurant.getAddress().getStreet());
            intent.putExtra("restoCity", restaurant.getAddress().getCity());
            intent.putExtra("getClosetime", getClosetime);
            intent.putExtra("getOpentime", getOpentime);
            startActivity(intent);
        }
    }




    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setPadding(0, Math.round(DensityPixelUtil.convertDpToPixel(24, mActivity)), 0, 0);
        mMap.getUiSettings().setZoomControlsEnabled(false);
        if (restaurantDetail != null) {
            addMarkerToMaps();
        }

    }

    @OnClick(R.id.img_overlay)
    void seeMoreGallery() {
        Intent intent = new Intent(DetailRestaurantAct.this, GalleryAct.class);
        intent.putExtra(Restaurant.RESTAURANT, restaurant.getId());
        startActivity(intent);
        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
    }

    void getGalleryList() {
        restClient = RestClient.getInstance();
        restClient.getApiService().getListGallery(restaurant.getId(), new Callback<ApiResponseList<Gallery>>() {
            @Override
            public void success(ApiResponseList<Gallery> galleryApiResponseList, Response response) {
                if (galleryApiResponseList.getMeta().getCode() == 200) {
                    List<Gallery> galleries = new ArrayList<Gallery>();
                    galleries.addAll(galleryApiResponseList.getData());
                    if (galleries.size() > 0) {
                        try {
                            Picasso.with(mActivity).load(galleries.get(0).getPic()).into(imgGallery1);
                            Picasso.with(mActivity).load(galleries.get(1).getPic()).into(imgGallery2);
                            Picasso.with(mActivity).load(galleries.get(2).getPic()).into(imgGallery3);
                        } catch (Exception ignored) {

                        }
                        if (galleries.size() <= 3) {
                            imgOverlay.setVisibility(View.GONE);
                            tvSeeMoreGallery.setVisibility(View.GONE);
                        }
                    } else {
                        linGallery.setVisibility(View.GONE);
                    }

                }
            }

            @Override
            public void failure(RetrofitError error) {
            }
        });
    }

    private void addMarkerToMaps() {
        final LatLng restaurantLocation = new LatLng(restaurantDetail.getLocation().getCoordinates().get(1), restaurantDetail.getLocation().getCoordinates().get(0));
        mMap.addMarker(new MarkerOptions().position(restaurantLocation).title(restaurantDetail.getName()).icon(BitmapDescriptorFactory.fromResource(R.mipmap.icn_marker_maps)));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(restaurantLocation, 15));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(19), 1500, null);
    }
}
