package com.dycode.foodgasm.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.dycode.foodgasm.FoodGasmApp;
import com.dycode.foodgasm.R;
import com.dycode.foodgasm.dao.DaoSession;
import com.dycode.foodgasm.model.ApiResponse;
import com.dycode.foodgasm.model.PostCreditCard;
import com.dycode.foodgasm.utils.CCUtils;
import com.dycode.foodgasm.utils.Constants;
import com.dycode.foodgasm.utils.NetworkErrorUtil;
import com.dycode.foodgasm.utils.Utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.card.payment.CardIOActivity;
import io.card.payment.CreditCard;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Okta Dwi Priatna on 09/10/2015.
 */
public class PaymentMethodAct extends BaseActivity {

    public static final String FLAG = "FLAG";
    public static final int FLAG_FROM_REORDER = 1;
    public static final int FLAG_DIRECT_PAYMENT = 2;
    public static final int FLAG_SET_CREDIT_CARD = 3;
    private static final int MY_SCAN_REQUEST_CODE = 0;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.tvMenu)
    TextView tvMenu;
    @BindView(R.id.etCardNumber)
    EditText etCardNumber;
    @BindView(R.id.etExpDate)
    EditText etExpDate;
    @BindView(R.id.etCvv)
    EditText etCvv;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    FoodGasmApp foodGasmApp;
    DaoSession daoSession;

    int expMonth, expYear;
    String cvv, textDateOld, textDateNew = "", cardNumber, expMonthString, expYearString;
    List<com.dycode.foodgasm.model.CreditCard> creditCardList = new ArrayList<com.dycode.foodgasm.model.CreditCard>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_method);
        ButterKnife.bind(this);

        // Obtain the shared Tracker instance.
        getTracker("Credit Card");

        tvMenu.setText(getResources().getText(R.string.save));
        tvMenu.setVisibility(View.VISIBLE);
        setToolbar(true);

        foodGasmApp = (FoodGasmApp) getApplication();
        daoSession = foodGasmApp.getDaoSession();

        etCardNumber.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_credit_card_black, 0);

        etCardNumber.addTextChangedListener(new TextWatcher() {
            private static final char space = ' ';

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                // Remove all spacing char
                int pos = 0;
                while (true) {
                    if (pos >= s.length()) break;
                    if (space == s.charAt(pos) && (((pos + 1) % 5) != 0 || pos + 1 == s.length())) {
                        s.delete(pos, pos + 1);
                    } else {
                        pos++;
                    }
                }

                // Insert char where needed.
                pos = 4;
                while (true) {
                    if (pos >= s.length()) break;
                    final char c = s.charAt(pos);
                    // Only if its a digit where there should be a space we insert a space
                    if ("0123456789".indexOf(c) >= 0) {
                        s.insert(pos, "" + space);
                    }
                    pos += 5;
                }

                // Check and change logo type card
                if (etCardNumber.length() >= 1 && etCardNumber.length() <= 2) {
                    etCardNumber.setCompoundDrawablesWithIntrinsicBounds(0, 0, CCUtils.getCardLogoCc(etCardNumber.getText().toString().replaceAll("\\s", "")), 0);
                }

            }
        });

        etExpDate.addTextChangedListener(new TextWatcher() {
            boolean isDateCondition = true;

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                textDateOld = textDateNew;
                textDateNew = s.toString();
                String lastChar;
                int index;

                if (textDateNew.length() > textDateOld.length() && isDateCondition) {
                    isDateCondition = false;
                    lastChar = "";

                    if (textDateNew.length() == 3) {
                        index = 2;
                        lastChar = textDateNew.substring(index);
                        String dateFormatted = String.format(getString(R.string.exp_date_value), textDateOld, lastChar);
                        etExpDate.setText(dateFormatted);
                        etExpDate.setSelection(etExpDate.length());
                    } else if (textDateNew.length() == 4) {
                        index = 3;
                        lastChar = textDateNew.substring(index);
                        String dateFormatted = String.format(getString(R.string.exp_date_valueb), textDateOld, lastChar);
                        etExpDate.setText(dateFormatted);
                        etExpDate.setSelection(etExpDate.length());
                    } else if (textDateNew.length() == 5) {
                        index = 4;
                        lastChar = textDateNew.substring(index);
                        String dateFormatted = String.format(getString(R.string.exp_date_normal), textDateOld, lastChar);
                        etExpDate.setText(dateFormatted);
                        etExpDate.setSelection(etExpDate.length());
                    }
                } else if (textDateNew.length() > textDateOld.length()) {
                    if (etExpDate.getText().length() == 3) {
                        index = 2;
                        lastChar = textDateNew.substring(index);
                        String dateFormatted = String.format(getString(R.string.exp_date_value), textDateOld, lastChar);
                        etExpDate.setText(dateFormatted);
                        etExpDate.setSelection(etExpDate.length());
                    }
                } else {
                    isDateCondition = textDateOld.length() == 3 && textDateNew.length() == 2 ||
                            textDateOld.length() == 4 && textDateNew.length() == 3 ||
                            textDateOld.length() == 5 && textDateNew.length() == 4;
                }

            }
        });

    }

    @OnClick(R.id.tvMenu)
    void onSaveClicked() {
        verifyCreditCardData();
    }

    public void registerCreditCardToVeritrans() {
        showProgressDialog("Saving Data...");
        restClientForVeritrans.getApiService().postCreditCardToVeritrans(cardNumber, expMonthString, expYearString, Constants.VERITRANS_CLIENT_KEY, new Callback<com.dycode.foodgasm.model.CreditCard>() {

            @Override
            public void success(com.dycode.foodgasm.model.CreditCard creditCard, Response response) {
                String transaction_id = creditCard.getTransactionId();
                String saved_token_id = creditCard.getSavedTokenId();
                String masked_card = creditCard.getMaskedCard();
                registerCreditCard(transaction_id, saved_token_id, masked_card);
            }

            @Override
            public void failure(RetrofitError error) {
                progressDialog.dismiss();
                showMaterialDialogAlert(NetworkErrorUtil.getErrorMessage(error));
            }
        });
    }

    public void registerCreditCard(String transaction_id, String saved_token_id, String masked_card) {
        restClient.getApiService().postCreditCard(Utils.getToken(mActivity).getAccessToken(), new PostCreditCard(transaction_id, saved_token_id, masked_card, ""), new Callback<ApiResponse<com.dycode.foodgasm.model.CreditCard>>() {
            @Override
            public void success(ApiResponse<com.dycode.foodgasm.model.CreditCard> creditCardApiResponse, Response response) {
                com.dycode.foodgasm.model.CreditCard creditCard = creditCardApiResponse.getData();
                com.dycode.foodgasm.dao.CreditCard creditCardDao = new com.dycode.foodgasm.dao.CreditCard();
                creditCardDao.set_id(creditCard.getId());
                creditCardDao.setTransaction_id(creditCard.getTransactionId());
                creditCardDao.setSaved_token_id(creditCard.getSavedTokenId());
                creditCardDao.setMasked_card(creditCard.getMaskedCard());
                creditCardDao.setState(true);
                creditCardDao.setIsEditSelected(false);
                creditCardDao.setProvider(creditCard.getProvider());
                daoSession.insertOrReplace(creditCardDao);

                patchCreditCard(creditCard.getId());
            }

            @Override
            public void failure(RetrofitError error) {
                progressDialog.dismiss();
                showMaterialDialogAlert(NetworkErrorUtil.getErrorMessage(error));
            }
        });
    }

    public void patchCreditCard(String selectedId) {
        restClient.getApiService().setCreditCardActive(selectedId, Utils.getToken(mActivity).getAccessToken(), new Callback<com.dycode.foodgasm.model.CreditCard>() {
            @Override
            public void success(com.dycode.foodgasm.model.CreditCard creditCard, Response response) {
                new MaterialDialog.Builder(mActivity)
                        .content(R.string.data_saved)
                        .positiveText(R.string.ok)
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(MaterialDialog dialog, DialogAction which) {
                                setResult(1);
                                finish();
                                overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                            }
                        })
                        .cancelable(true)
                        .show();
            }

            @Override
            public void failure(RetrofitError error) {
                progressDialog.dismiss();
                showToast("No Internet Connection");
            }
        });
    }

    @OnClick(R.id.ivScan)
    void onScanPress(View v) {
        Intent scanIntent = new Intent(this, CardIOActivity.class);

        // customize these values to suit your needs.
        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_EXPIRY, true); // default: false
        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_CVV, true); // default: false
        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_POSTAL_CODE, false); // default: false

        // MY_SCAN_REQUEST_CODE is arbitrary and is only used within this activity.
        startActivityForResult(scanIntent, MY_SCAN_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == MY_SCAN_REQUEST_CODE) {
            String resultDisplayStr;
            if (data != null && data.hasExtra(CardIOActivity.EXTRA_SCAN_RESULT)) {
                CreditCard scanResult = data.getParcelableExtra(CardIOActivity.EXTRA_SCAN_RESULT);

                // Never log a raw card number. Avoid displaying it, but if necessary use getFormattedCardNumber()
                resultDisplayStr = scanResult.getFormattedCardNumber();
                etCardNumber.setText(resultDisplayStr);


                // Do something with the raw number, e.g.:
                // myService.setCardNumber( scanResult.cardNumber );

                if (scanResult.isExpiryValid()) {
                    String subExpYear = (String.valueOf(scanResult.expiryYear)).substring(2, 4);
                    resultDisplayStr += scanResult.expiryMonth + " / " + subExpYear;
                    String resultExpDate = scanResult.expiryMonth + " / " + subExpYear;
                    expYear = scanResult.expiryYear;
                    if (String.valueOf(scanResult.expiryMonth).length() == 1) {
                        resultExpDate = "0" + scanResult.expiryMonth + " / " + subExpYear;
                        etExpDate.setText(resultExpDate);
                    } else {
                        expMonth = scanResult.expiryMonth;
                        etExpDate.setText(resultExpDate);
                    }
                }

                if (scanResult.cvv != null) {
                    // Never log or display a CVV
                    resultDisplayStr += scanResult.cvv.length();
                    cvv = scanResult.cvv;
                    etCvv.setText(cvv);
                }

                if (scanResult.postalCode != null) {
                    resultDisplayStr += "Postal Code: " + scanResult.postalCode + "\n";
                }

                verifyCreditCardData();
            } else {
                resultDisplayStr = "Scan was canceled.";
                showMaterialDialogAlert(resultDisplayStr);
            }
            // do something with resultDisplayStr, maybe display it in a textView
            // resultTextView.setText(resultStr);
        }
        // else handle other activity results
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
    }

    public static void start(Context context) {
        context.startActivity(new Intent(context, PaymentMethodAct.class));
    }

    @Override
    protected int getToolbarTitle() {
        return R.string.add_payment_method;
    }

    //Luhn Test for Credit Card
    public static boolean luhnTest(String number) {
        int s1 = 0, s2 = 0;
        String reverse = new StringBuffer(number).reverse().toString();
        for (int i = 0; i < reverse.length(); i++) {
            int digit = Character.digit(reverse.charAt(i), 10);
            if (i % 2 == 0) {//this is for odd digits, they are 1-indexed in the algorithm
                s1 += digit;
            } else {//add 2 * digit for 0-4, add 2 * digit - 9 for 5-9
                s2 += 2 * digit;
                if (digit >= 5) {
                    s2 -= 9;
                }
            }
        }
        return (s1 + s2) % 10 == 0;
    }

    public void verifyCreditCardData() {
        cardNumber = etCardNumber.getText().toString();
        String expDate = etExpDate.getText().toString();
        String cvv = etCvv.getText().toString();
        cardNumber = cardNumber.replaceAll("\\s+", "");
        Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
        int calendarMonth = calendar.get(Calendar.MONTH) + 1;
        int calendarYear = calendar.get(Calendar.YEAR);
        int subCalendarYear = Integer.valueOf(String.valueOf(calendarYear).substring(2, 4));

        if (!TextUtils.isEmpty(cardNumber) && !TextUtils.isEmpty(expDate) && !TextUtils.isEmpty(cvv)) {
            if (expDate.length() != 7) {
                showMaterialDialogAlert(R.string.payment_expdate_alert);
            } else if (cvv.length() != 3) {
                showMaterialDialogAlert(R.string.payment_cvv_alert);
            } else if (cardNumber.length() != 16) {
                showMaterialDialogAlert(R.string.payment_cardnumber_alert);
            } else if (expDate.length() == 7) {
                expYear = Integer.parseInt((etExpDate.getText().toString().substring(5, 7)));
                expMonth = Integer.parseInt((etExpDate.getText().toString().substring(0, 2)));

                if (expYear < subCalendarYear && expMonth < calendarMonth) {
                    showMaterialDialogAlert(R.string.payment_expdate_alert);
                } else if (expYear < subCalendarYear) {
                    showMaterialDialogAlert(R.string.payment_expdate_alert);
                } else if (expYear == subCalendarYear && expMonth == calendarMonth) {
                    showMaterialDialogAlert(R.string.payment_expdate_alert);
                } else {
                    if (luhnTest(cardNumber)) {
                        expYear = Integer.parseInt("20" + (etExpDate.getText().toString().substring(5, 7)));
                        expYearString = String.valueOf(expYear);
                        expMonthString = String.valueOf(expMonth);
                        if (expMonthString.length() == 1)
                            expMonthString = "0" + expMonthString;
                        registerCreditCardToVeritrans();
                    } else {
                        showMaterialDialogAlert(R.string.payment_cardnumber_alert);
                    }
                }
            } else {
                expYear = Integer.parseInt("20" + (etExpDate.getText().toString().substring(5, 7)));
                expYearString = String.valueOf(expYear);
                expMonthString = String.valueOf(expMonth);
                if (expMonthString.length() == 1)
                    expMonthString = "0" + expMonthString;
                registerCreditCardToVeritrans();
            }

        } else {
            showMaterialDialogAlert(R.string.payment_alert);
        }
    }
}
