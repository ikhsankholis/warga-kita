package com.dycode.foodgasm.activity;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.dycode.foodgasm.R;
import com.dycode.foodgasm.model.ApiResponse;
import com.dycode.foodgasm.model.Order;
import com.dycode.foodgasm.model.OrderDetail;
import com.dycode.foodgasm.model.PatchOrder;
import com.dycode.foodgasm.model.Restaurant;
import com.dycode.foodgasm.model.Times;
import com.dycode.foodgasm.model.UberPrice;
import com.dycode.foodgasm.model.UberPrices;
import com.dycode.foodgasm.receiver.FoodgasmNotificationHandler;
import com.dycode.foodgasm.service.GCMService;
import com.dycode.foodgasm.utils.Constants;
import com.dycode.foodgasm.utils.FoodgasmDateUtil;
import com.dycode.foodgasm.utils.NetworkErrorUtil;
import com.dycode.foodgasm.utils.SPManager;
import com.dycode.foodgasm.utils.Utils;
import com.google.android.gms.gcm.GcmNetworkManager;
import com.google.android.gms.gcm.PeriodicTask;
import com.google.android.gms.gcm.Task;
import com.sromku.simple.fb.SimpleFacebook;
import com.sromku.simple.fb.entities.Feed;
import com.sromku.simple.fb.listeners.OnPublishListener;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by asus pc on 15/10/2015.
 */
public class BookOrderCompleteAct extends BaseActivity implements View.OnClickListener {

    private static final String TAG = "BookCompletedAct";
    public static final String FLAG = "FLAG";
    public static final String FLAG_CLOSE = "FLAG_CLOSE";
    public static final int FLAG_WAIT_CONFIRMATION = 0;
    public static final int FLAG_ORDER_DECLINED = 2;
    public static final int FLAG_ORDER_ACCEPTED = 3;
    public static final int FLAG_TAKE_AWAY = 4;
    public static final int FLAG_BOOKING_ACCEPTED = 5;
    public static final int FLAG_ORDER_EXPIRED = 6;
    public static final int FLAG_ORDER_CANCELLED = 7;
    public static final int FLAG_ORDER_PAYMENT_DECLINE = -2;


    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.linBookWaiting)
    LinearLayout linBookWaiting;
    @BindView(R.id.linBookCompleted)
    LinearLayout linBookCompleted;
    @BindView(R.id.linBookDeclined)
    LinearLayout linBookDeclined;
    @BindView(R.id.linBookOnlyAccepted)
    LinearLayout linBookOnlyAccepted;
    @BindView(R.id.linOrderExpired)
    LinearLayout linOrderExpired;
    @BindView(R.id.linOrderCancelled)
    LinearLayout linOrderCancelled;
    @BindView(R.id.ivLogo)
    ImageView ivLogo;
    @BindView(R.id.tvCode)
    TextView tvCode;
    @BindView(R.id.tvWaitingCode)
    TextView tvWaitingCode;
    @BindView(R.id.tvBookAcceptedCode)
    TextView tvBookAcceptedCode;
    @BindView(R.id.tvInfo)
    TextView tvInfo;
    @BindView(R.id.tvPaymentInfo)
    TextView tvPaymentInfo;
    @BindView(R.id.tvBookAcceptedInfo)
    TextView tvBookAcceptedInfo;
    @BindView(R.id.ivWaitingChat)
    ImageView ivWaitingChat;
    @BindView(R.id.ivWaitingEmail)
    ImageView ivWaitingEmail;
    @BindView(R.id.ivWaitingFB)
    ImageView ivWaitingFB;
    @BindView(R.id.ivChat)
    ImageView ivChat;
    @BindView(R.id.ivEmail)
    ImageView ivEmail;
    @BindView(R.id.ivFB)
    ImageView ivFB;
    @BindView(R.id.ivBookAcceptedChat)
    ImageView ivBookAcceptedChat;
    @BindView(R.id.ivBookAcceptedEmail)
    ImageView ivBookAcceptedEmail;
    @BindView(R.id.ivBookAcceptedFB)
    ImageView ivBookAcceptedFB;
    @BindView(R.id.ivOrderUber)
    ImageView ivOrderUber;
    @BindView(R.id.ivOrderGrabBike)
    ImageView ivOrderGrabBike;
    @BindView(R.id.linOrderTrans)
    LinearLayout linOrderTrans;
    @BindView(R.id.tvChooseCourier)
    TextView tvChooseCourier;
    @BindView(R.id.tvBookAcceptedDesc)
    TextView tvBookAcceptedDesc;
    @BindView(R.id.lTakeAway)
    LinearLayout lTakeAway;
    @BindView(R.id.tvTitleDeclined)
    TextView tvTitleDeclined;
    @BindView(R.id.tvDescDeclined)
    TextView tvDescDeclined;


    @BindView(R.id.btnCancel)
    Button btnCancel;
    @BindView(R.id.btnResend)
    Button btnResend;
    @BindView(R.id.img_close)
    ImageView imgClose;

    SimpleFacebook mSimpleFacebook;

    int flag, notif_id;
    String bookingCode, bookingDay, bookingTime, orderId, restoName, durationEstimate;
    List<UberPrice> productsList = new ArrayList<>();
    Restaurant restaurant;
    String[] uberData = new String[]{};
    boolean isTakeAway;

    String stack;

    @Override
    public void onResume() {
        super.onResume();
        mSimpleFacebook = SimpleFacebook.getInstance(this);
        registerReceiver(mMessageReceiver, new IntentFilter("unique_name"));
    }

    @Override
    protected void onStart() {
        super.onStart();
//        Log.d("stack order > ",stack);
//        if (stack.equals("1")) {
//
//        } else {
//            GCMService.cancelAll(this);
//            Intent intent = new Intent(BookOrderCompleteAct.this, MainAct.class);
//            startActivity(intent);
//            finish();
//            overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
//        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        registerReceiver(mMessageReceiver, new IntentFilter("unique_name"));
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        onChangeFlagLayout(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_success);
        ButterKnife.bind(this);

        // Obtain the shared Tracker instance.
        getTracker("Order Complete");

        //mToolbar.setNavigationIcon(R.mipmap.ic_close);
        setSupportActionBar(mToolbar);

        onChangeFlagLayout(getIntent());
        registerReceiver(this.mMessageReceiver, new IntentFilter("unique_name"));

        if (SPManager.getString(BookOrderCompleteAct.this, "stack") != null) {
            if (SPManager.getString(BookOrderCompleteAct.this, "stack").equals("1")) {
                stack = "1";
                GcmNetworkManager gcmNetworkManager = GcmNetworkManager.getInstance(this);
                Task task = new PeriodicTask.Builder()
                        .setService(GCMService.class)
                        .setPeriod(60)
                        .setFlex(1)
                        .setTag(TAG)
                        .setPersisted(true)
                        .build();
                gcmNetworkManager.schedule(task);
            } else {
                stack = "0";
            }
        }

        isTakeAway = getIntent().getBooleanExtra("isTakeAway", isTakeAway);


    }

    void getOrderInfo() {
        showProgressDialog("Loading...");
        restClient.getApiService().getOrderDetail(orderId, Utils.getToken(mActivity).getAccessToken(), new Callback<ApiResponse<OrderDetail>>() {
            @Override
            public void success(ApiResponse<OrderDetail> orderDetailApiResponse, Response response) {
                progressDialog.dismiss();
                OrderDetail orderDetail = orderDetailApiResponse.getData();
                isTakeAway = orderDetail.getPreorder().getIsOrder() && !orderDetail.getBooking().getIsBooking();
                Calendar calendar = Calendar.getInstance();
                Date today = calendar.getTime();
                String bookDay = String.valueOf(FoodgasmDateUtil.getStringDate(orderDetail.getVisitDate()));
                DateFormat dateFormat = new SimpleDateFormat("MMM dd, yyy", java.util.Locale.getDefault());
                String todayAsString = dateFormat.format(today);

                bookingCode = orderDetail.getCode();
                if (!bookDay.equals(todayAsString)) {
                    bookingDay = "tomorrow";
                } else {
                    bookingDay = "today";
                }
                restoName = orderDetail.getResto().getName();
                bookingTime = String.valueOf(FoodgasmDateUtil.getHourAndMinute(orderDetail.getVisitDate()));
                String info = String.format(getString(R.string.table_ready), bookingDay, bookingTime);
                tvBookAcceptedInfo.setText(info);
                tvBookAcceptedCode.setText(orderDetail.getCode());
                if (isTakeAway) {
                    tvChooseCourier.setVisibility(View.GONE);
                    linOrderTrans.setVisibility(View.GONE);
                    tvInfo.setVisibility(View.GONE);
                    lTakeAway.setVisibility(View.VISIBLE);
                }


            }

            @Override
            public void failure(RetrofitError error) {
                progressDialog.dismiss();
                showAlertDialog("Failed", error.getMessage());
            }
        });
    }

    @Override
    public void onBackPressed() {
        SPManager.saveString(BookOrderCompleteAct.this, "stack", "0");
        SPManager.saveString(BookOrderCompleteAct.this, FLAG_CLOSE, "close");
        GCMService.cancelAll(this);
        Intent intent = new Intent(BookOrderCompleteAct.this, MainAct.class);
        intent.putExtra(FLAG_CLOSE, "close");
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        if (v == ivWaitingChat) {
            shareToChat();
        } else if (v == ivChat) {
            shareToChat();
        } else if (v == ivBookAcceptedChat) {
            shareToChat();
        } else if (v == ivWaitingEmail) {
            shareToEmail();
        } else if (v == ivEmail) {
            shareToEmail();
        } else if (v == ivBookAcceptedEmail) {
            shareToEmail();
        } else if (v == ivWaitingFB) {
            shareToFb();
        } else if (v == ivFB) {
            shareToFb();
        } else if (v == ivBookAcceptedFB) {
            shareToFb();
        } else if (v == ivOrderUber) {
            getPrices();
        } else if (v == ivOrderGrabBike) {
            openGrabApp();
        } else if (v == btnCancel) {
            onBackPressed();
        } else if (v == btnResend) {
            patchOrderResend(orderId);
        } else if (v == imgClose) {
            onBackPressed();
        }
    }

    OnPublishListener onPublishListener = new OnPublishListener() {

        @Override
        public void onComplete(String postId) {
            Log.i(TAG, "Published successfully. The new post id = " + postId);

        }
                /*
                * You can override other methods here:
                * onThinking(), onFail(String reason), onException(Throwable throwable)
                */

        @Override
        public void onFail(String reason) {
            super.onFail(reason);
            Log.i(TAG, "Failed to publish " + reason);
        }

        @Override
        public void onException(Throwable throwable) {
            super.onException(throwable);
            Log.i(TAG, "Failed to publish " + throwable.getMessage());
        }
    };

    void shareToFb() {
        Feed feed = new Feed.Builder()
                .setMessage("Hi! I've just booked an order at " + restoName + ". My booking code is " + bookingCode)
                .setName("FoodGasm")
                .setCaption(getResources().getString(R.string.highlight))
                .setDescription(getResources().getString(R.string.highlight))
                .setPicture("http://s9.postimg.org/7c7qd1n9n/ic_launcher.png")
                .setLink("http://www.foodgasm.id/")
                .build();

        mSimpleFacebook.publish(feed, true, onPublishListener);
    }

    void shareToEmail() {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:")); // only email apps should handle this
        intent.putExtra(Intent.EXTRA_TEXT, "Hi! I've just booked an order at " + restoName + ". My booking code is " + bookingCode);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    void shareToChat() {
        Uri uri = Uri.parse("smsto:");
        Intent intent = new Intent(Intent.ACTION_SENDTO, uri);
        intent.putExtra("sms_body", "Hi! I've just booked an order at " + restoName + ". My booking code is " + bookingCode);
        startActivity(intent);
    }

    void openGrabApp() {
        if (isAppInstalled("com.grabtaxi.passenger")) {
            //Your Code
            openApp(mActivity, "com.grabtaxi.passenger");
        } else {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.grabtaxi.passenger")));
        }
    }

    void getPrices() {
        showProgressDialog("Loading data...");
        restClientUberProduct.getApiService().getUberEstimatesPrice(Float.parseFloat(Utils.getRestoLatString(mActivity)), Float.parseFloat(Utils.getLngString(mActivity)), Float.parseFloat(Utils.getLatString(mActivity)), Float.parseFloat(Utils.getLngString(mActivity)), new Callback<UberPrices>() {

            @Override
            public void success(UberPrices uberPrices, Response response) {
                productsList.clear();
                productsList.addAll(uberPrices.getPrices());
                uberData = new String[productsList.size()];
                for (int pos = 0; pos < productsList.size(); pos++) {
                    getEstimates(pos);
                }

            }

            @Override
            public void failure(RetrofitError error) {
                if (progressDialog != null) progressDialog.dismiss();
                showToast("No Internet Connection");
            }
        });
    }

    void getEstimates(final int pos) {
        restClientUberProduct.getApiService().getUberEstimatesTime(Float.parseFloat(Utils.getLatString(mActivity)), Float.parseFloat(Utils.getLngString(mActivity)), Constants.Uber.CLIENT_ID, productsList.get(pos).getProductId(), new Callback<Times>() {

            @Override
            public void success(Times times, Response response) {
                if (progressDialog != null) progressDialog.dismiss();
                productsList.get(pos).setTimes(times);

                if (productsList.get(pos).getTimes() != null) {
                    if (productsList.get(pos).getTimes().getTimes().size() > 0) {
                        int durationInMin;
                        durationInMin = productsList.get(pos).getTimes().getTimes().get(0).getEstimate() / 60;
                        durationEstimate = String.valueOf(durationInMin);
                    } else durationEstimate = "";
                } else durationEstimate = "";

                String displayData = String.format(getString(R.string.uber_data), productsList.get(pos).getDisplayName(), productsList.get(pos).getEstimate(), durationEstimate);
                uberData[pos] = displayData;

                if (pos == productsList.size() - 1) {
                    new MaterialDialog.Builder(mActivity)
                            .items(uberData)
                            .itemsCallbackSingleChoice(-1, new MaterialDialog.ListCallbackSingleChoice() {
                                @Override
                                public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                                    try {
                                        PackageManager pm = mActivity.getPackageManager();
                                        pm.getPackageInfo("com.ubercab", PackageManager.GET_ACTIVITIES);
                                        String uri = "uber://?client_id=" + Constants.Uber.CLIENT_ID + "&action=setPickup&pickup[latitude]=" + restaurant.getLocation().getCoordinates().get(1) + "&pickup[longitude]="
                                                + restaurant.getLocation().getCoordinates().get(0) + "&dropoff[latitude]=" + Double.parseDouble(Utils.getLatString(mActivity)) +
                                                "&dropoff[longitude]=" + Double.parseDouble(Utils.getLngString(mActivity)) + "&product_id=" + productsList.get(pos).getProductId();
                                        Intent intent = new Intent(Intent.ACTION_VIEW);
                                        intent.setData(Uri.parse(uri));
                                        startActivity(intent);
                                    } catch (PackageManager.NameNotFoundException e) {
                                        // No Uber app! Open mobile website.
                                        String url = "https://m.uber.com/sign-up?client_id=" + Constants.Uber.CLIENT_ID + "&product_id=" + productsList.get(pos).getProductId();
                                        Intent i = new Intent(Intent.ACTION_VIEW);
                                        i.setData(Uri.parse(url));
                                        startActivity(i);
                                    }
                                    return true;
                                }
                            })
                            .show();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (progressDialog != null) progressDialog.dismiss();
                showToast("No Internet Connection");
            }
        });
    }

    public static boolean openApp(Activity activity, String packageName) {
        PackageManager manager = activity.getPackageManager();
        Intent i = manager.getLaunchIntentForPackage(packageName);
        if (i == null) {
            return false;
            //throw new PackageManager.NameNotFoundException();
        }
        i.addCategory(Intent.CATEGORY_LAUNCHER);
        activity.startActivity(i);
        return true;
    }

    private boolean isAppInstalled(String packageName) {
        PackageManager pm = getPackageManager();
        boolean installed = false;
        try {
            pm.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES);
            installed = true;
        } catch (PackageManager.NameNotFoundException e) {
            installed = false;
        }
        return installed;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        mSimpleFacebook.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void onChangeFlagLayout(Intent intent) {
        flag = intent.getExtras().getInt(FLAG);
        Log.d("CHECK FLAG > ", String.valueOf(flag));
        if (flag == FLAG_WAIT_CONFIRMATION) {
            linBookWaiting.setVisibility(View.VISIBLE);
            restoName = getIntent().getExtras().getString("restoName");
            bookingCode = getIntent().getExtras().getString("bookingCode");
            tvWaitingCode.setText(bookingCode);
            ivWaitingChat.setOnClickListener(this);
            ivWaitingEmail.setOnClickListener(this);
            ivWaitingFB.setOnClickListener(this);
            imgClose.setOnClickListener(this);

        } else if (flag == FLAG_TAKE_AWAY) {
            restaurant = getIntent().getExtras().getParcelable(Restaurant.RESTAURANT);
            linBookWaiting.setVisibility(View.VISIBLE);
            restoName = getIntent().getExtras().getString("restoName");
            bookingCode = getIntent().getExtras().getString("bookingCode");
            tvWaitingCode.setText(bookingCode);
            ivWaitingChat.setOnClickListener(this);
            ivWaitingEmail.setOnClickListener(this);
            ivWaitingFB.setOnClickListener(this);
            imgClose.setOnClickListener(this);
            tvBookAcceptedDesc.setText(getString(R.string.take_away_order_succses));
            SPManager.saveString(this, Constants.RestaurantLocation.LATITUDE, Double.toString(restaurant.getLocation().getCoordinates().get(1)));
            SPManager.saveString(this, Constants.RestaurantLocation.LONGITUDE, Double.toString(restaurant.getLocation().getCoordinates().get(0)));

        } else if (flag == FLAG_ORDER_DECLINED) {
            notif_id = getIntent().getExtras().getInt("id_notifications");
            clearNotif(notif_id);
            linBookWaiting.setVisibility(View.GONE);
            linBookDeclined.setVisibility(View.VISIBLE);
            imgClose.setOnClickListener(this);
            imgClose.setVisibility(View.VISIBLE);
            lTakeAway.setVisibility(View.VISIBLE);


        } else if (flag == FLAG_ORDER_PAYMENT_DECLINE) {
            notif_id = getIntent().getExtras().getInt("id_notifications");
            clearNotif(notif_id);
            linBookWaiting.setVisibility(View.GONE);
            linBookDeclined.setVisibility(View.VISIBLE);
            imgClose.setOnClickListener(this);
            imgClose.setVisibility(View.VISIBLE);
            lTakeAway.setVisibility(View.VISIBLE);
            tvTitleDeclined.setText(R.string.payment_decline_confirmation);
            tvDescDeclined.setText(null);

        } else if (flag == FLAG_ORDER_ACCEPTED) {
            orderId = getIntent().getExtras().getString(FoodgasmNotificationHandler.NOTIF_ORDER_ID);
            linBookWaiting.setVisibility(View.GONE);
            ivLogo.setVisibility(View.VISIBLE);
            linBookCompleted.setVisibility(View.VISIBLE);
            tvPaymentInfo.setVisibility(View.VISIBLE);
            ivChat.setOnClickListener(this);
            ivEmail.setOnClickListener(this);
            ivFB.setOnClickListener(this);
            ivOrderUber.setOnClickListener(this);
            ivOrderGrabBike.setOnClickListener(this);
            bookingDay = getIntent().getExtras().getString("bookingDay");
            bookingTime = getIntent().getExtras().getString("bookingTime");
            bookingCode = getIntent().getExtras().getString("bookingCode");
            String info = String.format(getString(R.string.table_ready), bookingDay, bookingTime);
            tvInfo.setText(info);
            tvCode.setText(bookingCode);
            isTakeAway = getIntent().getBooleanExtra("isTakeAway", isTakeAway);
            if (isTakeAway) {
//                tvChooseCourier.setVisibility(View.VISIBLE);
//                linOrderTrans.setVisibility(View.VISIBLE);
                tvChooseCourier.setVisibility(View.GONE);
                linOrderTrans.setVisibility(View.GONE);
                lTakeAway.setVisibility(View.VISIBLE);
                tvInfo.setVisibility(View.GONE);
            }
            restoName = getIntent().getExtras().getString("restoName");
            imgClose.setOnClickListener(this);
            getOrderInfo();

        } else if (flag == FLAG_BOOKING_ACCEPTED) {
            notif_id = getIntent().getExtras().getInt("id_notifications");
            clearNotif(notif_id);
            linBookWaiting.setVisibility(View.GONE);
            linBookOnlyAccepted.setVisibility(View.VISIBLE);
            tvPaymentInfo.setVisibility(View.GONE);
            ivBookAcceptedChat.setOnClickListener(this);
            ivBookAcceptedEmail.setOnClickListener(this);
            ivBookAcceptedFB.setOnClickListener(this);
            orderId = getIntent().getExtras().getString("orderId");
            imgClose.setOnClickListener(this);
            getOrderInfo();

        } else if (flag == FLAG_ORDER_EXPIRED) {
            notif_id = getIntent().getExtras().getInt("id_notifications");
            orderId = getIntent().getExtras().getString(FoodgasmNotificationHandler.NOTIF_ORDER_ID);
            clearNotif(notif_id);
            linBookWaiting.setVisibility(View.GONE);
            linOrderExpired.setVisibility(View.VISIBLE);
            btnCancel.setOnClickListener(this);
            btnResend.setOnClickListener(this);
            imgClose.setOnClickListener(this);
            imgClose.setVisibility(View.VISIBLE);

        } else if (flag == FLAG_ORDER_CANCELLED) {
            linBookWaiting.setVisibility(View.GONE);
            linBookDeclined.setVisibility(View.GONE);
            linOrderCancelled.setVisibility(View.VISIBLE);
            imgClose.setOnClickListener(this);
            imgClose.setVisibility(View.VISIBLE);
        }
    }

    void patchOrderResend(String orderId) {
        showProgressDialog("Loading...");
        restClient.getApiService().patchOrder(orderId, Utils.getToken(mActivity).getAccessToken(), new PatchOrder("new", null), new Callback<Order>() {

            @Override
            public void success(Order order, Response response) {
                progressDialog.dismiss();
                new MaterialDialog.Builder(mActivity)
                        .content(R.string.order_resend)
                        .positiveText(R.string.ok)
                        .onPositive((dialog, which) -> {
                            //TODO call cancel order API
                            onBackPressed();
                        })
                        .cancelable(false)
                        .build()
                        .show();
            }

            @Override
            public void failure(RetrofitError error) {
                progressDialog.dismiss();
                showMaterialDialogAlert(NetworkErrorUtil.getErrorMessage(error));
            }
        });
    }

    //register your activity onResume()
    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(mMessageReceiver);
    }

    private void updateUI(Intent intent, String type) {
        Intent i = getIntent();
//        finish();
//        startActivity(i);
//        finish();
        SPManager.saveString(this, Constants.Inbox.STATUS_INBOX, type);
        //Open BookOrderComplete with accepted booking message
        GCMService.cancelAll(this);
//            i = new Intent(mContext, MainAct.class);
        i = new Intent(this, MainAct.class);
        i.putExtra(BookOrderCompleteAct.FLAG, BookOrderCompleteAct.FLAG_BOOKING_ACCEPTED);

//        Log.d("ready > ","data ");
//        SPManager.saveString(this, Constants.Inbox.STATUS_INBOX, type);
//        //Open BookOrderComplete with accepted booking message
//        GCMService.cancelAll(this);
//        Intent i = new Intent(BookOrderCompleteAct.this, MainAct.class);
//        startActivity(i);
//        finish();
//        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String message = intent.getStringExtra("message");
            String type = intent.getStringExtra("type");
            Log.d("message >> ", message + " Broadcast >> ");
            stack = message;
            updateUI(intent, type);
        }
    };

}
