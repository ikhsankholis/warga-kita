package com.dycode.foodgasm.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.dycode.foodgasm.R;
import com.dycode.foodgasm.model.ApiResponse;
import com.dycode.foodgasm.model.DokuWallet;
import com.dycode.foodgasm.model.DokuWalletRegData;
import com.dycode.foodgasm.model.DokuWalletRegister;
import com.dycode.foodgasm.model.DokuWalletResponseNew;
import com.dycode.foodgasm.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

public class DokuWalletLoginCode extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.tvTitle)
    TextView tvTitle;

    @BindView(R.id.layoutLogin)
    LinearLayout layoutLogin;
    @BindView(R.id.btnSubmit)
    Button btnSubmit;
    @BindView(R.id.codeValue)
    EditText codeValue;

    List<DokuWalletRegData> listData;
    private String orderId;
    private String accountId;
    private String walletStatus;
    private String daftarDokuWallet;
    private String emailData;
    private String passwordData;
    private String codeData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.doku_wallet_login_code);
        ButterKnife.bind(this);
        mToolbar.setNavigationIcon(R.mipmap.ic_back);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        tvTitle.setText(R.string.dokuwallet);

        listData = new ArrayList<DokuWalletRegData>();
        emailData = getIntent().getExtras().getString("name", "email");
        passwordData = getIntent().getExtras().getString("email", "email");

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (!daftarDokuWallet.equals("0")) {
//                    PostDokuWalletRegister(emailValue.getText().toString(), passwordValue.getText().toString(), codeValue.getText().toString());
//                } else {
//                    DokuWalletRegister(emailValue.getText().toString(), passwordValue.getText().toString(), codeValue.getText().toString());
//                }
                if (codeValue.getText().length() > 4) {
                    DokuWalletRegister(emailData, passwordData, codeValue.getText().toString());
                } else {
                    showToast("Silahkan Lengkapi Code");
                }

            }
        });
    }

    private void PostDokuWalletRegister(String name, String email, String code) {
        showProgressDialog("Submitting..");
        restClient.getApiService().postRegisterDokuWallet(Utils.getToken(DokuWalletLoginCode.this).getAccessToken(), new DokuWalletRegister(name, email, code), new Callback<ApiResponse<DokuWalletRegData>>() {
            @Override
            public void success(ApiResponse<DokuWalletRegData> userApiResponse, Response response) {
                progressDialog.dismiss();
                JSONObject json;

                PostDokuWallet(orderId, userApiResponse.getData().getDokuId());
            }

            @Override
            public void failure(RetrofitError error) {
                progressDialog.dismiss();
                new MaterialDialog.Builder(DokuWalletLoginCode.this)
                        .title(R.string.alert)
                        .content(error.getMessage())
                        .positiveText(R.string.ok)
                        .cancelable(false)
                        .show();
            }
        });
    }

    private void PostDokuWallet(String orderid, String id) {
        showProgressDialog("Submitting..");
        restClient.getApiService().postDokuResponseWalletNew(orderid, Utils.getToken(DokuWalletLoginCode.this).getAccessToken(), new DokuWallet(id), new Callback<ApiResponse<DokuWalletResponseNew>>() {
            @Override
            public void success(ApiResponse<DokuWalletResponseNew> userApiResponse, Response response) {
                progressDialog.dismiss();

                Log.d("cek status doku wallet -->", userApiResponse.getData().getResponseCode());

                String status_code = userApiResponse.getData().getResponseCode();
                if (!userApiResponse.getData().getResponseCode().equals("0000")) {
                    new MaterialDialog.Builder(DokuWalletLoginCode.this)
                            .title(R.string.alert)
                            .content("Your Balance is not Enough")
                            .positiveText(R.string.ok)
                            .cancelable(false)
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    finish();
                                }
                            })
                            .show();
                } else {
                    Intent intent = new Intent(getApplicationContext(), ResultPaymentAct.class);
                    intent.putExtra("data", status_code);
                    startActivity(intent);
                    finish();
                }

            }

            @Override
            public void failure(RetrofitError error) {
                progressDialog.dismiss();
                new MaterialDialog.Builder(DokuWalletLoginCode.this)
                        .title(R.string.alert)
                        .content(error.getMessage())
                        .positiveText(R.string.ok)
                        .cancelable(false)
                        .show();
            }
        });
    }


    private void DokuWalletRegister(String name, String email, String code) {
        showProgressDialog("Submitting..");
        restClient.getApiService().postRegisterDokuWallet(Utils.getToken(DokuWalletLoginCode.this).getAccessToken(), new DokuWalletRegister(name, email, code), new Callback<ApiResponse<DokuWalletRegData>>() {
            @Override
            public void success(ApiResponse<DokuWalletRegData> userApiResponse, Response response) {
                progressDialog.dismiss();
                showToast("Pendaftaran Doku Wallet Sukses");
                setResult(2);
                finish();
            }

            @Override
            public void failure(RetrofitError error) {
                progressDialog.dismiss();

                String json = new String(((TypedByteArray) error.getResponse().getBody()).getBytes());
                Log.v("failure", json.toString());
                try {
                    JSONObject data = new JSONObject(json.toString());
                    String meta = data.getString("meta");
                    JSONObject messageErrorObj = new JSONObject(meta);
                    String messageError = messageErrorObj.getString("message");
                    Log.v("failure message", messageError);

                    new MaterialDialog.Builder(DokuWalletLoginCode.this)
                            .title(R.string.alert)
                            .content(messageError)
                            .positiveText(R.string.ok)
                            .cancelable(false)
                            .show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
    }

}
