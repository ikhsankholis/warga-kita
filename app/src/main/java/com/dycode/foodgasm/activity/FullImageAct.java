package com.dycode.foodgasm.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.dycode.foodgasm.R;
import com.dycode.foodgasm.adapters.FullImageViewAdapter;
import com.dycode.foodgasm.model.ApiResponseList;
import com.dycode.foodgasm.model.Gallery;
import com.dycode.foodgasm.model.Restaurant;
import com.dycode.foodgasm.rest.RestClient;
import com.dycode.foodgasm.utils.Utils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import uk.co.senab.photoview.PhotoViewAttacher;

/**
 * Created by asus pc on 13/10/2015.
 */
public class FullImageAct extends BaseActivity {

    PhotoViewAttacher mAttacher;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.ivFullImage)
    ImageView ivFullImage;
    @BindView(R.id.viewpager)
    ViewPager mViewPager;

    FullImageViewAdapter mAdapter;
    List<Gallery> galleryList = new ArrayList<Gallery>();
    String restaurant;
    int img;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_image);
        ButterKnife.bind(this);

        // Obtain the shared Tracker instance.
        getTracker("Photo Gallery");

        mToolbar.setNavigationIcon(R.mipmap.ic_navigation_arrow_back);
        setSupportActionBar(mToolbar);

        img = getIntent().getExtras().getInt("imgPosition");
        restaurant = getIntent().getExtras().getString(Restaurant.RESTAURANT);
        mAdapter = new FullImageViewAdapter(mActivity, galleryList);
        mViewPager.setAdapter(mAdapter);
        getGalleryList();
    }

    void getGalleryList() {
        restClient = RestClient.getInstance();
        restClient.getApiService().getListGallery( restaurant, new Callback<ApiResponseList<Gallery>>() {
            @Override
            public void success(ApiResponseList<Gallery> galleryApiResponseList, Response response) {
                if (galleryApiResponseList.getMeta().getCode() == 200) {
                    galleryList.clear();
                    galleryList.addAll(galleryApiResponseList.getData());
                    mAdapter.notifyDataSetChanged();
                    mViewPager.setCurrentItem(img);
                } else {
                    showAlertDialog("Failed", galleryApiResponseList.getMeta().getMessage());
                }
            }

            @Override
            public void failure(RetrofitError error) {
                showAlertDialog("Failed", error.getMessage());
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }
}
