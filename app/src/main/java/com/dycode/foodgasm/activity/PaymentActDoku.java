package com.dycode.foodgasm.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.dycode.foodgasm.FoodGasmApp;
import com.dycode.foodgasm.R;
import com.dycode.foodgasm.dao.CreditCard;
import com.dycode.foodgasm.dao.DaoSession;
import com.dycode.foodgasm.model.ApiResponse;
import com.dycode.foodgasm.model.Order;
import com.dycode.foodgasm.model.OrderDetail;
import com.dycode.foodgasm.model.PatchOrder;
import com.dycode.foodgasm.utils.Constants;
import com.dycode.foodgasm.utils.FoodgasmDateUtil;
import com.dycode.foodgasm.utils.NetworkErrorUtil;
import com.dycode.foodgasm.utils.SPManager;
import com.dycode.foodgasm.utils.Utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by asus pc on 22/02/2016.
 */
public class PaymentActDoku extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.tvMenu)
    TextView tvMenu;
    @BindView(R.id.tvDone)
    TextView tvDone;
    @BindView(R.id.wvPayment)
    WebView wvPayment;


    double total;
    String orderId;
    int notif_id;

    FoodGasmApp foodGasmApp;
    DaoSession daoSession;

    List<CreditCard> creditCard;
    String cvv, bank, tokenId, bookingDay, bookingTime, bookingCode, restoName;
    boolean isTakeAway = false;
    String urlDoku = "https://api.foodgasm.id/api/v1/payment/doku/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_doku);
        ButterKnife.bind(this);

        foodGasmApp = (FoodGasmApp) getApplication();
        daoSession = foodGasmApp.getDaoSession();
        notif_id = getIntent().getExtras().getInt("id_notifications");
        clearNotif(notif_id);

        tvTitle.setText(getResources().getString(R.string.payment));
        tvMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        tvDone.setVisibility(View.GONE);

        orderId = getIntent().getExtras().getString("orderId");

        wvPayment.getSettings().setJavaScriptEnabled(true);
        wvPayment.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                    case MotionEvent.ACTION_UP:
                        if (!v.hasFocus()) {
                            v.requestFocus();
                        }
                        break;
                }
                return false;
            }
        });
        wvPayment.setWebChromeClient(new WebChromeClient());
        wvPayment.setWebViewClient(new DokuWebViewClient());
        Log.d("uri cek >", urlDoku + "id=" + orderId + "/access_token==" + Utils.getToken(mActivity).getAccessToken());
        wvPayment.loadUrl(urlDoku + orderId + "?access_token=" + Utils.getToken(mActivity).getAccessToken());

//        wvPayment.loadUrl("http://ditto-api.foodgasm.id/doku/57ccf7271f1cc11015ab0229");

        getOrderInfo();

        tvDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mActivity, BookOrderCompleteAct.class);
                intent.putExtra(BookOrderCompleteAct.FLAG, BookOrderCompleteAct.FLAG_ORDER_ACCEPTED);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("bookingTime", bookingTime);
                intent.putExtra("bookingDay", bookingDay);
                intent.putExtra("bookingCode", bookingCode);
                intent.putExtra("isTakeAway", isTakeAway);
                intent.putExtra("restoName", restoName);
                startActivity(intent);
                overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
            }
        });
    }

    void getOrderInfo() {
        orderId = getIntent().getExtras().getString("orderId");
        showProgressDialog("Loading...");
        restClient.getApiService().getOrderDetail(orderId, Utils.getToken(mActivity).getAccessToken(), new Callback<ApiResponse<OrderDetail>>() {
            @Override
            public void success(ApiResponse<OrderDetail> orderDetailApiResponse, Response response) {
                progressDialog.dismiss();
                OrderDetail orderDetail = orderDetailApiResponse.getData();
                total = orderDetail.getPrice().getTotal();
                bookingCode = orderDetail.getCode();
                restoName = orderDetail.getResto().getName();

                Calendar calendar = Calendar.getInstance();
                Date today = calendar.getTime();
                String bookDay = String.valueOf(FoodgasmDateUtil.getStringDate(orderDetail.getVisitDate()));
                DateFormat dateFormat = new SimpleDateFormat("MMM dd, yyy", java.util.Locale.getDefault());
                String todayAsString = dateFormat.format(today);
                if (!bookDay.equals(todayAsString)) {
                    bookingDay = "tomorrow";
                } else {
                    bookingDay = "today";
                }
                bookingTime = String.valueOf(FoodgasmDateUtil.getHourAndMinute(orderDetail.getVisitDate()));

                if (orderDetail.getPreorder().getIsOrder() && !orderDetail.getBooking().getIsBooking())
                    isTakeAway = true;


            }

            @Override
            public void failure(RetrofitError error) {
                progressDialog.dismiss();
                showAlertDialog("Failed", error.getMessage());
            }
        });
    }

    private class DokuWebViewClient extends WebViewClient {

        public DokuWebViewClient() {
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            Log.d("VtLog", url);

            if (url.startsWith("https://api.foodgasm.id/callback/doku/redirect")) {
                //send token to server
                tvDone.setVisibility(View.VISIBLE);
                tvMenu.setVisibility(View.GONE);
            } else if (url.startsWith(Constants.getPaymentApiUrl() + "/redirect/") || url.contains("3dsecure")) {
                /* Do nothing */
            }
        }
    }

    void patchOrderCancel(String orderId) {
        showProgressDialog("Loading...");
        restClient.getApiService().patchOrder(orderId, Utils.getToken(mActivity).getAccessToken(), new PatchOrder("cancel", "User cancelled the order"), new Callback<Order>() {

            @Override
            public void success(Order order, Response response) {
                progressDialog.dismiss();
                new MaterialDialog.Builder(mActivity)
                        .content(R.string.order_cancelled)
                        .positiveText(R.string.ok)
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(MaterialDialog dialog, DialogAction which) {
                                //TODO call cancel order API
                                SPManager.saveString(getApplicationContext(), Constants.Inbox.STATUS_INBOX, null);

                                Intent intent = new Intent(PaymentActDoku.this, MainAct.class);
                                finish();
                                startActivity(intent);
                                overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                            }
                        })
                        .cancelable(false)
                        .build()
                        .show();
            }

            @Override
            public void failure(RetrofitError error) {
                progressDialog.dismiss();
                showMaterialDialogAlert(NetworkErrorUtil.getErrorMessage(error));
            }
        });
    }

    @Override
    public void onBackPressed() {
        new MaterialDialog.Builder(mActivity)
                .content(R.string.cancel_order)
                .positiveText(R.string.yes)
                .negativeText(R.string.no)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {
                        //TODO call cancel order API
                        patchOrderCancel(orderId);
                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .build()
                .show();
    }
}
