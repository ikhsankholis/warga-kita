package com.dycode.foodgasm.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.dycode.foodgasm.R;
import com.dycode.foodgasm.model.ApiResponseList;
import com.dycode.foodgasm.model.Gallery;
import com.dycode.foodgasm.model.Restaurant;
import com.dycode.foodgasm.rest.RestClient;
import com.dycode.foodgasm.utils.Utils;
import com.dycode.foodgasm.widget.DensityPixelUtil;
import com.dycode.foodgasm.widget.SpacesGridItemDecoration;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by asus pc on 12/10/2015.
 */
public class GalleryAct extends BaseActivity {
    RecyclerView mRecyclerView;
    RecyclerView.Adapter mAdapter;

    List<Gallery> galleryList = new ArrayList<>();
    String restaurant;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.tvTitle)
    TextView tvTitle;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);
        ButterKnife.bind(this);

        // Obtain the shared Tracker instance.
        getTracker("Gallery");

        restaurant = getIntent().getExtras().getString(Restaurant.RESTAURANT);
        setToolbar(true);
        getGalleryList();
        setUpRecyclerView();

    }

    void getGalleryList() {
        showProgressDialog("Getting Gallery List..");
        restClient = RestClient.getInstance();
        restClient.getApiService().getListGallery(restaurant, new Callback<ApiResponseList<Gallery>>() {
            @Override
            public void success(ApiResponseList<Gallery> galleryApiResponseList, Response response) {
                progressDialog.dismiss();
                if (galleryApiResponseList.getMeta().getCode() == 200) {
                    galleryList.clear();
                    galleryList.addAll(galleryApiResponseList.getData());
                    mAdapter.notifyDataSetChanged();
                } else {
                    showAlertDialog("Failed", galleryApiResponseList.getMeta().getMessage());
                }
            }

            @Override
            public void failure(RetrofitError error) {
                progressDialog.dismiss();
                showAlertDialog("Failed", error.getMessage());
            }
        });
    }

    private void setUpRecyclerView() {
        mRecyclerView = (RecyclerView) findViewById(R.id.rvGallery);
        mRecyclerView.setHasFixedSize(true);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(mActivity, 3);
        mRecyclerView.setLayoutManager(gridLayoutManager);
        mRecyclerView.addItemDecoration(new SpacesGridItemDecoration(Math.round(DensityPixelUtil.convertDpToPixel(4, mActivity))));
        mAdapter = new GalleryAdapter();
        mRecyclerView.setAdapter(mAdapter);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected int getToolbarTitle() {
        return R.string.gallery;
    }

    class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.ViewHolder> {

        @Override
        public GalleryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.grid_item_gallery, parent, false);
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(final GalleryAdapter.ViewHolder holder, final int position) {
            Picasso.with(mActivity)
                    .load(galleryList.get(position).getPic())
                    .networkPolicy(NetworkPolicy.OFFLINE)
                    .into(holder.imgThumbnail, new com.squareup.picasso.Callback() {
                        @Override
                        public void onSuccess() {

                        }

                        @Override
                        public void onError() {
                            //Try again online if cache failed
                            Picasso.with(mActivity)
                                    .load(galleryList.get(position).getPic())
                                    .error(R.mipmap.img_placeholder_image_load_error)
                                    .into(holder.imgThumbnail);
                        }
                    });

            holder.imgThumbnail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(GalleryAct.this, FullImageAct.class);
                    i.putExtra("imgPosition", position);
                    i.putExtra(Restaurant.RESTAURANT, restaurant);
                    startActivity(i);
                    overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                }
            });

        }

        @Override
        public int getItemCount() {
            return galleryList.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {

            public ImageView imgThumbnail;

            public ViewHolder(View itemView) {
                super(itemView);
                imgThumbnail = (ImageView) itemView.findViewById(R.id.imgThumbnail);

            }
        }

    }


}
