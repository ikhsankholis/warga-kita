package com.dycode.foodgasm.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.dycode.foodgasm.FoodGasmApp;
import com.dycode.foodgasm.R;
import com.dycode.foodgasm.model.ApiResponse;
import com.dycode.foodgasm.model.PostRegister;
import com.dycode.foodgasm.model.Profile;
import com.dycode.foodgasm.model.Register;
import com.dycode.foodgasm.utils.NetworkErrorUtil;
import com.dycode.foodgasm.utils.SPManager;
import com.dycode.foodgasm.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Okta Dwi Priatna on 08/10/2015.
 */
public class RegistrationAct extends BaseActivity {

    public static final String FLAG = "FLAG";
    public static final int FLAG_REGISTER_FROM_FB = 0;
    public static final int FLAG_DEFAULT_REGISTER = 1;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.etFullName)
    EditText etFullName;
    @BindView(R.id.etUserName)
    EditText etUserName;
    @BindView(R.id.etEmail)
    EditText etEmail;
    @BindView(R.id.etContact)
    EditText etContact;
    @BindView(R.id.etPassword)
    EditText etPassword;
    @BindView(R.id.etConfirmPassword)
    EditText etConfirmPassword;
    @BindView(R.id.linBtnContinue)
    LinearLayout linBtnContinue;
    @BindView(R.id.inputLayoutPassword)
    TextInputLayout inputLayoutPassword;
    @BindView(R.id.inputLayoutConfirmPassword)
    TextInputLayout inputLayoutConfirmPassword;

    int flag;
    String fullName, finalFullName, userName, email, contact, password, confirmPassword, socialId;
    private String photo;
    int dataNoemail = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        ButterKnife.bind(this);

        // Obtain the shared Tracker instance.
        getTracker("Register");

        flag = getIntent().getExtras().getInt(FLAG);
        if (flag == FLAG_REGISTER_FROM_FB) {
            inputLayoutPassword.setVisibility(View.GONE);
            inputLayoutConfirmPassword.setVisibility(View.GONE);
            fullName = getIntent().getExtras().getString("fullName");
            userName = getIntent().getExtras().getString("username");
            email = getIntent().getExtras().getString("email");
            photo = getIntent().getExtras().getString("photo");
            socialId = getIntent().getExtras().getString("socialId");

            etUserName.setVisibility(View.GONE);

            etFullName.setText(fullName);

            if (email != null) {
                dataNoemail = 1;
                etEmail.setText(email);
                etEmail.setKeyListener(null);
                etEmail.setEnabled(false);
            } else {
                dataNoemail = 2;
                email = etEmail.getText().toString();
            }

        }

        Boolean isRegister = SPManager.getIsRegister(this);
        if (isRegister) {
            Intent i = new Intent(this, MainAct.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
            finish();
        }

        setToolbar(true);

    }

    @OnClick(R.id.linBtnContinue)
    void toContinue() {
        inputForm();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
    }

    @Override
    protected int getToolbarTitle() {
        return R.string.registration;
    }

    public void inputForm() {
        contact = etContact.getText().toString();
        finalFullName = etFullName.getText().toString();

        if (flag == FLAG_DEFAULT_REGISTER) {
            userName = etUserName.getText().toString();
            email = etEmail.getText().toString();
            password = etPassword.getText().toString();
            confirmPassword = etConfirmPassword.getText().toString();

            if (TextUtils.isEmpty(finalFullName) || TextUtils.isEmpty(email) || TextUtils.isEmpty(contact) || TextUtils.isEmpty(password) || TextUtils.isEmpty(confirmPassword)) {
                onRegistrationFailed();
            } else if (!isValidEmail(email)) {
                showMaterialDialogAlert(R.string.alert, R.string.invalid_email);
            } else if (!password.equals(confirmPassword)) {
                etPassword.setError(getResources().getString(R.string.password_mismatch));
                etConfirmPassword.setError(getResources().getString(R.string.password_mismatch));
            } else {
                signUpProfile();
            }
        } else if (flag == FLAG_REGISTER_FROM_FB) {
            email = etEmail.getText().toString();
            String[] parts = email.split("@");
            userName = parts[0];
            Log.d("cek --->", finalFullName + " " + email + " " + contact + " " + userName);
            if (TextUtils.isEmpty(finalFullName) || TextUtils.isEmpty(email) || TextUtils.isEmpty(contact)) {
                onRegistrationFailed();
            } else if (!TextUtils.isEmpty(finalFullName) && !TextUtils.isEmpty(email) && !TextUtils.isEmpty(contact)) {
                //signUpProfileFB();
                //signUpProfile();
                if (dataNoemail == 1) {
                    putProfileData();
                } else {
                    signUpProfileFB();
                }

            }

        } else {
            onRegistrationFailed();
        }

    }

    void signUpProfile() {
        showProgressDialog("Registrating..");
        restClient.getApiService().postRegister(new PostRegister(finalFullName, userName, email, contact, password, confirmPassword, null, null, null, photo, false), new Callback<Register>() {
            @Override
            public void success(Register register, Response response) {
                progressDialog.dismiss();
                new MaterialDialog.Builder(mActivity)
                        .title("Success")
                        .content("Registration success! Please check your email to verify")
                        .positiveText(R.string.ok)
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(MaterialDialog materialDialog, DialogAction dialogAction) {
                                Intent intent = new Intent(mActivity, HighLightAct.class);
                                startActivity(intent);
                                overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                            }
                        })
                        .cancelable(false)
                        .show();
            }

            @Override
            public void failure(RetrofitError error) {
                progressDialog.dismiss();
                showAlertDialog("Error", NetworkErrorUtil.getErrorMessage(error));

            }
        });
    }

    void signUpProfileFB() {
        Log.d("hello", "-->cek FB LOGIN");
        showProgressDialog("Registrating..");
        restClient.getApiService().postRegister(new PostRegister(finalFullName, userName, email, contact, "android", "facebook", socialId, photo), new Callback<Register>() {
            @Override
            public void success(Register register, Response response) {
                progressDialog.dismiss();
                Utils.saveTokenFromFb(mActivity, register);
                putProfileData();
            }

            @Override
            public void failure(RetrofitError error) {
                progressDialog.dismiss();
                showAlertDialog("Error", NetworkErrorUtil.getErrorMessage(error));

            }
        });
    }

    void putProfileData() {
        Log.d("hello", "-->cek FB PUT");
        showProgressDialog("Registrating..");
        restClient.getApiService().putProfileData(Utils.getToken(mActivity).getAccessToken(), new PostRegister(fullName, null, email, contact, null, null, null, null, null, photo, false), new Callback<ApiResponse<Profile>>() {

            @Override
            public void success(ApiResponse<Profile> profileApiResponse, Response response) {
                progressDialog.dismiss();
                FoodGasmApp foodGasmApp = (FoodGasmApp) getApplication();
                foodGasmApp.registerWithNotificationHubs();
                SPManager.setIsRegister(mActivity, true);
                Intent intent = new Intent(mActivity, MainAct.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
            }

            @Override
            public void failure(RetrofitError error) {
                progressDialog.dismiss();
                showAlertDialog("Error", NetworkErrorUtil.getErrorMessage(error));

            }
        });
    }


    public void onRegistrationFailed() {
        showMaterialDialogAlert(R.string.registration_failed, R.string.please_fill);
    }
}
