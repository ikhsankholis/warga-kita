package com.dycode.foodgasm.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.dycode.foodgasm.R;
import com.dycode.foodgasm.model.Restaurant;
import com.dycode.foodgasm.model.Times;
import com.dycode.foodgasm.model.UberPrice;
import com.dycode.foodgasm.model.UberPrices;
import com.dycode.foodgasm.utils.Constants;
import com.dycode.foodgasm.utils.Utils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by asus pc on 26/11/2015.
 */
public class MapsAct extends BaseActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    protected GoogleMap googleMap;
    private String LOG_TAG = "MapsAct";
    List<LatLng> polyz;
    JSONArray array;

    // LogCat tag
    private static final String TAG = MapsAct.class.getSimpleName();
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;
    private Location mLastLocation;
    // Google client to interact with Google API
    private GoogleApiClient mGoogleApiClient;
    private boolean isConnected = false;
    // boolean flag to toggle periodic location updates
    private boolean mRequestingLocationUpdates = false;
    private LocationRequest mLocationRequest;
    // Location updates intervals in sec
    private static int UPDATE_INTERVAL = 10000; // 10 sec
    private static int FATEST_INTERVAL = 5000; // 5 sec
    private static int DISPLACEMENT = 10; // 10 meters

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.tvDistanceInfo)
    TextView tvDistanceInfo;
    @BindView(R.id.btnRequest)
    Button btnRequest;
    @BindView(R.id.tvDrivingDuration)
    TextView tvDrivingDuration;
    @BindView(R.id.tvWalkingDuration)
    TextView tvWalkingDuration;
    @BindView(R.id.linWalkingMode)
    LinearLayout linWalkingMode;
    @BindView(R.id.linDrivingMode)
    LinearLayout linDrivingMode;

    Restaurant restaurantDetail;
    String distanceFinal, durationTotal, walkingDurationTotal, durationEstimate;
    List<UberPrice> productsList = new ArrayList<UberPrice>();
    private ArrayList<Polyline> polylines;
    String[] uberData = new String[]{};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        setToolbar(true);
        ButterKnife.bind(this);

        // Obtain the shared Tracker instance.
        getTracker("Maps");

        restaurantDetail = getIntent().getExtras().getParcelable(Restaurant.RESTAURANT);
        Double distance = restaurantDetail.getDistance();
        int countDistance = distance.intValue();
        if (countDistance >= 1) {
            distanceFinal = String.valueOf(countDistance) + " " + "km";
        } else {
            Double roundedDistance = Math.round(100 * distance) / 100.0;
            Double countRoundedDistance = roundedDistance * 1000;
            int countFinalDistance = countRoundedDistance.intValue();

            distanceFinal = String.valueOf(countFinalDistance) + " " + "m";
        }
        String infoDistance = String.format(mActivity.getResources().getString(R.string.direction), distanceFinal);
        tvDistanceInfo.setText(infoDistance);

        // First we need to check availability of play services
        if (checkPlayServices()) {

            // Building the GoogleApi client
            buildGoogleApiClient();
            createLocationRequest();
        }
        polylines = new ArrayList<>();

        btnRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getPrices();
            }
        });
    }

    @Override
    protected int getToolbarTitle() {
        return R.string.string_null;
    }

    void getPrices() {
        showProgressDialog("Loading data...");
        restClientUberProduct.getApiService().getUberEstimatesPrice(Float.parseFloat(Utils.getLatString(mActivity)), Float.parseFloat(Utils.getLngString(mActivity)), Float.parseFloat(String.valueOf(restaurantDetail.getLocation().getCoordinates().get(1))), Float.parseFloat(String.valueOf(restaurantDetail.getLocation().getCoordinates().get(0))), new Callback<UberPrices>() {
            @Override
            public void success(UberPrices uberPrices, Response response) {
                productsList.clear();
                productsList.addAll(uberPrices.getPrices());
                uberData = new String[productsList.size()];
                Log.d("productsList.size()", String.valueOf(productsList.size()));
                for (int pos = 0; pos < productsList.size(); pos++) {
                    getEstimates(pos);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (progressDialog != null) progressDialog.dismiss();
                showToast("No Internet Connection");
            }
        });
    }

    void getEstimates(final int pos) {
        restClientUberProduct.getApiService().getUberEstimatesTime(Float.parseFloat(Utils.getLatString(mActivity)), Float.parseFloat(Utils.getLngString(mActivity)), Constants.Uber.CLIENT_ID, productsList.get(pos).getProductId(), new Callback<Times>() {

            @Override
            public void success(Times times, Response response) {
                if (progressDialog != null) progressDialog.dismiss();
                productsList.get(pos).setTimes(times);

                if (productsList.get(pos).getTimes() != null) {
                    if (productsList.get(pos).getTimes().getTimes().size() > 0) {
                        int durationInMin;
                        durationInMin = productsList.get(pos).getTimes().getTimes().get(0).getEstimate() / 60;
                        durationEstimate = String.valueOf(durationInMin);
                    } else durationEstimate = "";
                } else durationEstimate = "";

                String displayData = String.format(getString(R.string.uber_data), productsList.get(pos).getDisplayName(), productsList.get(pos).getEstimate(), durationEstimate);
                uberData[pos] = displayData;
                Log.d("uber Data > ", uberData[pos] + " " + String.valueOf(pos) + " productsList.size()" + String.valueOf(productsList.size()));

                if (pos == productsList.size() - 1) {
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            // Do something after 5s = 5000ms
                            new MaterialDialog.Builder(mActivity)
                                    .items(uberData)
                                    .itemsCallbackSingleChoice(-1, new MaterialDialog.ListCallbackSingleChoice() {
                                        @Override
                                        public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                                            try {
                                                PackageManager pm = mActivity.getPackageManager();
                                                pm.getPackageInfo("com.ubercab", PackageManager.GET_ACTIVITIES);
                                                String uri = "uber://?action=setPickup&pickup[latitude]=" + Float.parseFloat(Utils.getLatString(mActivity)) + "&pickup[longitude]="
                                                        + Float.parseFloat(Utils.getLngString(mActivity)) + "&dropoff[latitude]=" + Float.parseFloat(String.valueOf(restaurantDetail.getLocation().getCoordinates().get(1))) +
                                                        "&dropoff[longitude]=" + Float.parseFloat(String.valueOf(restaurantDetail.getLocation().getCoordinates().get(0))) + "&client_id=" + Constants.Uber.CLIENT_ID + "&product_id=" + productsList.get(pos).getProductId();
                                                Intent intent = new Intent(Intent.ACTION_VIEW);
                                                intent.setData(Uri.parse(uri));
                                                startActivity(intent);
                                            } catch (PackageManager.NameNotFoundException e) {
                                                // No Uber app! Open mobile website.
                                                String url = "https://m.uber.com/sign-up?client_id=" + Constants.Uber.CLIENT_ID;
                                                Intent i = new Intent(Intent.ACTION_VIEW);
                                                i.setData(Uri.parse(url));
                                                startActivity(i);
                                            }
                                            return true;
                                        }
                                    })
                                    .show();
                        }
                    }, 2000);

                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (progressDialog != null) progressDialog.dismiss();
                showToast("No Internet Connection");
            }
        });
    }

    protected void setUpMapIfNeeded() {
        if (googleMap == null) {
            MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
            mapFragment.getMapAsync((OnMapReadyCallback) mActivity);
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        setUpMapIfNeeded();
        checkPlayServices();

        // Resuming the periodic location updates
        if (mGoogleApiClient.isConnected() && !mRequestingLocationUpdates) {
            startLocationUpdates();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
            isConnected = false;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (isConnected)
            stopLocationUpdates();

    }

    /**
     * Method to display the location on UI
     */
    private void displayLocation() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

            if (mLastLocation != null) {
                double latitude = mLastLocation.getLatitude();
                double longitude = mLastLocation.getLongitude();

                if (googleMap != null) {
//                    CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLng(new LatLng(latitude, longitude));
//                    googleMap.animateCamera(cameraUpdate);
//                SPManager.saveString(this, Constants.Location.LATITUDE, Double.toString(latitude));
//                SPManager.saveString(this, Constants.Location.LONGITUDE, Double.toString(longitude));
                }

            } else {

                Toast.makeText(this, "(Couldn't get the location. Make sure location is enabled on the device)", Toast.LENGTH_SHORT).show();
            }
        }
    }

    /**
     * Creating google api client object
     */
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
    }

    /**
     * Creating location request object
     */
    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FATEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setSmallestDisplacement(DISPLACEMENT);
    }

    /**
     * Method to verify google play services on the device
     */
    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Toast.makeText(getApplicationContext(),
                        "This device is not supported.", Toast.LENGTH_LONG)
                        .show();
                finish();
            }
            return false;
        }
        return true;
    }

    /**
     * Starting the location updates
     */
    protected void startLocationUpdates() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    /**
     * Stopping location updates
     */
    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(
                mGoogleApiClient, this);
    }

    /**
     * Google api callback methods
     */
    @Override
    public void onConnectionFailed(ConnectionResult result) {
        isConnected = false;
        Log.d("MAPS", "Connection failed: ConnectionResult.getErrorCode() = "
                + result.getErrorCode());
    }

    @Override
    public void onConnected(Bundle arg0) {
        isConnected = true;
        // Once connected with google api, get the location
        displayLocation();

        if (mRequestingLocationUpdates) {
            startLocationUpdates();
        }
    }

    @Override
    public void onConnectionSuspended(int arg0) {
        isConnected = false;
        mGoogleApiClient.connect();
    }

    @Override
    public void onLocationChanged(Location location) {
        // Assign the new location
        mLastLocation = location;

        Log.d("MAPS", "Location changed!");
        // Displaying the new location on UI
        displayLocation();
    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {
        this.googleMap = googleMap;
        final LatLng restaurantLocation = new LatLng(restaurantDetail.getLocation().getCoordinates().get(1), restaurantDetail.getLocation().getCoordinates().get(0));
        googleMap.setPadding(0, dpToPx(24), 0, 0);
        googleMap.addMarker(new MarkerOptions().position(restaurantLocation).title(restaurantDetail.getName()));
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(restaurantLocation, 15));
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(16), 2000, null);

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            googleMap.setMyLocationEnabled(true);
        }
        googleMap.getUiSettings().setMyLocationButtonEnabled(true);
        googleMap.getUiSettings().setCompassEnabled(true);

        new GetDirection().execute();
    }

    class GetDirection extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog("Loading route...");
        }

        protected String doInBackground(String... args) {
            LatLng restaurantLocation = new LatLng(restaurantDetail.getLocation().getCoordinates().get(1), restaurantDetail.getLocation().getCoordinates().get(0));
            LatLng start = new LatLng(Double.parseDouble(Utils.getLatString(mActivity)), Double.parseDouble(Utils.getLngString(mActivity)));

            String startLocation = String.valueOf(start);
            String endLocation = String.valueOf(restaurantLocation);
            startLocation = startLocation.substring(10, startLocation.length() - 1);
            endLocation = endLocation.substring(10, endLocation.length() - 1);

            String stringUrl = "https://maps.googleapis.com/maps/api/directions/json?origin=" + startLocation + "&destination=" + endLocation + "&sensor=false&key=AIzaSyA4uf4dge3Pw0ezGTZSvgg9LAyPXqdNN4w";
            StringBuilder response = new StringBuilder();
            try {
                URL url = new URL(stringUrl);
                HttpURLConnection httpconn = (HttpURLConnection) url.openConnection();
                if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                    String strLine = null;

                    while ((strLine = input.readLine()) != null) {
                        response.append(strLine);
                    }
                    input.close();
                }

                String jsonOutput = response.toString();

                JSONObject jsonObject = new JSONObject(jsonOutput);

                // routesArray contains ALL routes
                JSONArray routesArray = jsonObject.getJSONArray("routes");
                // Grab the first route
                JSONObject route = routesArray.getJSONObject(0);

                JSONObject poly = route.getJSONObject("overview_polyline");
                String polyline = poly.getString("points");
                polyz = decodePoly(polyline);

                JSONArray legsArray = route.getJSONArray("legs");
                JSONObject duration = legsArray.getJSONObject(0);
                JSONObject durationText = duration.getJSONObject("duration");
                durationTotal = durationText.getString("text");

            } catch (Exception e) {

            }

            String stringWalkingUrl = "http://maps.googleapis.com/maps/api/directions/json?origin=" + startLocation + "&destination=" + endLocation + "&sensor=false&mode=walking";
            StringBuilder walkingResponse = new StringBuilder();
            try {
                URL url = new URL(stringWalkingUrl);
                HttpURLConnection httpconn = (HttpURLConnection) url.openConnection();
                if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                    String strLine = null;

                    while ((strLine = input.readLine()) != null) {
                        walkingResponse.append(strLine);
                    }
                    input.close();
                }

                String jsonOutput = walkingResponse.toString();

                JSONObject jsonObject = new JSONObject(jsonOutput);

                // routesArray contains ALL routes
                JSONArray routesArray = jsonObject.getJSONArray("routes");
                // Grab the first route
                JSONObject route = routesArray.getJSONObject(0);

                JSONArray legsArray = route.getJSONArray("legs");
                JSONObject duration = legsArray.getJSONObject(0);
                JSONObject durationText = duration.getJSONObject("duration");
                walkingDurationTotal = durationText.getString("text");

            } catch (Exception e) {

            }

            return null;

        }

        protected void onPostExecute(String file_url) {

            for (int i = 0; i < polyz.size() - 1; i++) {
                LatLng src = polyz.get(i);
                LatLng dest = polyz.get(i + 1);
                Polyline line = googleMap.addPolyline(new PolylineOptions()
                        .add(new LatLng(src.latitude, src.longitude), new LatLng(dest.latitude, dest.longitude))
                        .width(5).color(Color.RED).geodesic(true));

            }
            progressDialog.dismiss();
            tvDrivingDuration.setText(durationTotal);
            tvWalkingDuration.setText(walkingDurationTotal);
            linWalkingMode.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String url = "http://maps.google.com/maps?saddr=" + Utils.getLatString(mActivity) + "," + Double.parseDouble(Utils.getLngString(mActivity)) + "&daddr=" + restaurantDetail.getLocation().getCoordinates().get(1) + "," + restaurantDetail.getLocation().getCoordinates().get(0) + "&mode=walking";
                    Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(url));
                    intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
                    startActivity(intent);
                }
            });
            linDrivingMode.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String url = "http://maps.google.com/maps?saddr=" + Utils.getLatString(mActivity) + "," + Double.parseDouble(Utils.getLngString(mActivity)) + "&daddr=" + restaurantDetail.getLocation().getCoordinates().get(1) + "," + restaurantDetail.getLocation().getCoordinates().get(0) + "&mode=driving";
                    Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(url));
                    intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
                    startActivity(intent);
                }
            });

        }
    }

    /* Method to decode polyline points */
    private List<LatLng> decodePoly(String encoded) {

        List<LatLng> poly = new ArrayList<LatLng>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng((((double) lat / 1E5)),
                    (((double) lng / 1E5)));
            poly.add(p);
        }

        return poly;
    }

    public int dpToPx(int dp) {
        DisplayMetrics displayMetrics = mActivity.getResources().getDisplayMetrics();
        int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return px;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
    }
}
