package com.dycode.foodgasm.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.dycode.foodgasm.R;
import com.dycode.foodgasm.adapters.GridReviewAdapter;
import com.dycode.foodgasm.adapters.ReviewAdapter;
import com.dycode.foodgasm.model.ApiResponse;
import com.dycode.foodgasm.model.ApiResponseList;
import com.dycode.foodgasm.model.Comment;
import com.dycode.foodgasm.model.PostComment;
import com.dycode.foodgasm.model.Restaurant;
import com.dycode.foodgasm.model.ReviewPoints;
import com.dycode.foodgasm.rest.RestClient;
import com.dycode.foodgasm.utils.NetworkErrorUtil;
import com.dycode.foodgasm.utils.Utils;
import com.dycode.foodgasm.widget.SpacesItemDecoration;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by asus pc on 12/10/2015.
 */
public class ReviewAct extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.tvTitle)
    TextView tvTitle;

    RecyclerView rvReview;
    ReviewAdapter mAdapter;
    GridReviewAdapter mGridAdapter;
    Restaurant restaurantDetail;
    String restoInitial;
    List<Comment> commentList = new ArrayList<Comment>();
    List<ReviewPoints> reviewPointsList = new ArrayList<ReviewPoints>();
    List<String> reviewPoint = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review);
        ButterKnife.bind(this);

        // Obtain the shared Tracker instance.
        getTracker("Review List");

        setToolbar(true);
        restaurantDetail = getIntent().getExtras().getParcelable(Restaurant.RESTAURANT);
        getCommentList();
        setUpRecyclerView();
    }

    void getCommentList() {
        showProgressDialog("Getting Review List..");
        restClient = RestClient.getInstance();
        restClient.getApiService().getListComment( restaurantDetail.getId(), "true", "true", "tu", new Callback<ApiResponseList<Comment>>() {
            @Override
            public void success(ApiResponseList<Comment> commentApiResponseList, Response response) {
                progressDialog.dismiss();
                if (commentApiResponseList.getMeta().getCode() == 200) {
                    commentList.clear();
                    commentList.addAll(commentApiResponseList.getData());
                    mAdapter.notifyDataSetChanged();
                } else {
                    showAlertDialog("Failed", commentApiResponseList.getMeta().getMessage());
                }
            }

            @Override
            public void failure(RetrofitError error) {
                progressDialog.dismiss();
//                showAlertDialog("Failed", error.getMessage());
                showToast("No Internet Connection");
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if(Utils.getToken(this) != null){
            getMenuInflater().inflate(R.menu.menu_form, menu);
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        setResult(1);
        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            setResult(1);
            overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
        } else if (id == R.id.action_save) {
            AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
            final AlertDialog dialog = builder.create();
            LayoutInflater inflater = mActivity.getLayoutInflater();
            View dialogLayout = inflater.inflate(R.layout.review_dialog_layout, null);

            dialog.setView(dialogLayout);
            dialog.setCancelable(true);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.show();

            // Obtain the shared Tracker instance.
            getTracker("Compose Review");

            TextView tvRestaurantName = (TextView) dialog.findViewById(R.id.tvRestaurantName);
            TextView tvRestaurantInitialName = (TextView) dialog.findViewById(R.id.tvRestaurantInitialName);
            LinearLayout linSubmitReview = (LinearLayout) dialog.findViewById(R.id.linBtnSubmit);
            final RatingBar rbRestoRating = (RatingBar) dialog.findViewById(R.id.ratingBar);
            final EditText etWriteReview = (EditText) dialog.findViewById(R.id.etWriteReview);
            final ImageView ivLogo = (ImageView) dialog.findViewById(R.id.ivLogo);
            final LinearLayout linProblemContainer = (LinearLayout) dialog.findViewById(R.id.linProblemContainer);
            final GridView gvBadReview = (GridView) dialog.findViewById(R.id.gvBadReview);

            reviewPointsList.clear();
            for (int x = 0; x < restaurantDetail.getSetting().getReviewPoints().size(); x++) {
                String reviewPoint = restaurantDetail.getSetting().getReviewPoints().get(x);
                reviewPointsList.add(new ReviewPoints(reviewPoint));
            }
            mGridAdapter = new GridReviewAdapter(mActivity, reviewPointsList);
            gvBadReview.setAdapter(mGridAdapter);

            rbRestoRating.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
                @Override
                public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                    if (rbRestoRating.getRating() < 4){
                        reviewPointsList.clear();
                        for (int x = 0; x < restaurantDetail.getSetting().getReviewPoints().size(); x++) {
                            String reviewPoint = restaurantDetail.getSetting().getReviewPoints().get(x);
                            reviewPointsList.add(new ReviewPoints(reviewPoint));
                        }
                        linProblemContainer.setVisibility(View.VISIBLE);
                    }

                    else {
                        reviewPointsList.clear();
                        mGridAdapter.notifyDataSetChanged();
                        linProblemContainer.setVisibility(View.GONE);
                    }

                }

            });

            linSubmitReview.setOnClickListener(v -> {
                if (rbRestoRating.getRating() == 0 || etWriteReview.getText() == null)
                    showAlertDialog("Error", "Please complete all field required");

                else {
                    String review = etWriteReview.getText().toString();
                    double rating = rbRestoRating.getRating();
                    int ratingValue = (int) rating;
                    showProgressDialog("Submitting...");
                    if (rating < 4) {
                        for (int pos = 0; pos < reviewPointsList.size(); pos++) {
                            if (reviewPointsList.get(pos).getIsActive())
                                reviewPoint.add(reviewPointsList.get(pos).getReviewPoints());
                        }
                    }

                    restClient.getApiService().postComment(new PostComment(restaurantDetail.getId(), ratingValue, review, reviewPoint), Utils.getToken(mActivity).getAccessToken(), new Callback<ApiResponse<Comment>>() {
                        @Override
                        public void success(ApiResponse<Comment> commentApiResponse, Response response) {
                            progressDialog.dismiss();
                            dialog.dismiss();
                            new MaterialDialog.Builder(mActivity)
                                    .content(R.string.add_review_success)
                                    .positiveText(R.string.ok)
                                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                                        @Override
                                        public void onClick(MaterialDialog materialDialog, DialogAction which) {
                                            materialDialog.dismiss();
                                            getCommentList();
                                        }
                                    })
                                    .show();

                        }

                        @Override
                        public void failure(RetrofitError error) {
                            progressDialog.dismiss();
                            dialog.dismiss();
                            showAlertDialog("Add Comment Failed", NetworkErrorUtil.getErrorMessage(error));
                        }
                    });
                }
            });

            if (!TextUtils.isEmpty(restaurantDetail.getPic())) {
                tvRestaurantInitialName.setVisibility(View.GONE);
                Picasso.with(mActivity)
                        .load(restaurantDetail.getPic())
                        .resizeDimen(R.dimen.dimen_64dp, R.dimen.dimen_64dp)
                        .centerCrop()
                        .into(ivLogo, new com.squareup.picasso.Callback() {
                            @Override
                            public void onSuccess() {

                            }

                            @Override
                            public void onError() {

                            }
                        });

            }
            tvRestaurantName.setText(restaurantDetail.getName());
            tvRestaurantInitialName.setText(restoInitial);

            ImageView ivClose = (ImageView) dialog.findViewById(R.id.ivClosePromo);
            ivClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
        }
        return super.onOptionsItemSelected(item);
    }

    public static void start(Context context) {
        context.startActivity(new Intent(context, ReviewAct.class));
    }

    private void setUpRecyclerView() {
        rvReview = ButterKnife.findById(this, R.id.rvReview);
        RecyclerView.LayoutManager linLayoutManager = new LinearLayoutManager(this);
        rvReview.setLayoutManager(linLayoutManager);

        mAdapter = new ReviewAdapter(this, this, commentList);
        rvReview.setAdapter(mAdapter);
        rvReview.addItemDecoration(new SpacesItemDecoration(4));

    }

    @Override
    protected int getToolbarTitle() {
        return R.string.reviews;
    }


}
