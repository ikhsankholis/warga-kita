package com.dycode.foodgasm.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.dycode.foodgasm.R;
import com.dycode.foodgasm.model.ApiResponse;
import com.dycode.foodgasm.model.DokuWallet;
import com.dycode.foodgasm.model.DokuWalletRegData;
import com.dycode.foodgasm.model.DokuWalletRegEmail;
import com.dycode.foodgasm.model.DokuWalletRegister;
import com.dycode.foodgasm.model.DokuWalletResponseNew;
import com.dycode.foodgasm.utils.Utils;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class DokuWalletLogin extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.tvTitle)
    TextView tvTitle;

    @BindView(R.id.dokuLogo)
    ImageView dokuLogo;
    @BindView(R.id.emailTxt)
    TextView emailTxt;
    @BindView(R.id.emailValue)
    EditText emailValue;
    @BindView(R.id.nameTxt)
    TextView nameTxt;
    @BindView(R.id.nameValue)
    EditText nameValue;

    @BindView(R.id.codeTxt)
    TextView codeTxt;
    @BindView(R.id.codeValue)
    EditText codeValue;

    @BindView(R.id.layoutLogin)
    LinearLayout layoutLogin;
    @BindView(R.id.btnSubmit)
    Button btnSubmit;
    @BindView(R.id.masterCardSercure)
    ImageView masterCardSercure;
    @BindView(R.id.mainLayout)
    RelativeLayout mainLayout;
    @BindView(R.id.masterLayout)
    RelativeLayout masterLayout;
    private String orderid;

    List<DokuWalletRegData> listData;
    private String orderId;
    private String accountId;
    private String walletStatus;
    private String daftarDokuWallet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.doku_wallet_login);
        ButterKnife.bind(this);
        mToolbar.setNavigationIcon(R.mipmap.ic_back);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        tvTitle.setText(R.string.dokuwallet);

        listData = new ArrayList<DokuWalletRegData>();
        walletStatus = getIntent().getExtras().getString("walletStatus", "walletStatus");
        orderId = getIntent().getExtras().getString("orderId", "orderId");
        daftarDokuWallet = getIntent().getExtras().getString("daftarDokuWallet", "daftarDokuWallet");

        Log.d("walletStatus > ", walletStatus);
        if (walletStatus.equals("1")) {
            mainLayout.setVisibility(View.GONE);
            orderId = getIntent().getExtras().getString("orderId", "orderId");
            accountId = getIntent().getExtras().getString("accountId", "accountId");
            Log.d("orderid and account id", orderId + " > " + accountId);
            PostDokuWallet(orderId, accountId);
        }

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DokuWalletRegister(emailValue.getText().toString());
            }
        });
    }

    private void DokuWalletRegister(String email) {
        showProgressDialog("Submitting..");
        restClient.getApiService().postRegWalletEmail(Utils.getToken(DokuWalletLogin.this).getAccessToken(), email, new Callback<ApiResponse<DokuWalletRegEmail>>() {
            @Override
            public void success(ApiResponse<DokuWalletRegEmail> userApiResponse, Response response) {
                progressDialog.dismiss();
                Log.d("RESPONSE - >",
                        userApiResponse.getData().get_id()
                                + " " + userApiResponse.getData().getEmail()
                                + " " + userApiResponse.getData().getReject_reason()
                                + " " + userApiResponse.getData().getStatus());

                if (userApiResponse.getData().getStatus().equals("sent")) {
                    Intent intent = new Intent(getApplicationContext(), DokuWalletLoginCode.class);
                    intent.putExtra("name", nameValue.getText().toString());
                    intent.putExtra("email", emailValue.getText().toString());
                    intent.putExtra("code", orderId);
                    startActivity(intent);
                    finish();
                }

            }

            @Override
            public void failure(RetrofitError error) {
                progressDialog.dismiss();
                new MaterialDialog.Builder(DokuWalletLogin.this)
                        .title(R.string.alert)
                        .content(error.getMessage())
                        .positiveText(R.string.ok)
                        .cancelable(false)
                        .show();
            }
        });
    }

    private void PostDokuWalletRegister(String name, String email, String code) {
        showProgressDialog("Submitting..");
        restClient.getApiService().postRegisterDokuWallet(Utils.getToken(DokuWalletLogin.this).getAccessToken(), new DokuWalletRegister(name, email, code), new Callback<ApiResponse<DokuWalletRegData>>() {
            @Override
            public void success(ApiResponse<DokuWalletRegData> userApiResponse, Response response) {
                progressDialog.dismiss();
                //Log.d("dataResponse", userApiResponse.getMeta().getMessage());

                //Log.d("cekdata", userApiResponse.getData().getDokuId());
                JSONObject json;

                PostDokuWallet(orderId, userApiResponse.getData().getDokuId());
//                JSONObject json;
//                try {
//                    json = new JSONObject(userApiResponse.getData().toString());
//                    if (json.getString("res_response_code").equalsIgnoreCase("0000") && json != null) {
//                        Intent intent = new Intent(getApplicationContext(), ResultPayment.class);
//                        intent.putExtra("data", json.toString());
//                        startActivity(intent);
//                        finish();
//                        Toast.makeText(getApplicationContext(), " PAYMENT SUKSES", Toast.LENGTH_SHORT).show();
//                    } else {
//                        Intent intent = new Intent(DokuWalletLogin.this, ResultPayment.class);
//                        intent.putExtra("data", json.toString());
//                        startActivity(intent);
//                        finish();
//                        Toast.makeText(getApplicationContext(), "PAYMENT ERROR", Toast.LENGTH_SHORT).show();
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
            }

            @Override
            public void failure(RetrofitError error) {
                progressDialog.dismiss();
                new MaterialDialog.Builder(DokuWalletLogin.this)
                        .title(R.string.alert)
                        .content(error.getMessage())
                        .positiveText(R.string.ok)
                        .cancelable(false)
                        .show();
            }
        });
    }

    private void PostDokuWallet(String orderid, String id) {
        showProgressDialog("Submitting..");
        restClient.getApiService().postDokuResponseWalletNew(orderid, Utils.getToken(DokuWalletLogin.this).getAccessToken(), new DokuWallet(id), new Callback<ApiResponse<DokuWalletResponseNew>>() {
            @Override
            public void success(ApiResponse<DokuWalletResponseNew> userApiResponse, Response response) {
                progressDialog.dismiss();

                Log.d("cek status doku wallet -->", userApiResponse.getData().getResponseCode());

                String status_code = userApiResponse.getData().getResponseCode();
                if (!userApiResponse.getData().getResponseCode().equals("0000")) {
                    new MaterialDialog.Builder(DokuWalletLogin.this)
                            .title(R.string.alert)
                            .content("Your Balance is not Enough")
                            .positiveText(R.string.ok)
                            .cancelable(false)
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    finish();
                                }
                            })
                            .show();
                } else {
                    Intent intent = new Intent(getApplicationContext(), ResultPaymentAct.class);
                    intent.putExtra("data", status_code);
                    startActivity(intent);
                    finish();
                }

            }

            @Override
            public void failure(RetrofitError error) {
                progressDialog.dismiss();
                new MaterialDialog.Builder(DokuWalletLogin.this)
                        .title(R.string.alert)
                        .content(error.getMessage())
                        .positiveText(R.string.ok)
                        .cancelable(false)
                        .show();
            }
        });
    }


    private void DokuWalletRegister(String name, String email, String code) {
        showProgressDialog("Submitting..");
        restClient.getApiService().postRegisterDokuWallet(Utils.getToken(DokuWalletLogin.this).getAccessToken(), new DokuWalletRegister(name, email, code), new Callback<ApiResponse<DokuWalletRegData>>() {
            @Override
            public void success(ApiResponse<DokuWalletRegData> userApiResponse, Response response) {
                progressDialog.dismiss();
                showToast("Pendaftaran Doku Wallet Sukses");
                setResult(2);
                finish();
            }

            @Override
            public void failure(RetrofitError error) {
                progressDialog.dismiss();
                new MaterialDialog.Builder(DokuWalletLogin.this)
                        .title(R.string.alert)
                        .content(error.getMessage())
                        .positiveText(R.string.ok)
                        .cancelable(false)
                        .show();
            }
        });
    }

}
