package com.dycode.foodgasm.activity;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.dycode.foodgasm.R;
import com.dycode.foodgasm.model.AccessToken;
import com.dycode.foodgasm.model.ApiResponse;
import com.dycode.foodgasm.model.PostEmail;
import com.dycode.foodgasm.model.User;
import com.dycode.foodgasm.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by asus pc on 19/02/2016.
 */
public class ForgetPasswordAct extends BaseActivity {
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.inpEmail)
    EditText inpEmail;
    @BindView(R.id.btnSubmit)
    Button btnSubmit;

    String email, emailPattern;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);
        ButterKnife.bind(this);

        // Obtain the shared Tracker instance.
        getTracker("Forgot Password");
        //set background
        getWindow().setBackgroundDrawableResource(R.mipmap.img_highlightblur);

        mToolbar.setNavigationIcon(R.mipmap.ic_navigation_arrow_back);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        tvTitle.setText(R.string.forgot_password);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitEmail();
            }
        });
    }

    public void submitEmail(){
        if (!validate()) {
            onSubmitFailed();
            return;
        }

        showProgressDialog("Submitting..");
        restClient.getApiService().postEmailResetPassword(new PostEmail(email), new Callback<ApiResponse<User>>() {
            @Override
            public void success(ApiResponse<User> userApiResponse, Response response) {
                progressDialog.dismiss();
                new MaterialDialog.Builder(mActivity)
                        .content(R.string.email_sent)
                        .positiveText(R.string.ok)
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(MaterialDialog dialog, DialogAction which) {
                                dialog.dismiss();
                                finish();
                                overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                            }
                        })
                        .cancelable(false)
                        .show();
            }

            @Override
            public void failure(RetrofitError error) {
                progressDialog.dismiss();
                showMaterialDialogAlert(error.getMessage());
            }
        });
    }

    public boolean validate() {
        boolean valid = true;

        email = inpEmail.getText().toString().trim();
        emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

        valid = isValidEmail(email);

        return valid;
    }

    public void onSubmitFailed() {
        showMaterialDialogAlert(R.string.invalid_email);
        btnSubmit.setEnabled(true);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
    }
}
