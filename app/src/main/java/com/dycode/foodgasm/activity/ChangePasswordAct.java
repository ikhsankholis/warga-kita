package com.dycode.foodgasm.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.dycode.foodgasm.R;
import com.dycode.foodgasm.model.ApiResponse;
import com.dycode.foodgasm.model.ChangePassword;
import com.dycode.foodgasm.model.Profile;
import com.dycode.foodgasm.utils.NetworkErrorUtil;
import com.dycode.foodgasm.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by asus pc on 23/02/2016.
 */
public class ChangePasswordAct extends BaseActivity implements View.OnClickListener {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.tvMenu)
    TextView tvMenu;
    @BindView(R.id.etPassword)
    EditText etPassword;
    @BindView(R.id.etNewPassword)
    EditText etNewPassword;
    @BindView(R.id.etConfirmPassword)
    EditText etConfirmPassword;

    String password, newPassword, confirmPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        ButterKnife.bind(this);

        // Obtain the shared Tracker instance.
        getTracker("Change Password");
        setToolbar(true);
        tvMenu.setVisibility(View.VISIBLE);
        tvMenu.setOnClickListener(this);

    }

    public void validateData(){
        password = etPassword.getText().toString();
        newPassword = etNewPassword.getText().toString();
        confirmPassword = etConfirmPassword.getText().toString();

        if (TextUtils.isEmpty(password) || TextUtils.isEmpty(newPassword) || TextUtils.isEmpty(confirmPassword)) {
            onValidateFailed();
        }

        else if(!newPassword.equals(confirmPassword)){
            etNewPassword.setError(getResources().getString(R.string.password_mismatch));
            etConfirmPassword.setError(getResources().getString(R.string.password_mismatch));
        }

        else {
            putPasswordData();
        }

    }

    void putPasswordData(){
        showProgressDialog("Submitting..");
        restClient.getApiService().changePassword(Utils.getToken(mActivity).getAccessToken(), new ChangePassword(password, newPassword, confirmPassword), new Callback<ApiResponse<Profile>>() {

            @Override
            public void success(ApiResponse<Profile> profileApiResponse, Response response) {
                progressDialog.dismiss();
                new MaterialDialog.Builder(mActivity)
                        .title(R.string.success)
                        .content(R.string.change_password_success)
                        .positiveText(R.string.ok)
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(MaterialDialog dialog, DialogAction which) {
                                finish();
                                overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                            }
                        })
                        .cancelable(false)
                        .show();

            }

            @Override
            public void failure(RetrofitError error) {
                progressDialog.dismiss();
                showAlertDialog("Error", NetworkErrorUtil.getErrorMessage(error));
            }

        });
    }

    public void onValidateFailed() {
        showMaterialDialogAlert(R.string.failed, R.string.please_fill);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
    }

    @Override
    protected int getToolbarTitle() {
        return R.string.change_password;
    }

    @Override
    public void onClick(View v) {
        //TODO add change password API
        if (v==tvMenu){
            validateData();
        }
    }
}
