package com.dycode.foodgasm.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.dycode.foodgasm.R;
import com.dycode.foodgasm.model.ApiResponse;
import com.dycode.foodgasm.model.EcashBalance;
import com.dycode.foodgasm.model.EcashPaymentModel;
import com.dycode.foodgasm.model.Order;
import com.dycode.foodgasm.model.OrderDetail;
import com.dycode.foodgasm.model.PatchOrder;
import com.dycode.foodgasm.utils.CTools;
import com.dycode.foodgasm.utils.Constants;
import com.dycode.foodgasm.utils.FoodgasmDateUtil;
import com.dycode.foodgasm.utils.NetworkErrorUtil;
import com.dycode.foodgasm.utils.SPManager;
import com.dycode.foodgasm.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class EcashPaymentAct extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.tvMenu)
    TextView tvMenu;

    @BindView(R.id.id_ecash)
    TextView id_ecash;
    @BindView(R.id.txt_conf_no)
    TextView txtConNo;
    @BindView(R.id.txt_conf_yes)
    TextView txtConYes;
    @BindView(R.id.txt_total_payment_wallet)
    TextView txtTotalPaymentWallet;
    @BindView(R.id.txt_total_payment_value)
    TextView txt_total_payment_value;
    @BindView(R.id.txt_wallet_balance)
    TextView txt_wallet_balance;
    @BindView(R.id.mainLayout)
    RelativeLayout mainLayout;
    private String orderId, accountId;
    private int total;
    private int totalPrice;
    private String bookingCode;
    private String restoName;
    private String bookingTime;
    private String bookingDay;
    private boolean isTakeAway = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.e_cash_payment);
        ButterKnife.bind(this);
        //mToolbar.setNavigationIcon(R.mipmap.ic_back);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mToolbar.setNavigationIcon(R.mipmap.ic_back);
        tvMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                onBackPressed();
//                Intent intent = new Intent(EcashPayment.this, MainAct.class);
//                startActivity(intent);
//                finish();
                patchOrderCancel(orderId);
            }
        });

        tvTitle.setText(R.string.ecash);
        tvMenu.setVisibility(View.VISIBLE);
        tvMenu.setText("CANCEL");
        tvMenu.setTextColor(Color.parseColor("#eeaa4a"));

        orderId = getIntent().getExtras().getString("orderId", "orderId");
        accountId = getIntent().getExtras().getString("accountId", "accountId");
        Log.d("order id+account id > ", orderId + " " + accountId);

        txtConNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                finish();
                patchOrderCancel(orderId);
            }
        });
        txtConYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                PostDokuWallet(orderId, id_doku_wallet.getText().toString());
                postEcashPayment(orderId, accountId);
//                dummyPayment();
            }
        });

        getOrderInfo();
    }


    private void postEcashPayment(String orderid, String msisdn) {
        showProgressDialog("Submitting..");
        restClient.getApiService().postEcashPayment(orderid, Utils.getToken(EcashPaymentAct.this).getAccessToken(), new com.dycode.foodgasm.model.EcashPayment(msisdn), new Callback<ApiResponse<EcashPaymentModel>>() {
            @Override
            public void success(ApiResponse<EcashPaymentModel> userApiResponse, Response response) {
                progressDialog.dismiss();

                if (userApiResponse.getData().getStatus().equals("PROCESSED")) {
                    Intent intent = new Intent(mActivity, BookOrderCompleteAct.class);
                    intent.putExtra(BookOrderCompleteAct.FLAG, BookOrderCompleteAct.FLAG_ORDER_ACCEPTED);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("bookingTime", bookingTime);
                    intent.putExtra("bookingDay", bookingDay);
                    intent.putExtra("bookingCode", bookingCode);
                    intent.putExtra("isTakeAway", isTakeAway);
                    intent.putExtra("restoName", restoName);
                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                } else {
                    new MaterialDialog.Builder(mActivity)
                            .content(userApiResponse.getData().getDescription())
                            .positiveText(R.string.ok)
                            .cancelable(true)
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(MaterialDialog dialog, DialogAction which) {

                                }
                            })
                            .show();
                }

            }

            @Override
            public void failure(RetrofitError error) {
                progressDialog.dismiss();
                String message = NetworkErrorUtil.getErrorMessage(error);

                new MaterialDialog.Builder(EcashPaymentAct.this)
                        .title(R.string.alert)
                        .content(message)
                        .positiveText(R.string.ok)
                        .cancelable(false)
                        .show();
            }
        });
    }

    void patchOrderCancel(String orderId) {
        showProgressDialog("Loading...");
        restClient.getApiService().patchOrder(orderId, Utils.getToken(mActivity).getAccessToken(), new PatchOrder("cancel", "User cancelled the order"), new Callback<Order>() {

            @Override
            public void success(Order order, Response response) {
                progressDialog.dismiss();
                new MaterialDialog.Builder(mActivity)
                        .content(R.string.order_cancelled)
                        .positiveText(R.string.ok)
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(MaterialDialog dialog, DialogAction which) {
                                //TODO call cancel order API
                                SPManager.saveString(getApplicationContext(), Constants.Inbox.STATUS_INBOX, null);

                                Intent intent = new Intent(EcashPaymentAct.this, MainAct.class);
                                finish();
                                startActivity(intent);
                                overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                            }
                        })
                        .cancelable(false)
                        .build()
                        .show();
            }

            @Override
            public void failure(RetrofitError error) {
                progressDialog.dismiss();
                showMaterialDialogAlert(NetworkErrorUtil.getErrorMessage(error));
            }
        });
    }

    void getOrderInfo() {
        orderId = getIntent().getExtras().getString("orderId");
        showProgressDialog("Loading...");
        restClient.getApiService().getOrderDetail(orderId, Utils.getToken(mActivity).getAccessToken(), new Callback<ApiResponse<OrderDetail>>() {
            @Override
            public void success(ApiResponse<OrderDetail> orderDetailApiResponse, Response response) {
                getBalance(orderId, accountId);
                OrderDetail orderDetail = orderDetailApiResponse.getData();
//                total = orderDetail.getPrice().getSubtotal();
                total = (int) Math.ceil((orderDetail.getPrice().getSubtotal() * 0.001) * -1);
//                totalPrice = orderDetail.getPrice().getSubtotal() + total;
                totalPrice = orderDetail.getPrice().getTotal();

                bookingCode = orderDetail.getCode();
                restoName = orderDetail.getResto().getName();
                bookingTime = String.valueOf(FoodgasmDateUtil.getHourAndMinute(orderDetail.getVisitDate()));

                Calendar calendar = Calendar.getInstance();
                Date today = calendar.getTime();
                String bookDay = String.valueOf(FoodgasmDateUtil.getStringDate(orderDetail.getVisitDate()));
                DateFormat dateFormat = new SimpleDateFormat("MMM dd, yyy", java.util.Locale.getDefault());
                String todayAsString = dateFormat.format(today);
                if (!bookDay.equals(todayAsString)) {
                    bookingDay = "tomorrow";
                } else {
                    bookingDay = "today";
                }
                if (orderDetail.getPreorder().getIsOrder() && !orderDetail.getBooking().getIsBooking())
                    isTakeAway = true;

                id_ecash.setText(orderDetail.getMaskedCard());
                txtTotalPaymentWallet.setText("Total Payment");
                txt_total_payment_value.setText(CTools.getRP(String.valueOf(totalPrice), true));
            }

            @Override
            public void failure(RetrofitError error) {
                progressDialog.dismiss();
                showAlertDialog("Failed", error.getMessage());
            }
        });
    }

    private void dummyPayment() {
        showProgressDialog("Loading. . .");
        restClient.getApiService().getDummyPaymentEcash(new Callback<ApiResponse<EcashPaymentModel>>() {
            @Override
            public void success(ApiResponse<EcashPaymentModel> userApiResponse, Response response) {
                progressDialog.dismiss();
                if (userApiResponse.getData().getStatus().equals("PROCESSED")) {
                    Log.d("sukses > ", "Intent");
                    Intent intent = new Intent(EcashPaymentAct.this, BookOrderCompleteAct.class);
                    intent.putExtra(BookOrderCompleteAct.FLAG, BookOrderCompleteAct.FLAG_ORDER_ACCEPTED);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("bookingTime", bookingTime);
                    intent.putExtra("bookingDay", bookingDay);
                    intent.putExtra("bookingCode", bookingCode);
                    intent.putExtra("isTakeAway", isTakeAway);
                    intent.putExtra("restoName", restoName);
                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                } else {
                    new MaterialDialog.Builder(mActivity)
                            .content(userApiResponse.getData().getDescription())
                            .positiveText(R.string.ok)
                            .cancelable(true)
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(MaterialDialog dialog, DialogAction which) {

                                }
                            })
                            .show();
                }

            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    private void getBalance(String accountID, String msisdn) {
        restClient.getApiService().postBalanceEcash(Utils.getToken(EcashPaymentAct.this).getAccessToken(), new EcashBalance(accountID, msisdn), new Callback<ApiResponse>() {
            @Override
            public void success(ApiResponse userApiResponse, Response response) {
                progressDialog.dismiss();
                try {
                    JSONObject data = new JSONObject(userApiResponse.getData().toString());
                    String status = data.getString("status");
                    if (status.equals("PROCESSED")) {
                        int accountBalance = data.getInt("accountBalance");
                        Log.d("accountBalance", String.valueOf(accountBalance));
//                        String[] separated = accountBalance.split(".");
//                        Log.d("separated[0]", separated[0]);

                        txt_wallet_balance.setText("Balance: " + CTools.getRP(String.valueOf(accountBalance), true));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                progressDialog.dismiss();
                new MaterialDialog.Builder(EcashPaymentAct.this)
                        .title(R.string.alert)
                        .content(error.getMessage())
                        .positiveText(R.string.ok)
                        .cancelable(false)
                        .show();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(EcashPaymentAct.this, MainAct.class);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
    }
}
