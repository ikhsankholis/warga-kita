package com.dycode.foodgasm.activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.dycode.foodgasm.FoodGasmApp;
import com.dycode.foodgasm.R;
import com.dycode.foodgasm.adapters.CreditCardAdapter;
import com.dycode.foodgasm.dao.CreditCard;
import com.dycode.foodgasm.dao.CreditCardDao;
import com.dycode.foodgasm.dao.DaoSession;
import com.dycode.foodgasm.model.ApiResponse;
import com.dycode.foodgasm.model.ApiResponseList;
import com.dycode.foodgasm.model.EcashOTP;
import com.dycode.foodgasm.model.EcashValidate;
import com.dycode.foodgasm.model.Profile;
import com.dycode.foodgasm.utils.Utils;
import com.google.gson.Gson;
import com.hbb20.CountryCodePicker;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by asus pc on 29/01/2016.
 */
public class CreditCardAct extends BaseActivity implements CreditCardAdapter.StatusChangedListener {

    private static final String TAG = "CreditCardAct";
    public static final String FLAG = "FLAG";
    public static final int FLAG_EDIT_ENABLE = 0;
    public static final int FLAG_EDIT_DISABLED = 2;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.tvMenu)
    TextView tvMenu;
    @BindView(R.id.linAddCard)
    LinearLayout linAddCard;
    @BindView(R.id.linDeleteCard)
    LinearLayout linDeleteCard;
    @BindView(R.id.rvCreditCard)
    RecyclerView rvCreditCard;

    FoodGasmApp foodGasmApp;
    DaoSession daoSession;
    List<com.dycode.foodgasm.dao.CreditCard> creditCardList = new ArrayList<>();
    CreditCardAdapter mAdapter;
    LinearLayoutManager linLayoutManager;
    String id;
    int flag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_credit_card);
        ButterKnife.bind(this);

        // Obtain the shared Tracker instance.
        getTracker("Credit Card List");

        foodGasmApp = (FoodGasmApp) getApplication();
        daoSession = foodGasmApp.getDaoSession();

        tvMenu.setText(getResources().getText(R.string.edit));
        tvMenu.setVisibility(View.VISIBLE);
        mToolbar.setNavigationIcon(R.mipmap.ic_back);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                setResult(1, intent);
                finish();
                overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
            }
        });
        tvTitle.setText(R.string.payment_method);
        flag = getIntent().getExtras().getInt(FLAG);
        if (flag == FLAG_EDIT_DISABLED) {
//            tvMenu.setTextColor(getResources().getColor(R.color.colorGrey));
//            tvMenu.setOnClickListener(null);
//            tvMenu.setEnabled(false);
        }

        linLayoutManager = new LinearLayoutManager(mActivity);
        rvCreditCard.setLayoutManager(linLayoutManager);
        mAdapter = new CreditCardAdapter(mActivity, creditCardList);
        rvCreditCard.setAdapter(mAdapter);
        getCreditCardList();

    }

    public void getCreditCardList() {
        restClient.getApiService().getCreditCardList(Utils.getToken(mActivity).getAccessToken(), new Callback<ApiResponseList<com.dycode.foodgasm.model.CreditCard>>() {

            @Override
            public void success(ApiResponseList<com.dycode.foodgasm.model.CreditCard> creditCardApiResponseList, Response response) {
                 for (int pos = 0; pos < creditCardApiResponseList.getData().size(); pos++) {
                    CreditCard creditCardDao = new com.dycode.foodgasm.dao.CreditCard();
                    creditCardDao.set_id(creditCardApiResponseList.getData().get(pos).getId());
                    creditCardDao.setTransaction_id(creditCardApiResponseList.getData().get(pos).getTransactionId());
                    creditCardDao.setSaved_token_id(creditCardApiResponseList.getData().get(pos).getSavedTokenId());
                    creditCardDao.setMasked_card(creditCardApiResponseList.getData().get(pos).getMaskedCard());
                    creditCardDao.setState(creditCardApiResponseList.getData().get(pos).getState());
                    creditCardDao.setIsEditSelected(creditCardApiResponseList.getData().get(pos).getIsEditSelected());
                    creditCardDao.setCustomer_id(creditCardApiResponseList.getData().get(pos).getCustomer_id());
                    creditCardDao.setProvider(creditCardApiResponseList.getData().get(pos).getProvider());

                    daoSession.insertOrReplace(creditCardDao);
                }
                creditCardList.clear();
                creditCardList.addAll(daoSession.getCreditCardDao().queryBuilder().orderAsc(CreditCardDao.Properties.Saved_token_id).list());
                mAdapter.notifyDataSetChanged();
                checkBtnDeleteState();
            }

            @Override
            public void failure(RetrofitError error) {
                showToast("No Internet Connection");
            }
        });
    }

    public void removeCreditCard(String selectedId) {
        showProgressDialog("Deleting Data...");
        restClient.getApiService().removeCreditCard(selectedId, Utils.getToken(mActivity).getAccessToken(), new Callback<com.dycode.foodgasm.model.CreditCard>() {

            @Override
            public void success(com.dycode.foodgasm.model.CreditCard creditCard, Response response) {
                progressDialog.dismiss();
                List<CreditCard> creditCardListDao = daoSession.getCreditCardDao().queryBuilder().where(CreditCardDao.Properties._id.eq(id)).limit(1).list();
                creditCardListDao.get(0).setIsEditSelected(true);
                daoSession.getCreditCardDao().delete(creditCardListDao.get(0));
                showMaterialDialog(R.string.data_deleted);
                getCreditCardList();
            }

            @Override
            public void failure(RetrofitError error) {
                progressDialog.dismiss();
                showToast("No Internet Connection");
            }
        });
    }

    public void patchCreditCard(String selectedId) {
        showProgressDialog("Please Wait...");
        restClient.getApiService().setCreditCardActive(selectedId, Utils.getToken(mActivity).getAccessToken(), new Callback<com.dycode.foodgasm.model.CreditCard>() {

            @Override
            public void success(com.dycode.foodgasm.model.CreditCard creditCard, Response response) {
                progressDialog.dismiss();
                getCreditCardList();
            }

            @Override
            public void failure(RetrofitError error) {
                progressDialog.dismiss();
                showToast("No Internet Connection");
            }
        });
    }

    @OnClick(R.id.linAddCard)
    public void navigateToPaymentMethod() {

        final Dialog pdialog = new Dialog(CreditCardAct.this);
        pdialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pdialog.setContentView(R.layout.add_payment_pick);

        pdialog.show();


        RelativeLayout r_doku_wallet = (RelativeLayout) pdialog.findViewById(R.id.r_doku_wallet);
        RelativeLayout r_credit_no = (RelativeLayout) pdialog.findViewById(R.id.r_credit_no);
        RelativeLayout r_ecash = (RelativeLayout) pdialog.findViewById(R.id.r_ecash);
        r_doku_wallet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pdialog.dismiss();
                Intent intent = new Intent(CreditCardAct.this, DokuWalletLogin.class);
                intent.putExtra("daftarDokuWallet", "0");
                startActivityForResult(intent, 2);
                overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
            }
        });
        r_credit_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pdialog.dismiss();
                Intent intent = new Intent(mActivity, PaymentMethodAct.class);
                intent.putExtra(PaymentMethodAct.FLAG, PaymentMethodAct.FLAG_SET_CREDIT_CARD);
                startActivityForResult(intent, 0);
                overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
            }
        });
        r_ecash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pdialog.dismiss();
                getProfileData();
            }
        });

    }

    @OnClick(R.id.tvMenu)
    public void changeButtonState() {
        if (tvMenu.getText().toString().equalsIgnoreCase("edit")) {
            if (creditCardList.size() != 0) {
                for (int pos = 0; pos < creditCardList.size(); pos++) {
                    if (creditCardList.get(pos).getIsEditSelected()) {
                        creditCardList.get(pos).setIsEditSelected(false);
                    }
                }
            }

            checkBtnDeleteState();
            tvMenu.setText(R.string.cancel);
            mAdapter.setEditMode(true);
            linAddCard.setVisibility(View.GONE);
            linDeleteCard.setVisibility(View.VISIBLE);
            mAdapter.notifyDataSetChanged();

            linDeleteCard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (creditCardList.size() != 0) {
                        removeCreditCard(id);
                    }
                }
            });
        } else {
            linAddCard.setVisibility(View.VISIBLE);
            linDeleteCard.setVisibility(View.GONE);
            tvMenu.setText(R.string.edit);
            mAdapter.setEditMode(false);
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        setResult(1);
//        finish();
//        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
        Intent intent = new Intent();
        setResult(1, intent);
        finish();
        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        getCreditCardList();
    }

    void checkBtnDeleteState() {
        if (creditCardList.size() != 0) {
            for (int pos = 0; pos < creditCardList.size(); pos++) {
                if (creditCardList.get(pos).getIsEditSelected()) {
                    linDeleteCard.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            removeCreditCard(id);
                            checkBtnDeleteState();
                        }
                    });
                    linDeleteCard.setClickable(true);
                    linDeleteCard.setEnabled(true);
                    linDeleteCard.setBackgroundColor(Color.parseColor("#800000"));
                    return;
                } else {
                    linDeleteCard.setOnClickListener(null);
                    linDeleteCard.setClickable(false);
                    linDeleteCard.setEnabled(false);
                    linDeleteCard.setBackgroundColor(Color.parseColor("#d9d9d9"));
                }
            }
        } else {
            linDeleteCard.setOnClickListener(null);
            linDeleteCard.setClickable(false);
            linDeleteCard.setEnabled(false);
            linDeleteCard.setBackgroundColor(Color.parseColor("#d9d9d9"));
        }


    }

    @Override
    public void onStatusChanged() {
        checkBtnDeleteState();
    }

    @Override
    public void onSelectedIdChanged(String selectedId) {
        id = selectedId;
    }

    @Override
    public void onActiveIdSelected(String selectedId) {
        id = selectedId;
        patchCreditCard(id);
    }

    void verifyNumber(String phonenumber) {
        Log.d("phone > verify ", phonenumber);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final AlertDialog dialog = builder.create();
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogLayout = inflater.inflate(R.layout.dialog_review_verification_phone, null);

        dialog.setView(dialogLayout);
        dialog.setCancelable(true);
        dialog.show();

        final EditText inpPhoneNumber = (EditText) dialog.findViewById(R.id.inpPhoneNumber);
        final CountryCodePicker ccp = (CountryCodePicker) dialog.findViewById(R.id.ccp);
        final TextView txt_conf_yes = (TextView) dialog.findViewById(R.id.txt_conf_yes);
        final TextView txt_conf_no = (TextView) dialog.findViewById(R.id.txt_conf_no);
        inpPhoneNumber.setText(phonenumber);


        txt_conf_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String phoneNumber = inpPhoneNumber.getText().toString();
                String first = phoneNumber.substring(1);

                if (phoneNumber.startsWith("62")) {
                    phoneNumber.replace("62", "0");
                } else if (phoneNumber.startsWith("+62")) {
                    phoneNumber.replace("+62", "0");
                } else if (phoneNumber.startsWith("8")) {
                    phoneNumber = "0" + phoneNumber;
                    Log.d("phonenum > ", phoneNumber);
                }

                String phoneCcp = phoneNumber;
                Log.d("Cek > ", phoneCcp);


                validateAccount(phoneCcp);
                dialog.dismiss();

            }
        });

        txt_conf_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }

    void getProfileData() {
        showProgressDialog("Loading data...");
        restClient.getApiService().getProfileData(Utils.getToken(mActivity).getAccessToken(), new Callback<ApiResponse<Profile>>() {
            @Override
            public void success(ApiResponse<Profile> profileApiResponse, Response response) {
                progressDialog.dismiss();
                Log.d("Profile phone > ", profileApiResponse.getData().getPhone());
                String phone = profileApiResponse.getData().getPhone();
                if (phone.startsWith("62")) {
                    phone = phone.replaceFirst("62", "0");
                } else if (phone.startsWith("+62")) {
                    phone = "0" + phone.replace("+62", "");
                } else if (phone.startsWith("8")) {
                    phone = "0" + phone;
                    Log.d("phonenum > ", phone);
                }
//                verifyNumber(phone);
                validateAccount(phone);
            }

            @Override
            public void failure(RetrofitError error) {
                progressDialog.dismiss();
                showToast("No Internet Connection");
            }
        });
    }

    private void validateAccount(final String msisdn) {

        restClient.getApiService().getAccountValidate(Utils.getToken(mActivity).getAccessToken(), msisdn, new Callback<ApiResponse<EcashValidate>>() {
            @Override
            public void success(ApiResponse<EcashValidate> data, Response response) {
                progressDialog.dismiss();

                Log.d("status > ", data.getData().getStatus());
                String status = data.getData().getStatus();
                if (status.equals("PAYMENT_METHOD_REGISTERED")) {
                    Log.d("registered > ", "true");
                    new MaterialDialog.Builder(mActivity)
                            .content(data.getData().getStatus())
                            .positiveText(R.string.ok)
                            .autoDismiss(false)
                            .cancelable(false)
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(MaterialDialog dialog, DialogAction which) {
                                    dialog.dismiss();
                                }
                            })
                            .show();
                } else if (!data.getData().getStatus().equals("REGISTERED")) {
                    Log.d("registered > ", "REGISTERED");
                    EcashOTP reqOTPeCASH = new EcashOTP(msisdn, "N02");
                    reqOTPeCASH(reqOTPeCASH, data.getData().getStatus());
                } else {
                    Log.d("registered > ", " NOT REGISTERED");
                    EcashOTP reqOTPeCASH = new EcashOTP(msisdn, "N01");
                    reqOTPeCASH(reqOTPeCASH, data.getData().getStatus());
                }


            }

            @Override
            public void failure(RetrofitError error) {
                progressDialog.dismiss();
                showAlertDialog("Error", error.getMessage());
            }
        });

    }

    private void reqOTPeCASH(EcashOTP ecashOTP, final String status) {
        progressDialog.show();
        restClient.getApiService().postPhoneVerifyEcash(Utils.getToken(mActivity).getAccessToken(), ecashOTP, new Callback<ApiResponse>() {
            @Override
            public void success(ApiResponse data, Response response) {
                progressDialog.dismiss();

                Log.d("data String> ", new Gson().toJson(data.getData()));

                try {
                    JSONObject dataResponse = new JSONObject(new Gson().toJson(data.getData()));
                    String tittle = dataResponse.getString("status");
                    if (!tittle.equals("PROCESSED")) {
                        String description = dataResponse.getString("description");
                        new MaterialDialog.Builder(mActivity)
                                .content(description)
                                .positiveText(R.string.ok)
                                .autoDismiss(false)
                                .cancelable(false)
                                .onPositive(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(MaterialDialog dialog, DialogAction which) {
                                        dialog.dismiss();
                                    }
                                })
                                .show();
                    } else {
                        final String mobileno = dataResponse.getString("mobileno");

                        if (status.equals("REGISTERED")) {
                            Intent intent = new Intent(mActivity, ECashRegisterPairing.class);
                            intent.putExtra("ecashID", mobileno);
                            intent.putExtra("status", status);
                            startActivityForResult(intent, 0);
                            overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                        } else {
                            new MaterialDialog.Builder(mActivity)
                                    .content("Nomor Anda " + "[" + mobileno + "]" + " belum terdaftar, silahkan kontak Mandiri untuk detail lebih lanjut.")
                                    .positiveText(R.string.ok)
                                    .autoDismiss(false)
                                    .cancelable(false)
                                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                                        @Override
                                        public void onClick(MaterialDialog dialog, DialogAction which) {
                                            dialog.dismiss();
                                        }
                                    })
                                    .show();
//                            Intent intent = new Intent(mActivity, ECashRegister.class);
//                            intent.putExtra("ecashID", mobileno);
//                            intent.putExtra("status", status);
//                            startActivityForResult(intent, 0);
//                            overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                        }
//
//                        new MaterialDialog.Builder(mActivity)
//                                .content(tittle)
//                                .positiveText(R.string.ok)
//                                .cancelable(false)
//                                .autoDismiss(false)
//                                .onPositive(new MaterialDialog.SingleButtonCallback() {
//                                    @Override
//                                    public void onClick(MaterialDialog dialog, DialogAction which) {
//                                        dialog.dismiss();
//
//                                        if (status.equals("REGISTERED")) {
//                                            Intent intent = new Intent(mActivity, ECashRegisterPairing.class);
//                                            intent.putExtra("ecashID", mobileno);
//                                            intent.putExtra("status", status);
//                                            startActivityForResult(intent, 0);
//                                            overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
//                                        } else {
//                                            Intent intent = new Intent(mActivity, ECashRegister.class);
//                                            intent.putExtra("ecashID", mobileno);
//                                            intent.putExtra("status", status);
//                                            startActivityForResult(intent, 0);
//                                            overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
//                                        }
//
//                                    }
//                                })
//                                .show();

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void failure(RetrofitError error) {
                progressDialog.dismiss();
                showAlertDialog("Error", error.getMessage());
            }
        });
    }

}
