package com.dycode.foodgasm.activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.text.InputFilter;
import android.text.InputType;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.simplelist.MaterialSimpleListAdapter;
import com.doku.sdkocov2.DirectSDK;
import com.doku.sdkocov2.interfaces.iPaymentCallback;
import com.doku.sdkocov2.model.PaymentItems;
import com.dycode.foodgasm.FoodGasmApp;
import com.dycode.foodgasm.R;
import com.dycode.foodgasm.adapters.DialogDokuAdapter;
import com.dycode.foodgasm.dao.CreditCardDao;
import com.dycode.foodgasm.dao.DaoSession;
import com.dycode.foodgasm.model.ApiResponse;
import com.dycode.foodgasm.model.ApiResponseList;
import com.dycode.foodgasm.model.CreditCard;
import com.dycode.foodgasm.model.DirectPay;
import com.dycode.foodgasm.model.DokuResponse;
import com.dycode.foodgasm.model.DokuSample;
import com.dycode.foodgasm.model.DokuWallet;
import com.dycode.foodgasm.model.DokuWalletResponse;
import com.dycode.foodgasm.model.Order;
import com.dycode.foodgasm.model.PostOrder;
import com.dycode.foodgasm.model.Price;
import com.dycode.foodgasm.rest.RestClient;
import com.dycode.foodgasm.utils.ApiConnection;
import com.dycode.foodgasm.utils.AppsUtil;
import com.dycode.foodgasm.utils.CTools;
import com.dycode.foodgasm.utils.Constants;
import com.dycode.foodgasm.utils.NumberTextWatcher;
import com.dycode.foodgasm.utils.SPManager;
import com.dycode.foodgasm.utils.Utils;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by asus pc on 15/10/2015.
 */
public class TotalPayment extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.txt_paywith_invoice)
    TextView payWithInvoice;
    @BindView(R.id.l_pay_invoice)
    LinearLayout lPayinvoice;
    @BindView(R.id.l_pay)
    LinearLayout lPay;
    @BindView(R.id.img_paywith_invoice)
    ImageView imgPayInvoice;
    @BindView(R.id.invoice_edit)
    EditText invoice_edit;
    @BindView(R.id.pay_resto_name)
    TextView txtRestoName;
    @BindView(R.id.tvSubtotalValue)
    TextView tvSubtotalValue;
    @BindView(R.id.tvTaxValue)
    TextView tvTaxValue;
    @BindView(R.id.tvConvenienceChargeValue)
    TextView tvConvenienceChargeValue;
    @BindView(R.id.tvSurcharge)
    TextView tvSurcharge;
    @BindView(R.id.tvSurchargeValue)
    TextView tvSurchargeValue;
    @BindView(R.id.editText2)
    EditText editTextCoupon;

    private Integer total = 0;
    double tax;
    private int totalPay = 0;
    private Price pr;

    ArrayList<com.dycode.foodgasm.dao.Menu> menuList = new ArrayList<>();
    private DirectSDK directSDK;
    private JSONObject respongetTokenSDK;
    TelephonyManager telephonyManager;
    String invoiceNumber;
    private MaterialSimpleListAdapter aadapter;
    private String payWithtxt;
    private ArrayList<DokuSample> dokuList;

    private DokuSample dokuData;
    private LinearLayoutManager linLayoutManager;
    private DialogDokuAdapter mAdapter;

    List<com.dycode.foodgasm.dao.CreditCard> creditCardDaoList;

    List<com.dycode.foodgasm.model.CreditCard> creditCardList = new ArrayList<>();
    DaoSession daoSession;
    private String creditCardNumber;
    private String creditCardMaskedNumber;
    private Dialog pdialog;
    private RecyclerView recy_doku;
    private String restoAddres = "";
    private String restoName = "";
    private Object restoCity = "";
    private String jsonRespon;
    private String restoID;
    private String payWithtxtNew;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.total_payment2);
        ButterKnife.bind(this);
//        Dexter.initialize(this);
//        CheckPermission();
        dokuList = new ArrayList<DokuSample>();

        //tes Doku

        mToolbar.setNavigationIcon(R.mipmap.ic_back);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        tvTitle.setText(R.string.pay_invoice);

        editTextCoupon.setInputType(InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);
        editTextCoupon.setFilters(new InputFilter[]{new AllCaps(), new InputFilter.LengthFilter(6)});

        lPayinvoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });

        invoice_edit.addTextChangedListener(new NumberTextWatcher(invoice_edit));
        restoName = getIntent().getExtras().getString("restoName");
        restoAddres = getIntent().getExtras().getString("restoAddress");
        restoCity = getIntent().getExtras().getString("restoCity");
        txtRestoName.setText(restoName + "\n" + restoAddres + "\n" + restoCity);
        restoID = getIntent().getExtras().getString("restoID");
        daoSession = ((FoodGasmApp) this.getApplication()).getDaoSession();
        creditCardDaoList = daoSession.getCreditCardDao().queryBuilder().orderDesc(CreditCardDao.Properties.State).list();

        txtRestoName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SPManager.saveString(TotalPayment.this, "res_token_id", null);
            }
        });


        getCreditCardList();

//
        pr = new Price();
        pr.setSubtotal(1);
        pr.setDiscount(1);
        pr.setTax(1);
        pr.setBankCharge(1900);
        pr.setConvenience(1);
        pr.setTotal(27000);
        lPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                restClient.getApiService().postOrder(new PostOrder(Utils.getToken(TotalPayment.this).getUid(), "56e8ffc67d78d31d6aef192e", "2016-10-01T10:00:00.00+0700", true, 1, menuList, "", pr, "", "424242-4242", "creditcard"), Utils.getToken(TotalPayment.this).getAccessToken(), "creditcard", new Callback<ApiResponse<Order>>() {
//                    @Override
//                    public void success(ApiResponse<Order> orderApiResponse, Response response) {
//
//                    }
//
//                    @Override
//                    public void failure(RetrofitError error) {
//                        showAlertDialog("Add Order Failed", error.getMessage());
//                    }
//                });
                if (invoice_edit.length() < 2) {
                    showToast("Invoice Kosong");
                } else {
                    payMethod();
                }

            }
        });

    }

//    private void CheckPermission() {
//        Dexter.checkPermissions(new MultiplePermissionsListener() {
//            @Override
//            public void onPermissionsChecked(MultiplePermissionsReport report) {
//
//            }
//
//            @Override
//            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
//                token.continuePermissionRequest();
//            }
//        }, Manifest.permission.READ_PHONE_STATE);
//    }

    public void registerDokuToken(int position) {

        invoiceNumber = String.valueOf(AppsUtil.nDigitRandomNo(10));
        telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

        PaymentItems cardDetails = null;
        cardDetails = InputCard(position);
        //showLog(InputCard(position).getDataWords());
        directSDK = new DirectSDK();
        directSDK.setCart_details(cardDetails);


        if (payWithtxt.equals("Doku Wallet")) {
            Log.d("paywith", payWithtxt + "04");
            directSDK.setPaymentChannel(2);
        } else {
            directSDK.setPaymentChannel(1);
        }
        directSDK.getResponse(new iPaymentCallback() {
            @Override
            public void onSuccess(final String text) {
                Log.d("cek token DOKU-->", text);

                showProgressDialog("Submitting..");
                jsonRespon = text;
                try {
                    respongetTokenSDK = new JSONObject(text);
                    progressDialog.dismiss();
                    if (respongetTokenSDK.getString("res_response_code").equalsIgnoreCase("0000")) {
                        Log.d("cek token id -->", respongetTokenSDK.getString("res_token_id"));
                        SPManager.saveString(TotalPayment.this, "res_token_id", respongetTokenSDK.getString("res_token_id"));
                        showProgressDialog("Silahkan Tunggu");

                        restClient.getApiService().postDokuResponseSandbox(dokuData.getOrder_id(), Utils.getToken(TotalPayment.this).getAccessToken(), new DokuResponse(jsonRespon), new Callback<ApiResponse<String>>() {
                            @Override
                            public void success(ApiResponse<String> userApiResponse, Response response) {
                                progressDialog.dismiss();
                                JSONObject json;
                                try {
                                    json = new JSONObject(userApiResponse.getData().toString());
                                    if (json.getString("res_response_code").equalsIgnoreCase("0000") && json != null) {
                                        Intent intent = new Intent(getApplicationContext(), ResultPaymentAct.class);
                                        intent.putExtra("data", json.toString());
                                        startActivity(intent);
                                        finish();
                                        Toast.makeText(getApplicationContext(), " PAYMENT SUKSES", Toast.LENGTH_SHORT).show();
                                    } else {
                                        Intent intent = new Intent(TotalPayment.this, ResultPaymentAct.class);
                                        intent.putExtra("data", json.toString());
                                        startActivity(intent);
                                        finish();
                                        Toast.makeText(getApplicationContext(), "PAYMENT ERROR", Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void failure(RetrofitError error) {
                                progressDialog.dismiss();
                                new MaterialDialog.Builder(TotalPayment.this)
                                        .title(R.string.alert)
                                        .content(error.getMessage())
                                        .positiveText(R.string.ok)
                                        .cancelable(false)
                                        .show();
                            }
                        });
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onError(final String text) {
                //showLog(text);
            }

            @Override
            public void onException(Exception eSDK) {
                eSDK.printStackTrace();
            }
        }, getApplicationContext());
    }

    public void getCreditCardList() {
        showProgressDialog("Loading...");
        restClient.getApiService().getCreditCardList(Utils.getToken(this).getAccessToken(), new Callback<ApiResponseList<CreditCard>>() {
                    @Override
                    public void success(ApiResponseList<com.dycode.foodgasm.model.CreditCard> creditCardApiResponseList, Response response) {
                        progressDialog.dismiss();
                        creditCardList.clear();
                        creditCardList.addAll(creditCardApiResponseList.getData());

                        for (int pos = 0; pos < creditCardList.size(); pos++) {
                            //Log.d("lalaa", String.valueOf(creditCardApiResponseList.getData().get(pos).getMaskedCard()));
                            com.dycode.foodgasm.dao.CreditCard creditCardDao = new com.dycode.foodgasm.dao.CreditCard();
                            creditCardDao.set_id(creditCardList.get(pos).getId());
                            creditCardDao.setTransaction_id(creditCardList.get(pos).getTransactionId());
                            creditCardDao.setSaved_token_id(creditCardList.get(pos).getSavedTokenId());
                            creditCardDao.setMasked_card(creditCardList.get(pos).getMaskedCard());
                            creditCardDao.setState(creditCardList.get(pos).getState());
                            creditCardDao.setIsEditSelected(creditCardList.get(pos).getIsEditSelected());
                            creditCardDao.setCustomer_id(creditCardList.get(pos).getCustomer_id());
                            creditCardDao.setProvider(creditCardList.get(pos).getProvider());
                            daoSession.insertOrReplace(creditCardDao);
                        }
                        checkCreditCardDisplay();
                    }


                    @Override
                    public void failure(RetrofitError error) {
                        progressDialog.dismiss();
                        //showToast("No Internet Connection");
                    }
                }
        );
    }

    public void checkCreditCardDisplay() {
        creditCardDaoList = daoSession.getCreditCardDao().queryBuilder().orderDesc(CreditCardDao.Properties.State).list();
        if (creditCardDaoList.size() == 1) {
            String creditCardNumber = creditCardDaoList.get(0).getMasked_card();
            creditCardMaskedNumber = creditCardDaoList.get(0).getMasked_card();
            creditCardNumber = creditCardNumber.substring(creditCardNumber.length() - 4, creditCardNumber.length());
        }
    }

    private PaymentItems InputCard(int position) {
        String dataWord = AppsUtil.SHA1(dokuData.getReq_amount() + dokuData.getReq_mall_id() + "cCOc42ElK71g" + dokuData.getReq_transaction_id() + 360 + telephonyManager.getDeviceId());
        String customerID = String.valueOf(AppsUtil.nDigitRandomNo(8));
        PaymentItems paymentItems = new PaymentItems();
        paymentItems.setDataAmount(dokuData.getReq_amount());
        paymentItems.setDataBasket(dokuData.getReq_basket());
        paymentItems.setDataCurrency(dokuData.getReq_currency());
        paymentItems.setDataWords(dataWord);
        paymentItems.setDataMerchantChain(dokuData.getReq_chain_merchant());
        paymentItems.setDataSessionID(dokuData.getReq_session_id());
        paymentItems.setDataTransactionID(dokuData.getReq_transaction_id());
        paymentItems.setDataMerchantCode(dokuData.getReq_mall_id());
        paymentItems.setDataImei(telephonyManager.getDeviceId());
        paymentItems.setMobilePhone("");
        paymentItems.setPublicKey("MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAiwz9tMtvtz2Ch2UbvzUr9XX1Olb5nPG7egobwlcyBoblY1aqIUw2LFJeTGtOZ5JMAk7w8AFG36ii6b2lo5K8bORvx3StVcavwT8pD8VMQ2AI9uaEmseS4Gn8AJzPSMV5lw/1lMpTDId2ug0pH5xnAS4StAvrKQ47wyQ1tVE/v7UbApVlX/ok/I24FN7uvKoteEXPGsLrwFRcjseXYYNDZ1OIyqIPc5HftL1PiW6P29Fjn++3aEFtHl+xDyOkBqo95NdHqQAqH9RF3jladh21PsGlQHZeTXSfSUz9/a9ebkbOc41TLvtJt50f5MzwGfoTheBOiGh4xwaX/Y+NHkgSDQIDAQAB");
        paymentItems.isProduction(true);
        paymentItems.setCustomerID(customerID);
        if (position != 400) {
            Log.d("Hello token", dokuData.getSaved_token_id());
            paymentItems.setTokenPayment(dokuData.getSaved_token_id());
            paymentItems.setCustomerID(dokuData.getCustomer_id());
        }
        return paymentItems;
    }

    private void payMethod() {

        pdialog = new Dialog(TotalPayment.this);
        pdialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pdialog.setContentView(R.layout.payment_pick);

        recy_doku = (RecyclerView) pdialog.findViewById(R.id.recy_doku);
        mAdapter = new DialogDokuAdapter(this, creditCardDaoList);
        linLayoutManager = new LinearLayoutManager(this);
        recy_doku.setHasFixedSize(true);
        recy_doku.setLayoutManager(linLayoutManager);
        recy_doku.setAdapter(mAdapter);

        recy_doku.addOnItemTouchListener(new TotalPayment.RecyclerTouchListener(getApplicationContext(), recy_doku, new TotalPayment.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                //showLog("hello >>" + (String.valueOf(position)));

                if (creditCardList.get(position).getProvider() != null) {
                    if (creditCardList.get(position).getProvider().equals("dokuwallet")) {
                        payWithtxt = "Doku Wallet";
                        pdialog.dismiss();
                        tax = 0.03;
                        total = Integer.valueOf(invoice_edit.getText().toString().replace(",", "")) * 3 / 100;
                        totalPay = Integer.valueOf(invoice_edit.getText().toString().replace(",", ""));
                        payWithInvoice.setText("Doku Wallet");
                        imgPayInvoice.setImageResource(R.mipmap.doku_wallet);
                        tvSubtotalValue.setText(CTools.getRP(invoice_edit.getText().toString().replace(",", ""), false));
                        tvSurchargeValue.setText(CTools.getRP(String.valueOf(total + 1000), false));
                        tvConvenienceChargeValue.setText(CTools.getRP(String.valueOf(total + totalPay + 1000).replace("", ""), false));
                        payWithtxtNew = "0";
                        getDokuSample(position, creditCardDaoList.get(position).get_id());
                    } else {
                        payWithtxt = "Doku";
                        pdialog.dismiss();
                        tax = 0.03;
                        total = Integer.valueOf(invoice_edit.getText().toString().replace(",", "")) * 3 / 100;
                        totalPay = Integer.valueOf(invoice_edit.getText().toString().replace(",", ""));
                        payWithInvoice.setText("Doku");
                        imgPayInvoice.setImageResource(R.mipmap.doku_logo);
                        tvSubtotalValue.setText(CTools.getRP(invoice_edit.getText().toString().replace(",", ""), false));
                        tvSurchargeValue.setText(CTools.getRP(String.valueOf(total + 1000), false));
                        tvConvenienceChargeValue.setText(CTools.getRP(String.valueOf(total + totalPay + 1000).replace("", ""), false));

                        getDokuSample(position, creditCardDaoList.get(position).get_id());
                    }
                }

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
        pdialog.show();
        RelativeLayout r_credit = (RelativeLayout) pdialog.findViewById(R.id.r_credit);
        RelativeLayout r_doku = (RelativeLayout) pdialog.findViewById(R.id.r_doku);
        RelativeLayout r_dompetku = (RelativeLayout) pdialog.findViewById(R.id.r_dompetku);
        RelativeLayout r_doku_wallet = (RelativeLayout) pdialog.findViewById(R.id.r_doku_wallet);

        r_doku.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                payWithtxt = "Doku";
                pdialog.dismiss();
                tax = 0.03;
                total = Integer.valueOf(invoice_edit.getText().toString().replace(",", "")) * 3 / 100;
                totalPay = Integer.valueOf(invoice_edit.getText().toString().replace(",", ""));
                payWithInvoice.setText("Doku");
                imgPayInvoice.setImageResource(R.mipmap.doku_logo);
                tvSubtotalValue.setText(CTools.getRP(invoice_edit.getText().toString().replace(",", ""), false));
                tvSurchargeValue.setText(CTools.getRP(String.valueOf(total + 1000), false));
                tvConvenienceChargeValue.setText(CTools.getRP(String.valueOf(total + totalPay + 1000).replace("", ""), false));

                getDokuSample(400, null);

            }
        });

        r_doku_wallet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                payWithtxt = "Doku Wallet";
                payWithtxtNew = "1";
                pdialog.dismiss();
                tax = 0.03;
                total = Integer.valueOf(invoice_edit.getText().toString().replace(",", "")) * 3 / 100;
                totalPay = Integer.valueOf(invoice_edit.getText().toString().replace(",", ""));
                payWithInvoice.setText("Doku");
                imgPayInvoice.setImageResource(R.mipmap.doku_wallet);
                tvSubtotalValue.setText(CTools.getRP(invoice_edit.getText().toString().replace(",", ""), false));
                tvSurchargeValue.setText(CTools.getRP(String.valueOf(total + 1000), false));
                tvConvenienceChargeValue.setText(CTools.getRP(String.valueOf(total + totalPay + 1000).replace("", ""), false));

                getDokuSample(400, null);
//                Intent intent = new Intent(getApplicationContext(), DokuWalletLogin.class);
//                intent.putExtra("orderId", dokuData.getOrder_id());
//                intent.putExtra("accountId", "");
//                startActivity(intent);
            }
        });

        r_dompetku.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pdialog.dismiss();
                new MaterialDialog.Builder(TotalPayment.this)
                        .title("Dompetku Payment")
                        .inputType(InputType.TYPE_CLASS_NUMBER)
                        .input("Masukin PIN", "", new MaterialDialog.InputCallback() {
                            @Override
                            public void onInput(MaterialDialog dialog, CharSequence input) {
                                // Do something
                                SPManager.saveString(TotalPayment.this, "pin", input.toString());

                                tax = 0.03;
                                total = Integer.valueOf(invoice_edit.getText().toString().replace(",", "")) * 3 / 100;
                                totalPay = Integer.valueOf(invoice_edit.getText().toString().replace(",", ""));
                                payWithInvoice.setText("TBank");
                                imgPayInvoice.setImageResource(R.mipmap.tbank);
                                tvSubtotalValue.setText(CTools.getRP(invoice_edit.getText().toString().replace(",", ""), false));
                                tvSurchargeValue.setText(CTools.getRP(String.valueOf(total + 2500), false));
                                tvConvenienceChargeValue.setText(CTools.getRP(String.valueOf(total + totalPay + 2500).replace("", ""), false));
                            }
                        })
                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                dialog.dismiss();
                            }
                        })
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                restClient.getApiService().postOrder(new PostOrder(Utils.getToken(TotalPayment.this).getUid(), "56e8ffc67d78d31d6aef192e", "2016-10-01T10:00:00.00+0700", true, 1, menuList, "", pr, "", "424242-4242", "creditcard"), Utils.getToken(TotalPayment.this).getAccessToken(), "creditcard", new Callback<ApiResponse<Order>>() {
                                    @Override
                                    public void success(ApiResponse<Order> orderApiResponse, Response response) {

                                    }

                                    @Override
                                    public void failure(RetrofitError error) {
                                        showAlertDialog("Add Order Failed", error.getMessage());
                                    }
                                });
                            }
                        })
                        .show();
            }
        });


        RelativeLayout r_credit_no = (RelativeLayout) pdialog.findViewById(R.id.r_credit_no);


        if (creditCardDaoList.size() > 0) {
            r_credit.setVisibility(View.GONE);
            r_credit_no.setVisibility(View.GONE);
        } else {
            r_credit_no.setVisibility(View.GONE);
        }

        r_credit_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pdialog.dismiss();
                Intent intent = new Intent(TotalPayment.this, CreditCardAct.class);
                intent.putExtra(CreditCardAct.FLAG, CreditCardAct.FLAG_EDIT_DISABLED);
                startActivityForResult(intent, 0);
                overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);

            }
        });

//        pdialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


//
//        new MaterialDialog.Builder(TotalPayment.this)
//                .title("Pay With")
//                .items(dataPay)
//                .autoDismiss(true)
//                .itemsCallback(new MaterialDialog.ListCallback() {
//                    @Override
//                    public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
//
//                        if (invoice_edit.length() > 2) {
//                            if (dataPay.get(which).equals("Creditcard")) {
//                                tax = 0.03;
//                                total = Integer.valueOf(invoice_edit.getText().toString().replace(",", "")) * 3 / 100;
//                                totalPay = Integer.valueOf(invoice_edit.getText().toString().replace(",", ""));
//                                payWithInvoice.setText("Creditcard");
//                                imgPayInvoice.setImageResource(R.mipmap.ic_credit_card_black);
//                                tvSubtotalValue.setText(CTools.getRP(invoice_edit.getText().toString().replace(",", ""), false));
//                                tvSurchargeValue.setText(CTools.getRP(String.valueOf(total + 2500), false));
//                                tvConvenienceChargeValue.setText(CTools.getRP(String.valueOf(total + totalPay + 2500).replace("", ""), false));
//
//
//                            } else if (dataPay.get(which).equals("TBank")) {
//                                tax = 0.03;
//                                total = Integer.valueOf(invoice_edit.getText().toString().replace(",", "")) * 3 / 100;
//                                totalPay = Integer.valueOf(invoice_edit.getText().toString().replace(",", ""));
//                                payWithInvoice.setText("TBank");
//                                imgPayInvoice.setImageResource(R.mipmap.tbank);
//                                tvSubtotalValue.setText(CTools.getRP(invoice_edit.getText().toString().replace(",", ""), false));
//                                tvSurchargeValue.setText(CTools.getRP(String.valueOf(total + 2500), false));
//                                tvConvenienceChargeValue.setText(CTools.getRP(String.valueOf(total + totalPay + 2500).replace("", ""), false));
//                            } else {
//                                tax = 0.03;
//                                total = Integer.valueOf(invoice_edit.getText().toString().replace(",", "")) * 3 / 100;
//                                totalPay = Integer.valueOf(invoice_edit.getText().toString().replace(",", ""));
//                                payWithInvoice.setText("Doku");
//                                imgPayInvoice.setImageResource(R.mipmap.doku_logo);
//                                tvSubtotalValue.setText(CTools.getRP(invoice_edit.getText().toString().replace(",", ""), false));
//                                tvSurchargeValue.setText(CTools.getRP(String.valueOf(total + 1000), false));
//                                tvConvenienceChargeValue.setText(CTools.getRP(String.valueOf(total + totalPay + 1000).replace("", ""), false));
//                            }
//                        }
//                    }
//
//                })
//                .show();
    }

    private class RequestPayment extends AsyncTask<String, String, JSONObject> {

        private ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(TotalPayment.this);
            pDialog.setMessage("Mohon Tunggu ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject defResp = null;

            try {
                List<NameValuePair> data = new ArrayList<NameValuePair>(3);
                data.add(new BasicNameValuePair("data", jsonRespon));

                // Getting JSON from URL
                String conResult = ApiConnection.httpsConnection(TotalPayment.this,
                        Constants.URL_CHARGING_DOKU_DAN_CC, data);
                Log.d("CHARGING PAYMENT", conResult);

                defResp = new JSONObject(conResult);

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return defResp;
        }

        @Override
        protected void onPostExecute(JSONObject json) {

            pDialog.dismiss();

            if (json != null) {
                try {

                    if (json.getString("res_response_code").equalsIgnoreCase("0000") && json != null) {
                        Intent intent = new Intent(getApplicationContext(), ResultPaymentAct.class);
                        intent.putExtra("data", json.toString());
                        startActivity(intent);
                        finish();
                        Toast.makeText(getApplicationContext(), " PAYMENT SUKSES", Toast.LENGTH_SHORT).show();
                    } else {
                        Intent intent = new Intent(getApplicationContext(), ResultPaymentAct.class);
                        intent.putExtra("data", json.toString());
                        startActivity(intent);
                        finish();
                        Toast.makeText(getApplicationContext(), "PAYMENT ERROR", Toast.LENGTH_SHORT).show();


                    }


                    //showProgressDialog("Silahkan Tunggu");
//                    restClient.getApiService().postDokuResponse(new DokuResponse(jsonRespon), new Callback<ApiResponse<String>>() {
//                        @Override
//                        public void success(ApiResponse<String> userApiResponse, Response response) {
//                            progressDialog.dismiss();
////                        Log.d("status", userApiResponse.getData());
//
//                        }
//
//                        @Override
//                        public void failure(RetrofitError error) {
//                            progressDialog.dismiss();
//                            showMaterialDialogAlert(error.getMessage());
//                        }
//                    });
                } catch (JSONException e) {
                    e.printStackTrace();

                }

            }
        }
    }


    private void getDokuSample(final int position, String credential) {
        String walletPay = "";
        Log.d("cek doku wallet paywith>", payWithtxt);
        if (payWithtxt.equals("Doku Wallet")) {
            walletPay = "dokuwallet";
        } else {
            walletPay = "dokucc";
        }

        restClient = RestClient.getInstance();
        restClient.getApiService().getDokuSample(Utils.getToken(TotalPayment.this).getAccessToken(), new DirectPay(String.valueOf(total + totalPay + 1000), restoID, walletPay, credential), new Callback<ApiResponse<DokuSample>>() {
            @Override
            public void success(ApiResponse<DokuSample> categoryApiResponseList, Response response) {
                Log.d("cek dOku sample", String.valueOf(categoryApiResponseList.getData().getReq_amount()));
                dokuData = categoryApiResponseList.getData();

                if (payWithtxt.equals("Doku Wallet")) {
                    if (payWithtxtNew.equals("1")) {
                        Log.d("belum", "belum");
                        Intent intent = new Intent(getApplicationContext(), DokuWalletLogin.class);
                        intent.putExtra("walletStatus", "0");
                        intent.putExtra("orderId", dokuData.getOrder_id());
                        startActivity(intent);
                    } else {
                        if (creditCardList.get(position).getCustomer_id() != null) {
                            Log.d("pernah login wallet", creditCardList.get(position).getCustomer_id());
                            //PostDokuWallet(dokuData.getOrder_id(), creditCardList.get(position).getCustomer_id());
                            Intent intent = new Intent(TotalPayment.this, DokuWalletLogin.class);
                            intent.putExtra("walletStatus", "1");
                            intent.putExtra("orderId", dokuData.getOrder_id());
                            intent.putExtra("accountId", creditCardList.get(position).getCustomer_id());
                            startActivity(intent);
                        }
                    }
                } else {
                    registerDokuToken(position);
                }


            }

            @Override
            public void failure(RetrofitError error) {
                //showToast("No Internet Connection");
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        setResult(RESULT_OK, intent);
        finish();
        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0 && resultCode == 1) {
            List<com.dycode.foodgasm.dao.CreditCard> creditCard = daoSession.getCreditCardDao().queryBuilder().orderDesc(CreditCardDao.Properties.State).list();
            if (creditCard != null && creditCard.size() != 0 && creditCardDaoList != null && creditCardDaoList.size() != 0) {
                Log.d("cek data isi", "ada");
                String creditCardNumber = creditCard.get(0).getMasked_card();
                creditCardMaskedNumber = creditCardDaoList.get(0).getMasked_card();
            } else {
                Log.d("cek data isi", "kosong");
                getCreditCardList();
            }
        }
    }

    public static class AllCaps implements InputFilter {
        public CharSequence filter(CharSequence source, int start, int end,
                                   Spanned dest, int dstart, int dend) {
            for (int i = start; i < end; i++) {
                if (Character.isLowerCase(source.charAt(i))) {
                    char[] v = new char[end - start];
                    TextUtils.getChars(source, start, end, v, 0);
                    String s = new String(v).toUpperCase();

                    if (source instanceof Spanned) {
                        SpannableString sp = new SpannableString(s);
                        TextUtils.copySpansFrom((Spanned) source,
                                start, end, null, sp, 0);
                        return sp;
                    } else {
                        return s;
                    }
                }
            }

            return null; // keep original
        }
    }


    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private TotalPayment.ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final TotalPayment.ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }


    private void PostDokuWallet(String orderid, String id) {
        showProgressDialog("Submitting..");
        restClient.getApiService().postDokuResponseWallet(orderid, Utils.getToken(TotalPayment.this).getAccessToken(), new DokuWallet(id), new Callback<ApiResponse<DokuWalletResponse>>() {
            @Override
            public void success(ApiResponse<DokuWalletResponse> userApiResponse, Response response) {
                progressDialog.dismiss();
                Log.d("cek status doku wallet -->", userApiResponse.getData().getResponseCode());
                String status_code = userApiResponse.getData().getResponseCode();

                if (!userApiResponse.getData().getResponseCode().equals("0000")) {
                    new MaterialDialog.Builder(TotalPayment.this)
                            .title(R.string.alert)
                            .content(userApiResponse.getMeta().getMessage())
                            .positiveText(R.string.ok)
                            .cancelable(false)
                            .show();
                } else {
                    Intent intent = new Intent(getApplicationContext(), ResultPaymentAct.class);
                    intent.putExtra("data", status_code);
                    startActivity(intent);
                    finish();

//                    if (userApiResponse.getData().getResponseCode().equalsIgnoreCase("0000")) {
//                        Intent intent = new Intent(getApplicationContext(), ResultPayment.class);
//                        intent.putExtra("data", userApiResponse.getData().toString());
//                        startActivity(intent);
//                        finish();
//                    } else {
//                        Intent intent = new Intent(getApplicationContext(), ResultPayment.class);
//                        intent.putExtra("data", userApiResponse.getData().toString());
//                        startActivity(intent);
//                        finish();
//                    }
//                    JSONObject json;
//                    try {
//                        json = new JSONObject(userApiResponse.getData().toString());
//                        if (json.getString("res_response_code").equalsIgnoreCase("0000") && json != null) {
//                            Intent intent = new Intent(getApplicationContext(), ResultPayment.class);
//                            intent.putExtra("data", json.toString());
//                            startActivity(intent);
//                            finish();
//                            Toast.makeText(getApplicationContext(), " PAYMENT SUKSES", Toast.LENGTH_SHORT).show();
//                        } else {
//                            Intent intent = new Intent(TotalPayment.this, ResultPayment.class);
//                            intent.putExtra("data", json.toString());
//                            startActivity(intent);
//                            finish();
//                            Toast.makeText(getApplicationContext(), "PAYMENT ERROR", Toast.LENGTH_SHORT).show();
//                        }
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
                }

            }

            @Override
            public void failure(RetrofitError error) {
                progressDialog.dismiss();
                new MaterialDialog.Builder(TotalPayment.this)
                        .title(R.string.alert)
                        .content(error.getMessage())
                        .positiveText(R.string.ok)
                        .cancelable(false)
                        .show();
            }
        });
    }
}
