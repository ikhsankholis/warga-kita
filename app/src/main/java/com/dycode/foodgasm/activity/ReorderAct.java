package com.dycode.foodgasm.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.doku.sdkocov2.DirectSDK;
import com.doku.sdkocov2.interfaces.iPaymentCallback;
import com.doku.sdkocov2.model.PaymentItems;
import com.dycode.foodgasm.FoodGasmApp;
import com.dycode.foodgasm.R;
import com.dycode.foodgasm.dao.CreditCardDao;
import com.dycode.foodgasm.dao.DaoSession;
import com.dycode.foodgasm.feature.pay.PayActivity;
import com.dycode.foodgasm.model.ApiResponse;
import com.dycode.foodgasm.model.DokuResponse;
import com.dycode.foodgasm.model.DokuSample;
import com.dycode.foodgasm.model.Inbox;
import com.dycode.foodgasm.model.Menu;
import com.dycode.foodgasm.model.Order;
import com.dycode.foodgasm.model.OrderDetail;
import com.dycode.foodgasm.model.PatchOrder;
import com.dycode.foodgasm.receiver.FoodgasmNotificationHandler;
import com.dycode.foodgasm.rest.RestClient;
import com.dycode.foodgasm.utils.AppsUtil;
import com.dycode.foodgasm.utils.Constants;
import com.dycode.foodgasm.utils.FoodgasmDateUtil;
import com.dycode.foodgasm.utils.FormatterUtil;
import com.dycode.foodgasm.utils.SPManager;
import com.dycode.foodgasm.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by asus pc on 23/12/2015.
 */
public class ReorderAct extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.tvRestaurantName)
    TextView tvRestaurantName;
    @BindView(R.id.tvInfo)
    TextView tvInfo;
    @BindView(R.id.tvDate)
    TextView tvDate;
    @BindView(R.id.linMenu)
    LinearLayout linMenu;
    @BindView(R.id.tvNote)
    TextView tvNote;
    @BindView(R.id.linCancel)
    LinearLayout linCancel;
    @BindView(R.id.linConfirm)
    LinearLayout linConfirm;
    @BindView(R.id.tv_share_code)
    TextView mTvShareCodeView;
    @BindView(R.id.tvSubtotalValue)
    TextView tvSubtotalValue;
    @BindView(R.id.tvConvenienceChargeValue)
    TextView tvConvenienceChargeValue;
    @BindView(R.id.tvTotalValue)
    TextView tvTotalValue;
    @BindView(R.id.tvVoucherDisc)
    TextView tvVoucherDisc;
    @BindView(R.id.tvVoucherDiscValue)
    TextView tvVoucherDiscValue;
    @BindView(R.id.linDisc)
    LinearLayout linDisc;

    int subTotal = 0, tax = 0, total = 0,  disc = 0, convenience_charge = 0, countTax, menuPriceFinal, notif_id;

    String bookingCode, bookTimeInDay, bookTime, orderId;

    ArrayList<Menu> menuList = new ArrayList<>();
    private Inbox inbox;
    private List<Inbox> inboxList;
    private DokuSample dokuData;

    //doku
    private DirectSDK directSDK;
    private JSONObject respongetTokenSDK;
    TelephonyManager telephonyManager;
    String invoiceNumber;
    List<com.dycode.foodgasm.dao.CreditCard> creditCardDaoList;
    DaoSession daoSession;
    private String jsonRespon;

    MaterialDialog progressDialog;
    private String position;


    private OrderDetail orderDetail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reorder);
        ButterKnife.bind(this);

        // Obtain the shared Tracker instance.
        getTracker(getResources().getString(R.string.order_confirmation));

        tvTitle.setText(R.string.order_confirmation);
        orderId = getIntent().getExtras().getString(FoodgasmNotificationHandler.NOTIF_ORDER_ID);
        notif_id = getIntent().getExtras().getInt("id_notifications");

        clearNotif(notif_id);

        getOrderDetail();
    }

    @OnClick(R.id.linCancel)
    void patchCancelOrder() {
        new MaterialDialog.Builder(mActivity)
                .content(R.string.cancel_order)
                .positiveText(R.string.yes)
                .negativeText(R.string.no)
                .onPositive((dialog, which) -> {
                    cancelOrder();
                })
                .onNegative((dialog, which) -> dialog.dismiss())
                .build()
                .show();
    }

    @OnClick(R.id.linConfirm)
    void proceedToPayment() {
        SPManager.saveString(ReorderAct.this, Constants.Inbox.STATUS_INBOX, null);
        if(orderDetail != null && orderDetail.getPaymentWith() != null && orderDetail.getPaymentWith().equals("free")){
            confirmFreeOrder();
        }else {
            PayActivity.Companion.start(this, orderId, notif_id);
        }
    }

    void cancelOrder() {
        showProgressDialog("Loading...");
        restClient.getApiService().patchOrder(orderId, Utils.getToken(mActivity).getAccessToken(), new PatchOrder("cancel", "User cancelled the order"), new Callback<Order>() {
            @Override
            public void success(Order order, Response response) {

                progressDialog.dismiss();
                new MaterialDialog.Builder(mActivity)
                        .content(R.string.order_cancelled)
                        .positiveText(R.string.ok)
                        .onPositive((dialog, which) -> {
                            SPManager.saveString(ReorderAct.this, Order.ORDER_ID, null);
                            SPManager.saveString(ReorderAct.this, Constants.Inbox.STATUS_INBOX, null);
                            Intent intent = new Intent(mActivity, BookOrderCompleteAct.class);
                            intent.putExtra(BookOrderCompleteAct.FLAG, BookOrderCompleteAct.FLAG_ORDER_CANCELLED);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra(FoodgasmNotificationHandler.NOTIF_ORDER_ID, orderDetail.getId());
                            intent.putExtra("bookingTime", "bookingTime");
                            intent.putExtra("bookingDay", "bookingDay");
                            intent.putExtra("bookingCode", "bookingCode");
                            intent.putExtra("isTakeAway", "isTakeAway");
                            intent.putExtra("restoName", "restoName");
                            startActivity(intent);
                        })
                        .cancelable(false)
                        .build()
                        .show();
            }

            @Override
            public void failure(RetrofitError error) {
                progressDialog.dismiss();
                showAlertDialog("Failed", error.getMessage());
            }
        });
    }

    void confirmFreeOrder() {
        showProgressDialog("Loading...");
        restClient.getApiService().patchOrder(orderId, Utils.getToken(mActivity).getAccessToken(), new PatchOrder("accept", ""), new Callback<Order>() {
            @Override
            public void success(Order order, Response response) {
                progressDialog.dismiss();
                SPManager.saveString(ReorderAct.this, Order.ORDER_ID, null);
                SPManager.saveString(ReorderAct.this, Constants.Inbox.STATUS_INBOX, null);
                Intent intent = new Intent(mActivity, BookOrderCompleteAct.class);
                intent.putExtra(BookOrderCompleteAct.FLAG, BookOrderCompleteAct.FLAG_BOOKING_ACCEPTED);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("bookingTime", "bookingTime");
                intent.putExtra("bookingDay", "bookingDay");
                intent.putExtra("bookingCode", "bookingCode");
                intent.putExtra("isTakeAway", "isTakeAway");
                intent.putExtra("restoName", "restoName");;
                intent.putExtra(FoodgasmNotificationHandler.NOTIF_ORDER_ID, orderId);
                startActivity(intent);
            }

            @Override
            public void failure(RetrofitError error) {
                progressDialog.dismiss();
                showAlertDialog("Failed", error.getMessage());
            }
        });
    }

    void getOrderDetail() {
        //showProgressDialog("Loading...");
        progressDialog = new MaterialDialog.Builder(ReorderAct.this)
                .content("Loading...")
                .cancelable(true)
                .progress(true, 0).build();
        progressDialog.show();
        restClient.getApiService().getOrderDetail(orderId, Utils.getToken(mActivity).getAccessToken(), new Callback<ApiResponse<OrderDetail>>() {


            @Override
            public void success(ApiResponse<OrderDetail> orderDetailApiResponse, Response response) {
                progressDialog.dismiss();
                orderDetail = orderDetailApiResponse.getData();
                tvRestaurantName.setText(orderDetail.getResto().getName());
//                convenience_charge = orderDetail.getPrice().getConvenience();
                convenience_charge = (int) Math.ceil(orderDetail.getPrice().getSubtotal() * -0.001);
                String info = String.format(getString(R.string.info_order_detail), String.valueOf(orderDetail.getNumOfPeople()));
                tvInfo.setText(info);

                // Set Price
                convenience_charge = orderDetail.getPrice().getConvenience();
                subTotal = orderDetail.getPrice().getSubtotal();
                disc = Math.round(orderDetail.getPrice().getDiscount());
                tax = orderDetail.getPrice().getTax();
//                bank_charge = orderDetail.getPrice().getBankCharge();
//                total = orderDetail.getPrice().getTotal() + convenience_charge;
//                total = orderDetail.getPrice().getSubtotal() + convenience_charge;
                total = orderDetail.getPrice().getTotal();

                String info_datetime = String.format(getString(R.string.info_order_datetime), FoodgasmDateUtil.getStringDate(orderDetail.getVisitDate()), FoodgasmDateUtil.getHourAndMinute(orderDetail.getVisitDate()));
                tvDate.setText(info_datetime);
                tvNote.setText(orderDetail.getNote());
                mTvShareCodeView.setText(orderDetail.getCode());
//                tvTaxValue.setText(String.valueOf(orderDetail.getPrice().getTax()));
                String subTotalValue = (String.format(Locale.ENGLISH, "%,d", subTotal)).replace(',', ',');
                String taxValue = (String.format(Locale.ENGLISH,"%,d", (int) tax)).replace(',', ',');
                String convenienceChargeValue = (String.format(Locale.ENGLISH,"%,d", convenience_charge)).replace(',', ',');
                String totalValue = (String.format(Locale.ENGLISH,"%,d", total)).replace(',', ',');
                String discValue = (String.format(Locale.ENGLISH,"%,d", disc)).replace(',', ',');

                tvSubtotalValue.setText(subTotalValue);
                tvConvenienceChargeValue.setText(convenienceChargeValue);
                tvTotalValue.setText(totalValue);
                if(disc > 0){
                    linDisc.setVisibility(View.VISIBLE);
                    tvVoucherDiscValue.setText(discValue);
                }else{
                    linDisc.setVisibility(View.GONE);
                }



                for (int pos = 0; pos < orderDetail.getPreorder().getMenus().size(); pos++) {
                    menuList.add(orderDetail.getPreorder().getMenus().get(pos));
                }
                countTax = orderDetail.getPrice().getTax() == 0 ? 0 : orderDetail.getPrice().getTax();
                inflateMenu();

                Calendar calendar = Calendar.getInstance();
                Date today = calendar.getTime();
                String bookDay = String.valueOf(FoodgasmDateUtil.getStringDate(orderDetail.getVisitDate()));
                DateFormat dateFormat = new SimpleDateFormat("MMM dd, yyy", java.util.Locale.getDefault());
                String todayAsString = dateFormat.format(today);
                if (!bookDay.equals(todayAsString)) {
                    bookTimeInDay = "tomorrow";
                } else {
                    bookTimeInDay = "today";
                }
                bookTime = String.valueOf(FoodgasmDateUtil.getHourAndMinute(orderDetail.getVisitDate()));
                bookingCode = orderDetail.getCode();
            }

            @Override
            public void failure(RetrofitError error) {
                progressDialog.dismiss();
                showAlertDialog("Failed", error.getMessage());
            }
        });
    }

    private void inflateMenu() {
        final LayoutInflater inflater = LayoutInflater.from(mActivity);
        for (int pos = 0; pos < menuList.size(); pos++) {
            final View view = inflater.inflate(R.layout.row_reorder, linMenu, false);
            TextView tvName = ButterKnife.findById(view, R.id.tvFoodName);
            TextView tvDesc = ButterKnife.findById(view, R.id.tvDesc);
            final TextView tvPrice = ButterKnife.findById(view, R.id.tvPrice);
            final TextView tvCount = ButterKnife.findById(view, R.id.tvCount);
            final Menu menu = menuList.get(pos);

            tvCount.setText(String.valueOf(menu.getAmount()));
            tvName.setText(menu.getName());
            menuPriceFinal = menu.getAmount() * menu.getPrice() - menu.getDiscountPrice() < 0 ? 0 : (menu.getAmount() * menu.getPrice() - menu.getDiscountPrice());
            int menuPriceTotal = menuPriceFinal + (menuPriceFinal * 5 / 100);
            String menuPriceSub = (FormatterUtil.convertToIDRFormat(menu.getSubtotalPrice()));
//            tvPrice.setText(String.valueOf(menuPriceTotal));
            tvPrice.setText(menuPriceSub);
            if (menu.getAvailable()) {
                tvDesc.setText(R.string.available);
//                subTotal = subTotal + menuPriceFinal;
//                disc = menu.getDiscountPrice();
//                tax = countTax == 0 ? 0 : countTax;
////                bank_charge = (subTotal + tax + convenience_charge) * 5 / 100;
//                total = subTotal + tax + convenience_charge + bank_charge;
            } else {

                tvDesc.setText(R.string.not_available);
                tvDesc.setTextColor(Utils.getColor(this, android.R.color.holo_red_light));
                tvCount.setVisibility(View.INVISIBLE);
                tvPrice.setVisibility(View.INVISIBLE);
            }
            displayTotal();
            linMenu.addView(view);

        }

    }

    private void displayTotal() {
        String subTotalValue = (String.format(Locale.ENGLISH, "%,d", subTotal)).replace(',', ',');
        String taxValue = (String.format(Locale.ENGLISH, "%,d", tax)).replace(',', ',');
        String convenienceChargeValue = ( String.format(Locale.ENGLISH, "%,d", convenience_charge)).replace(',', ',');
        String totalValue = (String.format(Locale.ENGLISH, "%,d", total)).replace(',', ',');

        if (subTotal == 0) {
            linConfirm.setEnabled(false);
            linConfirm.setBackground(Utils.getDrawable(mActivity, R.color.colorGreyDisableConfirm));
            linConfirm.getBackground().setAlpha(50);
        }

        tvSubtotalValue.setText(subTotalValue);
        tvConvenienceChargeValue.setText(convenienceChargeValue);
        tvTotalValue.setText(totalValue);

    }

    public void getDokuSample(final int pos) {
        restClient = RestClient.getInstance();
        restClient.getApiService().getDokuSampleSandBox(inboxList.get(pos).getId(), Utils.getToken(this).getAccessToken(), new Callback<ApiResponse<DokuSample>>() {
            @Override
            public void success(ApiResponse<DokuSample> categoryApiResponseList, Response response) {
                dokuData = categoryApiResponseList.getData();
                registerDokuToken(pos);
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d("error", error.getMessage());
            }
        });
    }

    public void registerDokuToken(int pos) {
        progressDialog = new MaterialDialog.Builder(this)
                .content("Submitting..")
                .cancelable(true)
                .progress(true, 0).build();

        telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

        PaymentItems cardDetails = null;
        cardDetails = InputCard(pos);

        directSDK = new DirectSDK();
        directSDK.setCart_details(cardDetails);
        directSDK.setPaymentChannel(1);
        directSDK.getResponse(new iPaymentCallback() {
            @Override
            public void onSuccess(final String text) {
                Log.d("cek token DOKU-->", text);
                progressDialog.dismiss();
                jsonRespon = text;
                try {
                    respongetTokenSDK = new JSONObject(text);
                    progressDialog.dismiss();
                    if (respongetTokenSDK.getString("res_response_code").equalsIgnoreCase("0000")) {
                        SPManager.saveString(ReorderAct.this, "res_token_id", respongetTokenSDK.getString("res_token_id"));
                        progressDialog.setContent("Silahkan Tunggu");
                        progressDialog.show();
                        restClient.getApiService().postDokuResponseSandbox(inbox.getId(), Utils.getToken(ReorderAct.this).getAccessToken(), new DokuResponse(jsonRespon), new Callback<ApiResponse<String>>() {
                            @Override
                            public void success(ApiResponse<String> userApiResponse, Response response) {
                                progressDialog.dismiss();
                                JSONObject json;
                                try {
                                    json = new JSONObject(userApiResponse.getData().toString());
                                    if (json.getString("res_response_code").equalsIgnoreCase("0000") && json != null) {
                                        Intent intent = new Intent(getApplicationContext(), ResultPaymentAct.class);
                                        intent.putExtra("data", json.toString());
                                        startActivity(intent);
                                        finish();
                                        Toast.makeText(getApplicationContext(), " PAYMENT SUKSES", Toast.LENGTH_SHORT).show();
                                    } else {
                                        Intent intent = new Intent(getApplicationContext(), ResultPaymentAct.class);
                                        intent.putExtra("data", json.toString());
                                        startActivity(intent);
                                        finish();
                                        Toast.makeText(getApplicationContext(), "PAYMENT ERROR", Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void failure(RetrofitError error) {
                                progressDialog.dismiss();
                                new MaterialDialog.Builder(ReorderAct.this)
                                        .title(R.string.alert)
                                        .content(error.getMessage())
                                        .positiveText(R.string.ok)
                                        .cancelable(false)
                                        .show();
                            }
                        });
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onError(final String text) {
                //showLog(text);
                Log.d("error doku ", text);
            }

            @Override
            public void onException(Exception eSDK) {
                eSDK.printStackTrace();
            }
        }, getApplicationContext());
    }

    private PaymentItems InputCard(int pos) {
        daoSession = ((FoodGasmApp) getApplication()).getDaoSession();
        creditCardDaoList = daoSession.getCreditCardDao().queryBuilder().orderDesc(CreditCardDao.Properties.State).list();

        String dataWord = AppsUtil.SHA1(dokuData.getReq_amount() + dokuData.getReq_mall_id() + "cCOc42ElK71g" + dokuData.getReq_transaction_id() + 360 + telephonyManager.getDeviceId());
        String customerID = String.valueOf(AppsUtil.nDigitRandomNo(8));
        PaymentItems paymentItems = new PaymentItems();
        paymentItems.setDataAmount(dokuData.getReq_amount());
        paymentItems.setDataBasket(dokuData.getReq_basket());
        paymentItems.setDataCurrency(dokuData.getReq_currency());
        paymentItems.setDataWords(dataWord);
        paymentItems.setDataMerchantChain(dokuData.getReq_chain_merchant());
        paymentItems.setDataSessionID(dokuData.getReq_session_id());
        paymentItems.setDataTransactionID(dokuData.getReq_transaction_id());
        paymentItems.setDataMerchantCode(dokuData.getReq_mall_id());
        paymentItems.setDataImei(telephonyManager.getDeviceId());
        paymentItems.setMobilePhone("");
        paymentItems.setPublicKey("MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAiwz9tMtvtz2Ch2UbvzUr9XX1Olb5nPG7egobwlcyBoblY1aqIUw2LFJeTGtOZ5JMAk7w8AFG36ii6b2lo5K8bORvx3StVcavwT8pD8VMQ2AI9uaEmseS4Gn8AJzPSMV5lw/1lMpTDId2ug0pH5xnAS4StAvrKQ47wyQ1tVE/v7UbApVlX/ok/I24FN7uvKoteEXPGsLrwFRcjseXYYNDZ1OIyqIPc5HftL1PiW6P29Fjn++3aEFtHl+xDyOkBqo95NdHqQAqH9RF3jladh21PsGlQHZeTXSfSUz9/a9ebkbOc41TLvtJt50f5MzwGfoTheBOiGh4xwaX/Y+NHkgSDQIDAQAB");
        paymentItems.isProduction(true);
        paymentItems.setCustomerID(customerID);

        if (inboxList.get(pos).getPaymentCredential() != null && !inboxList.get(pos).getPaymentCredential().isEmpty() && !inboxList.get(pos).getPaymentCredential().equals("null")) {
            if (dokuData.getCustomer_id() != null) {
                Log.d("Hello token", dokuData.getSaved_token_id());
                paymentItems.setTokenPayment(dokuData.getSaved_token_id());
                paymentItems.setCustomerID(dokuData.getCustomer_id());
            }
        }

        return paymentItems;
    }
}
