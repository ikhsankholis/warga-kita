package com.dycode.foodgasm.activity;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.dycode.foodgasm.R;
import com.dycode.foodgasm.model.ApiResponse;
import com.dycode.foodgasm.model.EcashOTP;
import com.dycode.foodgasm.model.EcashRegister;
import com.dycode.foodgasm.utils.Utils;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

public class ECashRegisterPairing extends BaseActivity {


    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.ivLogo)
    ImageView ivLogo;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.textView33)
    TextView textView33;
    @BindView(R.id.textView34)
    TextView textView34;
    @BindView(R.id.textView35)
    TextView textView35;
    @BindView(R.id.ecPIN)
    EditText ecPIN;
    @BindView(R.id.txtlabel)
    TextView txtlabel;
    @BindView(R.id.ecOTP)
    EditText ecOTP;
    @BindView(R.id.txtResendOtp)
    TextView txtResendOtp;
    @BindView(R.id.linBtnContinue)
    LinearLayout linBtnContinue;
    private String mobilePhone;
    private String dateOfBirthS;
    private String status;
    boolean statusRegistered = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.e_cash_register_pairing);
        ButterKnife.bind(this);
        toolbar.setNavigationIcon(R.mipmap.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        tvTitle.setText(R.string.ecash);

        mobilePhone = getIntent().getExtras().getString("ecashID");
        status = getIntent().getExtras().getString("status");

        if (status.equals("REGISTERED")) {
            statusRegistered = true;
        }
        textView34.setText(mobilePhone);

    }


    @OnClick(R.id.linBtnContinue)
    void registerEcash() {
        String phoneNumber = mobilePhone;
        String pin = ecPIN.getText().toString();
        String otp = ecOTP.getText().toString();

        Log.d("registerd", String.valueOf(statusRegistered));
        statusRegistered = true;
        postRegisterEcash(phoneNumber, pin, otp, true);

    }


    void postRegisterEcash(String mobilePhone, String pin, String otp, boolean statusRegistered) {
        showProgressDialog("Loading...");
        restClient.getApiService().postEcashRegister(Utils.getToken(mActivity).getAccessToken(), new EcashRegister(mobilePhone, pin, otp, true), new Callback<ApiResponse>() {
            @Override
            public void success(ApiResponse data, Response response) {
                progressDialog.dismiss();

                Log.d("data Status > ", data.getMeta().getMessage());
                new MaterialDialog.Builder(mActivity)
                        .content(data.getMeta().getMessage())
                        .positiveText(R.string.ok)
                        .cancelable(true)
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(MaterialDialog dialog, DialogAction which) {
                                setResult(2);
                                finish();
                            }
                        })
                        .show();

            }

            @Override
            public void failure(RetrofitError error) {
                progressDialog.dismiss();
                String json = new String(((TypedByteArray) error.getResponse().getBody()).getBytes());
                Log.v("failure", json.toString());
                try {
                    JSONObject dataError = new JSONObject(json.toString());
                    JSONObject meta = dataError.getJSONObject("meta");
                    String code = meta.getString("code");
                    if (code.equals("400")) {
                        String message = meta.getString("message");
                        new MaterialDialog.Builder(mActivity)
                                .content("Server request error. Please check back later")
                                .positiveText(R.string.ok)
                                .cancelable(true)
                                .onPositive(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(MaterialDialog dialog, DialogAction which) {
                                        dialog.dismiss();
                                    }
                                })
                                .show();
                    } else {

                        showAlertDialog("Error", error.getMessage());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        });
    }

    @OnClick(R.id.txtResendOtp)
    void onResendOtp() {
        if (status.equals("REGISTERED")) {
            statusRegistered = true;
            EcashOTP reqOTPeCASH = new EcashOTP(mobilePhone, "N02");
            reqOTPeCASH(reqOTPeCASH);
        } else {
            EcashOTP reqOTPeCASH = new EcashOTP(mobilePhone, "N01");
            reqOTPeCASH(reqOTPeCASH);
        }

    }

    private void reqOTPeCASH(EcashOTP ecashOTP) {
        showProgressDialog("Loading...");
        restClient.getApiService().postPhoneVerifyEcash(Utils.getToken(mActivity).getAccessToken(), ecashOTP, new Callback<ApiResponse>() {
            @Override
            public void success(ApiResponse data, Response response) {
                progressDialog.dismiss();
                Log.d("data String> ", new Gson().toJson(data.getData()));
            }

            @Override
            public void failure(RetrofitError error) {
                progressDialog.dismiss();
                showAlertDialog("Error", error.getMessage());
            }
        });
    }
}
