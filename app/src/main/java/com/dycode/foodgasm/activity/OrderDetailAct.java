package com.dycode.foodgasm.activity;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import com.dycode.foodgasm.FoodGasmApp;
import com.dycode.foodgasm.R;
import com.dycode.foodgasm.dao.DaoSession;
import com.dycode.foodgasm.dao.RestoAdress;
import com.dycode.foodgasm.dao.RestoAdressDao;
import com.dycode.foodgasm.model.ApiResponse;
import com.dycode.foodgasm.model.Menu;
import com.dycode.foodgasm.model.Order;
import com.dycode.foodgasm.model.OrderDetail;
import com.dycode.foodgasm.utils.CCUtils;
import com.dycode.foodgasm.utils.FoodgasmDateUtil;
import com.dycode.foodgasm.utils.FormatterUtil;
import com.dycode.foodgasm.utils.SPManager;
import com.dycode.foodgasm.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by asus pc on 04/11/2015.
 */
public class OrderDetailAct extends BaseActivity {

    @BindView(R.id.tvRestaurantName)
    TextView tvRestaurantName;
    @BindView(R.id.tvAddress)
    TextView tvAddress;
    @BindView(R.id.tvInfo)
    TextView tvInfo;
    @BindView(R.id.tvDate)
    TextView tvDate;
    @BindView(R.id.linMenu)
    LinearLayout linMenu;
    @BindView(R.id.tvNote)
    TextView tvNote;
    @BindView(R.id.tvSubtotalValue)
    TextView tvSubtotalValue;
    @BindView(R.id.tvTaxValue)
    TextView tvTaxValue;
    @BindView(R.id.tvConvenienceChargeValue)
    TextView tvConvenienceChargeValue;
    @BindView(R.id.tvTotalValue)
    TextView tvTotalValue;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.scrollView)
    ScrollView scrollView;
    @BindView(R.id.tv_share_code)
    TextView mTvShareCodeView;
    @BindView(R.id.tv_bank_total_value)
    TextView mTvBankTotalValueView;
    @BindView(R.id.l_paywith)
    LinearLayout l_paywith;

    @BindView(R.id.image_logo_cc_doku)
    ImageView imgCcDoku;
    @BindView(R.id.txt_paywith)
    TextView txtPayWith;

    String order, restoStreet, restoDistrict, restoCity, restoProvince, restoPostalCode;
    int subTotal = 0, tax = 0, countTax = 0, total = 0, bank_charge = 0, convenience_charge = 0, menuPriceFinal;

    ArrayList<Menu> menuList = new ArrayList<>();
    List<RestoAdress> restoAddress;

    FoodGasmApp foodGasmApp;
    DaoSession daoSession;
    private int totalCharge;
    private String creditCardNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_detail);
        ButterKnife.bind(this);

        // Obtain the shared Tracker instance.
        getTracker("History Order Detail");

        foodGasmApp = (FoodGasmApp) getApplication();
        daoSession = foodGasmApp.getDaoSession();

        setToolbar(true);
        order = getIntent().getExtras().getString(Order.ORDER);
        inflateMenu(0);
        getOrderDetail();

        if (SPManager.getString(this, "orderDetail").equals("waiting")) {
            l_paywith.setVisibility(View.GONE);
        }

    }

    void getOrderDetail() {
        restClient.getApiService().getOrderDetail(order, Utils.getToken(mActivity).getAccessToken(), new Callback<ApiResponse<OrderDetail>>() {
            @Override
            public void success(ApiResponse<OrderDetail> orderDetailApiResponse, Response response) {
                progressBar.setVisibility(View.GONE);
                scrollView.setVisibility(View.VISIBLE);
                OrderDetail orderDetail = orderDetailApiResponse.getData();
                tvRestaurantName.setText(orderDetail.getResto().getName());
                mTvShareCodeView.setText(orderDetail.getCode());

                if (orderDetail.getMaskedCard() != null) {
                    creditCardNumber = orderDetail.getMaskedCard().substring(orderDetail.getMaskedCard().length() - 4, orderDetail.getMaskedCard().length());
                }


                if (orderDetail.getPaymentWith().equals("dokucc")) {
                    txtPayWith.setText("Doku");
                    imgCcDoku.setImageResource(R.mipmap.doku_logo);
                } else if (orderDetail.getPaymentWith().equals("ecash")) {
                    txtPayWith.setText(orderDetail.getMaskedCard());
                    imgCcDoku.setImageResource(R.mipmap.image_logo_cc_ecash);
                } else if (orderDetail.getPaymentWith().equals("creditcard")) {
                    txtPayWith.setText(creditCardNumber);
                    if(orderDetail.getMaskedCard() != null){
                        imgCcDoku.setImageResource(CCUtils.getCardLogo(orderDetail.getMaskedCard().replaceAll("\\s", "")));
                    }
                } else if(orderDetail.getPaymentWith().equals("free")){
                    txtPayWith.setText("Free Charge");
                    imgCcDoku.setVisibility(View.GONE);
                }else {
                    txtPayWith.setText(creditCardNumber);
                    if (orderDetail.getMaskedCard() != null) {
                        imgCcDoku.setImageResource(CCUtils.getCardLogo(orderDetail.getMaskedCard().replaceAll("\\s", "")));
                    }
                    if (orderDetail.getPaymentWith().equals("dokuwallet")) {
                        imgCcDoku.setImageResource(R.mipmap.doku_wallet);
                    }
                }


                restoAddress = daoSession.getRestoAdressDao().queryBuilder().where(RestoAdressDao.Properties._id.eq(orderDetail.getResto().getId())).list();
                if (restoAddress.size() != 0) {
                    restoStreet = restoAddress.get(0).getStreet();
                    restoDistrict = restoAddress.get(0).getDistrict();
                    restoCity = restoAddress.get(0).getCity();
                    restoProvince = restoAddress.get(0).getProvince();
                    restoPostalCode = restoAddress.get(0).getPostalCode();
                    String fullAddress = String.format(getString(R.string.resto_address), restoStreet, restoDistrict, restoCity, restoProvince, restoPostalCode);
                    tvAddress.setText(fullAddress);
                } else {
                    tvAddress.setVisibility(View.GONE);
                }

                // Set Price
//                convenience_charge = orderDetail.getPrice().getConvenience();
                convenience_charge = (int) Math.ceil(orderDetail.getPrice().getSubtotal() * -0.001);
                subTotal = orderDetail.getPrice().getSubtotal();
                tax = orderDetail.getPrice().getTax();
                bank_charge = orderDetail.getPrice().getBankCharge();
//                total = orderDetail.getPrice().getTotal();
                total = orderDetail.getPrice().getTotal();

                totalCharge = Integer.valueOf(convenience_charge) + Integer.valueOf(bank_charge);

                displayTotal();

                String info;
                if(orderDetail.getNumOfPeople() > 0){
                    info = String.format(getString(R.string.info_order_detail), orderDetail.getNumOfPeople());
                }else{
                    info = getString(R.string.take_away);
                }
                tvInfo.setText(info);
                String info_datetime = String.format(getString(R.string.info_order_datetime), FoodgasmDateUtil.getStringDate(orderDetail.getVisitDate()), FoodgasmDateUtil.getHourAndMinute(orderDetail.getVisitDate()));
                tvDate.setText(info_datetime);
                tvNote.setText(orderDetail.getNote());
                for (int pos = 0; pos < orderDetail.getPreorder().getMenus().size(); pos++) {
                    menuList.add(orderDetail.getPreorder().getMenus().get(pos));
                }
                countTax = orderDetail.getPrice().getTax() == 0 ? 0 : orderDetail.getPrice().getTax();
                inflateMenu(orderDetail.getPrice().getConvenience());

            }

            @Override
            public void failure(RetrofitError error) {
                progressBar.setVisibility(View.GONE);
                scrollView.setVisibility(View.VISIBLE);
//                showAlertDialog("Failed", error.getMessage());
                showToast("No Internet Connection");
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            setResult(1);
            finish();
            overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        setResult(1);
        finish();
        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
    }

    @Override
    protected int getToolbarTitle() {
        return R.string.detail_order;
    }

    private void inflateMenu(int convienceCharge) {
        final LayoutInflater inflater = LayoutInflater.from(mActivity);
        for (int pos = 0; pos < menuList.size(); pos++) {
            final View view = inflater.inflate(R.layout.row_reorder, linMenu, false);
            TextView tvName = ButterKnife.findById(view, R.id.tvFoodName);
            TextView tvDesc = ButterKnife.findById(view, R.id.tvDesc);
            final TextView tvPrice = ButterKnife.findById(view, R.id.tvPrice);
            final TextView tvCount = ButterKnife.findById(view, R.id.tvCount);
            final Menu menu = menuList.get(pos);

            tvCount.setText(String.valueOf(menu.getAmount()));
            tvName.setText(menu.getName());
            menuPriceFinal = menu.getAmount() * menu.getPrice() - menu.getDiscountPrice() < 0 ? 0 : (menu.getAmount() * menu.getPrice() - menu.getDiscountPrice());
            String menuPrice = (FormatterUtil.convertToIDRFormat(menuPriceFinal));
            String menuPriceSub = (FormatterUtil.convertToIDRFormat(menu.getSubtotalPrice()));
            tvPrice.setText(menuPriceSub);
            if (menu.getAvailable()) {
                tvDesc.setText(R.string.available);
//                subTotal = subTotal + menuPriceFinal;
//                tax = countTax == 0 ? 0 : countTax;
//                bank_charge = (subTotal + tax + convenience_charge) * 5 / 100;
//                total = subTotal + tax + convenience_charge + bank_charge;
            } else {
                view.setVisibility(View.GONE);
                tvDesc.setText(R.string.not_available);
                tvDesc.setTextColor(Utils.getColor(this, android.R.color.holo_red_light));
                tvCount.setVisibility(View.INVISIBLE);
                tvPrice.setVisibility(View.INVISIBLE);
            }

//            if (subTotal == 0) {
////                tax = 0;
////                total = 0;
////                convenience_charge = 0;
////                bank_charge = 0;
//            } else {
//                tax = countTax == 0 ? 0 : countTax;
//                convenience_charge = convienceCharge;
////                convenience_charge = 20000;
//                bank_charge = (subTotal + tax + convenience_charge) * 5 / 100;
//                total = subTotal + tax + convenience_charge + bank_charge;
//            }
//            displayTotal();

            linMenu.addView(view);

        }

    }

    private void displayTotal() {

        String subTotalValue = (String.format("%,d", subTotal)).replace(',', ',');
        String taxValue = (String.format("%,d", tax)).replace(',', ',');
        String convenienceChargeValue = (String.format("%,d", convenience_charge)).replace(',', ',');
        String totalValue = (String.format("%,d", total)).replace(',', ',');
        String bankChargeParsing = (String.format("%,d", bank_charge)).replace(',', ',');

        String totalCharges = (String.format("%,d", totalCharge)).replace(',', ',');

        tvSubtotalValue.setText(subTotalValue);
//        tvTaxValue.setText(taxValue);
        tvTaxValue.setText("INCLUDED");
//        tvConvenienceChargeValue.setText(convenienceChargeValue);
        tvConvenienceChargeValue.setText(convenienceChargeValue);
        tvTotalValue.setText(totalValue);
        mTvBankTotalValueView.setText(bankChargeParsing);

    }
}
