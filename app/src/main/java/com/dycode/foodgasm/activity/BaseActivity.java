package com.dycode.foodgasm.activity;

import android.Manifest;
import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.annotation.DrawableRes;
import android.support.annotation.IdRes;
import android.support.annotation.IntegerRes;
import android.support.annotation.StringRes;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.dycode.foodgasm.FoodGasmApp;
import com.dycode.foodgasm.R;
import com.dycode.foodgasm.dao.DaoSession;
import com.dycode.foodgasm.feature.skiplogin.SkipLoginActivity;
import com.dycode.foodgasm.feature.splashscreen.SplashScreenAct;
import com.dycode.foodgasm.inject.ApplicationComponent;
import com.dycode.foodgasm.listener.UnregisterNotificationListener;
import com.dycode.foodgasm.model.EventUnauthorized;
import com.dycode.foodgasm.rest.RestClient;
import com.dycode.foodgasm.rest.RestClientForUploadFile;
import com.dycode.foodgasm.rest.RestClientForVeritrans;
import com.dycode.foodgasm.rest.RestClientUberProduct;
import com.dycode.foodgasm.utils.Constants;
import com.dycode.foodgasm.utils.Utils;
import com.facebook.login.LoginManager;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.ButterKnife;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Okta Dwi Priatna on 02/10/2015.
 */
public abstract class BaseActivity extends AppCompatActivity {
    protected RestClient restClient = RestClient.getInstance();
    protected RestClientForUploadFile restClientForUploadFile = RestClientForUploadFile.getInstance();
    protected RestClientUberProduct restClientUberProduct = RestClientUberProduct.getInstance();
    protected RestClientForVeritrans restClientForVeritrans = RestClientForVeritrans.getInstance();
    private static boolean isVisible;
    private static boolean mIsInForegroundMode;

    protected MaterialDialog progressDialog;
    protected MaterialDialog alertDialog;

    protected Activity mActivity = this;

    Toolbar mToolbar;
    TextView tvTitle;

    Tracker mTracker;

    protected EventBus bus = EventBus.getDefault();

    @Override
    protected void onStart() {
        super.onStart();
        isVisible = true;

        if((this instanceof SkipLoginActivity) || (this instanceof HighLightAct) || (this instanceof LoginAct)){
            if(!bus.isRegistered(this))
                bus.register(this);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        mIsInForegroundMode = false;

        if(!(this instanceof SkipLoginActivity) && !(this instanceof HighLightAct) && !(this instanceof LoginAct) && !(this instanceof BookOrderAct) && !(this instanceof MenuDetailAct) &&  !(this instanceof SplashScreenAct) ){
            bus.unregister(this);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        isVisible = true;
        mIsInForegroundMode = true;

        if(!(this instanceof SkipLoginActivity) && !(this instanceof HighLightAct) && !(this instanceof LoginAct) && !(this instanceof BookOrderAct) && !(this instanceof MenuDetailAct) && !(this instanceof SplashScreenAct)){
            bus.register(this);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        isVisible = false;
        if((this instanceof SkipLoginActivity) || (this instanceof HighLightAct) || (this instanceof LoginAct)){
            bus.unregister(this);
        }
        super.onDestroy();
    }

    public static boolean isViewVisible() {
        return isVisible;
    }

    public static boolean isInForeground() {
        return mIsInForegroundMode;
    }



    protected void setToolbar() {
        setToolbar(false, R.mipmap.ic_back);
    }

    protected void setToolbar(boolean isHaveArrow) {
        setToolbar(isHaveArrow, R.mipmap.ic_back);
    }

    protected void setToolbar(@DrawableRes int navigationIcon) {
        setToolbar(false, navigationIcon);
    }

    protected void setToolbar(boolean isHaveArrow, @DrawableRes int navigationIcon) {

        mToolbar = ButterKnife.findById(this, getToolbarId());
        tvTitle = ButterKnife.findById(this, R.id.tvTitle);

        setSupportActionBar(mToolbar);
        setTitle("");
        tvTitle.setText(getToolbarTitle());

        if (isHaveArrow) {
            mToolbar.setNavigationIcon(navigationIcon);
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                    overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                }
            });
        }
    }

    protected
    @IdRes
    int getToolbarId() {
        return R.id.toolbar;
    }

    protected
    @StringRes
    int getToolbarTitle() {
        return R.string.app_name;
    }

    protected void showProgressDialog(String message) {
        if(isDestroyed()){
            return;
        }
        progressDialog = new MaterialDialog.Builder(this)
                .content(message)
                .cancelable(true)
                .progress(true, 0).build();
        progressDialog.show();
    }

    protected void showProgressDialogNotCancelable(String message, boolean isCancelable) {
        if(isDestroyed()){
            return;
        }
        if (isCancelable) {
            progressDialog = new MaterialDialog.Builder(this)
                    .content(message)
                    .cancelable(true)
                    .progress(true, 0).build();
            progressDialog.show();
        } else {
            progressDialog = new MaterialDialog.Builder(this)
                    .content(message)
                    .cancelable(false)
                    .progress(true, 0).build();
            progressDialog.show();
        }

    }

    protected void showAlertDialog(String title, String message) {
        if(isDestroyed()){
            return;
        }
        alertDialog = new MaterialDialog.Builder(mActivity)
                .title(title)
                .content(message)
                .positiveText("OK")
                .callback(new MaterialDialog.ButtonCallback() {

                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        super.onPositive(dialog);
                        dialog.dismiss();
                    }
                }).build();
        alertDialog.show();
    }

    protected void showAlertDialog(final String message, final String positiveText, final String negativeText) {
        if(isDestroyed()){
            return;
        }
        alertDialog = new MaterialDialog.Builder(mActivity)
                .content(message)
                .positiveText(positiveText)
                .negativeText(negativeText)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {
                        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + message));
                        if (ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions(mActivity, new String[]{Manifest.permission.CALL_PHONE}, Constants.Permission.PHONE_CALL);
                        } else {
                            startActivity(intent);
                        }
                    }
                }).onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {
                        dialog.dismiss();
                    }
                }).build();
        alertDialog.show();
    }

    protected void showRationaleDialog(int content, MaterialDialog.ButtonCallback callback) {
        if(isDestroyed()){
            return;
        }
        new MaterialDialog.Builder(this)
                .title(R.string.permission_location_storage_rationale)
                .content(content)
                .positiveText(R.string.action_grant_access)
                .positiveColor(ContextCompat.getColor(this, R.color.colorAccent))
                .negativeText(R.string.action_cancel)
                .negativeColor(ContextCompat.getColor(this, R.color.textRed))
                .callback(callback)
                .build()
                .show();
    }

    protected void showMaterialDialog(int content) {
        if(isDestroyed()){
            return;
        }
        new MaterialDialog.Builder(mActivity)
                .content(content)
                .positiveText(R.string.ok)
                .cancelable(true)
                .show();
    }

    protected void showMaterialDialogAlert(int content) {
        if(isDestroyed()){
            return;
        }
        new MaterialDialog.Builder(mActivity)
                .title(R.string.alert)
                .content(content)
                .positiveText(R.string.ok)
                .cancelable(false)
                .show();
    }

    protected void showMaterialDialogAlert(String content) {
        if(isDestroyed()){
            return;
        }
        new MaterialDialog.Builder(mActivity)
                .title(R.string.alert)
                .content(content)
                .positiveText(R.string.ok)
                .cancelable(false)
                .show();
    }

    protected void showMaterialDialogAlert(int title, int content) {
        if(isDestroyed()){
            return;
        }
        new MaterialDialog.Builder(mActivity)
                .title(title)
                .content(content)
                .positiveText(R.string.ok)
                .cancelable(false)
                .show();
    }

    protected void showToast(String message) {
        if(isDestroyed()){
            return;
        }
        Toast.makeText(mActivity, message, Toast.LENGTH_SHORT).show();
    }

    protected void showLog(String message) {
        if(isDestroyed()){
            return;
        }
        Toast.makeText(mActivity, message, Toast.LENGTH_SHORT).show();
    }

    protected void getTracker(String screenName) {
        FoodGasmApp application = (FoodGasmApp) getApplication();
        mTracker = application.getTracker();
        mTracker.setScreenName(screenName);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    public static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    //Calligraphy
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    public void clearNotif(int id) {
        NotificationManager nMgr = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        nMgr.cancel(id);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventUnauthorized(EventUnauthorized eventUnauthorized){
        AlertDialog alertDialog = new AlertDialog.Builder(this)
                .setMessage("It seems you are logged in on another device. Log out from that device to continue using foodgasm on this device.")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        showProgressDialog("Logging Out...");
                        FoodGasmApp foodGasmApp = (FoodGasmApp) getApplication();
                        foodGasmApp.unregisterNotificationHub(new UnregisterNotificationListener() {
                            @Override
                            public void onSuccess() {
                                LoginManager.getInstance().logOut();
                                progressDialog.dismiss();
                                Utils.logoutApps(mActivity);

                                //Clear database
                                DaoSession daoSession = ((FoodGasmApp) getApplication()).getDaoSession();
                                daoSession.getCategoryDao().deleteAll();
                                daoSession.getCommentDao().deleteAll();
                                daoSession.getDealDao().deleteAll();
                                daoSession.getGalleryDao().deleteAll();
                                daoSession.getProfileDao().deleteAll();
                                daoSession.getOrderHistoryDao().deleteAll();
                                daoSession.getOrderMenuDao().deleteAll();
                                daoSession.getRestaurantDao().deleteAll();
                                daoSession.getMenuDao().deleteAll();
                                daoSession.getCreditCardDao().deleteAll();
                                daoSession.getRestoAdressDao().deleteAll();
                                daoSession.getMenuVersionDao().deleteAll();

                                Intent i = new Intent(mActivity, HighLightAct.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(i);
                                overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                                finishAffinity();
                            }

                            @Override
                            public void onFailed() {
                                progressDialog.dismiss();
                                showAlertDialog(getResources().getString(R.string.alert_logout_failed), getResources().getString(R.string.alert_no_internet_connection));

                            }
                        });
                    }
                })
                .create();
        alertDialog.show();
    }


    public ApplicationComponent getApplicationComponent(){
        return ((FoodGasmApp) getApplicationContext()).getComponent();
    }

}
