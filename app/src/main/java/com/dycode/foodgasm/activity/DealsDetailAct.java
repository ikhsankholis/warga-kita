package com.dycode.foodgasm.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.dycode.foodgasm.R;
import com.dycode.foodgasm.adapters.DealsDetailAdapter;
import com.dycode.foodgasm.constant.PrefCons;
import com.dycode.foodgasm.listener.EndlessRecyclerOnScrollListener;
import com.dycode.foodgasm.model.ApiResponse;
import com.dycode.foodgasm.model.DealsDetail;
import com.dycode.foodgasm.model.Restaurant;
import com.dycode.foodgasm.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by asus pc on 22/01/2016.
 */
public class DealsDetailAct extends BaseActivity {

    @BindView(R.id.rvRestaurant)
    RecyclerView rvRestaurant;
    @BindView(R.id.srlRestaurantList)
    SwipeRefreshLayout mSwipeRefreshLayout;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.ivLogo)
    ImageView ivLogo;
    @BindView(R.id.tvTitle)
    TextView tvTitle;

    public static LinearLayoutManager linLayoutManager;
    public static DealsDetailAdapter mAdapter;
    List<Restaurant> restaurantList = new ArrayList<Restaurant>();
    int limit = 10;
    String dealsId;
    EndlessRecyclerOnScrollListener mEndlessScrollListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deals_detail);
        ButterKnife.bind(this);

        // Obtain the shared Tracker instance.
        getTracker("Deals Restaurant List");

        mToolbar.setNavigationIcon(R.mipmap.ic_back);
        tvTitle.setVisibility(View.GONE);
        ivLogo.setVisibility(View.VISIBLE);
        setSupportActionBar(mToolbar);
        dealsId = getIntent().getExtras().getString("dealsId");
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorAccent);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshScrollListener();
            }
        });

        if (Utils.getListResto(mActivity, PrefCons.PREF_DEALS_DETAIL) != null) {
            restaurantList = Utils.getListResto(mActivity, PrefCons.PREF_DEALS_DETAIL);
            rvRestaurant.setVisibility(View.VISIBLE);
        }
        mAdapter = new DealsDetailAdapter(mActivity, restaurantList);
        linLayoutManager = new LinearLayoutManager(mActivity);
        rvRestaurant.setHasFixedSize(true);
        rvRestaurant.setLayoutManager(linLayoutManager);
        rvRestaurant.setAdapter(mAdapter);
        mEndlessScrollListener = new EndlessRecyclerOnScrollListener(linLayoutManager, true) {
            @Override
            public void onLoadMore(int current_page) {
                if (restaurantList.size() >= 10){
                    rvRestaurant.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mAdapter.notifyDataSetChanged();
                        }
                    }, 200);
                    mAdapter.showFooter(true);
                    getDealsDetail(false);
                }
            }
        };
        rvRestaurant.addOnScrollListener(mEndlessScrollListener);
        mSwipeRefreshLayout.setRefreshing(true);
        getDealsDetail(true);
    }

    private void refreshScrollListener() {
        addScrollListener(true);
        getDealsDetail(true);
    }

    private void addScrollListener(Boolean isScrollStatus) {
        if (isScrollStatus) {
            rvRestaurant.clearOnScrollListeners();
            mEndlessScrollListener.reset(0, true);
            rvRestaurant.addOnScrollListener(mEndlessScrollListener);
            mAdapter.onAttachedToRecyclerView(rvRestaurant);
        }
    }

    void getDealsDetail(final boolean isReplaceData) {
        if (TextUtils.isEmpty(Utils.getLatString(mActivity)) || TextUtils.isEmpty(Utils.getLngString(mActivity))) {
            return;
        }

        int skip = isReplaceData ? 0 : restaurantList.size();
        restClient.getApiService().getDetailDeals(dealsId,  Utils.getLatString(mActivity), Utils.getLngString(mActivity), skip, limit, new Callback<ApiResponse<DealsDetail>>() {
            @Override
            public void success(ApiResponse<DealsDetail> dealsDetailApiResponse, Response response) {
                mSwipeRefreshLayout.setRefreshing(false);
                if (isReplaceData) {
                    restaurantList.clear();
                    if (dealsDetailApiResponse.getData().getRestaurants().size() > 0) {
                        Utils.saveListResto(mActivity, dealsDetailApiResponse.getData().getRestaurants(), PrefCons.PREF_DEALS_DETAIL);
                        restaurantList.addAll(Utils.getListResto(mActivity, PrefCons.PREF_DEALS_DETAIL));
                    }
                }

                else {
                    restaurantList.addAll(dealsDetailApiResponse.getData().getRestaurants());
                    mAdapter.notifyDataSetChanged();
                }

                if (dealsDetailApiResponse.getData().getRestaurants().size() == 0 && isReplaceData) {
                    rvRestaurant.setVisibility(View.GONE);
                }

                mAdapter.showFooter(false);
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void failure(RetrofitError error) {
                mSwipeRefreshLayout.setRefreshing(false);
                showToast("No Internet Connection");
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
        }
        else if (id == R.id.action_search) {
            Intent intent = new Intent(mActivity, SearchRestoAct.class);
            intent.putExtra(SearchRestoAct.FLAG, SearchRestoAct.FLAG_SEARCH_RESTO_DEALS);
            intent.putExtra("dealsId", dealsId);
            startActivity(intent);
            overridePendingTransition(R.anim.slide_in_right, R.anim.abc_fade_out);

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
    }
}
