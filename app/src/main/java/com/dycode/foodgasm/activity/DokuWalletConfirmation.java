package com.dycode.foodgasm.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.dycode.foodgasm.R;
import com.dycode.foodgasm.model.ApiResponse;
import com.dycode.foodgasm.model.DokuWallet;
import com.dycode.foodgasm.model.DokuWalletBalance;
import com.dycode.foodgasm.model.DokuWalletBalanceResponse;
import com.dycode.foodgasm.model.DokuWalletResponseNew;
import com.dycode.foodgasm.model.Order;
import com.dycode.foodgasm.model.OrderDetail;
import com.dycode.foodgasm.model.PatchOrder;
import com.dycode.foodgasm.utils.CTools;
import com.dycode.foodgasm.utils.Constants;
import com.dycode.foodgasm.utils.FoodgasmDateUtil;
import com.dycode.foodgasm.utils.NetworkErrorUtil;
import com.dycode.foodgasm.utils.SPManager;
import com.dycode.foodgasm.utils.Utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class DokuWalletConfirmation extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.tvMenu)
    TextView tvMenu;

    @BindView(R.id.id_doku_wallet)
    TextView id_doku_wallet;
    @BindView(R.id.txt_conf_no)
    TextView txtConNo;
    @BindView(R.id.txt_conf_yes)
    TextView txtConYes;
    @BindView(R.id.txt_total_payment_wallet)
    TextView txtTotalPaymentWallet;
    @BindView(R.id.txt_total_payment_value)
    TextView txt_total_payment_value;
    @BindView(R.id.txt_wallet_balance)
    TextView txt_wallet_balance;
    @BindView(R.id.mainLayout)
    RelativeLayout mainLayout;


    private String orderId;
    private String accountId;
    private String walletStatus;
    private int total;
    private String bookingCode;
    private String restoName;
    private int totalPrice;

    private String bookingTime;
    private String bookingDay;
    private boolean isTakeAway = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.doku_payment);
        ButterKnife.bind(this);
        //mToolbar.setNavigationIcon(R.mipmap.ic_back);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mToolbar.setNavigationIcon(R.mipmap.ic_back);
        tvMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                patchOrderCancel(orderId);
            }
        });

        tvTitle.setText(R.string.dokuwallet);
        tvTitle.setAllCaps(false);
        tvMenu.setVisibility(View.VISIBLE);
        tvMenu.setText("CANCEL");
        tvMenu.setTextColor(Color.parseColor("#eeaa4a"));

        walletStatus = getIntent().getExtras().getString("walletStatus", "walletStatus");
        orderId = getIntent().getExtras().getString("orderId", "orderId");

        if (walletStatus.equals("1")) {
            orderId = getIntent().getExtras().getString("orderId", "orderId");
            accountId = getIntent().getExtras().getString("accountId", "accountId");
        }

        txtConNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                patchOrderCancel(orderId);
            }
        });
        txtConYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PostDokuWallet(orderId, id_doku_wallet.getText().toString());
            }
        });

        getOrderInfo();
        getBalance(accountId);
    }

    void getOrderInfo() {
        orderId = getIntent().getExtras().getString("orderId");
        showProgressDialog("Loading...");
        restClient.getApiService().getOrderDetail(orderId, Utils.getToken(mActivity).getAccessToken(), new Callback<ApiResponse<OrderDetail>>() {
            @Override
            public void success(ApiResponse<OrderDetail> orderDetailApiResponse, Response response) {
                progressDialog.dismiss();
                OrderDetail orderDetail = orderDetailApiResponse.getData();
//                total = orderDetail.getPrice().getSubtotal();
                total = (int) Math.ceil((orderDetail.getPrice().getTotal() * 0.001) * -1);
//                totalPrice = orderDetail.getPrice().getTotal() + total;
                totalPrice = orderDetail.getPrice().getTotal();

                bookingCode = orderDetail.getCode();
                restoName = orderDetail.getResto().getName();
                bookingTime = String.valueOf(FoodgasmDateUtil.getHourAndMinute(orderDetail.getVisitDate()));

                Calendar calendar = Calendar.getInstance();
                Date today = calendar.getTime();
                String bookDay = String.valueOf(FoodgasmDateUtil.getStringDate(orderDetail.getVisitDate()));
                DateFormat dateFormat = new SimpleDateFormat("MMM dd, yyy", java.util.Locale.getDefault());
                String todayAsString = dateFormat.format(today);
                if (!bookDay.equals(todayAsString)) {
                    bookingDay = "tomorrow";
                } else {
                    bookingDay = "today";
                }
                if (orderDetail.getPreorder().getIsOrder() && !orderDetail.getBooking().getIsBooking())
                    isTakeAway = true;

                id_doku_wallet.setText(orderDetail.getMaskedCard());
                txtTotalPaymentWallet.setText("Total Payment");
                txt_total_payment_value.setText(CTools.getRP(String.valueOf(totalPrice), true));
            }

            @Override
            public void failure(RetrofitError error) {
                progressDialog.dismiss();
                showAlertDialog("Failed", error.getMessage());
            }
        });
    }

    private void PostDokuWallet(String orderid, String id) {
        showProgressDialog("Submitting..");
        restClient.getApiService().postDokuResponseWalletNew(orderid, Utils.getToken(DokuWalletConfirmation.this).getAccessToken(), new DokuWallet(id), new Callback<ApiResponse<DokuWalletResponseNew>>() {
            @Override
            public void success(ApiResponse<DokuWalletResponseNew> userApiResponse, Response response) {
                progressDialog.dismiss();

                Log.d("cek status doku wallet -->", userApiResponse.getData().getResponseCode());

                String status_code = userApiResponse.getData().getResponseCode();
                if (!userApiResponse.getData().getResponseCode().equals("0000")) {
                    new MaterialDialog.Builder(DokuWalletConfirmation.this)
                            .title(R.string.alert)
                            .content("Your Balance is not Enough")
                            .positiveText(R.string.ok)
                            .cancelable(false)
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                                    dialog.dismiss();
                                }
                            })
                            .show();
                } else {
                    Intent intent = new Intent(mActivity, BookOrderCompleteAct.class);
                    intent.putExtra(BookOrderCompleteAct.FLAG, BookOrderCompleteAct.FLAG_ORDER_ACCEPTED);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("bookingTime", bookingTime);
                    intent.putExtra("bookingDay", bookingDay);
                    intent.putExtra("bookingCode", bookingCode);
                    intent.putExtra("isTakeAway", isTakeAway);
                    intent.putExtra("restoName", restoName);
                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                }

            }

            @Override
            public void failure(RetrofitError error) {
                progressDialog.dismiss();
                new MaterialDialog.Builder(DokuWalletConfirmation.this)
                        .title(R.string.alert)
                        .content(error.getMessage())
                        .positiveText(R.string.ok)
                        .cancelable(false)
                        .show();
            }
        });
    }

    private void getBalance(String accountId) {
        //showProgressDialog("Submitting..");
        restClient.getApiService().postBalanceDokuWallet(Utils.getToken(DokuWalletConfirmation.this).getAccessToken(), new DokuWalletBalance(Constants.DOKU_BALANCE_ENDPOINT, accountId), new Callback<ApiResponse<DokuWalletBalanceResponse>>() {
            @Override
            public void success(ApiResponse<DokuWalletBalanceResponse> userApiResponse, Response response) {
                progressDialog.dismiss();

                Log.d("BALANCE DOKU WALLET -->", userApiResponse.getData().getResponseCode());
                if (!userApiResponse.getData().getResponseCode().equals("5000")) {
                    txt_wallet_balance.setText("Balance: " + CTools.getRP(userApiResponse.getData().getLastBalance(), true));
                } else {
                    mainLayout.setVisibility(View.GONE);
                    new MaterialDialog.Builder(DokuWalletConfirmation.this)
                            .title(R.string.alert)
                            .content("Maaf akun tidak ditemukan")
                            .positiveText(R.string.ok)
                            .cancelable(false)
                            .show();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                progressDialog.dismiss();
                new MaterialDialog.Builder(DokuWalletConfirmation.this)
                        .title(R.string.alert)
                        .content(error.getMessage())
                        .positiveText(R.string.ok)
                        .cancelable(false)
                        .show();
            }
        });
    }

    void patchOrderCancel(String orderId) {
        showProgressDialog("Loading...");
        restClient.getApiService().patchOrder(orderId, Utils.getToken(mActivity).getAccessToken(), new PatchOrder("cancel", "User cancelled the order"), new Callback<Order>() {

            @Override
            public void success(Order order, Response response) {
                progressDialog.dismiss();
                new MaterialDialog.Builder(mActivity)
                        .content(R.string.order_cancelled)
                        .positiveText(R.string.ok)
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(MaterialDialog dialog, DialogAction which) {
                                //TODO call cancel order API
                                SPManager.saveString(getApplicationContext(), Constants.Inbox.STATUS_INBOX, null);

                                Intent intent = new Intent(DokuWalletConfirmation.this, MainAct.class);
                                finish();
                                startActivity(intent);
                                overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                            }
                        })
                        .cancelable(false)
                        .build()
                        .show();
            }

            @Override
            public void failure(RetrofitError error) {
                progressDialog.dismiss();
                showMaterialDialogAlert(NetworkErrorUtil.getErrorMessage(error));
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(DokuWalletConfirmation.this, MainAct.class);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
    }
}
