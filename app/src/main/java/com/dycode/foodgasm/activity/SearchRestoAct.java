package com.dycode.foodgasm.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.dycode.foodgasm.FoodGasmApp;
import com.dycode.foodgasm.R;
import com.dycode.foodgasm.adapters.SearchRestaurantAdapter;
import com.dycode.foodgasm.dao.DaoSession;
import com.dycode.foodgasm.dao.SearchHistory;
import com.dycode.foodgasm.dao.SearchHistoryDao;
import com.dycode.foodgasm.model.ApiResponse;
import com.dycode.foodgasm.model.ApiResponseList;
import com.dycode.foodgasm.model.DealsDetail;
import com.dycode.foodgasm.model.Restaurant;
import com.dycode.foodgasm.rest.RestClient;
import com.dycode.foodgasm.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by asus pc on 11/12/2015.
 */
public class SearchRestoAct extends BaseActivity implements AdapterView.OnItemClickListener {
    public static final String FLAG = "FLAG";
    public static final String KEYWORD = "KEYWORD";
    public static final String CATEGORY = "CATEGORY";
    public static final String LOCATION = "LOCATION";

    public static final int FLAG_SEARCH_RESTO = 0;
    public static final int FLAG_SEARCH_RESTO_DEALS = 1;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.listRestaurant)
    ListView listRestaurant;
//    @BindView(R.id.tvCancel)
//    TextView tvCancel;
//    @BindView(R.id.etSearch)
//    EditText etSearch;

    FoodGasmApp foodGasmApp;
    DaoSession daoSession;

    SearchRestaurantAdapter mAdapter;
    List<Restaurant> restaurantList = new ArrayList<Restaurant>();
    String keyword, dealsId;
    int flag;

    private String keywords = "";
    private String locations = "";
    private String categorys = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_searched_category);
        ButterKnife.bind(this);

        // Obtain the shared Tracker instance.
        getTracker("Restaurant Search");

        setSupportActionBar(mToolbar);
        setToolbar(true);

        foodGasmApp = (FoodGasmApp) getApplication();
        daoSession = foodGasmApp.getDaoSession();
        flag = getIntent().getExtras().getInt(FLAG);

        keywords = getIntent().getExtras().getString(KEYWORD);
        locations = getIntent().getExtras().getString(LOCATION);
        categorys = getIntent().getExtras().getString(CATEGORY);


        mAdapter = new SearchRestaurantAdapter(mActivity, restaurantList);
        listRestaurant.setAdapter(mAdapter);
        listRestaurant.setOnItemClickListener(SearchRestoAct.this);
        for (SearchHistory searchHistory : daoSession.getSearchHistoryDao().queryBuilder().orderDesc(SearchHistoryDao.Properties.Created_at).where(SearchHistoryDao.Properties.Created_at.notEq(0)).list()) {
            restaurantList.add(new Restaurant(searchHistory.getResto_id(), searchHistory.getKeyword(), searchHistory.getResto_city()));
        }

        getSearchedRestaurantList(keywords, categorys, locations);

    }

    @Override
    protected int getToolbarTitle() {
        return R.string.search;
    }

    void getSearchedRestaurantList(String name, String category, String location) {
        restClient = RestClient.getInstance();
        restClient.getApiService().getListRestaurant(Utils.getToken(mActivity).getAccessToken(), Utils.getLatString(mActivity), Utils.getLngString(mActivity), name, category, location, 0, 0, new Callback<ApiResponseList<Restaurant>>() {
            @Override
            public void success(ApiResponseList<Restaurant> restaurantApiResponseList, Response response) {
                if (restaurantApiResponseList.getMeta().getCode() == 200) {
                    restaurantList.clear();
                    restaurantList.addAll(restaurantApiResponseList.getData());
                    mAdapter.notifyDataSetChanged();
                } else {
                    showAlertDialog("Failed", restaurantApiResponseList.getMeta().getMessage());
                }
            }

            @Override
            public void failure(RetrofitError error) {
//                showAlertDialog("Failed", error.getMessage());
                showToast("No Internet Connection");
            }
        });
    }

    void getSearchedDealsRestaurantList(String query) {
        restClient.getApiService().getDetailDeals(dealsId,  Utils.getLatString(mActivity), Utils.getLngString(mActivity), 0, 0, new Callback<ApiResponse<DealsDetail>>() {
                    @Override
                    public void success(ApiResponse<DealsDetail> dealsDetailApiResponse, Response response) {
                        restaurantList.clear();
                        restaurantList.addAll(dealsDetailApiResponse.getData().getRestaurants());
                        mAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        showAlertDialog("Failed", error.getMessage());
                    }
                }

        );
    }

    void addSearchHistoryRecord(String query) {
        SearchHistory searchHistory = new SearchHistory();
        Long timeStamp = System.currentTimeMillis();
        searchHistory.setCreated_at(timeStamp);
        searchHistory.setKeyword(query);
        daoSession.getSearchHistoryDao().insertOrReplace(searchHistory);

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        SearchHistory searchHistory = new SearchHistory();
        Long timeStamp = System.currentTimeMillis();
        searchHistory.setKeyword(restaurantList.get(position).getName());
        searchHistory.setResto_city(restaurantList.get(position).getAddress().getCity());
        searchHistory.setCreated_at(timeStamp);
        daoSession.getSearchHistoryDao().insertOrReplace(searchHistory);

        Intent intent = new Intent(mActivity, DetailRestaurantAct.class);
        intent.putExtra(Restaurant.RESTAURANT, restaurantList.get(position));
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
    }

}
