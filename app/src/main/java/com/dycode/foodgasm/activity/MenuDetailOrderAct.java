package com.dycode.foodgasm.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.crashlytics.android.Crashlytics;
import com.dycode.foodgasm.FoodGasmApp;
import com.dycode.foodgasm.R;
import com.dycode.foodgasm.adapters.DetailMenuAdapter;
import com.dycode.foodgasm.adapters.SearchMenuAdapter;
import com.dycode.foodgasm.dao.CategoryDao;
import com.dycode.foodgasm.dao.DaoSession;
import com.dycode.foodgasm.dao.MenuDao;
import com.dycode.foodgasm.dao.MenuVersion;
import com.dycode.foodgasm.dao.MenuVersionDao;
import com.dycode.foodgasm.fragment.DetailMenuOrderFragment;
import com.dycode.foodgasm.model.ApiResponseList;
import com.dycode.foodgasm.model.Category;
import com.dycode.foodgasm.model.EventBusAction;
import com.dycode.foodgasm.model.Menu;
import com.dycode.foodgasm.model.Restaurant;
import com.dycode.foodgasm.utils.FormatterUtil;
import com.dycode.foodgasm.utils.SPManager;
import com.dycode.foodgasm.utils.Utils;
import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.greenrobot.dao.query.DeleteQuery;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by asus pc on 19/10/2015.
 * Dari review order > order more
 */
public class MenuDetailOrderAct extends BaseActivity implements DetailMenuAdapter.MenuCountListener, DetailMenuAdapter.MenuScrollListener, SearchMenuAdapter.MenuCountListener, View.OnClickListener {

    public static final String FLAG = "FLAG";
    public static final int FLAG_FIRST_TIME_ORDER = 0;
    public static final int FLAG_ORDER_MORE = 1;


    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.tabs)
    SmartTabLayout mTabs;
    @BindView(R.id.viewpager)
    ViewPager mPager;
    @BindView(R.id.ivRestaurantPict)
    ImageView ivRestaurantPict;
    @BindView(R.id.tvRestaurantName)
    TextView tvRestaurantName;
    @BindView(R.id.linProceed)
    LinearLayout linProceed;
    @BindView(R.id.tvPrice)
    TextView tvPrice;
    @BindView(R.id.linSearch)
    LinearLayout linSearch;
    @BindView(R.id.linStartSearch)
    LinearLayout linStartSearch;
    @BindView(R.id.etSearch)
    EditText etSearch;
    @BindView(R.id.tvCancel)
    TextView tvCancel;
    @BindView(R.id.divider)
    View divider;
    @BindView(R.id.ivSearch)
    ImageView ivSearch;
    @BindView(R.id.listSearchMenu)
    ListView listSearchMenu;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.linContainer)
    LinearLayout linContainer;
    @BindView(R.id.tvProcess)
    TextView tvProcess;


    SearchMenuAdapter mAdapter;
    Restaurant restaurant;

    ArrayList<com.dycode.foodgasm.dao.Menu> menuList = new ArrayList<>();
    List<Menu> categoryMenuList = new ArrayList<>();
    List<com.dycode.foodgasm.dao.Category> categoryDaoList = new ArrayList<>();
    List<Category> categoryList = new ArrayList<>();
    List<com.dycode.foodgasm.dao.Menu> searchMenuList = new ArrayList<>();

    FoodGasmApp foodGasmApp;
    DaoSession daoSession;
    String personAmount, day, minute;
    int flag, tax, value_id;
    ViewPagerAdapter adapter;

    private String taxResto;
    private double taxRestoD;
    private String getOpentime, getClosetime;
    private String commission;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_detail);
        ButterKnife.bind(this);

        foodGasmApp = (FoodGasmApp) getApplication();
        daoSession = foodGasmApp.getDaoSession();
        mToolbar.setNavigationIcon(R.mipmap.ic_navigation_arrow_back);
        setSupportActionBar(mToolbar);

        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        flag = getIntent().getExtras().getInt(FLAG);
        ivSearch.setColorFilter(Utils.getColor(this, R.color.windowBackground));
        restaurant = getIntent().getExtras().getParcelable(Restaurant.RESTAURANT);

        // Obtain the shared Tracker instance.
        getTracker(restaurant.getName() + " Menu List (" + restaurant.getId() + ")");

        tax = getIntent().getExtras().getInt("tax");
        value_id = getIntent().getExtras().getInt("id");
        personAmount = getIntent().getExtras().getString("personAmount");
        day = getIntent().getExtras().getString("day");
        minute = getIntent().getExtras().getString("minute");
        commission = getIntent().getExtras().getString("commission");
        Picasso.with(this).load(restaurant.getPic()).into(ivRestaurantPict);
        tvRestaurantName.setText(restaurant.getName());

        if (flag == FLAG_ORDER_MORE) {

            linProceed.setVisibility(View.VISIBLE);
            List<com.dycode.foodgasm.dao.Menu> databaseList = daoSession.getMenuDao().queryBuilder().where(MenuDao.Properties.Amount.gt(0), MenuDao.Properties.RestoId.eq(restaurant.getId())).list();
            int totalPrice = 0;
            for (com.dycode.foodgasm.dao.Menu menuloop : databaseList) {
                taxResto = SPManager.getString(MenuDetailOrderAct.this, "taxResto");
                taxRestoD = Double.parseDouble(taxResto);
                totalPrice += menuloop.getPrice() * menuloop.getAmount();
            }
            if (totalPrice == 0)
                linProceed.setVisibility(View.GONE);

            Log.d("order more >>", String.valueOf(totalPrice));
//            tvPrice.setText(FormatterUtil.convertToIDRFormat(totalPrice));
            int a = totalPrice + (totalPrice * Integer.valueOf(commission) / 100);
            int d = (int) Math.ceil((double) (a + a * Double.valueOf(taxRestoD) / 100));
            int commissionPrice = (int) Math.ceil((double) (d + d * Double.valueOf(commission) / 100));

            tvPrice.setText(FormatterUtil.getGermanCurrencyFormat(d));
        }

        mAdapter = new SearchMenuAdapter(mActivity, searchMenuList);
        listSearchMenu.setAdapter(mAdapter);
        linProceed.setOnClickListener(this);
        linSearch.setOnClickListener(this);
        tvCancel.setOnClickListener(this);
        getCategoryList(false, null);


        if (restaurant.getClosedState() == 1) {
            tvProcess.setText("WE'RE CLOSED");

            linProceed.setEnabled(false);
            linProceed.setBackgroundColor(Color.parseColor("#AAAAAA"));

        }


    }


    void getSearchedMenu(String query) {
        searchMenuList.clear();
        searchMenuList.addAll(daoSession.getMenuDao().queryBuilder().where(MenuDao.Properties.Name.like("%" + query + "%"), MenuDao.Properties.RestoId.eq(restaurant.getId())).list());
        mAdapter.notifyDataSetChanged();
    }

    void getCategoryList(final boolean update, final ArrayList<com.dycode.foodgasm.dao.Menu> menuList) {
        restClient.getApiService().getListMenuCategories(restaurant.getId(), new Callback<ApiResponseList<Category>>() {
            @Override
            public void success(ApiResponseList<Category> categoryApiResponseList, Response response) {
                categoryList.clear();
                categoryList.addAll(categoryApiResponseList.getData());
                if (categoryList.size() == 0) {
                    new MaterialDialog.Builder(mActivity)
                            .content(R.string.no_menu)
                            .positiveText(R.string.ok)
                            .cancelable(false)
                            .onPositive((materialDialog, dialogAction) -> materialDialog.dismiss())
                            .show();
                } else {
                    List<MenuVersion> menuVersionList = daoSession.getMenuVersionDao().queryBuilder().where(MenuVersionDao.Properties.RestoId.eq(restaurant.getId()), MenuVersionDao.Properties.MenuVersion.eq(restaurant.getMenuVersion())).limit(1).list();
                    if (menuVersionList.size() == 1) {
                        categoryDaoList = daoSession.getCategoryDao().queryBuilder().orderAsc(CategoryDao.Properties.Position).where(CategoryDao.Properties._id.isNotNull(), CategoryDao.Properties.RestoId.eq(restaurant.getId())).list();

                        if (flag != 1 && update) {
                            adapter.removeFragment();
                        }

                        for (int cPos = 0; cPos < categoryDaoList.size(); cPos++) {

                            String categoryID = categoryDaoList.get(cPos).get_id();
                            List<com.dycode.foodgasm.dao.Menu> menuListOld;

                            if (flag == 1) {
                                // Validation
                                ArrayList<com.dycode.foodgasm.dao.Menu> menuForAddMore = getIntent().getParcelableArrayListExtra(com.dycode.foodgasm.dao.Menu.MENU);

                                if (update) {

                                    menuListOld = menuList;

                                    for (int checkNewOld = 0; checkNewOld < menuListOld.size(); checkNewOld++) {
                                        daoSession.getMenuDao().insertOrReplace(menuListOld.get(checkNewOld));
                                    }
                                } else if (!update) {
                                    menuListOld = daoSession.getMenuDao().queryBuilder().where(MenuDao.Properties.CategoryId.eq(categoryID), MenuDao.Properties.RestoId.eq(restaurant.getId())).list();

                                    for (int checkNewOld = 0; checkNewOld < menuListOld.size(); checkNewOld++) {

                                        for (int checkNewMenu = 0; checkNewMenu < menuForAddMore.size(); checkNewMenu++) {
                                            if (menuListOld.get(checkNewOld).get_id().equalsIgnoreCase(menuForAddMore.get(checkNewMenu).get_id())) {
                                                menuListOld.get(checkNewOld).setAmount(menuForAddMore.get(checkNewMenu).getAmount());
                                                daoSession.getMenuDao().insertOrReplace(menuListOld.get(checkNewOld));
                                                break;
                                            } else {
                                                menuListOld.get(checkNewOld).setAmount(0);
                                                daoSession.getMenuDao().insertOrReplace(menuListOld.get(checkNewOld));
                                            }
                                        }
                                    }

                                    adapter.addFrag(DetailMenuOrderFragment.newInstance(restaurant.getId(), categoryDaoList.get(cPos).get_id(), categoryDaoList.get(cPos).getName(), flag), categoryDaoList.get(cPos).getName());
                                }

                            } else {
                                // Validation

                                if (update) {

                                    menuListOld = menuList;

                                    for (int checkNewOld = 0; checkNewOld < menuListOld.size(); checkNewOld++) {
                                        daoSession.getMenuDao().insertOrReplace(menuListOld.get(checkNewOld));
                                    }

                                    adapter.addFrag(DetailMenuOrderFragment.newInstance(restaurant.getId(), categoryDaoList.get(cPos).get_id(), categoryDaoList.get(cPos).getName(), menuList, 1), categoryDaoList.get(cPos).getName());
                                    adapter.notifyDataSetChanged();
                                    mPager.getAdapter().notifyDataSetChanged();

                                } else {
                                    adapter.addFrag(DetailMenuOrderFragment.newInstance(restaurant.getId(), categoryDaoList.get(cPos).get_id(), categoryDaoList.get(cPos).getName(), flag), categoryDaoList.get(cPos).getName());
                                }

                            }
                        }
                        if (flag == 1) {
                            ArrayList<com.dycode.foodgasm.dao.Menu> menuForAddMore = getIntent().getParcelableArrayListExtra(com.dycode.foodgasm.dao.Menu.MENU);
                            if (menuForAddMore.size() > 0) {
                                for (int menuPos = 0; menuPos < menuForAddMore.size(); menuPos++) {
                                    com.dycode.foodgasm.dao.Menu menu = menuForAddMore.get(menuPos);
                                    if (menuForAddMore.get(menuPos).get_id().equals(menu.get_id())) {
                                        menu.setAmount(menuForAddMore.get(menuPos).getAmount());
                                    }
                                }
                            }
                        }
                    } else {
                        final DeleteQuery<com.dycode.foodgasm.dao.Category> categoryDeleteQuery = daoSession.queryBuilder(com.dycode.foodgasm.dao.Category.class)
                                .where(CategoryDao.Properties.RestoId.eq(restaurant.getId()))
                                .buildDelete();
                        categoryDeleteQuery.executeDeleteWithoutDetachingEntities();
                        daoSession.clear();
                        final DeleteQuery<com.dycode.foodgasm.dao.Menu> menuDeleteQuery = daoSession.queryBuilder(com.dycode.foodgasm.dao.Menu.class)
                                .where(MenuDao.Properties.RestoId.eq(restaurant.getId()))
                                .buildDelete();
                        menuDeleteQuery.executeDeleteWithoutDetachingEntities();
                        daoSession.clear();

                        for (int pos = 0; pos < categoryList.size(); pos++) {
                            Category category = categoryList.get(pos);
                            com.dycode.foodgasm.dao.Category categoryDao = new com.dycode.foodgasm.dao.Category();
                            categoryDao.set_id(category.getId());
                            categoryDao.setRestoId(restaurant.getId());
                            categoryDao.setName(category.getName());
                            categoryDao.setPosition(category.getPosition());
                            daoSession.getCategoryDao().insertOrReplace(categoryDao);

                            categoryMenuList.clear();
                            categoryMenuList.addAll(category.getMenus());
                            for (int x = 0; x < categoryMenuList.size(); x++) {
                                Menu menu = categoryMenuList.get(x);
                                com.dycode.foodgasm.dao.Menu menuDao = new com.dycode.foodgasm.dao.Menu();
                                menuDao.set_id(menu.getId());
                                menuDao.setCategoryId(category.getId());
                                menuDao.setCategoryName(category.getName());
                                daoSession.getMenuDao().insertOrReplace(menuDao);

                                List<com.dycode.foodgasm.dao.Menu> menuDaoList = daoSession.getMenuDao().queryBuilder().where(MenuDao.Properties.CategoryId.eq(category.getId()), MenuDao.Properties._id.eq(menu.getId())).list();
                                for (int i = 0; i < menuDaoList.size(); i++) {
                                    menuDao = menuDaoList.get(i);
                                    menuDao.setName(menu.getName());
                                    menuDao.setPrice(menu.getPrice());
                                    menuDao.setDescription(menu.getDescription());
                                    menuDao.setPic(menu.getImage());
                                    menuDao.setDiscount(menu.getDiscountPrice());
                                    menuDao.setAmount(0);
                                    menuDao.setSubtotalPrice(menu.getSubtotalPrice());
                                    menuDao.setRestoId(restaurant.getId());
                                    daoSession.getMenuDao().insertOrReplace(menuDao);
                                }
                            }
                        }

                        com.dycode.foodgasm.dao.MenuVersion menuVersion = new com.dycode.foodgasm.dao.MenuVersion();
                        menuVersion.setRestoId(restaurant.getId());
                        menuVersion.setMenuVersion(restaurant.getMenuVersion());
                        daoSession.getMenuVersionDao().insertOrReplace(menuVersion);

                        categoryDaoList = daoSession.getCategoryDao().queryBuilder().orderAsc(CategoryDao.Properties.Position).where(CategoryDao.Properties._id.isNotNull(), CategoryDao.Properties.RestoId.eq(restaurant.getId())).list();

                        if (!update) {
                            for (int cPos = 0; cPos < categoryDaoList.size(); cPos++) {
                                adapter.addFrag(DetailMenuOrderFragment.newInstance(restaurant.getId(), categoryDaoList.get(cPos).get_id(), categoryDaoList.get(cPos).getName(), flag), categoryDaoList.get(cPos).getName());
                            }
                        }

                    }
                }
                progressBar.setVisibility(View.GONE);
                linContainer.setVisibility(View.VISIBLE);
                mPager.setAdapter(adapter);
                mPager.setOffscreenPageLimit(categoryList.size() - 1);
                mTabs.setViewPager(mPager);
            }

            @Override
            public void failure(RetrofitError error) {
                progressBar.setVisibility(View.GONE);
                linContainer.setVisibility(View.VISIBLE);
                showAlertDialog("Failed", error.getMessage());
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
    }

    @Override
    public void onChangeCount(com.dycode.foodgasm.dao.Menu menu, int posMenu, int newCount) {
        linProceed.setVisibility(View.VISIBLE);
        //change database
        daoSession.getMenuDao().insertOrReplace(menu);

        menuList.clear();
        List<com.dycode.foodgasm.dao.Menu> databaseList = daoSession.getMenuDao().queryBuilder().where(MenuDao.Properties.Amount.gt(0), MenuDao.Properties.RestoId.eq(restaurant.getId())).list();
        menuList.addAll(databaseList);

        int totalPrice = 0;
        for (com.dycode.foodgasm.dao.Menu menuloop : databaseList) {
            taxResto = SPManager.getString(MenuDetailOrderAct.this, "taxResto");
            taxRestoD = Double.parseDouble(taxResto);
            totalPrice += menuloop.getPrice() * menuloop.getAmount();
        }
        if (totalPrice == 0)
            linProceed.setVisibility(View.GONE);

        int a = totalPrice + (totalPrice * Integer.valueOf(commission) / 100);
        int d = (int) Math.ceil((double) (a + a * Double.valueOf(taxRestoD) / 100));
        int commissionPrice = (int) Math.ceil((double) (d + d * Double.valueOf(commission) / 100));

        tvPrice.setText(FormatterUtil.getGermanCurrencyFormat(d));

//        tvPrice.setText(FormatterUtil.convertToIDRFormat(totalPrice));

        // Synchronize scroll
        DetailMenuOrderFragment childFragment = (DetailMenuOrderFragment) adapter.getItem(mPager.getCurrentItem());
        childFragment.synchronizeScroll(posMenu, false);

    }

    @Override
    public void onClickedMenuAmount(com.dycode.foodgasm.dao.Menu menu, int posMenu) {
        // Synchronize scroll
        DetailMenuOrderFragment childFragment = (DetailMenuOrderFragment) adapter.getItem(mPager.getCurrentItem());
        childFragment.synchronizeScroll(posMenu, true);
    }

    @Override
    public void onChangeSearchedMenuCount(com.dycode.foodgasm.dao.Menu menu, int posMenu, int newCount) {
        linProceed.setVisibility(View.VISIBLE);
        //change
        searchMenuList.get(posMenu).setAmount(newCount);
        daoSession.getMenuDao().insertOrReplace(searchMenuList.get(posMenu));
        menuList.clear();
        List<com.dycode.foodgasm.dao.Menu> databaseList = daoSession.getMenuDao().queryBuilder().where(MenuDao.Properties.Amount.gt(0), MenuDao.Properties.RestoId.eq(restaurant.getId())).list();
        menuList.addAll(databaseList);
        int totalPrice = 0;

        // Refresh data
        getCategoryList(true, menuList);

        for (com.dycode.foodgasm.dao.Menu menuloop : menuList) {
            taxResto = SPManager.getString(MenuDetailOrderAct.this, "taxResto");
            taxRestoD = Double.parseDouble(taxResto);
            totalPrice += menuloop.getPrice() * menuloop.getAmount();
        }
        if (totalPrice == 0)
            linProceed.setVisibility(View.GONE);

        int a = totalPrice + (totalPrice * Integer.valueOf(commission) / 100);
        int d = (int) Math.ceil((double) (a + a * (int) taxRestoD / 100));
        int commissionPrice = (int) Math.ceil((double) (d + d * Double.valueOf(commission) / 100));

        tvPrice.setText(FormatterUtil.getGermanCurrencyFormat(d));

//        tvPrice.setText(FormatterUtil.convertToIDRFormat(totalPrice));

    }

    @Override
    public void onClick(View v) {
        if (v == linProceed) {

            // Check if menu size 0
            checkMenuList();
            bus.post(new EventBusAction("updateMenu", menuList));

            if (value_id == 1) {
                Intent intent = new Intent(MenuDetailOrderAct.this, ReviewOrderAct.class);
                intent.putExtra("id", value_id);
                intent.putExtra(ReviewOrderAct.FLAG, ReviewOrderAct.FLAG_ORDER);
                intent.putExtra(Restaurant.RESTAURANT, restaurant);
                intent.putExtra(com.dycode.foodgasm.dao.Menu.MENU, menuList);
                intent.putExtra("personAmount", personAmount);
                intent.putExtra("day", day);
                intent.putExtra("minute", minute);
                intent.putExtra("tax", tax);
                intent.putExtra("commission", commission);

                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivityForResult(intent, 0);
                if (flag == 1) {
                    finish();
                }
                overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
            } else if (value_id == 2) {
                Intent intent = new Intent(MenuDetailOrderAct.this, ReviewOrderAct.class);
                intent.putExtra("id", value_id);
                intent.putExtra(ReviewOrderAct.FLAG, ReviewOrderAct.FLAG_TAKE_AWAY);
                intent.putExtra(Restaurant.RESTAURANT, restaurant);
                intent.putExtra(com.dycode.foodgasm.dao.Menu.MENU, menuList);
                intent.putExtra("personAmount", personAmount);
                intent.putExtra("day", day);
                intent.putExtra("minute", minute);
                intent.putExtra("tax", tax);
                intent.putExtra("commission", commission);
                startActivityForResult(intent, 0);
                if (flag == 1) {
                    finish();
                }
                overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
            }
        } else if (v == linSearch) {
//            searchMenuList.clear();
            divider.setVisibility(View.GONE);
            linSearch.setVisibility(View.GONE);
            mTabs.setVisibility(View.GONE);
            linStartSearch.setVisibility(View.VISIBLE);
            mPager.setVisibility(View.GONE);
            listSearchMenu.setVisibility(View.VISIBLE);
            etSearch.requestFocus();
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(etSearch, InputMethodManager.SHOW_IMPLICIT);

            etSearch.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    getSearchedMenu(s.toString());
                }
            });
        } else if (v == tvCancel) {
//            for (int pos = 0; pos < adapter.getCount(); pos++) {
//                ((DetailMenuOrderFragment) adapter.getItem(pos)).getMenus(false);
//            }

            linSearch.setVisibility(View.VISIBLE);
            linStartSearch.setVisibility(View.GONE);
            mTabs.setVisibility(View.VISIBLE);
            divider.setVisibility(View.VISIBLE);
            mPager.setVisibility(View.VISIBLE);
            listSearchMenu.setVisibility(View.GONE);

            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(etSearch.getWindowToken(), 0);
        }
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        public void removeFragment() {
            mFragmentList.clear();
            mFragmentTitleList.clear();
        }

        public void removeFragmentSpesific() {
        }


        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0 && resultCode == 1) {
            for (int pos = 0; pos < adapter.getCount(); pos++) {
                ((DetailMenuOrderFragment) adapter.getItem(pos)).getMenus(false);
            }
            linProceed.setVisibility(View.GONE);
        }
    }

    private void checkMenuList() {
        if (menuList.size() == 0) {
            List<com.dycode.foodgasm.dao.Menu> databaseList = daoSession.getMenuDao().queryBuilder().where(MenuDao.Properties.Amount.gt(0), MenuDao.Properties.RestoId.eq(restaurant.getId())).list();
            menuList.addAll(databaseList);
        }
    }
}
