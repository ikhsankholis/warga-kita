package com.dycode.foodgasm.activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.dycode.foodgasm.FoodGasmApp;
import com.dycode.foodgasm.R;
import com.dycode.foodgasm.dao.CreditCard;
import com.dycode.foodgasm.dao.CreditCardDao;
import com.dycode.foodgasm.dao.DaoSession;
import com.dycode.foodgasm.model.ApiResponse;
import com.dycode.foodgasm.model.CreditCardOrder;
import com.dycode.foodgasm.model.Order;
import com.dycode.foodgasm.model.OrderDetail;
import com.dycode.foodgasm.model.PatchOrder;
import com.dycode.foodgasm.model.Payment;
import com.dycode.foodgasm.model.PostPayment;
import com.dycode.foodgasm.utils.Constants;
import com.dycode.foodgasm.utils.FoodgasmDateUtil;
import com.dycode.foodgasm.utils.NetworkErrorUtil;
import com.dycode.foodgasm.utils.SPManager;
import com.dycode.foodgasm.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.co.veritrans.android.api.VTUtil.VTConfig;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by asus pc on 22/02/2016.
 */
public class PaymentAct extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.tvMenu)
    TextView tvMenu;
    @BindView(R.id.wvPayment)
    WebView wvPayment;
    @BindView(R.id.relInputCvv)
    RelativeLayout relInputCvv;
    @BindView(R.id.tvCardNumber)
    TextView tvCardNumber;
    @BindView(R.id.tvProfileName)
    TextView tvProfileName;
    @BindView(R.id.inpCvv)
    EditText inpCvv;

    double total;
    String orderId;
    int notif_id;

    FoodGasmApp foodGasmApp;
    DaoSession daoSession;

    List<CreditCard> creditCard;
    String cvv, bank, tokenId, bookingDay, bookingTime, bookingCode, restoName;
    boolean isTakeAway = false;
    private String token_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        ButterKnife.bind(this);

        foodGasmApp = (FoodGasmApp) getApplication();
        daoSession = foodGasmApp.getDaoSession();
        notif_id = getIntent().getExtras().getInt("id_notifications");
        clearNotif(notif_id);

        tvTitle.setText(getResources().getString(R.string.payment));
        tvMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        getOrderInfo();
    }

    @OnClick(R.id.btnContinue)
    public void verifyData() {
        if (!validate()) {
            onSubmitFailed();
            return;
        }
        submitDataToVeritrans();
    }

    public boolean validate() {
        boolean valid;

        cvv = inpCvv.getText().toString();
        valid = !TextUtils.isEmpty(cvv) && cvv.length() == 3;

        return valid;
    }

    public void onSubmitFailed() {
        showMaterialDialogAlert(R.string.payment_cvv_alert);
    }

    void getOrderInfo() {
        orderId = getIntent().getExtras().getString("orderId");
        showProgressDialog("Loading...");
        restClient.getApiService().getOrderDetail(orderId, Utils.getToken(mActivity).getAccessToken(), new Callback<ApiResponse<OrderDetail>>() {
            @Override
            public void success(ApiResponse<OrderDetail> orderDetailApiResponse, Response response) {
                progressDialog.dismiss();
                OrderDetail orderDetail = orderDetailApiResponse.getData();
                tvProfileName.setText(orderDetail.getUser().getFullname());
                total = orderDetail.getPrice().getTotal();
                bookingCode = orderDetail.getCode();
                restoName = orderDetail.getResto().getName();

                total = total + 2500;
                Log.d("total > ", String.valueOf(total));

                Calendar calendar = Calendar.getInstance();
                Date today = calendar.getTime();
                String bookDay = String.valueOf(FoodgasmDateUtil.getStringDate(orderDetail.getVisitDate()));
                DateFormat dateFormat = new SimpleDateFormat("MMM dd, yyy", java.util.Locale.getDefault());
                String todayAsString = dateFormat.format(today);
                if (!bookDay.equals(todayAsString)) {
                    bookingDay = "tomorrow";
                } else {
                    bookingDay = "today";
                }
                bookingTime = String.valueOf(FoodgasmDateUtil.getHourAndMinute(orderDetail.getVisitDate()));

                if (orderDetail.getPreorder().getIsOrder() && !orderDetail.getBooking().getIsBooking())
                    isTakeAway = true;

                creditCard = daoSession.getCreditCardDao().queryBuilder().where(CreditCardDao.Properties.State.eq(true)).limit(1).list();
                if (creditCard.size() != 0) {
                    String creditCardNumber = creditCard.get(0).getMasked_card();
                    creditCardNumber = creditCardNumber.substring(creditCardNumber.length() - 4, creditCardNumber.length());
                    creditCardNumber = String.format(getString(R.string.credit_card_display), creditCardNumber);
//                    tvCardNumber.setText(creditCardNumber);
                    tvCardNumber.setText(creditCard.get(0).getMasked_card());
                } else tvCardNumber.setText("");

                getSavedToken(orderDetail.getPaymentCredential());

            }

            @Override
            public void failure(RetrofitError error) {
                progressDialog.dismiss();
                showAlertDialog("Failed", error.getMessage());
            }
        });
    }

    void getSavedToken(String paymentCredential) {
        showProgressDialog("Loading...");
        restClient.getApiService().setCreditCardActivePay(paymentCredential, Utils.getToken(mActivity).getAccessToken(), new Callback<ApiResponse<CreditCardOrder>>() {

            @Override
            public void success(ApiResponse<CreditCardOrder> creditCardOrder, Response response) {
                progressDialog.dismiss();
                Log.d("debug ","creditCardOrder.getSavedTokenId()  " + creditCardOrder.getData().getSavedTokenId());
//                tvCardNumber.setText(creditCardOrder.getData().getMaskedCard());
                token_id = creditCardOrder.getData().getSavedTokenId();
//                tvCardNumber.setText(String.format(getString(R.string.credit_card_display), creditCardOrder.getData().getMaskedCard()));
                String text = creditCardOrder.getData().getMaskedCard();
                String star = "**** **** **** " + text.substring(text.length() - 4);
                tvCardNumber.setText(star);
            }

            @Override
            public void failure(RetrofitError error) {
                progressDialog.dismiss();
                showMaterialDialogAlert(NetworkErrorUtil.getErrorMessage(error));
            }
        });
    }

    void patchOrderCancel(String orderId) {

        showProgressDialog("Loading...");
        restClient.getApiService().patchOrder(orderId, Utils.getToken(mActivity).getAccessToken(), new PatchOrder("cancel", "User cancelled the order"), new Callback<Order>() {

            @Override
            public void success(Order order, Response response) {
                progressDialog.dismiss();
//                new MaterialDialog.Builder(mActivity)
//                        .content(R.string.order_cancelled)
//                        .positiveText(R.string.ok)
//                        .onPositive(new MaterialDialog.SingleButtonCallback() {
//                            @Override
//                            public void onClick(MaterialDialog dialog, DialogAction which) {
//                                //TODO call cancel order API
//                                dialog.dismiss();
//                                SPManager.saveString(getApplicationContext(), Constants.Inbox.STATUS_INBOX, null);
//
//                                Intent intent = new Intent(PaymentAct.this, MainAct.class);
//                                finish();
//                                startActivity(intent);
//                                overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
//                            }
//                        })
//                        .cancelable(false)
//                        .build()
//                        .show();
                SPManager.saveString(getApplicationContext(), Constants.Inbox.STATUS_INBOX, null);

                Intent intent = new Intent(PaymentAct.this, MainAct.class);
                finish();
                startActivity(intent);
                overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
            }

            @Override
            public void failure(RetrofitError error) {
                progressDialog.dismiss();
                showMaterialDialogAlert(NetworkErrorUtil.getErrorMessage(error));
            }
        });
    }

    void submitDataToVeritrans() {
        //set environment
        VTConfig.VT_IsProduction = true;
        //set client key
        VTConfig.CLIENT_KEY = Constants.VERITRANS_CLIENT_KEY;

        //TODO: Set your card details based on user input.
//        List<CreditCard> creditCard = daoSession.getCreditCardDao().queryBuilder().where(CreditCardDao.Properties.State.eq(true)).limit(1).list();
//        final String token_id = creditCard.get(0).getSaved_token_id();

        List<CreditCard> creditCard = daoSession.getCreditCardDao().queryBuilder().orderDesc(CreditCardDao.Properties.State).list();
//        final String token_id = creditCard.get(0).getSaved_token_id();

        Log.d("token id > ", token_id);
        String client_key = Constants.VERITRANS_CLIENT_KEY;

        showProgressDialog("Submitting...");
        restClientForVeritrans.getApiService().getVeritransToken(token_id, cvv, true, client_key, String.valueOf(total), true, "credit_card", new Callback<com.dycode.foodgasm.model.CreditCard>() {
            @Override
            public void success(com.dycode.foodgasm.model.CreditCard creditCard, Response response) {
                progressDialog.dismiss();
                wvPayment.setVisibility(View.VISIBLE);
                relInputCvv.setVisibility(View.GONE);
                String redirect_url = creditCard.getRedirect_url();
                bank = creditCard.getBank();
                tokenId = creditCard.getTokenId();

                wvPayment.getSettings().setJavaScriptEnabled(true);
                wvPayment.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        switch (event.getAction()) {
                            case MotionEvent.ACTION_DOWN:
                            case MotionEvent.ACTION_UP:
                                if (!v.hasFocus()) {
                                    v.requestFocus();
                                }
                                break;
                        }
                        return false;
                    }
                });
                wvPayment.setWebChromeClient(new WebChromeClient());
                wvPayment.setWebViewClient(new VtWebViewClient(tokenId, bank));
                wvPayment.loadUrl(redirect_url);

            }

            @Override
            public void failure(RetrofitError error) {
                progressDialog.dismiss();
                showMaterialDialogAlert(NetworkErrorUtil.getErrorMessage(error));
            }
        });

    }

    private class SendTokenAsync extends AsyncTask<String, Void, String> {
        private String tokenId;
        private String bank;

        private SendTokenAsync(String tokenId, String bank) {
            this.tokenId = tokenId;
            this.bank = bank;
        }

        @Override
        protected String doInBackground(String... strings) {
            sendPaymentData(tokenId, bank);
            return "";
        }
    }

    private class VtWebViewClient extends WebViewClient {

        String tokenId;
        String bank;

        public VtWebViewClient(String tokenId, String bank) {
            this.tokenId = tokenId;
            this.bank = bank;
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);

            Log.d("VtLog", url);

            if (url.startsWith(Constants.getPaymentApiUrl() + "/callback/")) {
                //send token to server
                SendTokenAsync sendTokenAsync = new SendTokenAsync(tokenId, bank);
                sendTokenAsync.execute(tokenId, bank);

            } else if (url.startsWith(Constants.getPaymentApiUrl() + "/redirect/") || url.contains("3dsecure")) {
                /* Do nothing */
            }
        }

    }

    void sendPaymentData(String tokenId, String bank) {
        restClient.getApiService().postPayment(orderId, new PostPayment(tokenId, bank), Utils.getToken(mActivity).getAccessToken(), new Callback<ApiResponse<Payment>>() {
            @Override
            public void success(ApiResponse<Payment> paymentApiResponse, Response response) {
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // Do something after 5s = 5000ms
                        getVeritransStatus();
                    }
                }, 5000);

            }

            @Override
            public void failure(RetrofitError error) {
                if(error.getResponse().getStatus() == 202){
                    showDeclinedOrderDialog();
                }else{
                    showMaterialDialogAlert(NetworkErrorUtil.getErrorMessage(error));
                }
            }
        });
    }

    void showDeclinedOrderDialog(){
        MaterialDialog materialDialog = new MaterialDialog.Builder(mActivity)
                .content("Transaction has been decline. Please contact your bank")
                .cancelable(false)
                .positiveText("OK")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        Intent intent = new Intent(mActivity, BookOrderCompleteAct.class);
                        intent.putExtra(BookOrderCompleteAct.FLAG, BookOrderCompleteAct.FLAG_ORDER_PAYMENT_DECLINE);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("bookingTime", bookingTime);
                        intent.putExtra("bookingDay", bookingDay);
                        intent.putExtra("bookingCode", bookingCode);
                        intent.putExtra("isTakeAway", isTakeAway);
                        intent.putExtra("restoName", restoName);
                        startActivity(intent);
                    }
                })
                .build();
        materialDialog.show();
    }

    void getVeritransStatus() {
        //showProgressDialog("PROCESING PAYMENT...");
        restClient.getApiService().getStatusVeritrans(orderId, Utils.getToken(mActivity).getAccessToken(), new Callback<ApiResponse>() {
            @Override
            public void success(ApiResponse apiResponse, Response response) {
                Log.d("status", String.valueOf(apiResponse.getData()));
                progressDialog.dismiss();
                try {
                    JSONObject data = new JSONObject(apiResponse.getData().toString());
                    String _id = data.getString("_id");
                    int state = data.getInt("state");
                    String stateMessage = data.getString("stateMessage");
                    String vtstate = data.getString("vtstate");
                    Log.d("state > ", String.valueOf(state));

                    if (vtstate.equals("DONE") && state == 4) {
                        showDeclinedOrderDialog(    );
                    } else if (state == 1) {
                        Intent intent = new Intent(mActivity, BookOrderCompleteAct.class);
                        intent.putExtra(BookOrderCompleteAct.FLAG, BookOrderCompleteAct.FLAG_ORDER_ACCEPTED);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("bookingTime", bookingTime);
                        intent.putExtra("bookingDay", bookingDay);
                        intent.putExtra("bookingCode", bookingCode);
                        intent.putExtra("isTakeAway", isTakeAway);
                        intent.putExtra("restoName", restoName);
                        startActivity(intent);
                        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                    } else if (state == 2) {
                        showDeclinedOrderDialog();
                    } else {
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                // Do something after 5s = 5000ms
                                getVeritransStatus();
                            }
                        }, 5000);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        new MaterialDialog.Builder(mActivity)
                .content(R.string.cancel_order)
                .positiveText(R.string.yes)
                .negativeText(R.string.no)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {
                        //TODO call cancel order API
                        dialog.dismiss();
                        patchOrderCancel(orderId);
                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .build()
                .show();
    }
}
