package com.dycode.foodgasm.activity;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.dycode.foodgasm.FoodGasmApp;
import com.dycode.foodgasm.R;
import com.dycode.foodgasm.data.model.LoginEvent;
import com.dycode.foodgasm.feature.register.RegisterActivity;
import com.dycode.foodgasm.feature.skiplogin.SkipLoginActivity;
import com.dycode.foodgasm.model.AccessToken;
import com.dycode.foodgasm.model.Menu;
import com.dycode.foodgasm.model.PostLogin;
import com.dycode.foodgasm.model.PostRegister;
import com.dycode.foodgasm.model.Register;
import com.dycode.foodgasm.model.Restaurant;
import com.dycode.foodgasm.utils.NetworkErrorUtil;
import com.dycode.foodgasm.utils.SPManager;
import com.dycode.foodgasm.utils.Utils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sromku.simple.fb.Permission;
import com.sromku.simple.fb.SimpleFacebook;
import com.sromku.simple.fb.entities.Photo;
import com.sromku.simple.fb.entities.Profile;
import com.sromku.simple.fb.listeners.OnLoginListener;
import com.sromku.simple.fb.listeners.OnPhotosListener;
import com.sromku.simple.fb.listeners.OnProfileListener;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.InterruptedIOException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by Okta Dwi Priatna on 08/10/2015.
 */
public class LoginAct extends BaseActivity implements View.OnClickListener {

    private static final String TAG = "LoginAct";
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.btnSignIn)
    Button btnSignIn;
    @BindView(R.id.inpUsername)
    EditText inpUsername;
    @BindView(R.id.inpPassword)
    EditText inpPassword;
    @BindView(R.id.vFiller)
    View vFiller;
    @BindView(R.id.tvConnect)
    TextView tvConnect;
    @BindView(R.id.tvForgot)
    TextView tvForgot;
    @BindView(R.id.svLogin)
    ScrollView svLogin;

    String fullName, email, socialId;
    SimpleFacebook mSimpleFacebook;
    NetworkInfo nf;
    ConnectivityManager cn;

    Profile.Properties properties = new Profile.Properties.Builder()
            .add(Profile.Properties.ID)
            .add(Profile.Properties.NAME)
            .add(Profile.Properties.EMAIL)
            .add(Profile.Properties.PICTURE)
            .build();


    @Subscribe(threadMode = ThreadMode.MAIN)
    void onLoginEvent(LoginEvent event){
        finish();
    }

    @Override
    public void onResume() {
        super.onResume();
        mSimpleFacebook = SimpleFacebook.getInstance(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        // Obtain the shared Tracker instance.
        getTracker("Sign In");

        //set background
        getWindow().setBackgroundDrawableResource(R.mipmap.img_highlightblur);

        Boolean isRegister = SPManager.getIsRegister(this);
        if (isRegister) {
            Intent i = new Intent(this, MainAct.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
            finish();
        }

        mToolbar.setNavigationIcon(R.mipmap.ic_navigation_arrow_back);
        mToolbar.setNavigationOnClickListener(v -> onBackPressed());
        setSupportActionBar(mToolbar);
        cn = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        nf = cn.getActiveNetworkInfo();
        btnSignIn.setOnClickListener(this);
        tvConnect.setOnClickListener(this);
        tvForgot.setOnClickListener(this);
        inpPassword.setOnClickListener(this);
        inpUsername.setOnClickListener(this);


    }

    public void login() {
        Log.d(TAG, "Login");
        String username = inpUsername.getText().toString();
        String password = inpPassword.getText().toString();
        Log.d(TAG, "Login with username " + username + " and password " + password);

        if (!validate()) {
            onLoginFailed();
            return;
        }


        showProgressDialog("Logging in..");
        restClient.getApiService().postLogin(new PostLogin(username, password), new Callback<AccessToken>() {
            @Override
            public void success(AccessToken accessToken, Response response) {
                progressDialog.dismiss();
                Utils.saveToken(mActivity, accessToken);
                FoodGasmApp foodGasmApp = (FoodGasmApp) getApplication();
                foodGasmApp.registerWithNotificationHubs();
                SPManager.setIsRegister(mActivity, true);
                if (getIntent().getExtras() != null && getIntent().getExtras().getBundle("EXTRA_ORDER") != null) {
                    Bundle bundleOrder = getIntent().getExtras().getBundle("EXTRA_ORDER");
                    Intent intent = new Intent(mActivity, ReviewOrderAct.class);
                    intent.putExtra(SkipLoginActivity.Companion.getEXTRA_ID(), bundleOrder.getInt(SkipLoginActivity.Companion.getEXTRA_ID()));
                    intent.putExtra(ReviewOrderAct.FLAG, bundleOrder.getInt(ReviewOrderAct.FLAG));
                    intent.putExtra(Restaurant.RESTAURANT, (Restaurant) bundleOrder.getParcelable(Restaurant.RESTAURANT));
                    intent.putExtra(Menu.MENU, bundleOrder.getParcelableArrayList(Menu.MENU));
                    intent.putExtra(SkipLoginActivity.Companion.getEXTRA_PERSON(), bundleOrder.getString(SkipLoginActivity.Companion.getEXTRA_PERSON()));
                    intent.putExtra(SkipLoginActivity.Companion.getEXTRA_DAY(), bundleOrder.getString(SkipLoginActivity.Companion.getEXTRA_DAY()));
                    intent.putExtra(SkipLoginActivity.Companion.getEXTRA_MINUTE(), bundleOrder.getString(SkipLoginActivity.Companion.getEXTRA_MINUTE()));
                    intent.putExtra(SkipLoginActivity.Companion.getEXTRA_FLAG(), bundleOrder.getInt(SkipLoginActivity.Companion.getEXTRA_FLAG()));
                    intent.putExtra(SkipLoginActivity.Companion.getEXTRA_BOOKTYPE(), bundleOrder.getInt(SkipLoginActivity.Companion.getEXTRA_BOOKTYPE()));
                    intent.putExtra(SkipLoginActivity.Companion.getEXTRA_COMMISION(), bundleOrder.getString(SkipLoginActivity.Companion.getEXTRA_COMMISION()));
                    intent.putExtra(SkipLoginActivity.Companion.getEXTRA_CLOSE_TIME(), bundleOrder.getString(SkipLoginActivity.Companion.getEXTRA_CLOSE_TIME()));
                    intent.putExtra(SkipLoginActivity.Companion.getEXTRA_OPEN_TIME(), bundleOrder.getString(SkipLoginActivity.Companion.getEXTRA_OPEN_TIME()));
                    mActivity.startActivity(intent);
                    bus.post(new LoginEvent());
                } else {
                    Intent intent = new Intent(mActivity, MainAct.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }
                finish();
            }

            @Override
            public void failure(RetrofitError error) {
                progressDialog.dismiss();

                String message;
                try {
                    AccessToken accessToken = (AccessToken) error.getBodyAs(AccessToken.class);
                    message = accessToken.getErrorDescription();
                } catch (Exception e) {
                    if (error.getCause() instanceof SocketTimeoutException || error.getCause() instanceof InterruptedIOException) {
                        message = "Your request took too long time to process. Please try again.";
                    } else if (error.getCause() instanceof UnknownHostException) {
                        message = "Failed to establish connection.";
                    } else {
                        message = error.getMessage();
                    }
                }

                showAlertDialog("Login failed", message);

            }
        });
    }

    public void onLoginFailed() {
        showMaterialDialogAlert(R.string.login_failed);
        btnSignIn.setEnabled(true);
    }

    public boolean validate() {
        boolean valid = true;

        String username = inpUsername.getText().toString();
        String password = inpPassword.getText().toString();

        if (TextUtils.isEmpty(username)) {
            valid = false;
        } else {
            inpUsername.setError(null);
        }

        if (password.isEmpty()) {
            valid = false;
        } else {
            inpPassword.setError(null);
        }

        return valid;
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
    }

    @Override
    protected int getToolbarTitle() {
        return R.string.string_null;
    }

    OnLoginListener onLoginListener = new OnLoginListener() {
        @Override
        public void onException(Throwable throwable) {
            Log.i(TAG, throwable.getMessage());
            Toast.makeText(mActivity, throwable.getMessage(), Toast.LENGTH_SHORT).show();

        }

        @Override
        public void onFail(String reason) {
            Toast.makeText(mActivity, reason, Toast.LENGTH_SHORT).show();

        }

        @Override
        public void onLogin(String accessToken, List<Permission> acceptedPermissions, List<Permission> declinedPermissions) {
            Log.i(TAG, "Logged in");

            mSimpleFacebook.getProfile(properties, onProfileListener);
            mSimpleFacebook.getPhotos("Profile", onPhotosListener);

        }

        @Override
        public void onCancel() {
            Toast.makeText(mActivity, "Login Canceled", Toast.LENGTH_SHORT).show();
        }
    };

    OnProfileListener onProfileListener = new OnProfileListener() {
        @Override
        public void onComplete(Profile profile) {
            Log.i(TAG, "My profile name = " + profile.getName());
            Log.d(TAG, "My profile email = " + profile.getEmail());
            Log.e(TAG, "My profile ID = " + profile.getId());
            Log.e(TAG, "My profile Picture = " + "https://graph.facebook.com/" + profile.getId() + "/picture?type=large");
            Log.d("hello", "hello");

            fullName = profile.getName();
            email = profile.getEmail();
            socialId = profile.getId();
            signUpProfile();


        }

        @Override
        public void onFail(String reason) {
            super.onFail(reason);
            Toast.makeText(mActivity, reason, Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onException(Throwable throwable) {
            super.onException(throwable);
            Log.i(TAG, throwable.getMessage());
            Toast.makeText(mActivity, throwable.getMessage(), Toast.LENGTH_SHORT).show();
        }


        /*
     * You can override other methods here:
     * onThinking(), onFail(String reason), onException(Throwable throwable)
     */
    };

    OnPhotosListener onPhotosListener = new OnPhotosListener() {
        @Override
        public void onComplete(List<Photo> photos) {
            Log.i(TAG, "Number of photos = " + photos.size());
            Log.i(TAG, "Profile photos = " + photos.get(0).getPicture());
        }

    };

    void signUpProfile() {
        showProgressDialog("Verifying Data...");
        restClient.getApiService().postRegister(new PostRegister(null, null, email, null, null, null, null, "facebook", socialId, null, false), new Callback<Register>() {
            @Override
            public void success(Register register, Response response) {
                progressDialog.dismiss();
                Utils.saveTokenFromFb(mActivity, register);
                if (register.getIsFirstTime()) {
                    RegisterActivity.Companion.start(LoginAct.this, fullName, email, null, socialId);
                    overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                } else {
                    FoodGasmApp foodGasmApp = (FoodGasmApp) getApplication();
                    foodGasmApp.registerWithNotificationHubs();
                    SPManager.setIsRegister(mActivity, true);
                    Intent intent = new Intent(mActivity, MainAct.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                }

            }

            @Override
            public void failure(RetrofitError error) {
                progressDialog.dismiss();
                Toast.makeText(mActivity, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        mSimpleFacebook.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onClick(View v) {

        if (v == inpUsername) {
            Log.d("Scroll down Check ", "");
            svLogin.fullScroll(ScrollView.FOCUS_DOWN);
            svLogin.scrollTo(0, svLogin.getBottom());
        }
        if (v == btnSignIn) {
            if (nf != null && nf.isConnected())
                login();
            else
                showAlertDialog(mActivity.getResources().getString(R.string.error_no_connection_title), mActivity.getResources().getString(R.string.error_no_connection));
        } else if (v == tvConnect)
            mSimpleFacebook.login(onLoginListener);
        else if (v == tvForgot) {
            Intent intent = new Intent(LoginAct.this, ForgetPasswordAct.class);
            startActivity(intent);
            overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

}
