package com.dycode.foodgasm.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.dycode.foodgasm.FoodGasmApp;
import com.dycode.foodgasm.R;
import com.dycode.foodgasm.data.model.LoginEvent;
import com.dycode.foodgasm.feature.register.RegisterActivity;
import com.dycode.foodgasm.fragment.FragmentHighlight;
import com.dycode.foodgasm.fragment.FragmentHighlight1;
import com.dycode.foodgasm.fragment.FragmentHighlight2;
import com.dycode.foodgasm.fragment.FragmentHighlight3;
import com.dycode.foodgasm.model.PostRegister;
import com.dycode.foodgasm.model.Register;
import com.dycode.foodgasm.utils.SPManager;
import com.dycode.foodgasm.utils.Utils;
import com.dycode.foodgasm.widget.CircleIndicator;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.sromku.simple.fb.Permission;
import com.sromku.simple.fb.SimpleFacebook;
import com.sromku.simple.fb.entities.Profile;
import com.sromku.simple.fb.listeners.OnLoginListener;
import com.sromku.simple.fb.listeners.OnProfileListener;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Okta Dwi Priatna on 08/10/2015.
 */
public class HighLightAct extends BaseActivity implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks {

    private static final String TAG = HighLightAct.class.getSimpleName();
    private static final int RC_SIGN_IN = 2334;

    @BindView(R.id.pager)
    ViewPager mPager;
    @BindView(R.id.indicator)
    CircleIndicator indicator;
    @BindView(R.id.btnSignIn)
    Button btnSignIn;
    @BindView(R.id.btnSignUp)
    Button btnSignUp;
    @BindView(R.id.btnConnectToFb)
    Button btnConnectToFb;
    @BindView(R.id.ivBackground)
    ImageView ivBackground;

    Timer timer;
    int count = 0;
    String fullName, email, socialId, photo, provider;

    SimpleFacebook mSimpleFacebook;

    Profile.Properties properties = new Profile.Properties.Builder()
            .add(Profile.Properties.ID)
            .add(Profile.Properties.NAME)
            .add(Profile.Properties.EMAIL)
            .add(Profile.Properties.PICTURE)
            .build();


    //Google Login
    GoogleApiClient mGoogleApiClient;

    @Override
    public void onResume() {
        super.onResume();
        mSimpleFacebook = SimpleFacebook.getInstance(this);
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    void onLoginEvent(LoginEvent event) {
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_highlight);
        ButterKnife.bind(this);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestId()
                .requestProfile()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addConnectionCallbacks(this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();


        // Obtain the shared Tracker instance.
        getTracker("Welcome");

        Boolean isRegister = SPManager.getIsRegister(this);
        if (isRegister) {
            Intent i = new Intent(this, MainAct.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
            finish();
        }

        setupViewPager();
        btnSignIn.setOnClickListener(this);
        btnSignUp.setOnClickListener(this);
        btnConnectToFb.setOnClickListener(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(mGoogleApiClient != null)
            mGoogleApiClient.stopAutoManage(this);
    }

    void signUpProfile() {
        showProgressDialog("Verifying Data...");
        restClient.getApiService().postRegister(new PostRegister(fullName, null, email, null, null, null, null, provider, socialId, photo, false), new Callback<Register>() {
            @Override
            public void success(Register register, Response response) {
                progressDialog.dismiss();
                Utils.saveTokenFromFb(mActivity, register);
                if (register.getIsFirstTime()) {
                    RegisterActivity.Companion.start(HighLightAct.this, fullName, email, photo, socialId);
                    overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                } else {
                    FoodGasmApp foodGasmApp = (FoodGasmApp) getApplication();
                    foodGasmApp.registerWithNotificationHubs();
                    SPManager.setIsRegister(mActivity, true);
                    Intent intent = new Intent(mActivity, MainAct.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }

            }

            @Override
            public void failure(RetrofitError error) {
                progressDialog.dismiss();
                Toast.makeText(mActivity, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    OnLoginListener onLoginListener = new OnLoginListener() {
        @Override
        public void onException(Throwable throwable) {
            Log.i(TAG, throwable.getMessage());
            throwable.printStackTrace();
            Toast.makeText(mActivity, throwable.getMessage(), Toast.LENGTH_SHORT).show();

        }

        @Override
        public void onFail(String reason) {
        }

        @Override
        public void onLogin(String accessToken, List<Permission> acceptedPermissions, List<Permission> declinedPermissions) {
            mSimpleFacebook.getProfile(properties, onProfileListener);
        }

        @Override
        public void onCancel() {
        }
    };

    OnProfileListener onProfileListener = new OnProfileListener() {
        @Override
        public void onComplete(Profile profile) {
            fullName = profile.getName();
            email = profile.getEmail();
            socialId = profile.getId();
//            photo = profile.getPicture();
            photo = "https://graph.facebook.com/" + socialId + "/picture?type=large";
            provider = "facebook";
            signUpProfile();
        }

        @Override
        public void onFail(String reason) {
            super.onFail(reason);
            Toast.makeText(mActivity, reason, Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onException(Throwable throwable) {
            super.onException(throwable);
            Log.i(TAG, throwable.getMessage());
            Toast.makeText(mActivity, throwable.getMessage(), Toast.LENGTH_SHORT).show();
        }

    };

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
    }

    private void setupViewPager() {
        final GetStartedPagerAdapter adapter = new GetStartedPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new FragmentHighlight());
        adapter.addFrag(new FragmentHighlight1());
        adapter.addFrag(new FragmentHighlight2());
        adapter.addFrag(new FragmentHighlight3());

        mPager.setAdapter(adapter);
        mPager.setOffscreenPageLimit(4);
        mPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position > 0) {
                    ivBackground.setImageResource(R.mipmap.img_highlightblur);
                } else {
                    ivBackground.setImageResource(R.mipmap.img_highlight);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        indicator.setViewPager(mPager);

        // Timer for auto sliding
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (count <= 4) {
                            mPager.setCurrentItem(count, true);
                            count++;
                        } else {
                            count = 0;
                            mPager.setCurrentItem(count, true);
                        }
                    }
                });
            }
        }, 1000, 5000);

    }

    @OnClick(R.id.tvDemo)
    void skiplogin() {
        MainAct.start(this);
    }

    @OnClick(R.id.btnConnectToGoogle)
    void connectToGoogle() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onClick(View v) {
        if (v == btnSignIn) {
            Intent intent = new Intent(this, LoginAct.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
        } else if (v == btnSignUp) {
            RegisterActivity.Companion.start(this);
            overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
        } else if (v == btnConnectToFb) {
            mSimpleFacebook.login(onLoginListener);
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Crashlytics.log(connectionResult.getErrorCode() + " " + connectionResult.getErrorMessage());
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                status -> {

                });
    }

    @Override
    public void onConnectionSuspended(int i) {

    }


    private class GetStartedPagerAdapter extends FragmentPagerAdapter {

        private final List<Fragment> mFragmentList = new ArrayList<>();

        public GetStartedPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment) {
            mFragmentList.add(fragment);
        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }
    }

    public static void start(Context context) {
        context.startActivity(new Intent(context, HighLightAct.class));
    }

    private void handleSignInResult(GoogleSignInResult result) {
        if (result.isSuccess()) {
            GoogleSignInAccount acct = result.getSignInAccount();
            if (acct != null) {
                socialId = acct.getId();
                email = acct.getEmail();
                fullName = acct.getDisplayName();

                if (acct.getPhotoUrl() != null)
                    photo = acct.getPhotoUrl().toString();
                provider = "google";
                signUpProfile();
                if (mGoogleApiClient.isConnected()) {
                    Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                            status -> {

                            });
                }
            }
        } else {
            // Signed out, show unauthenticated UI.

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
        mSimpleFacebook.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }
}
