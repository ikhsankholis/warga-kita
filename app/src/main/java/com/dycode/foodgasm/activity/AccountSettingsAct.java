package com.dycode.foodgasm.activity;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.dycode.foodgasm.FoodGasmApp;
import com.dycode.foodgasm.R;
import com.dycode.foodgasm.dao.DaoSession;
import com.dycode.foodgasm.listener.UnregisterNotificationListener;
import com.dycode.foodgasm.model.ApiResponse;
import com.dycode.foodgasm.model.PostRegister;
import com.dycode.foodgasm.model.Profile;
import com.dycode.foodgasm.model.ReqPhoneVerify;
import com.dycode.foodgasm.utils.NetworkErrorUtil;
import com.dycode.foodgasm.utils.Utils;
import com.facebook.login.LoginManager;
import com.hbb20.CountryCodePicker;
import com.soundcloud.android.crop.Crop;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

/**
 * Created by asus pc on 19/10/2015.
 */
public class AccountSettingsAct extends BaseActivity implements View.OnClickListener {

    private static final String TAG = AccountSettingsAct.class.getSimpleName();
    public static int MEDIA_TYPE_IMAGE = 1;
    public static String IMAGE_DIRECTORY_NAME = "FoodGasm";
    private final int REQUEST_CAMERA = 0;
    private final int SELECT_FILE = 1;
    private final int PIC_CROP = 2;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.tvMenu)
    TextView tvMenu;
    @BindView(R.id.tvRealName)
    TextView tvRealName;
    @BindView(R.id.inpFullName)
    EditText inpFullName;
    @BindView(R.id.inpEmail)
    EditText inpEmail;
    @BindView(R.id.inpPhoneNumber)
    EditText inpPhoneNumber;
    @BindView(R.id.linContainer)
    LinearLayout linContainer;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.ivProfilePict)
    CircleImageView ivProfilePict;

    @BindView(R.id.ccp)
    CountryCodePicker ccp;
    @BindView(R.id.tvVerified)
    TextView tvVerified;

    @BindView(R.id.btnChangePassword)
    Button btnChangePassword;

    Uri selectedImageUri;
    Bitmap imgFinale;
    TypedFile profilePicture;
    String fullName, email, phone;
    private String phoneNumber;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_settings);
        ButterKnife.bind(this);

        // Obtain the shared Tracker instance.
        getTracker("Account Settings");

        mToolbar.setNavigationIcon(R.mipmap.ic_back);
        setSupportActionBar(mToolbar);
        tvTitle.setText(R.string.account_settings);
        tvMenu.setVisibility(View.VISIBLE);
        tvMenu.setText(R.string.save);
        tvMenu.setOnClickListener(this);
        ivProfilePict.setOnClickListener(this);
        btnChangePassword.setOnClickListener(this);

        getProfileData();
    }

    public void setProfileData() {
//        if (tvVerified.getText().toString().equals("Verify")) {
//            reqPhoneVerifySetData();
//        } else {

        fullName = inpFullName.getText().toString();
        email = inpEmail.getText().toString();
        phone = inpPhoneNumber.getText().toString();


        if (TextUtils.isEmpty(fullName) && TextUtils.isEmpty(email) && TextUtils.isEmpty(phone)) {
            showMaterialDialogAlert(R.string.alert, R.string.please_fill);
        } else if (TextUtils.isEmpty(fullName)) {
            showMaterialDialogAlert(R.string.alert, R.string.empty_full_name);
        } else if (TextUtils.isEmpty(email)) {
            showMaterialDialogAlert(R.string.alert, R.string.empty_email);
        } else if (!isValidEmail(email)) {
            showMaterialDialogAlert(R.string.alert, R.string.invalid_email);
        } else if (TextUtils.isEmpty(phone)) {
            showMaterialDialogAlert(R.string.alert, R.string.empty_phone);
        } else putProfileData();
    }

//    }

    void getProfileData() {
        showProgressDialog("Loading data...");
        restClient.getApiService().getProfileData(Utils.getToken(mActivity).getAccessToken(), new Callback<ApiResponse<Profile>>() {
            @Override
            public void success(ApiResponse<Profile> profileApiResponse, Response response) {
                progressDialog.dismiss();
                progressBar.setVisibility(View.GONE);
                linContainer.setVisibility(View.VISIBLE);
                final Profile profile = profileApiResponse.getData();
                if (!TextUtils.isEmpty(profile.getProfilePicture()))
                    Picasso.with(mActivity).load(profile.getProfilePicture()).into(ivProfilePict);

                if (profile.getProvider() != null) {
                    if (profile.getProvider().equals("facebook"))
                        btnChangePassword.setVisibility(View.GONE);
                }


                tvRealName.setText(profile.getFullname());
                inpFullName.setText(profile.getFullname());
                inpEmail.setText(profile.getEmail());

                phoneNumber = TextUtils.isEmpty(profile.getPhone()) ? "" : profile.getPhone();

                if (phoneNumber != null) {
                    if (phoneNumber.startsWith("62")) {
                        phoneNumber = phoneNumber.replaceFirst("62", "");
                    } else if (phoneNumber.startsWith("+62")) {
                        phoneNumber = phoneNumber.replace("+62", "");
                    } else if (phoneNumber.startsWith("0")) {
                        phoneNumber = phoneNumber.replaceFirst("0", "");
                    }
                }


                inpPhoneNumber.setText(phoneNumber);


                if (profile.getUserPhoneVerified().equals("false")) {
                    Log.d("Phone Status >>", profile.getUserPhoneVerified());

                    tvVerified.setText("Verify");
                    tvVerified.setBackgroundResource(R.drawable.bg_rounded_textview_verify);
                } else {
                    tvVerified.setText("Verified");
                    tvVerified.setBackgroundResource(R.drawable.bg_rounded_textview_verify_gray);
                }

                inpPhoneNumber.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                        String phoneNumber = TextUtils.isEmpty(profile.getPhone()) ? "" : profile.getPhone();
                        if (phoneNumber.startsWith("62")) {
                            phoneNumber.replace("62", "");
                        } else if (phoneNumber.startsWith("+62")) {
                            phoneNumber.replace("+62", "");
                        } else if (phoneNumber.startsWith("0")) {
                            phoneNumber.replace("0", "");
                        }
                        Log.d("editable", charSequence.toString() + "compare > " + phoneNumber);

                        if (!charSequence.toString().equals(phoneNumber)) {
                            Log.d("tes", "sample");
                            tvVerified.setText("Verify");
                            tvVerified.setBackgroundResource(R.drawable.bg_rounded_textview_verify);
                        } else {
                            Log.d("tes 1", "sample 1");
                            tvVerified.setText("Verified");
                            tvVerified.setBackgroundResource(R.drawable.bg_rounded_textview_verify_gray);
                        }
                    }

                    @Override
                    public void afterTextChanged(Editable editable) {

                    }
                });
            }

            @Override
            public void failure(RetrofitError error) {
                progressDialog.dismiss();
                progressBar.setVisibility(View.GONE);
                linContainer.setVisibility(View.VISIBLE);
//                showAlertDialog("Failed", error.getMessage());
                //showToast("No Internet Connection");
//                Log.d("error > ", String.valueOf(error.getMessage()));
            }
        });
    }

    void putProfileData() {
        showProgressDialog("Saving...");
        restClient.getApiService().putProfileData(Utils.getToken(mActivity).getAccessToken(), new PostRegister(inpFullName.getText().toString(), null, inpEmail.getText().toString(), inpPhoneNumber.getText().toString(), null, null, null, null, null, null, false), new Callback<ApiResponse<Profile>>() {

            @Override
            public void success(ApiResponse<Profile> profileApiResponse, Response response) {
                if (profilePicture == null) {
                    progressDialog.dismiss();
                    new MaterialDialog.Builder(mActivity)
                            .content(R.string.data_saved)
                            .positiveText(R.string.ok)
                            .cancelable(true)
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(MaterialDialog dialog, DialogAction which) {
                                    getProfileData();
                                }
                            })
                            .show();
                } else
                    uploadPhoto();
            }

            @Override
            public void failure(RetrofitError error) {
                progressDialog.dismiss();
                showAlertDialog("Error", NetworkErrorUtil.getErrorMessage(error));
//                showToast("No Internet Connection");

            }
        });
    }

    void uploadPhoto() {
        restClientForUploadFile.getApiService().uploadPhoto(Utils.getToken(mActivity).getAccessToken(), profilePicture, new Callback<ApiResponse<Profile>>() {
            @Override
            public void success(ApiResponse<Profile> profileApiResponse, Response response) {
                progressDialog.dismiss();
                new MaterialDialog.Builder(mActivity)
                        .content(R.string.data_saved)
                        .positiveText(R.string.ok)
                        .autoDismiss(true)
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(MaterialDialog dialog, DialogAction which) {
                                getProfileData();
                            }
                        })
                        .show();
            }

            @Override
            public void failure(RetrofitError error) {
                progressDialog.dismiss();
                showAlertDialog("Failed", NetworkErrorUtil.getErrorMessage(error));
//                showToast("No Internet Connection");

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            setResult(1);
            finish();
            overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        if (v == tvMenu) {
            setProfileData();
        } else if (v == ivProfilePict) {
            Dialog dialog = new AlertDialog.Builder(this)
                    .setTitle(R.string.add_photo)
                    .setItems(new String[]{getResources().getString(R.string.take_photo), getResources().getString(R.string.choose_frm_gallery)},
                            new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog,
                                                    int which) {
                                    switch (which) {
                                        case 0:
                                            takeFromCamera();
                                            break;
                                        case 1:
                                            pickFromGallery();
                                            break;
                                        default:
                                            break;
                                    }

                                }
                            }).create();
            dialog.show();
        } else if (v == btnChangePassword) {
            Intent i = new Intent(AccountSettingsAct.this, ChangePasswordAct.class);
            startActivity(i);
            overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
        }
    }

    @OnClick(R.id.tvVerified)
    void submitVerified(View view) {
        if (tvVerified.getText().toString().equals("Verify")) {
            String newPhoneNumber = "";
            if (inpPhoneNumber.getText().toString().startsWith("62")) {
                newPhoneNumber = inpPhoneNumber.getText().toString().replaceFirst("62", "");
            } else if (inpPhoneNumber.getText().toString().startsWith("+62")) {
                newPhoneNumber = inpPhoneNumber.getText().toString().replace("+62", "");
            } else if (inpPhoneNumber.getText().toString().startsWith("0")) {
                newPhoneNumber = inpPhoneNumber.getText().toString().replaceFirst("0", "");
            } else {
                newPhoneNumber = inpPhoneNumber.getText().toString();
            }

            if (!phoneNumber.equals(newPhoneNumber)) {
                Log.d("not", "equals");
                reqPhoneVerify(inpPhoneNumber.getText().toString());
            } else {
                Log.d("equals > ", "equals");
                reqPhoneVerify();
            }

        }
    }

    private void reqPhoneVerify() {
        progressDialog.show();
        Log.d("phone number ", phoneNumber);

        restClient.getApiService().getPhoneVerify(Utils.getToken(mActivity).getAccessToken(), ccp.getSelectedCountryCode() + phoneNumber, new Callback<ApiResponse<ReqPhoneVerify>>() {
            @Override
            public void success(final ApiResponse<ReqPhoneVerify> reqPhoneVerifyApiResponse, Response response) {
                progressDialog.dismiss();

                Log.d("code Verify", reqPhoneVerifyApiResponse.getData().getCode());
                Log.d("token Verify", reqPhoneVerifyApiResponse.getData().getToken_id());

                AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
                final AlertDialog dialog = builder.create();
                LayoutInflater inflater = mActivity.getLayoutInflater();
                View dialogLayout = inflater.inflate(R.layout.dialog_verification_phone, null);

                dialog.setView(dialogLayout);
                dialog.setCancelable(false);
                dialog.show();

                final EditText txt_pin_number = (EditText) dialog.findViewById(R.id.txt_pin_number);
                final TextView txt_conf_yes = (TextView) dialog.findViewById(R.id.txt_conf_yes);
                final TextView txt_conf_no = (TextView) dialog.findViewById(R.id.txt_conf_no);
                final TextView txtResendOtp = (TextView) dialog.findViewById(R.id.txtResendOtp);

                txt_conf_yes.setOnClickListener(view -> {
                    if (txt_pin_number.length() > 3) {
                        dialog.dismiss();
                        ReqPhoneVerify reqPhoneVerify = new ReqPhoneVerify(txt_pin_number.getText().toString(), reqPhoneVerifyApiResponse.getData().getToken_id());
                        reqPhoneNumber(reqPhoneVerify);
                    }
                });

                txt_conf_no.setOnClickListener(view -> dialog.dismiss());

                txtResendOtp.setOnClickListener(view -> {
                    dialog.dismiss();
                    reqPhoneVerify(txt_pin_number.getText().toString());
                });

            }

            @Override
            public void failure(RetrofitError error) {
                progressDialog.dismiss();
                String message = NetworkErrorUtil.getErrorMessage(error);
                showAlertDialog("Error", message);
            }
        });
    }

    private void reqPhoneVerify(String phoneNumb) {
        progressDialog.show();

        Log.d("phone number ", phoneNumb);
        if (phoneNumb.startsWith("62")) {
            phoneNumber = phoneNumb.replaceFirst("62", "");
        } else if (phoneNumb.startsWith("+62")) {
            phoneNumber = phoneNumb.replace("+62", "");
        } else if (phoneNumb.startsWith("0")) {
            phoneNumber = phoneNumb.replaceFirst("0", "");
        } else {
            phoneNumber = phoneNumb;
            Log.d("cek phone ", phoneNumb);
        }

        restClient.getApiService().getPhoneVerify(Utils.getToken(mActivity).getAccessToken(), ccp.getSelectedCountryCode() + phoneNumber, new Callback<ApiResponse<ReqPhoneVerify>>() {
            @Override
            public void success(final ApiResponse<ReqPhoneVerify> reqPhoneVerifyApiResponse, Response response) {
                progressDialog.dismiss();

                Log.d("code Verify", reqPhoneVerifyApiResponse.getData().getCode());
                Log.d("token Verify", reqPhoneVerifyApiResponse.getData().getToken_id());

                AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
                final AlertDialog dialog = builder.create();
                LayoutInflater inflater = mActivity.getLayoutInflater();
                View dialogLayout = inflater.inflate(R.layout.dialog_verification_phone, null);

                dialog.setView(dialogLayout);
                dialog.setCancelable(false);
                dialog.show();

                final EditText txt_pin_number = (EditText) dialog.findViewById(R.id.txt_pin_number);
                final TextView txt_conf_yes = (TextView) dialog.findViewById(R.id.txt_conf_yes);
                final TextView txt_conf_no = (TextView) dialog.findViewById(R.id.txt_conf_no);
                final TextView txtResendOtp = (TextView) dialog.findViewById(R.id.txtResendOtp);

                txt_conf_yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (txt_pin_number.length() > 3) {
                            dialog.dismiss();
                            ReqPhoneVerify reqPhoneVerify = new ReqPhoneVerify(txt_pin_number.getText().toString(), reqPhoneVerifyApiResponse.getData().getToken_id());
                            reqPhoneNumber(reqPhoneVerify);
                        }
                    }
                });

                txt_conf_no.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });

                txtResendOtp.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                        reqPhoneVerify();
                    }
                });

            }

            @Override
            public void failure(RetrofitError error) {
                progressDialog.dismiss();

                String message = NetworkErrorUtil.getErrorMessage(error);
                showAlertDialog("Error", message);
            }
        });
    }

    private void reqPhoneVerifySetData() {
        progressDialog.show();
        Log.d("phone number ", inpPhoneNumber.getText().toString());
        String phone = inpPhoneNumber.getText().toString();
        if (phone.startsWith("62")) {
            phone.replace("62", "");
            phone = phone.replace("62", "");
        } else if (phone.startsWith("+62")) {
            phone.replace("+62", "");
            phone = phone.replace("+62", "");
        } else if (phone.startsWith("0")) {
            phone.replace("0", "");
            phone = phone.replace("0", "");
        }


        restClient.getApiService().getPhoneVerify(Utils.getToken(mActivity).getAccessToken(), ccp.getSelectedCountryCode() + phone, new Callback<ApiResponse<ReqPhoneVerify>>() {
            @Override
            public void success(final ApiResponse<ReqPhoneVerify> reqPhoneVerifyApiResponse, Response response) {
                progressDialog.dismiss();

                Log.d("code Verify", reqPhoneVerifyApiResponse.getData().getCode());
                Log.d("token Verify", reqPhoneVerifyApiResponse.getData().getToken_id());

                AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
                final AlertDialog dialog = builder.create();
                LayoutInflater inflater = mActivity.getLayoutInflater();
                View dialogLayout = inflater.inflate(R.layout.dialog_verification_phone, null);

                dialog.setView(dialogLayout);
                dialog.setCancelable(false);
                dialog.show();

                final EditText txt_pin_number = (EditText) dialog.findViewById(R.id.txt_pin_number);
                final TextView txt_conf_yes = (TextView) dialog.findViewById(R.id.txt_conf_yes);
                final TextView txt_conf_no = (TextView) dialog.findViewById(R.id.txt_conf_no);

                txt_conf_yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (txt_pin_number.length() > 3) {
                            dialog.dismiss();
                            ReqPhoneVerify reqPhoneVerify = new ReqPhoneVerify(txt_pin_number.getText().toString(), reqPhoneVerifyApiResponse.getData().getToken_id());
                            reqPhoneNumber(reqPhoneVerify);
                        }
                    }
                });

                txt_conf_no.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });


            }

            @Override
            public void failure(RetrofitError error) {
                progressDialog.dismiss();
                showAlertDialog("Failed", NetworkErrorUtil.getErrorMessage(error));
            }
        });
    }

    private void reqPhoneNumber(ReqPhoneVerify reqPhoneVerify) {
        progressDialog.show();
        restClient.getApiService().postPhoneVerify(Utils.getToken(mActivity).getAccessToken(), reqPhoneVerify, new Callback<ApiResponse>() {
            @Override
            public void success(ApiResponse data, Response response) {
                progressDialog.dismiss();

                String message = data.getMeta().getMessage();
                Log.d("message > ", message);
                String code = String.valueOf(data.getMeta().getCode());

                if (message.equals("Verification Completed")) {
                    new MaterialDialog.Builder(mActivity)
                            .content(message)
                            .positiveText(R.string.ok)
                            .cancelable(false)
                            .autoDismiss(false)
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(MaterialDialog dialog, DialogAction which) {
                                    dialog.dismiss();
                                    setProfileData();
//                                getProfileData();
                                }
                            })
                            .show();
                } else {
                    new MaterialDialog.Builder(mActivity)
                            .content(message)
                            .positiveText(R.string.ok)
                            .cancelable(true)
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(MaterialDialog dialog, DialogAction which) {
                                    dialog.dismiss();
                                }
                            })
                            .show();
                }


            }

            @Override
            public void failure(RetrofitError error) {
                progressDialog.dismiss();

                showAlertDialog("Error", NetworkErrorUtil.getErrorMessage(error));
            }
        });
    }


    private void pickFromGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        startActivityForResult(Intent.createChooser(intent, "Choose a Picture"), SELECT_FILE);
    }

    private void takeFromCamera() {

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
        } else {
            cameraIntent();
        }

    }

    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        selectedImageUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, selectedImageUri);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    private File getOutputMediaFile(int type) {

        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            //RUNTIME PERMISSION Android M
            if (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), IMAGE_DIRECTORY_NAME);

                if (!mediaStorageDir.exists()) {
                    if (!mediaStorageDir.mkdirs()) {
                        Log.d(IMAGE_DIRECTORY_NAME, "Oops, failed create " + IMAGE_DIRECTORY_NAME + " directory");
                        return null;
                    }
                }
                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());

                File mediaFile;
                if (type == MEDIA_TYPE_IMAGE) {
                    mediaFile = new File(mediaStorageDir.getPath() + File.separator + "IMG_" + timeStamp + ".jpg");
                } else {
                    return null;
                }

                return mediaFile;

            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
            }

        } else {
            showToast("You don't have external memory storage for save image");
        }

        return null;
    }

    @SuppressWarnings("static-access")
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_CAMERA) {
                previewCapturedImage();

            } else if (requestCode == SELECT_FILE) {
                try {
                    selectedImageUri = data.getData();
                    performCrop();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (requestCode == Crop.REQUEST_CROP) {
                handleCrop(resultCode, data);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 0) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                cameraIntent();
            }
        }
    }

    private void previewCapturedImage() {
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 8;

            performCrop();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    private void performCrop() {
        try {
            Uri destination = Uri.fromFile(new File(getCacheDir(), "cropped"));
            Crop.of(selectedImageUri, destination).asSquare().start(this);
        } catch (ActivityNotFoundException anfe) {
            String errorMessage = "Whoops - your device doesn't support the crop action!";
            Toast toast = Toast.makeText(mActivity, errorMessage, Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    public Uri getImageUri(Activity inActivity, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inActivity.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    private void handleCrop(int resultCode, Intent result) {
        if (resultCode == RESULT_OK) {
            Uri imageURI = Crop.getOutput(result);

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageURI);
                ivProfilePict.setImageBitmap(bitmap);
                profilePicture = new TypedFile("multipart/form-data", new File(getRealPathFromURI(getImageUri(this, bitmap))));
            } catch (Exception e) {
                showToast(e.toString());
            }

        } else if (resultCode == Crop.RESULT_ERROR) {
            showToast(Crop.getError(result).getMessage());
        }
    }
}
