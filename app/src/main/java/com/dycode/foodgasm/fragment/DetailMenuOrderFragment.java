package com.dycode.foodgasm.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import com.dycode.foodgasm.FoodGasmApp;
import com.dycode.foodgasm.R;
import com.dycode.foodgasm.adapters.DetailMenuAdapter;
import com.dycode.foodgasm.dao.DaoSession;
import com.dycode.foodgasm.dao.MenuDao;
import com.dycode.foodgasm.model.ApiResponseList;
import com.dycode.foodgasm.model.Category;
import com.dycode.foodgasm.model.Restaurant;
import com.dycode.foodgasm.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by asus pc on 19/10/2015.
 */
public class DetailMenuOrderFragment extends BaseFragment implements DetailMenuAdapter.MenuCountListener, DetailMenuAdapter.MenuScrollListener {

    @BindView(R.id.listMenu)
    ListView listMenu;

    DetailMenuAdapter mAdapter;
    List<com.dycode.foodgasm.dao.Menu> menuList = new ArrayList<>();

    DetailMenuAdapter.MenuCountListener mListener;
    DetailMenuAdapter.MenuScrollListener menuScrollListener;
    String restaurant, categoryName, categoryID;
    int flag;
    static int update;

    FoodGasmApp foodGasmApp;
    DaoSession daoSession;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mListener = (DetailMenuAdapter.MenuCountListener) context;
        menuScrollListener = (DetailMenuAdapter.MenuScrollListener) context;
    }

    public static DetailMenuOrderFragment newInstance(String restaurant, String categoryID, String categoryName, int flag) {

        Bundle b = new Bundle();
        b.putString(Restaurant.RESTAURANT, restaurant);
        b.putString(Category.CATEGORY_ID, categoryID);
        b.putString(Category.CATEGORY, categoryName);
        b.putInt("flag", flag);
        update = flag;

        DetailMenuOrderFragment frag = new DetailMenuOrderFragment();
        frag.setArguments(b);

        return frag;
    }

    public static DetailMenuOrderFragment newInstance(String restaurant, String categoryID, String categoryName, ArrayList<com.dycode.foodgasm.dao.Menu> menu, int flag) {

        Bundle b = new Bundle();
        b.putString(Restaurant.RESTAURANT, restaurant);
        b.putString(Category.CATEGORY_ID, categoryID);
        b.putString(Category.CATEGORY, categoryName);
        b.putParcelableArrayList(com.dycode.foodgasm.dao.Menu.MENU, menu);
        b.putInt("flag", flag);

        update = flag;
        DetailMenuOrderFragment frag = new DetailMenuOrderFragment();
        frag.setArguments(b);

        return frag;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        foodGasmApp = (FoodGasmApp) getActivity().getApplication();
        daoSession = foodGasmApp.getDaoSession();
        restaurant = getArguments().getString(Restaurant.RESTAURANT);
        categoryID = getArguments().getString(Category.CATEGORY_ID);
        categoryName = getArguments().getString(Category.CATEGORY);
        flag = getArguments().getInt("flag");
        menuList = daoSession.getMenuDao().queryBuilder().where(MenuDao.Properties.CategoryId.eq(categoryID), MenuDao.Properties.RestoId.eq(restaurant)).list();
        mAdapter = new DetailMenuAdapter(this, menuList);
        listMenu.setAdapter(mAdapter);

        if (flag == 0) getMenus(false);
        else getMenus(true);
    }



    public void getMenus(boolean isAddOrder) {
        menuList.clear();
        List<com.dycode.foodgasm.dao.Menu> menuDaoList = daoSession.getMenuDao().queryBuilder().where(MenuDao.Properties.CategoryId.eq(categoryID), MenuDao.Properties.RestoId.eq(restaurant)).list();
        if(menuDaoList.size() > 0){
            for (int menuPos = 0; menuPos < menuDaoList.size(); menuPos++) {
                if (!isAddOrder && update !=1) {
                    menuDaoList.get(menuPos).setAmount(0);
                    daoSession.getMenuDao().insertOrReplace(menuDaoList.get(menuPos));
                }else if(update == 1){
                    menuDaoList.get(menuPos).setAmount(menuDaoList.get(menuPos).getAmount());
                    daoSession.getMenuDao().insertOrReplace(menuDaoList.get(menuPos));
                }
            }
        }

        menuList.addAll(daoSession.getMenuDao().queryBuilder().where(MenuDao.Properties.CategoryId.eq(categoryID), MenuDao.Properties.RestoId.eq(restaurant)).list());

        if (menuList.size() > 0) {
            listMenu.setVisibility(View.VISIBLE);
            mAdapter.notifyDataSetChanged();
        } else {
            listMenu.setVisibility(View.VISIBLE);
        }
        mAdapter.notifyDataSetChanged();
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_detail_menu;
    }


    @Override
    public void onChangeCount(com.dycode.foodgasm.dao.Menu menu, int posMenu, int newCount) {
        menuList.get(posMenu).setAmount(newCount);
        mListener.onChangeCount(menu, posMenu, newCount);
    }

    @Override
    public void onClickedMenuAmount(com.dycode.foodgasm.dao.Menu menu, int posMenu) {
        menuScrollListener.onClickedMenuAmount(menu, posMenu);
    }

    public void synchronizeScroll(final int posMenu, boolean isMenuClicked) {
        if (posMenu == menuList.size() - 1) {
            if (isMenuClicked) {
                final int offset = 1;
                listMenu.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        listMenu.smoothScrollToPositionFromTop(posMenu, offset, 500);
                    }
                }, 200);
            } else {
                int offset = 0;
                mAdapter.notifyDataSetChanged();
                listMenu.smoothScrollToPositionFromTop(posMenu, offset, 500);
            }

        }
    }

}
