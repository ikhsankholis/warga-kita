package com.dycode.foodgasm.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.dycode.foodgasm.FoodGasmApp;
import com.dycode.foodgasm.R;
import com.dycode.foodgasm.activity.AboutAct;
import com.dycode.foodgasm.activity.AccountSettingsAct;
import com.dycode.foodgasm.activity.CreditCardAct;
import com.dycode.foodgasm.activity.HighLightAct;
import com.dycode.foodgasm.dao.DaoSession;
import com.dycode.foodgasm.feature.historyorder.HistoryOrderActivity;
import com.dycode.foodgasm.listener.UnregisterNotificationListener;
import com.dycode.foodgasm.model.ApiResponse;
import com.dycode.foodgasm.model.Profile;
import com.dycode.foodgasm.utils.Utils;
import com.facebook.login.LoginManager;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by asus pc on 19/10/2015.
 */
public class ProfileFragment extends BaseFragment {
    @BindView(R.id.l_container)
    LinearLayout linContainer;
    @BindView(R.id.relAccountSetting)
    RelativeLayout relSettings;
    @BindView(R.id.relAbout)
    RelativeLayout relAbout;
    @BindView(R.id.relPayment)
    RelativeLayout relPayment;
    @BindView(R.id.relHistory)
    RelativeLayout relHistory;
    @BindView(R.id.tvRealName)
    TextView tvRealName;
    @BindView(R.id.tvEmail)
    TextView tvEmail;
    @BindView(R.id.tvPhoneNumber)
    TextView tvPhoneNumber;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tvTitle)
    TextView tvTitle;


    private Fragment mCurrentFragment;
    private FragmentManager fragmentManager;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        toolbar.setBackgroundResource(R.color.dark_toolbar);
        tvTitle.setTextColor(ContextCompat.getColor(mContext, android.R.color.white));
        tvTitle.setText(R.string.my_account);

        relSettings.setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), AccountSettingsAct.class);
            startActivityForResult(intent, 0);
            getActivity().overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
        });

        relAbout.setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), AboutAct.class);
            startActivity(intent);
            getActivity().overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
        });

        relPayment.setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), CreditCardAct.class);
            intent.putExtra(CreditCardAct.FLAG, CreditCardAct.FLAG_EDIT_ENABLE);
            startActivity(intent);
            getActivity().overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
        });

        relHistory.setOnClickListener(v -> HistoryOrderActivity.Companion.start(mContext));


        getProfileData();
        fragmentManager = getFragmentManager();
    }


    @Override
    protected int getTitle() {
        return R.string.profile;
    }

    void getProfileData() {
        restClient.getApiService().getProfileData(Utils.getToken(mContext).getAccessToken(), new Callback<ApiResponse<Profile>>() {
            @Override
            public void success(ApiResponse<Profile> profileApiResponse, Response response) {
                if(getActivity() == null){
                    return;
                }

                final Profile profile = profileApiResponse.getData();
                tvRealName.setText(profile.getFullname());
                tvEmail.setText(profile.getEmail());
                String phone = profile.getPhone();
                if (!TextUtils.isEmpty(phone)) {
                    if (phone.startsWith("62")) {
                        phone = "+" + phone;
                    } else if (phone.startsWith("0")) {
                        phone = phone.replaceFirst("0", "+62");
                    } else if (phone.startsWith("8")) {
                        phone = "+62" + phone;
                    }
                }
                tvPhoneNumber.setText(phone);

                if (profile.getUserPhoneVerified().equals("false")) {
                    new MaterialDialog.Builder(getActivity())
                            .title("Your mobile number has not been verified")
                            .content("Please verify your mobile number")
                            .positiveText(R.string.ok)
                            .negativeText(R.string.cancel)
                            .cancelable(true)
                            .onPositive((dialog, which) -> {
                                if (dialog.isShowing() && isResumed()) {
                                    dialog.dismiss();
                                }
                                Intent intent = new Intent(getActivity(), AccountSettingsAct.class);
                                startActivityForResult(intent, 0);
                                getActivity().overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                            })
                            .onNegative((dialog, which) -> dialog.dismiss())
                            .show();
                }
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_profile;
    }


    @OnClick(R.id.textLogout)
    void logout() {
        showProgressDialog("Logging Out...");
        FoodGasmApp foodGasmApp = (FoodGasmApp) getActivity().getApplication();
        foodGasmApp.unregisterNotificationHub(new UnregisterNotificationListener() {
            @Override
            public void onSuccess() {
                LoginManager.getInstance().logOut();
                progressDialog.dismiss();
                Utils.logoutApps(mContext);

                //Clear database
                DaoSession daoSession = ((FoodGasmApp) getActivity().getApplication()).getDaoSession();
                daoSession.getCategoryDao().deleteAll();
                daoSession.getCommentDao().deleteAll();
                daoSession.getDealDao().deleteAll();
                daoSession.getGalleryDao().deleteAll();
                daoSession.getProfileDao().deleteAll();
                daoSession.getOrderHistoryDao().deleteAll();
                daoSession.getOrderMenuDao().deleteAll();
                daoSession.getRestaurantDao().deleteAll();
                daoSession.getMenuDao().deleteAll();
                daoSession.getCreditCardDao().deleteAll();
                daoSession.getRestoAdressDao().deleteAll();
                daoSession.getMenuVersionDao().deleteAll();

                Intent i = new Intent(getActivity(), HighLightAct.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                getActivity().overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
            }

            @Override
            public void onFailed() {
                progressDialog.dismiss();
                showAlertDialog(getResources().getString(R.string.alert_logout_failed), getResources().getString(R.string.alert_no_internet_connection));

            }

        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0 && resultCode == 1) {
            getProfileData();
        } else {
        }
    }


}
