package com.dycode.foodgasm.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dycode.foodgasm.R;
import com.dycode.foodgasm.activity.BookOrderAct;
import com.dycode.foodgasm.activity.BookOrderCompleteAct;
import com.dycode.foodgasm.dao.Menu;
import com.dycode.foodgasm.model.ApiResponse;
import com.dycode.foodgasm.model.Order;
import com.dycode.foodgasm.model.PostOrder;
import com.dycode.foodgasm.model.Price;
import com.dycode.foodgasm.model.Restaurant;
import com.dycode.foodgasm.utils.Utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import butterknife.BindView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by asus pc on 16/10/2015.
 */
public class ReviewOrderType1Fragment extends BaseFragment implements View.OnClickListener {

    @BindView(R.id.linConfirm)
    LinearLayout linConfirm;
    @BindView(R.id.tvRestaurantName)
    TextView tvRestaurantName;
    @BindView(R.id.tvAddress)
    TextView tvAddress;
    @BindView(R.id.tvInfo)
    TextView tvInfo;
    @BindView(R.id.tvTime)
    TextView tvTime;
    @BindView(R.id.linEdit)
    LinearLayout linEdit;

    int id_value;
    Restaurant restaurant;
    String personAmount, day, minute, bookingCode, finalDateIso, dateIso = "";
    Date dateIsoFormatted;
    private String commission;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Obtain the shared Tracker instance.
        getTracker("Booking Order");

        restaurant = getActivity().getIntent().getExtras().getParcelable(Restaurant.RESTAURANT);
        personAmount = getArguments().getString("personAmount");
        day = getArguments().getString("day");
        minute = getArguments().getString("minute");
        id_value = getActivity().getIntent().getExtras().getInt("id");
        commission = getArguments().getString("commission");

        tvRestaurantName.setText(restaurant.getName());
        String fullAddress = String.format(getString(R.string.resto_address), restaurant.getAddress().getStreet(), restaurant.getAddress().getDistrict(), restaurant.getAddress().getCity(), restaurant.getAddress().getProvince(), restaurant.getAddress().getPostalCode());
        tvAddress.setText(fullAddress);
        final String infoOrderTime = String.format(getString(R.string.info_order_datetime), day, minute);
        String infoOrderDetail = String.format(getString(R.string.info_order_detail), personAmount);
        tvInfo.setText(infoOrderDetail);
        tvTime.setText(infoOrderTime);

        String dateString = day + " " + minute;
        SimpleDateFormat dateFormat = new SimpleDateFormat("MMM d, yyyy hh:mm", Locale.getDefault());
        SimpleDateFormat dateFormatIso = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault());
        dateFormatIso.setTimeZone(TimeZone.getTimeZone("UTC+7"));

        Date convertedDate;
        try {
            convertedDate = dateFormat.parse(dateString);
            dateIso = dateFormatIso.format(convertedDate);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        try {
            dateIsoFormatted = dateFormatIso.parse(dateIso);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        linConfirm.setOnClickListener(this);
        linEdit.setOnClickListener(this);

    }

    public static ReviewOrderType1Fragment newInstance(String personAmount, String day, String minute,String commission) {

        Bundle b = new Bundle();
        b.putString("personAmount", personAmount);
        b.putString("day", day);
        b.putString("minute", minute);
        b.putString("commission", commission);

        ReviewOrderType1Fragment frag = new ReviewOrderType1Fragment();
        frag.setArguments(b);

        return frag;
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_review_order_type1;
    }

    @Override
    public void onClick(View v) {
        final int personNum = Integer.parseInt(personAmount.replaceAll("[\\D]", ""));
        finalDateIso = dateIso;
        if (v == linConfirm) {
            List<Menu> menuList = new ArrayList<>();
            Price priceList = new Price();

            restClient.getApiService().postOrder(new PostOrder(Utils.getToken(mContext).getUid(), restaurant.getId(), finalDateIso, true, personNum, menuList, "", priceList, ""), Utils.getToken(mContext).getAccessToken(), "creditcard", new Callback<ApiResponse<Order>>() {
                @Override
                public void success(ApiResponse<Order> orderApiResponse, Response response) {
                    Order order = orderApiResponse.getData();
                    bookingCode = order.getCode();
                    Intent intent = new Intent(mContext, BookOrderCompleteAct.class);
                    intent.putExtra(BookOrderCompleteAct.FLAG, BookOrderCompleteAct.FLAG_WAIT_CONFIRMATION);
                    intent.putExtra("orderId", order.getCode());
                    intent.putExtra("restoName", order.getResto().getName());
                    intent.putExtra("bookingCode", order.getCode());
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    getActivity().overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                }

                @Override
                public void failure(RetrofitError error) {
                    showAlertDialog("Add Order Failed", error.getMessage());

                }
            });
        } else if (v == linEdit) {
            Intent intent = new Intent(mContext, BookOrderAct.class);
            intent.putExtra("id", id_value);
            intent.putExtra(Restaurant.RESTAURANT, restaurant);
            intent.putExtra(BookOrderAct.MENU_TYPE, BookOrderAct.FLAG_REVIEW_EDIT);
            intent.putExtra("personAmount", personAmount);
            intent.putExtra("day", day);
            intent.putExtra("minute", minute);
            intent.putExtra("commission", commission);
            startActivity(intent);
            getActivity().overridePendingTransition(android.R.anim.fade_in, R.anim.abc_fade_out);
        }
    }
}
