package com.dycode.foodgasm.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.dycode.foodgasm.FoodGasmApp;
import com.dycode.foodgasm.R;
import com.dycode.foodgasm.inject.ApplicationComponent;
import com.dycode.foodgasm.rest.RestClient;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import org.greenrobot.eventbus.EventBus;

import butterknife.ButterKnife;

/**
 * Created by Okta Dwi Priatna on 02/10/2015.
 */
public abstract class BaseFragment extends Fragment {
    Toolbar mToolbar;
    protected Context mContext;
    ProgressBar pbar;

    protected RestClient restClient = RestClient.getInstance();
    protected MaterialDialog progressDialog;
    MaterialDialog alertDialog;

    Tracker mTracker;
    protected EventBus bus = EventBus.getDefault();

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(getLayout(), container, false);
        ButterKnife.bind(this, view);

//        Optional pBar
//        try {
//            pbar = ButterKnife.findById(view, R.id.pBar);
//        }catch (Exception e){
//
//        }

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setToolbar(view);

    }

    protected void setToolbar(View view) {
        if (!hasCustomToolbar()) return;

        mToolbar = ButterKnife.findById(view, getToolbarId());
        mToolbar.setTitle(getTitle());

    }

    protected
    @IdRes
    int getToolbarId() {
        return R.id.toolbar;
    }

    public boolean hasCustomToolbar() {
        return false;
    }

    protected abstract
    @LayoutRes
    int getLayout();

    protected
    @StringRes
    int getTitle() {
        return R.string.app_name;
    }


    public interface SnackBarListener {
        void onError();

        void onError(String message);
    }

    protected void showProgressDialog(String message) {
        if(getActivity() == null || getActivity().isDestroyed()){
            return;
        }
        progressDialog = new MaterialDialog.Builder(mContext)
                .content(message)
                .cancelable(true)
                .progress(true, 0).build();
        progressDialog.show();
    }

    protected void showAlertDialog(String title, String message) {
        if(getActivity() == null || getActivity().isDestroyed()){
            return;
        }
        alertDialog = new MaterialDialog.Builder(mContext)
                .title(title)
                .content(message)
                .positiveText("OK")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        super.onPositive(dialog);
                        dialog.dismiss();
                    }
                }).build();
        alertDialog.show();
    }

    protected void showToast( String message) {
        if(getActivity() == null || getActivity().isDestroyed()){
            return;
        }
        Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
    }

    protected void getTracker(String screenName){
        FoodGasmApp application = (FoodGasmApp) getActivity().getApplication();
        mTracker = application.getTracker();
        mTracker.setScreenName(screenName);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    protected void showMaterialDialog(int content) {
        if(getActivity() == null || getActivity().isDestroyed()){
            return;
        }
        new MaterialDialog.Builder(mContext)
                .content(content)
                .positiveText(R.string.ok)
                .cancelable(true)
                .show();
    }

    public ApplicationComponent getApplicationComponent(){
        return ((FoodGasmApp) getActivity().getApplicationContext()).getComponent();
    }
}
