package com.dycode.foodgasm.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import com.dycode.foodgasm.FoodGasmApp;
import com.dycode.foodgasm.R;
import com.dycode.foodgasm.adapters.DetailMenuAdapter;
import com.dycode.foodgasm.dao.DaoSession;
import com.dycode.foodgasm.dao.Menu;
import com.dycode.foodgasm.dao.MenuDao;
import com.dycode.foodgasm.model.Category;
import com.dycode.foodgasm.model.Restaurant;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by Okta Dwi Priatna on 12/10/2015.
 */
public class DetailMenuFragment extends BaseFragment implements DetailMenuAdapter.MenuCountListener, DetailMenuAdapter.MenuScrollListener {

    @BindView(R.id.listMenu)
    ListView listMenu;

    DetailMenuAdapter mAdapter;
    List<com.dycode.foodgasm.dao.Menu> menuList = new ArrayList<com.dycode.foodgasm.dao.Menu>();

    DetailMenuAdapter.MenuCountListener mListener;
    DetailMenuAdapter.MenuScrollListener menuScrollListener;
    String restaurant, categoryName, categoryID;
    int flag;
    static int update;

    FoodGasmApp foodGasmApp;
    DaoSession daoSession;


    public static DetailMenuFragment newInstance(String restaurant, String categoryID, String categoryName, int flag, ArrayList<com.dycode.foodgasm.dao.Menu> menuArrayList) {

        DetailMenuFragment frag = new DetailMenuFragment();

        Bundle b = new Bundle();
        b.putString(Restaurant.RESTAURANT, restaurant);
        b.putString(Category.CATEGORY_ID, categoryID);
        b.putString(Category.CATEGORY, categoryName);
        b.putParcelableArrayList("menuList", menuArrayList);

        update = flag;
        b.putString("flag", flag + "");
        frag.setArguments(b);

        return frag;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mListener = (DetailMenuAdapter.MenuCountListener) context;
        menuScrollListener = (DetailMenuAdapter.MenuScrollListener) context;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        foodGasmApp = (FoodGasmApp) getActivity().getApplication();
        daoSession = foodGasmApp.getDaoSession();

        restaurant = getArguments().getString(Restaurant.RESTAURANT);
        categoryID = getArguments().getString(Category.CATEGORY_ID);
        categoryName = getArguments().getString(Category.CATEGORY);
//        flag = getArguments().getInt("flag");

        if (getArguments().getParcelableArrayList("menuList") == null)
            menuList = daoSession.getMenuDao().queryBuilder().orderAsc(MenuDao.Properties.Name).where(MenuDao.Properties.CategoryId.eq(categoryID), MenuDao.Properties.RestoId.eq(restaurant)).list();
        else
            menuList = getArguments().getParcelableArrayList("menuList");

        mAdapter = new DetailMenuAdapter(this, menuList);
        listMenu.setAdapter(mAdapter);

        if (getArguments().getParcelableArrayList("menuList") == null) getMenus(false);
        else getMenus(true);

    }


    public void getMenus(boolean isAddOrder) {

        if (update == 0) {
            for (int menuPos = 0; menuPos < menuList.size(); menuPos++) {
                menuList.get(menuPos).setAmount(0);
            }

            listMenu.setVisibility(View.VISIBLE);
        } else {
            for (int menuPos = 0; menuPos < menuList.size(); menuPos++) {
                menuList.get(menuPos).setAmount(menuList.get(menuPos).getAmount());
            }

            listMenu.setVisibility(View.VISIBLE);
        }

        mAdapter.notifyDataSetChanged();

    }


    @Override
    protected int getLayout() {
        return R.layout.fragment_detail_menu;
    }

    @Override
    public void onChangeCount(com.dycode.foodgasm.dao.Menu menu, int posMenu, int newCount) {
        menuList.get(posMenu).setAmount(newCount);
        mListener.onChangeCount(menu, posMenu, newCount);
    }

    public void synchronizeScroll(final int posMenu, boolean isMenuClicked) {
        if (posMenu == menuList.size() - 1) {
            if (isMenuClicked) {
                final int offset = 1;
                listMenu.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        listMenu.smoothScrollToPositionFromTop(posMenu, offset, 500);
                    }
                }, 200);
            } else {
                int offset = 0;
                mAdapter.notifyDataSetChanged();
                listMenu.smoothScrollToPositionFromTop(posMenu, offset, 500);
            }

        }
    }

    @Override
    public void onClickedMenuAmount(Menu menu, int posMenu) {
        menuScrollListener.onClickedMenuAmount(menu, posMenu);
    }

}
