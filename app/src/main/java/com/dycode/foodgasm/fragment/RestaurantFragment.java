package com.dycode.foodgasm.fragment;

import android.content.Context;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.dycode.foodgasm.FoodGasmApp;
import com.dycode.foodgasm.R;
import com.dycode.foodgasm.adapters.RestaurantWithHeaderAdapter;
import com.dycode.foodgasm.constant.PrefCons;
import com.dycode.foodgasm.dao.DaoSession;
import com.dycode.foodgasm.dao.RestoAdress;
import com.dycode.foodgasm.listener.EndlessRecyclerOnScrollListener;
import com.dycode.foodgasm.model.ApiResponseList;
import com.dycode.foodgasm.model.Restaurant;
import com.dycode.foodgasm.utils.AppStatus;
import com.dycode.foodgasm.utils.NetworkErrorUtil;
import com.dycode.foodgasm.utils.Utils;
import com.lzy.widget.HeaderViewPager;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Okta Dwi Priatna on 02/10/2015.
 */
public class RestaurantFragment extends BaseFragment {

    @BindView(R.id.linContainer)
    LinearLayout linContainer;
    @BindView(R.id.srlRestaurantList)
    SwipeRefreshLayout mSwipeRefreshLayout;
    @BindView(R.id.fab)
    FloatingActionButton fab;
    @BindView(R.id.linCor)
    CoordinatorLayout linCor;

    public static LinearLayoutManager linLayoutManager;
    public static RestaurantWithHeaderAdapter mAdapter;
    public static RecyclerView rvRestaurant;
    List<Restaurant> restaurantList = new ArrayList<Restaurant>();
    int limit = 10;
    EndlessRecyclerOnScrollListener mEndlessScrollListener;

    FoodGasmApp foodGasmApp;
    DaoSession daoSession;
    private HeaderViewPager scrollableLayout;
    private ViewPager pagerHeader;
    private ArrayList<RestaurantFragment> fragments;
    private Snackbar snackbar;

    public static RestaurantFragment newInstance() {
        return new RestaurantFragment();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        foodGasmApp = (FoodGasmApp) getActivity().getApplication();
        daoSession = foodGasmApp.getDaoSession();

        fragments = new ArrayList<>();
        fragments.add(RestaurantFragment.newInstance());

        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorAccent);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshScrollListener();
            }
        });
        rvRestaurant = (RecyclerView) getActivity().findViewById(R.id.rvRestaurant);
        if (Utils.getListResto(mContext, PrefCons.PREF_HISTORY) != null) {
            restaurantList = Utils.getListResto(mContext, PrefCons.PREF_HISTORY);
            rvRestaurant.setVisibility(View.VISIBLE);
        }
        mAdapter = new RestaurantWithHeaderAdapter(mContext, restaurantList);
        linLayoutManager = new LinearLayoutManager(mContext);
        rvRestaurant.setHasFixedSize(true);
        rvRestaurant.setLayoutManager(linLayoutManager);
        rvRestaurant.setAdapter(mAdapter);
        mEndlessScrollListener = new EndlessRecyclerOnScrollListener(linLayoutManager, true) {
            @Override
            public void onLoadMore(int current_page) {
                if (restaurantList.size() >= 6) {
                    fab.setVisibility(View.VISIBLE);
                    rvRestaurant.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mAdapter.notifyDataSetChanged();
                        }
                    }, 2000);
                    mAdapter.showFooter(true);
                    getRestaurantList(false);
                }
            }
        };

        linLayoutManager.isSmoothScrollbarEnabled();
//        rvRestaurant.addOnScrollListener(mEndlessScrollListener);
        mSwipeRefreshLayout.setRefreshing(false);
        fab.setVisibility(View.GONE);


        snackbar = Snackbar.make(linCor, "Check internet Connection", Snackbar.LENGTH_INDEFINITE)
                .setActionTextColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
        snackbar.getView().setBackgroundColor(Color.parseColor("#E31F35"));

        rvRestaurant.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int data_posisi = linLayoutManager.findLastVisibleItemPosition();
                int data_posisi_last = linLayoutManager.findLastCompletelyVisibleItemPosition() + 1;
//                Log.d("current", String.valueOf(data_posisi_last) + " " + String.valueOf(mAdapter.getItemCount()));
                if (data_posisi_last == mAdapter.getItemCount()) {
//                    refreshScrollListener();
                    getRestaurantListLoadMore(true);

                }

                if (data_posisi > 5) {
                    fab.setVisibility(View.VISIBLE);
                } else {
                    fab.setVisibility(View.GONE);
                }

                if (AppStatus.getInstance(getActivity()).isOnline()) {
                    snackbar.dismiss();
                } else {
                    snackbar.show();
                }
            }
        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                linLayoutManager.smoothScrollToPosition(rvRestaurant, null, 0);
            }
        });

        if (AppStatus.getInstance(mContext).isOnline()) {
            snackbar.dismiss();
        } else {
            snackbar.show();
        }

        //getRestaurantList(true);
    }


    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private void refreshScrollListener() {
        addScrollListener(true);
        getRestaurantList(true);
    }

    private void addScrollListener(Boolean isScrollStatus) {
        if (isScrollStatus) {
            rvRestaurant.clearOnScrollListeners();
            mEndlessScrollListener.reset(0, true);
            rvRestaurant.addOnScrollListener(mEndlessScrollListener);
            mAdapter.onAttachedToRecyclerView(rvRestaurant);
        }
    }

    public void updateLocation(boolean isReplace) {
        getRestaurantList(isReplace);
    }

    void getRestaurantList(final boolean isReplaceData) {
        try {
            if (TextUtils.isEmpty(Utils.getLatString(mContext))
                    || TextUtils.isEmpty(Utils.getLngString(mContext))
                    || Utils.getLatString(mContext).equals(null)
                    || Utils.getLngString(mContext).equals(null)) {
                return;
            }
            int skip = isReplaceData ? 0 : restaurantList.size();
            Double firstdistance = isReplaceData ? 0 : restaurantList.get(0).getDistance();
            restClient.getApiService().getListRestaurantDistance(Utils.getLatString(mContext), Utils.getLngString(mContext), null, skip, limit, 3, firstdistance, new Callback<ApiResponseList<Restaurant>>() {
                @Override
                public void success(ApiResponseList<Restaurant> restaurantApiResponseList, Response response) {
                    mSwipeRefreshLayout.setRefreshing(false);

                    if (isReplaceData) {
                        restaurantList.clear();
                        if (restaurantApiResponseList.getData().size() > 0) {
                            Utils.saveListResto(mContext, restaurantApiResponseList.getData(), PrefCons.PREF_HISTORY);
                            restaurantList.addAll(Utils.getListResto(mContext, PrefCons.PREF_HISTORY));
                        }
                    } else {
                        restaurantList.addAll(restaurantApiResponseList.getData());
                        rvRestaurant.setVisibility(View.VISIBLE);
                        mAdapter.notifyDataSetChanged();
                    }

                    if (restaurantApiResponseList.getData().size() == 0 && isReplaceData) {
                        rvRestaurant.setVisibility(View.GONE);
                    }

                    int startPos = restaurantList.size() - restaurantApiResponseList.getData().size();

                    for (int pos = startPos; pos < restaurantList.size(); pos++) {
                        RestoAdress restoAdress = new RestoAdress();

                        restoAdress.set_id(restaurantList.get(pos).getId());
                        restoAdress.setStreet(restaurantList.get(pos).getAddress().getStreet());
                        restoAdress.setDistrict(restaurantList.get(pos).getAddress().getDistrict());
                        restoAdress.setCity(restaurantList.get(pos).getAddress().getCity());
                        restoAdress.setProvince(restaurantList.get(pos).getAddress().getProvince());
                        restoAdress.setPostalCode(restaurantList.get(pos).getAddress().getPostalCode());

                        daoSession.getRestoAdressDao().insertOrReplace(restoAdress);
                    }

                    mAdapter.showFooter(false);
                    mAdapter.notifyDataSetChanged();


                }


                @Override
                public void failure(RetrofitError error) {
                    mSwipeRefreshLayout.setRefreshing(false);
                    Log.d("error a > ",String.valueOf(error.getBody()));
                    if (error.getKind().equals(RetrofitError.Kind.NETWORK)) {
                        snackbar.show();
                    } else {
                        snackbar.dismiss();
                    }
                    //showToast("No Internet Connection");
                }
            });
        } catch (Exception e) {

        }

    }

    void getRestaurantListLoadMore(final boolean isReplaceData) {
        try {
            if (TextUtils.isEmpty(Utils.getLatString(mContext))
                    || TextUtils.isEmpty(Utils.getLngString(mContext))
                    || Utils.getLatString(mContext).equals(null)
                    || Utils.getLngString(mContext).equals(null)) {
                return;
            }
            int skip = isReplaceData ? 0 : restaurantList.size();
            Double firstdistance = isReplaceData ? 0 : restaurantList.get(0).getDistance();
            restClient.getApiService().getListRestaurantDistance(Utils.getLatString(mContext), Utils.getLngString(mContext), null, mAdapter.getItemCount() + 1, limit, 3, firstdistance, new Callback<ApiResponseList<Restaurant>>() {
                @Override
                public void success(ApiResponseList<Restaurant> restaurantApiResponseList, Response response) {

                    if (restaurantApiResponseList.getData().size() != 0) {
                        mSwipeRefreshLayout.setRefreshing(false);
                        if (isReplaceData) {
                            if (restaurantApiResponseList.getData().size() > 0) {
                                Utils.saveListResto(mContext, restaurantApiResponseList.getData(), PrefCons.PREF_HISTORY);
                                restaurantList.addAll(Utils.getListResto(mContext, PrefCons.PREF_HISTORY));
                            }
                        } else {
                            restaurantList.addAll(restaurantApiResponseList.getData());
                            rvRestaurant.setVisibility(View.VISIBLE);
                            mAdapter.notifyDataSetChanged();
                        }

                        if (restaurantApiResponseList.getData().size() == 0 && isReplaceData) {
                            rvRestaurant.setVisibility(View.GONE);
                        }

                        int startPos = restaurantList.size() - restaurantApiResponseList.getData().size();
                        for (int pos = startPos; pos < restaurantList.size(); pos++) {
                            RestoAdress restoAdress = new RestoAdress();

                            restoAdress.set_id(restaurantList.get(pos).getId());
                            restoAdress.setStreet(restaurantList.get(pos).getAddress().getStreet());
                            restoAdress.setDistrict(restaurantList.get(pos).getAddress().getDistrict());
                            restoAdress.setCity(restaurantList.get(pos).getAddress().getCity());
                            restoAdress.setProvince(restaurantList.get(pos).getAddress().getProvince());
                            restoAdress.setPostalCode(restaurantList.get(pos).getAddress().getPostalCode());

                            daoSession.getRestoAdressDao().insertOrReplace(restoAdress);
                        }

                        mAdapter.showFooter(false);
                        mAdapter.notifyDataSetChanged();
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    mSwipeRefreshLayout.setRefreshing(false);
                    Log.d("error b > ",String.valueOf(error.getMessage()));
                    if (error.getKind().equals(RetrofitError.Kind.NETWORK)) {
                        snackbar.show();
                    } else {
                        snackbar.dismiss();
                    }
                    //showToast("No Internet Connection");
                }
            });
        } catch (Exception e) {

        }

    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_restaurant;
    }

}
