package com.dycode.foodgasm.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.dycode.foodgasm.FoodGasmApp;
import com.dycode.foodgasm.R;
import com.dycode.foodgasm.activity.BookOrderAct;
import com.dycode.foodgasm.activity.BookOrderCompleteAct;
import com.dycode.foodgasm.activity.CreditCardAct;
import com.dycode.foodgasm.activity.TotalPayment;
import com.dycode.foodgasm.adapters.DialogDokuAdapter;
import com.dycode.foodgasm.dao.CreditCard;
import com.dycode.foodgasm.dao.CreditCardDao;
import com.dycode.foodgasm.dao.DaoSession;
import com.dycode.foodgasm.feature.confirmationtimer.ConfirmationTimerAct;
import com.dycode.foodgasm.model.ApiResponse;
import com.dycode.foodgasm.model.ApiResponseList;
import com.dycode.foodgasm.model.Order;
import com.dycode.foodgasm.model.PostOrder;
import com.dycode.foodgasm.model.Price;
import com.dycode.foodgasm.model.Profile;
import com.dycode.foodgasm.model.ReqPhoneVerify;
import com.dycode.foodgasm.model.Restaurant;
import com.dycode.foodgasm.model.Voucher;
import com.dycode.foodgasm.utils.CCUtils;
import com.dycode.foodgasm.utils.FoodgasmDateUtil;
import com.dycode.foodgasm.utils.FormatterUtil;
import com.dycode.foodgasm.utils.NetworkErrorUtil;
import com.dycode.foodgasm.utils.PaymentPicker;
import com.dycode.foodgasm.utils.SPManager;
import com.dycode.foodgasm.utils.Utils;
import com.hbb20.CountryCodePicker;

import org.ksoap2.serialization.SoapPrimitive;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

//Todo: Rewrite this whole class inn kotlin

public class ReviewOrderType2Fragment extends BaseFragment {

    @BindView(R.id.tvRestaurantName)
    TextView tvRestaurantName;
    @BindView(R.id.tvInfo)
    TextView tvInfo;
    @BindView(R.id.tvDate)
    TextView tvDate;
    @BindView(R.id.linMenu)
    LinearLayout linMenu;
    @BindView(R.id.tvSubtotalValue)
    TextView tvSubtotalValue;
    @BindView(R.id.tvConvenienceChargeValue)
    TextView tvConvenienceChargeValue;
    @BindView(R.id.tvTotalValue)
    TextView tvTotalValue;
    @BindView(R.id.tvVoucherDisc)
    TextView tvVoucherDisc;
    @BindView(R.id.tvVoucherDiscValue)
    TextView tvVoucherDiscValue;
    @BindView(R.id.linDisc)
    LinearLayout linDisc;
    @BindView(R.id.linEdit)
    LinearLayout linEdit;
    @BindView(R.id.linConfirm)
    LinearLayout linConfirm;
    @BindView(R.id.etVoucher)
    EditText etVoucher;
    @BindView(R.id.tvCardNumber)
    TextView tvCardNumber;
    @BindView(R.id.linReview)
    LinearLayout linReview;
    @BindView(R.id.linPaymentMethod)
    LinearLayout linPaymentMethod;


    @BindView(R.id.imgCcDoku)
    ImageView imgCcDoku;


    @BindView(R.id.sv_order)
    ScrollView svOrder;
    @BindView(R.id.btnApplyVoucher)
    Button btnApplyVoucher;


    int subTotal = 0, total = 0, disc = 0, convenience_charge = 0, bank_charge = 0, id_value, flag, voucherDisc = 0, menuPriceFinal, menuDisc = 0, voucherDiscountInPercent = 0, minTransaction = 0, maxDiscount = 0;
    double tax;
    String day, personAmount, minute, note, restoName, coupon = "", menuPrice, categoryId, valueType;
    Boolean isCreditCard = false;

    DaoSession daoSession;

    ArrayList<com.dycode.foodgasm.dao.Menu> menuList = new ArrayList<>();
    Restaurant restaurant;
    Price pr;
    List<CreditCard> creditCardDaoList;
    List<com.dycode.foodgasm.model.CreditCard> creditCardList = new ArrayList<>();
    Date dateIsoFormatted;
    String creditCardMaskedNumber;
    String paymentCredential;
    final static String ISO8601DATEFORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSZ";
    private int personNum;
    private int finalPersonNum;
    private String finalDateIso;
    private int paymentMethod = 0;
    private int totalBankcharge = 0;
    private int totalCharge;
    private View positiveAction;
    private EditText edit_pin, passwordInput, edit_no_hp, edit_kode_merchant;
    private static String TAG = "MainAct";
    SoapPrimitive resultString;
    //    private AsyncCallPay task;
    private String creditCardNumber;
    private String payWithtxt;
    private DialogDokuAdapter mAdapter;
    private LinearLayoutManager linLayoutManager;
    private String pickDokuPay;
    private int taxMenu;
    private String commission;
    private String getOpentime, getClosetime;
    private String verifyStatus = "";
    private Double d;


    public static ReviewOrderType2Fragment newInstance(ArrayList<com.dycode.foodgasm.dao.Menu> menuList, Parcelable restaurant, String personAmount, String day, String minute, int flag, int taxMenu, String commission, String getOpentime, String getClosetime) {

        Bundle b = new Bundle();
        b.putParcelableArrayList(com.dycode.foodgasm.dao.Menu.MENU, menuList);
        b.putParcelable(Restaurant.RESTAURANT, restaurant);
        b.putString("personAmount", personAmount);
        b.putString("day", day);
        b.putString("minute", minute);
        b.putInt("flag", flag);
        b.putInt("tax", taxMenu);
        b.putString("commission", commission);
        b.putString("getOpentime", getOpentime);
        b.putString("getClosetime", getClosetime);

        ReviewOrderType2Fragment frag = new ReviewOrderType2Fragment();
        frag.setArguments(b);
        return frag;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Obtain the shared Tracker instance.
        getTracker("Review Order");
        menuList = getArguments().getParcelableArrayList(com.dycode.foodgasm.dao.Menu.MENU);
        restaurant = getArguments().getParcelable(Restaurant.RESTAURANT);
        personAmount = getArguments().getString("personAmount");
        day = getArguments().getString("day");
        minute = getArguments().getString("minute");
        taxMenu = getArguments().getInt("tax");
        commission = getArguments().getString("commission");
        id_value = getActivity().getIntent().getExtras().getInt("id");
        flag = getActivity().getIntent().getExtras().getInt("flag");
        categoryId = getArguments().getString("categoryId");
        commission = getArguments().getString("commission");
        getOpentime = getArguments().getString("getOpentime");
        getClosetime = getArguments().getString("getClosetime");
        Log.d(" Commission", String.valueOf(commission));


        daoSession = ((FoodGasmApp) getActivity().getApplication()).getDaoSession();
        tvRestaurantName.setText(restaurant.getName());
        final String infoOrderTime = String.format(getString(R.string.info_order_datetime), day, minute);
        String infoOrderDetail = String.format(getString(R.string.info_order_detail), personAmount);

        if (id_value == 2) {
            tvInfo.setText(R.string.by_myself);
            tvDate.setText(R.string.order_now);
            linEdit.setVisibility(View.GONE);
        } else {
            tvInfo.setText(infoOrderDetail);
            tvDate.setText(infoOrderTime);

        }
        linReview.requestFocus();

        //convenience_charge = restaurant.getConvenience();


        if (etVoucher.hasFocus())
            ((Activity) mContext).getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        etVoucher.setInputType(InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);
        etVoucher.setFilters(new InputFilter[]{new TotalPayment.AllCaps(), new InputFilter.LengthFilter(16)});
        etVoucher.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    btnApplyVoucher.setEnabled(true);
                } else {
                    btnApplyVoucher.setEnabled(false);
                }

            }
        });
        getCreditCardList();
        inflateMenu();
        addOnClickListener();
        getProfileData();

        creditCardDaoList = daoSession.getCreditCardDao().queryBuilder().orderAsc(CreditCardDao.Properties.Saved_token_id).list();
        if (creditCardDaoList.size() != 0) {
            creditCardNumber = creditCardDaoList.get(0).getMasked_card();
            creditCardMaskedNumber = creditCardDaoList.get(0).getMasked_card();
            paymentCredential = creditCardDaoList.get(0).get_id();
            creditCardNumber = creditCardNumber.substring(creditCardNumber.length() - 4, creditCardNumber.length());
            //tvCardNumber.setText(creditCardNumber);
            //imgCcDoku.setImageResource(CCUtils.getCardLogo(creditCardMaskedNumber.replaceAll("\\s", "")));
        }


    }

    void addOnClickListener() {
        linEdit.setOnClickListener(v -> {
            Intent intent = new Intent(mContext, BookOrderAct.class);
            intent.putExtra("id", id_value);
            intent.putExtra(Restaurant.RESTAURANT, restaurant);
            intent.putExtra(BookOrderAct.MENU_TYPE, BookOrderAct.FLAG_REVIEW_EDIT);
            intent.putExtra(com.dycode.foodgasm.dao.Menu.MENU, menuList);
            intent.putExtra("personAmount", personAmount);
            intent.putExtra("day", day);
            intent.putExtra("minute", minute);
            intent.putExtra("tax", tax);
            intent.putExtra("commission", commission);
            intent.putExtra("getClosetime", getClosetime);
            intent.putExtra("getOpentime", getOpentime);
            startActivity(intent);

        });

        String dateString = day + " " + minute;
        SimpleDateFormat dateFormat = new SimpleDateFormat("MMM d, yyyy HH:mm", Locale.getDefault());
        SimpleDateFormat dateFormatIso = new SimpleDateFormat(ISO8601DATEFORMAT, Locale.getDefault());

        dateFormatIso.setTimeZone(TimeZone.getTimeZone("GMT+7"));

        Date convertedDate;
        String dateIso = "";
        try {
            convertedDate = dateFormat.parse(dateString);
            dateIso = dateFormatIso.format(convertedDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        try {
            dateIsoFormatted = dateFormatIso.parse(dateIso);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        personNum = Integer.parseInt(personAmount.replaceAll("[\\D]", ""));
        finalPersonNum = personNum;
        finalDateIso = dateIso;



        linConfirm.setOnClickListener(v -> {
            if (verifyStatus != null && !verifyStatus.equals("false")) {
                int menuAmmount = 0;
                for(int i = 0; i< menuList.size(); i++){
                    menuAmmount+= menuList.get(i).getAmount();
                }
                if (menuAmmount == 0) {
                    showAlertDialog(getResources().getString(R.string.alert), getResources().getString(R.string.order_alert_msg));
                } else {
                    paymentMethodAct();
                }
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                final AlertDialog dialog = builder.create();
                LayoutInflater inflater = getActivity().getLayoutInflater();
                View dialogLayout = inflater.inflate(R.layout.dialog_review_verification_phone, null);

                dialog.setView(dialogLayout);
                dialog.setCancelable(true);
                dialog.show();

                final EditText inpPhoneNumber = (EditText) dialog.findViewById(R.id.inpPhoneNumber);
                final CountryCodePicker ccp = (CountryCodePicker) dialog.findViewById(R.id.ccp);
                final TextView txt_conf_yes = (TextView) dialog.findViewById(R.id.txt_conf_yes);
                final TextView txt_conf_no = (TextView) dialog.findViewById(R.id.txt_conf_no);

                txt_conf_yes.setOnClickListener(view -> {
                    String phoneCcp = ccp.getSelectedCountryCode() + inpPhoneNumber.getText().toString();
                    Log.d("Cek > ", phoneCcp);
                    dialog.dismiss();
                    reqPhoneVerify(phoneCcp);
                });

                txt_conf_no.setOnClickListener(view -> dialog.dismiss());
            }

        });


        linPaymentMethod.setOnClickListener(v -> {
            openPaymentMethodDialog();

        });
    }

    private void openPaymentMethodDialog(){
        payWithtxt = "";

        final Dialog pdialog = new Dialog(getActivity());
        pdialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pdialog.setContentView(R.layout.payment_pick);

        RecyclerView recy_doku = (RecyclerView) pdialog.findViewById(R.id.recy_doku);
        mAdapter = new DialogDokuAdapter(getActivity(), creditCardDaoList);
        linLayoutManager = new LinearLayoutManager(getActivity());
        recy_doku.setHasFixedSize(true);
        recy_doku.setLayoutManager(linLayoutManager);
        recy_doku.setAdapter(mAdapter);
        pdialog.show();

        recy_doku.addOnItemTouchListener(new RecyclerTouchListener(getActivity().getApplicationContext(), recy_doku, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                CreditCard cDao = creditCardDaoList.get(position);
                creditCardMaskedNumber = cDao.getMasked_card();
                paymentCredential = cDao.get_id();

                payWithtxt = cDao.getProvider();
                pdialog.dismiss();
                svOrder.post(() -> svOrder.fullScroll(ScrollView.FOCUS_DOWN));
                svOrder.scrollTo(0, svOrder.getBottom());
                linConfirm.setEnabled(true);
                paymentMethod = 1;

                imgCcDoku.setVisibility(View.VISIBLE);

                if (creditCardDaoList.get(position).getProvider().equals("dokuwallet")) {
                    tvCardNumber.setText(creditCardDaoList.get(position).getMasked_card());
                    imgCcDoku.setImageResource(R.mipmap.doku_wallet);
                } else if (creditCardDaoList.get(position).getProvider().equals("ecash")) {
                    Log.d("ecash ", "ecash");
                    tvCardNumber.setText(creditCardDaoList.get(position).getMasked_card());
                    imgCcDoku.setImageResource(R.mipmap.image_logo_cc_ecash);
                } else {
                    tvCardNumber.setText(creditCardDaoList.get(position).getMasked_card().substring(creditCardDaoList.get(position).getMasked_card().length() - 4, creditCardDaoList.get(position).getMasked_card().length()));
                    imgCcDoku.setImageResource(CCUtils.getCardLogo(creditCardDaoList.get(position).getMasked_card().replaceAll("\\s", "")));
                }

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        RelativeLayout r_credit = (RelativeLayout) pdialog.findViewById(R.id.r_credit);
        RelativeLayout r_doku = (RelativeLayout) pdialog.findViewById(R.id.r_doku);
        RelativeLayout r_doku_wallet = (RelativeLayout) pdialog.findViewById(R.id.r_doku_wallet);
        RelativeLayout r_credit_no = (RelativeLayout) pdialog.findViewById(R.id.r_credit_no);
        RelativeLayout r_dompetku = (RelativeLayout) pdialog.findViewById(R.id.r_dompetku);


        if (creditCardDaoList.size() > 0 || isCreditCard) {
            r_credit.setVisibility(View.GONE);
            r_credit_no.setVisibility(View.GONE);
        } else {
            r_credit_no.setVisibility(View.VISIBLE);
        }
        r_credit_no.setVisibility(View.VISIBLE);
        r_doku_wallet.setOnClickListener(v1 -> {
            pdialog.dismiss();

            Intent intent = new Intent(getActivity(), CreditCardAct.class);
            intent.putExtra(CreditCardAct.FLAG, CreditCardAct.FLAG_EDIT_DISABLED);
            startActivityForResult(intent, 0);
            if(getActivity() != null)
                getActivity().overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);

        });

        r_credit.setOnClickListener(v12 -> {
            payWithtxt = "Creditcard";
            pdialog.dismiss();

            svOrder.post(() -> svOrder.fullScroll(ScrollView.FOCUS_DOWN));
            svOrder.scrollTo(0, svOrder.getBottom());
            linConfirm.setEnabled(true);
            paymentMethod = 1;
            tvCardNumber.setText(creditCardNumber);
            imgCcDoku.setImageResource(CCUtils.getCardLogo(creditCardList.get(0).getMaskedCard().replaceAll("\\s", "")));
        });

        r_doku.setOnClickListener(v13 -> {
            payWithtxt = "Doku";
            pdialog.dismiss();

            svOrder.post(() -> svOrder.fullScroll(ScrollView.FOCUS_DOWN));
            svOrder.scrollTo(0, svOrder.getBottom());
            tvCardNumber.setText("Doku");
            imgCcDoku.setImageResource(R.mipmap.doku_logo);
            linConfirm.setEnabled(true);
            paymentMethod = 2;
        });

        r_credit_no.setOnClickListener(v14 -> {
            pdialog.dismiss();
            Intent intent = new Intent(getActivity(), CreditCardAct.class);
            intent.putExtra(CreditCardAct.FLAG, CreditCardAct.FLAG_EDIT_DISABLED);
            startActivityForResult(intent, 0);
            if(getActivity() != null)
                getActivity().overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);


        });

        r_dompetku.setOnClickListener(v15 -> {
            payWithtxt = "Dompetku";
            pdialog.dismiss();

            svOrder.post(new Runnable() {
                @Override
                public void run() {
                    svOrder.fullScroll(ScrollView.FOCUS_DOWN);
                }
            });
            svOrder.scrollTo(0, svOrder.getBottom());
            tvCardNumber.setText("Dompetku");
            imgCcDoku.setImageResource(R.mipmap.doku_logo);
            linConfirm.setEnabled(true);
            paymentMethod = 5;
        });
        pdialog.show();
    }

    private void paymentMethodAct() {
        if (paymentMethod == 0) {
            //handle voucher disc
            if ((voucherDisc == subTotal  || voucherDiscountInPercent == subTotal) && !TextUtils.isEmpty(coupon)) {
                paymentCredential = null;
                creditCardMaskedNumber = null;
                restClient.getApiService().postOrderNew(new PostOrder(Utils.getToken(mContext).getUid(), restaurant.getId(), finalDateIso, false, 0, menuList, note, pr, coupon, creditCardMaskedNumber, pickDokuPay, paymentCredential), Utils.getToken(mContext).getAccessToken(), pickDokuPay, paymentCredential, new Callback<ApiResponse<Order>>() {
                    @Override
                    public void success(ApiResponse<Order> orderApiResponse, Response response) {
                        String bookingCode = orderApiResponse.getData().getCode();
                        restoName = orderApiResponse.getData().getResto().getName();

                        SPManager.saveString(mContext, "stack", "1");
                        SPManager.saveString(mContext, BookOrderCompleteAct.FLAG, String.valueOf(BookOrderCompleteAct.FLAG_WAIT_CONFIRMATION));
                        SPManager.saveString(mContext, "bookingCode", bookingCode);
                        SPManager.saveString(mContext, "restoName", restoName);
                        SPManager.saveString(mContext, SPManager.ORDER_CREATED_TIME, FoodgasmDateUtil.getTimestampNow());
                        SPManager.saveString(mContext, Order.ORDER_ID, orderApiResponse.getData().getId());

//                        startActivity(intent);
                        ConfirmationTimerAct.Companion.start(mContext, orderApiResponse.getData().getId());
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        showAlertDialog("Add Order Failed", error.getMessage());
                    }
                });
            } else {
                showAlertDialog(getResources().getString(R.string.alert), getResources().getString(R.string.no_payment));
            }
        } else if (paymentMethod == 1) {
            int menuAmmount = 0;
            for(int i = 0; i< menuList.size(); i++){
                menuAmmount+= menuList.get(i).getAmount();
            }
            if (menuAmmount == 0)
                showAlertDialog(getResources().getString(R.string.alert), getResources().getString(R.string.order_alert_msg));
            else {
                if (creditCardDaoList.size() >= 0 || isCreditCard) {
                    if (id_value == 1) {
                        if (payWithtxt.equals("dokuwallet")) {
                            id1();
                        } else {
                            id1();
                        }

                    } else if (id_value == 2) {
                        String contentDialog = "";
                        String contentDialogX = "";
                        if (payWithtxt.equals("dokuwallet")) {
                            pickDokuPay = "dokuwallet";
                            Log.d("wallet", "wallet");

                            for (int i = 0; i < menuList.size(); i++) {
                                menuList.get(i).setSubtotalPrice(menuList.get(i).getPrice() * menuList.get(i).getAmount());
                                menuList.get(i).setDiscountPrice(menuList.get(i).getDiscount());
                            }
//                                        restClient.getApiService().postOrderNew(new PostOrder(Utils.getToken(mContext).getUid(), restaurant.getId(), finalDateIso, false, 0, menuList, note, pr, coupon, creditCardMaskedNumber, "creditcard"), Utils.getToken(mContext).getAccessToken(), "creditcard",paymentCredential, new Callback<ApiResponse<Order>>() {
                            restClient.getApiService().postOrderNew(new PostOrder(Utils.getToken(mContext).getUid(), restaurant.getId(), finalDateIso, false, 0, menuList, note, pr, coupon, creditCardMaskedNumber, pickDokuPay, paymentCredential), Utils.getToken(mContext).getAccessToken(), pickDokuPay, paymentCredential, new Callback<ApiResponse<Order>>() {
                                @Override
                                public void success(ApiResponse<Order> orderApiResponse, Response response) {
                                    String bookingCode = orderApiResponse.getData().getCode();
                                    restoName = orderApiResponse.getData().getResto().getName();

                                    SPManager.saveString(mContext, "stack", "1");
                                    SPManager.saveString(mContext, BookOrderCompleteAct.FLAG, String.valueOf(BookOrderCompleteAct.FLAG_WAIT_CONFIRMATION));
                                    SPManager.saveString(mContext, "bookingCode", bookingCode);
                                    SPManager.saveString(mContext, "restoName", restoName);
                                    SPManager.saveString(mContext, SPManager.ORDER_CREATED_TIME, FoodgasmDateUtil.getTimestampNow());
                                    SPManager.saveString(mContext, Order.ORDER_ID, orderApiResponse.getData().getId());

//                                    startActivity(intent);
                                    ConfirmationTimerAct.Companion.start(mContext, orderApiResponse.getData().getId());
                                }

                                @Override
                                public void failure(RetrofitError error) {
                                    showAlertDialog("Add Order Failed", error.getMessage());
                                }
                            });
                        } else if (payWithtxt.equals("ecash")) {
                            pickDokuPay = "ecash";
                            Log.d("ecash", "ecash");
                            for (int i = 0; i < menuList.size(); i++) {
                                menuList.get(i).setSubtotalPrice(menuList.get(i).getPrice() * menuList.get(i).getAmount());
                                menuList.get(i).setDiscountPrice(menuList.get(i).getDiscount());
                            }
                            restClient.getApiService().postOrderNew(new PostOrder(Utils.getToken(mContext).getUid(), restaurant.getId(), finalDateIso, false, 0, menuList, note, pr, coupon, creditCardMaskedNumber, pickDokuPay, paymentCredential), Utils.getToken(mContext).getAccessToken(), pickDokuPay, paymentCredential, new Callback<ApiResponse<Order>>() {
                                @Override
                                public void success(ApiResponse<Order> orderApiResponse, Response response) {
                                    String bookingCode = orderApiResponse.getData().getCode();
                                    restoName = orderApiResponse.getData().getResto().getName();

                                    SPManager.saveString(mContext, "stack", "1");
                                    SPManager.saveString(mContext, BookOrderCompleteAct.FLAG, String.valueOf(BookOrderCompleteAct.FLAG_WAIT_CONFIRMATION));
                                    SPManager.saveString(mContext, "bookingCode", bookingCode);
                                    SPManager.saveString(mContext, "restoName", restoName);
                                    SPManager.saveString(mContext, SPManager.ORDER_CREATED_TIME, FoodgasmDateUtil.getTimestampNow());
                                    SPManager.saveString(mContext, Order.ORDER_ID, orderApiResponse.getData().getId());

                                    ConfirmationTimerAct.Companion.start(mContext, orderApiResponse.getData().getId());
                                }

                                @Override
                                public void failure(RetrofitError error) {
                                    showAlertDialog("Add Order Failed", error.getMessage());
                                }
                            });
                        } else {
                            pickDokuPay = "creditcard";
                            //pr.setAdditionalCharge(2500);
                            contentDialog = "Additional Cost +2500 IDR, are you sure you want to continue?";
                            contentDialogX = "You will be charged for this transaction.";
                            Log.d("creditcard", "creditcard");
                            new MaterialDialog.Builder(mContext)
                                    .content(contentDialogX + "\n" + contentDialog)
                                    .positiveText(R.string.ok)
                                    .negativeText(R.string.cancel)
                                    .cancelable(false)
                                    .onPositive((materialDialog, dialogAction) -> {
                                        for (int i = 0; i < menuList.size(); i++) {
                                            menuList.get(i).setSubtotalPrice(menuList.get(i).getPrice() * menuList.get(i).getAmount());
                                            menuList.get(i).setDiscountPrice(menuList.get(i).getDiscount());
                                        }
                                        restClient.getApiService().postOrderNew(new PostOrder(Utils.getToken(mContext).getUid(), restaurant.getId(), finalDateIso, false, 0, menuList, note, pr, coupon, creditCardMaskedNumber, pickDokuPay, paymentCredential), Utils.getToken(mContext).getAccessToken(), pickDokuPay, paymentCredential, new Callback<ApiResponse<Order>>() {
                                            @Override
                                            public void success(ApiResponse<Order> orderApiResponse, Response response) {
                                                String bookingCode = orderApiResponse.getData().getCode();
                                                restoName = orderApiResponse.getData().getResto().getName();

                                                SPManager.saveString(mContext, "stack", "1");
                                                SPManager.saveString(mContext, BookOrderCompleteAct.FLAG, String.valueOf(BookOrderCompleteAct.FLAG_WAIT_CONFIRMATION));
                                                SPManager.saveString(mContext, "bookingCode", bookingCode);
                                                SPManager.saveString(mContext, "restoName", restoName);
                                                SPManager.saveString(mContext, SPManager.ORDER_CREATED_TIME, FoodgasmDateUtil.getTimestampNow());
                                                SPManager.saveString(mContext, Order.ORDER_ID, orderApiResponse.getData().getId());

                                                ConfirmationTimerAct.Companion.start(mContext, orderApiResponse.getData().getId());
                                            }

                                            @Override
                                            public void failure(RetrofitError error) {
                                                showAlertDialog("Add Order Failed", error.getMessage());
                                            }
                                        });
                                    })
                                    .onNegative((materialDialog, dialogAction) -> {
                                        materialDialog.dismiss();
                                        linConfirm.setEnabled(true);
                                        linConfirm.setClickable(true);
                                    })
                                    .show();
                        }


                    }
                    linConfirm.setClickable(false);
                    linConfirm.setEnabled(false);

                } else {
                    Log.d("payment method > ", String.valueOf(paymentMethod));
                    final Dialog dialog = new Dialog(mContext);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.no_payment_layout);

                    Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
                    Button btnAdd = (Button) dialog.findViewById(R.id.btnConfirm);

                    btnCancel.setOnClickListener(v -> dialog.dismiss());

                    btnAdd.setOnClickListener(v -> {
                        Intent intent = new Intent(getActivity(), CreditCardAct.class);
                        intent.putExtra(CreditCardAct.FLAG, CreditCardAct.FLAG_EDIT_DISABLED);
                        startActivityForResult(intent, 0);
                        dialog.dismiss();
                    });

                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    dialog.show();
                }
            }
        } else if (paymentMethod == 2) {
            if (id_value == 1) {
                id2();
            } else if (id_value == 2) {
                new MaterialDialog.Builder(mContext)
                        .content(R.string.certainty_doku)
                        .positiveText(R.string.ok)
                        .negativeText(R.string.cancel)
                        .cancelable(false)
                        .onPositive((materialDialog, dialogAction) -> {
                            for (int i = 0; i < menuList.size(); i++) {
                                menuList.get(i).setSubtotalPrice(menuList.get(i).getPrice() * menuList.get(i).getAmount());
                                menuList.get(i).setDiscountPrice(menuList.get(i).getDiscount());
                            }
                            restClient.getApiService().postOrderNew(new PostOrder(Utils.getToken(mContext).getUid(), restaurant.getId(), finalDateIso, false, 0, menuList, note, pr, coupon, null, "creditcard"), Utils.getToken(mContext).getAccessToken(), "creditcard", null, new Callback<ApiResponse<Order>>() {
                                @Override
                                public void success(ApiResponse<Order> orderApiResponse, Response response) {
                                    String bookingCode = orderApiResponse.getData().getCode();
                                    restoName = orderApiResponse.getData().getResto().getName();

                                    SPManager.saveString(mContext, "stack", "1");
                                    SPManager.saveString(mContext, BookOrderCompleteAct.FLAG, String.valueOf(BookOrderCompleteAct.FLAG_WAIT_CONFIRMATION));
                                    SPManager.saveString(mContext, "bookingCode", bookingCode);
                                    SPManager.saveString(mContext, "restoName", restoName);
                                    SPManager.saveString(mContext, SPManager.ORDER_CREATED_TIME, FoodgasmDateUtil.getTimestampNow());
                                    SPManager.saveString(mContext, Order.ORDER_ID, orderApiResponse.getData().getId());
                                    ConfirmationTimerAct.Companion.start(mContext, orderApiResponse.getData().getId());
                                }

                                @Override
                                public void failure(RetrofitError error) {
                                    showAlertDialog("Add Order Failed", error.getMessage());
                                }
                            });
                        })
                        .onNegative((materialDialog, dialogAction) -> {
                            materialDialog.dismiss();
                            linConfirm.setEnabled(true);
                            linConfirm.setClickable(true);
                        })
                        .show();
            }
        }
    }

    private void id1() {
        if (payWithtxt.equals("dokuwallet")) {
            pickDokuPay = "dokuwallet";
            proceedApiId1();
        } else if (payWithtxt.equals("ecash")) {
            pickDokuPay = "ecash";
            proceedApiId1();
        } else {
            pickDokuPay = "creditcard";
            String contentDialog = "Additional Cost +2500 IDR, are you sure you want to continue?";
            String contentDialogX = "You will be charged for this transaction.";
            new MaterialDialog.Builder(mContext)
                    .content(contentDialogX + "\n" + contentDialog)
                    .positiveText(R.string.ok)
                    .negativeText(R.string.cancel)
                    .cancelable(false)
                    .onPositive((materialDialog, dialogAction) -> proceedApiId1())
                    .onNegative((materialDialog, dialogAction) -> {
                        materialDialog.dismiss();
                        linConfirm.setEnabled(true);
                        linConfirm.setClickable(true);
                    })
                    .show();
        }

    }

    private void proceedApiId1() {
        for (int i = 0; i < menuList.size(); i++) {
            menuList.get(i).setSubtotalPrice(menuList.get(i).getPrice() * menuList.get(i).getAmount());
            menuList.get(i).setDiscountPrice(menuList.get(i).getDiscount());
        }
        restClient.getApiService().postOrderNew(new PostOrder(Utils.getToken(mContext).getUid(), restaurant.getId(), finalDateIso, true, finalPersonNum, menuList, note, pr, coupon, creditCardMaskedNumber, pickDokuPay, paymentCredential), Utils.getToken(mContext).getAccessToken(), pickDokuPay, paymentCredential, new Callback<ApiResponse<Order>>() {
            @Override
            public void success(ApiResponse<Order> orderApiResponse, Response response) {
                String bookingCode = orderApiResponse.getData().getCode();
                restoName = orderApiResponse.getData().getResto().getName();

                SPManager.saveString(mContext, "stack", "1");
                SPManager.saveString(mContext, BookOrderCompleteAct.FLAG, String.valueOf(BookOrderCompleteAct.FLAG_WAIT_CONFIRMATION));
                SPManager.saveString(mContext, "bookingCode", bookingCode);
                SPManager.saveString(mContext, "restoName", restoName);
                SPManager.saveString(mContext, SPManager.ORDER_CREATED_TIME, FoodgasmDateUtil.getTimestampNow());
                SPManager.saveString(mContext, Order.ORDER_ID, orderApiResponse.getData().getId());

                ConfirmationTimerAct.Companion.start(mContext, orderApiResponse.getData().getId());
            }

            @Override
            public void failure(RetrofitError error) {
                showAlertDialog("Add Order Failed", NetworkErrorUtil.getErrorMessage(error));
            }
        });
    }

    private void id2() {
        String contentDialog = "";
        String contentDialogX = "";
        if (payWithtxt.equals("dokuwallet")) {
            pickDokuPay = "dokuwallet";
            Log.d("wallet", "wallet");
            proceedApi();
        } else if (payWithtxt.equals("ecash")) {
            pickDokuPay = "ecash";
            Log.d("ecash", "ecash");
            proceedApi();
        } else {
            pickDokuPay = "creditcard";
            //pr.setAdditionalCharge(2500);
            contentDialog = "Additional Cost +2500 IDR, are you sure you want to continue?";
            contentDialogX = "You will be charged for this transaction.";
            Log.d("creditcard", "creditcard");
            new MaterialDialog.Builder(mContext)
                    .content(contentDialogX + "\n" + contentDialog)
                    .positiveText(R.string.ok)
                    .negativeText(R.string.cancel)
                    .cancelable(false)
                    .onPositive((materialDialog, dialogAction) -> proceedApi())
                    .onNegative((materialDialog, dialogAction) -> {
                        materialDialog.dismiss();
                        linConfirm.setEnabled(true);
                        linConfirm.setClickable(true);
                    })
                    .show();
        }

    }

    private void proceedApi() {
        for (int i = 0; i < menuList.size(); i++) {
            menuList.get(i).setSubtotalPrice(menuList.get(i).getPrice() * menuList.get(i).getAmount());
            menuList.get(i).setDiscountPrice(menuList.get(i).getDiscount());
        }
        restClient.getApiService().postOrderNew(new PostOrder(Utils.getToken(mContext).getUid(), restaurant.getId(), finalDateIso, false, 0, menuList, note, pr, coupon, null, pickDokuPay), Utils.getToken(mContext).getAccessToken(), pickDokuPay, null, new Callback<ApiResponse<Order>>() {
            @Override
            public void success(ApiResponse<Order> orderApiResponse, Response response) {
                String bookingCode = orderApiResponse.getData().getCode();
                restoName = orderApiResponse.getData().getResto().getName();

                SPManager.saveString(mContext, "stack", "1");
                SPManager.saveString(mContext, BookOrderCompleteAct.FLAG, String.valueOf(BookOrderCompleteAct.FLAG_WAIT_CONFIRMATION));
                SPManager.saveString(mContext, "bookingCode", bookingCode);
                SPManager.saveString(mContext, "restoName", restoName);
                SPManager.saveString(mContext, SPManager.ORDER_CREATED_TIME, FoodgasmDateUtil.getTimestampNow());
                SPManager.saveString(mContext, Order.ORDER_ID, orderApiResponse.getData().getId());
                ConfirmationTimerAct.Companion.start(mContext, orderApiResponse.getData().getId());

            }

            @Override
            public void failure(RetrofitError error) {
                showAlertDialog("Add Order Failed", NetworkErrorUtil.getErrorMessage(error));
            }
        });
    }

    private void inflateMenu() {
        linMenu.removeAllViews();
        final LayoutInflater inflater = LayoutInflater.from(getActivity());
        for (int pos = 0; pos < menuList.size(); pos++) {
            final View view = inflater.inflate(R.layout.row_menu_review, linMenu, false);
            TextView tvName = ButterKnife.findById(view, R.id.tvFoodName);
            TextView tvDesc = ButterKnife.findById(view, R.id.tvDesc);
            final TextView tvPrice = ButterKnife.findById(view, R.id.tvPrice);
            final TextView tvCount = ButterKnife.findById(view, R.id.tvCountValue);
            final com.dycode.foodgasm.dao.Menu menu = menuList.get(pos);
            final ImageView imgCountPlus = ButterKnife.findById(view, R.id.imgCountPlus);
            final ImageView imgCountMinus = ButterKnife.findById(view, R.id.imgCountMinus);
            final EditText inpNote = ButterKnife.findById(view, R.id.inpNote);

            if (menu.getAmount() == 0) {
                view.setVisibility(View.GONE);
            }

            tvCount.setText(String.valueOf(menu.getAmount()));
            tvName.setText(menu.getName());
            if (TextUtils.isEmpty(menu.getDescription())) {
                tvDesc.setVisibility(View.GONE);
            } else {
                tvDesc.setVisibility(View.VISIBLE);
                tvDesc.setText(menu.getDescription());
            }


            menuPriceFinal = menu.getAmount() * menu.getPrice() - menu.getDiscount() < 0 ? 0 : (menu.getAmount() * menu.getPrice() - menu.getDiscount());

            double taxRestoD = restaurant.getTax();
            Double d = new Double(taxRestoD);
            int a = menuPriceFinal + (menuPriceFinal * d.intValue() / 100);
            double c = (int) Math.ceil((double) (a + a * Double.parseDouble(commission) / 100));
            int totalPricePajak = (int) c;

            tvPrice.setText(FormatterUtil.getGermanCurrencyFormat(totalPricePajak));
            subTotal = subTotal + totalPricePajak;

            disc = menu.getDiscount();
            tax = subTotal * restaurant.getTax() / 100;
            bank_charge = (int) Math.ceil(0.05 * (subTotal + (int) tax));
            total = subTotal;
            float mathConenience = total;
            convenience_charge = (int) Math.ceil((int) mathConenience * -0.001);
            total = total + convenience_charge;


            totalCharge = convenience_charge + bank_charge;
            displayTotal();
            pr = new Price();
            pr.setSubtotal(subTotal);
            pr.setDiscount(voucherDisc);
            pr.setTax((int) tax);
            pr.setBankCharge(bank_charge);
            pr.setConvenience(convenience_charge);
            pr.setTotal(total);


            linMenu.addView(view);
            imgCountPlus.setOnClickListener(v -> {
                int newCount = Integer.parseInt(tvCount.getText().toString()) + 1;
                tvCount.setText(String.valueOf(newCount));
                menu.setAmount(newCount);
                if (newCount == 0) {
                    tvCount.setVisibility(View.GONE);
                    linMenu.removeView(view);
                    menu.setAmount(0);
                }

                double taxRestoDisc = restaurant.getTax();
                menuPriceFinal = menu.getAmount() * menu.getPrice() - menu.getDiscount() < 0 ? 0 : (menu.getAmount() * menu.getPrice() - menu.getDiscount());

                Double d1 = taxRestoDisc;
                int a2 = menuPriceFinal + (menuPriceFinal * d1.intValue() / 100);
                double totalPricePajakD = (int) Math.ceil((double) (a2 + a2 * Double.parseDouble(commission) / 100));
                int totalPricePajakInt = (int) totalPricePajakD;

                tvPrice.setText(FormatterUtil.getGermanCurrencyFormat(totalPricePajakInt));
                if (!TextUtils.isEmpty(coupon)) calculateDiscount();
                else if (voucherDisc > 0) calculateTotal(voucherDisc);
                else calculateTotal(voucherDiscountInPercent);
            });

            imgCountMinus.setOnClickListener(v -> {
                int newCount = Integer.parseInt(tvCount.getText().toString()) - 1;
                tvCount.setText(String.valueOf(newCount));
                menu.setAmount(newCount);
                if (newCount == 0) {
                    tvCount.setVisibility(View.GONE);
                    linMenu.removeView(view);
                    menu.setAmount(0);
                }

                double taxRestoDisc = restaurant.getTax();
                menuPriceFinal = menu.getAmount() * menu.getPrice() - menu.getDiscount() < 0 ? 0 : (menu.getAmount() * menu.getPrice() - menu.getDiscount());

                Double d1 = taxRestoDisc;
                int a2 = menuPriceFinal + (menuPriceFinal * d1.intValue() / 100);
                double totalPricePajakD = (int) Math.ceil((double) (a2 + a2 * Double.parseDouble(commission) / 100));
                int totalPricePajakInt = (int) totalPricePajakD;

                tvPrice.setText(FormatterUtil.getGermanCurrencyFormat(totalPricePajakInt));
                if (!TextUtils.isEmpty(coupon)) calculateDiscount();
                else if (voucherDisc > 0) calculateTotal(voucherDisc);
                else calculateTotal(voucherDiscountInPercent);
            });


            inpNote.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    menu.setNote(editable.toString());
                }
            });

        }

    }


    private void calculateSubtotal(){
        subTotal = 0;
        for (int position = 0; position < menuList.size(); position++) {
            final com.dycode.foodgasm.dao.Menu menuCount = menuList.get(position);

            menuPriceFinal = menuCount.getAmount() * menuCount.getPrice() - menuCount.getDiscount() < 0 ? 0 : (menuCount.getAmount() * menuCount.getPrice() - menuCount.getDiscount());
            double taxRestoD = restaurant.getTax();

            Double d = Double.valueOf(taxRestoD);
            int a = menuPriceFinal + (menuPriceFinal * d.intValue() / 100);
            double c = (int) Math.ceil((double) (a + a * Double.parseDouble(commission) / 100));
            int totalPricePajak = (int) c;
            subTotal = subTotal + totalPricePajak;

            disc = menuCount.getDiscount();
        }
    }

    private void calculateTotal(int voucherDisc) {
        calculateSubtotal();
        int voucherDiscount = 0;
        if (voucherDisc > 0) voucherDiscount = voucherDisc;

        tax = subTotal * restaurant.getTax() / 100;
        bank_charge = (int) Math.ceil(0.05 * (subTotal + (int) tax));
        total = subTotal;


        total = total - voucherDiscount > 0 ? total - voucherDiscount : 0;


        totalCharge = convenience_charge + bank_charge;
        float mathConenience = total;
        convenience_charge = (int) Math.ceil((int) mathConenience * -0.001);
        total = total + convenience_charge;

        displayTotal();
        pr = new Price();
        pr.setSubtotal(subTotal);
        pr.setDiscount(voucherDiscount);
        pr.setTax((int) tax);
        pr.setBankCharge(bank_charge);
        pr.setConvenience(convenience_charge);
        pr.setTotal(total);

        if (subTotal <= 0) {
            tax = 0;
            total = 0;
            convenience_charge = 0;
            bank_charge = 0;
        }
        displayTotal();
    }

    private void displayTotal() {
        String subTotalValue = (String.format(Locale.ENGLISH, "%,d", subTotal)).replace(',', ',');
        String taxValue = (String.format(Locale.ENGLISH, "%,d", (int) tax)).replace(',', ',');
        String convenienceChargeValue = (String.format(Locale.ENGLISH, "%,d", convenience_charge)).replace(',', ',');
        String bankCharge = (String.format(Locale.ENGLISH, "%,d", bank_charge)).replace(',', ',');
        String totalValue = (String.format(Locale.ENGLISH, "%,d", total)).replace(',', ',');

        String totalBankcharges = (String.format(Locale.ENGLISH, "%,d", bank_charge + convenience_charge)).replace(',', ',');

        tvSubtotalValue.setText(subTotalValue);
        tvConvenienceChargeValue.setText(convenienceChargeValue);
        tvTotalValue.setText(totalValue);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0 && resultCode == 1) {
            getCreditCardList();
            List<CreditCard> creditCard = daoSession.getCreditCardDao().queryBuilder().orderAsc(CreditCardDao.Properties.Saved_token_id).list();
            if (creditCard != null && creditCard.size() != 0 && creditCardDaoList != null && creditCardDaoList.size() != 0) {
                String creditCardNumber = creditCard.get(0).getMasked_card();
                creditCardMaskedNumber = creditCardDaoList.get(0).getMasked_card();
                paymentCredential = creditCardDaoList.get(0).get_id();
                creditCardNumber = creditCardNumber.substring(creditCardNumber.length() - 4, creditCardNumber.length());
                tvCardNumber.setText(creditCardNumber);
                if (creditCard.size() != 0)
                    isCreditCard = true;
            } else {
                getCreditCardList();
            }
        } else {
            Log.d("get creditcard list", "");
            getCreditCardList();
        }
    }

    public void getCreditCardList() {
        showProgressDialog("Loading...");
        restClient.getApiService().getCreditCardList(Utils.getToken(mContext).getAccessToken(), new Callback<ApiResponseList<com.dycode.foodgasm.model.CreditCard>>() {
                    @Override
                    public void success(ApiResponseList<com.dycode.foodgasm.model.CreditCard> creditCardApiResponseList, Response response) {
                        progressDialog.dismiss();
                        creditCardList.clear();
                        creditCardList.addAll(creditCardApiResponseList.getData());
                        for (int pos = 0; pos < creditCardList.size(); pos++) {
                            CreditCard creditCardDao = new com.dycode.foodgasm.dao.CreditCard();
                            creditCardDao.set_id(creditCardList.get(pos).getId());
                            creditCardDao.setTransaction_id(creditCardList.get(pos).getTransactionId());
                            creditCardDao.setSaved_token_id(creditCardList.get(pos).getSavedTokenId());
                            creditCardDao.setMasked_card(creditCardList.get(pos).getMaskedCard());
                            creditCardDao.setState(creditCardList.get(pos).getState());
                            creditCardDao.setIsEditSelected(creditCardList.get(pos).getIsEditSelected());
                            creditCardDao.setCustomer_id(creditCardList.get(pos).getCustomer_id());
                            creditCardDao.setProvider(creditCardList.get(pos).getProvider());

                            daoSession.insertOrReplace(creditCardDao);
                        }
                        checkCreditCardDisplay();
                    }


                    @Override
                    public void failure(RetrofitError error) {
                        progressDialog.dismiss();
                        showToast("No Internet Connection");
                    }
                }

        );
    }

    public void checkCreditCardDisplay() {
        creditCardDaoList = daoSession.getCreditCardDao().queryBuilder().orderAsc(CreditCardDao.Properties.Saved_token_id).list();
        if (creditCardDaoList.size() == 1) {
            String creditCardNumber = creditCardDaoList.get(0).getMasked_card();
            creditCardMaskedNumber = creditCardDaoList.get(0).getMasked_card();
            paymentCredential = creditCardDaoList.get(0).get_id();
            creditCardNumber = creditCardNumber.substring(creditCardNumber.length() - 4, creditCardNumber.length());
        } else tvCardNumber.setText("Select Payment Method");

    }


    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private ReviewOrderType2Fragment.ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final ReviewOrderType2Fragment.ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }

    void getProfileData() {
        restClient.getApiService().getProfileData(Utils.getToken(getActivity()).getAccessToken(), new Callback<ApiResponse<Profile>>() {
            @Override
            public void success(ApiResponse<Profile> profileApiResponse, Response response) {
                final Profile profile = profileApiResponse.getData();
                verifyStatus = profile.getUserPhoneVerified();
                if (verifyStatus == null) verifyStatus = null;
            }

            @Override
            public void failure(RetrofitError error) {
                progressDialog.dismiss();
                showToast("No Internet Connection / missing token");
                verifyStatus = null;
            }
        });
    }


    private void reqPhoneVerify(String phoneNumber) {
        progressDialog.show();
        restClient.getApiService().getPhoneVerify(Utils.getToken(getActivity()).getAccessToken(), phoneNumber, new Callback<ApiResponse<ReqPhoneVerify>>() {
            @Override
            public void success(final ApiResponse<ReqPhoneVerify> reqPhoneVerifyApiResponse, Response response) {
                progressDialog.dismiss();
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                final AlertDialog dialog = builder.create();
                LayoutInflater inflater = getActivity().getLayoutInflater();
                View dialogLayout = inflater.inflate(R.layout.dialog_verification_phone, null);

                dialog.setView(dialogLayout);
                dialog.setCancelable(true);
                dialog.show();

                final EditText txt_pin_number = (EditText) dialog.findViewById(R.id.txt_pin_number);
                final TextView txt_conf_yes = (TextView) dialog.findViewById(R.id.txt_conf_yes);
                final TextView txt_conf_no = (TextView) dialog.findViewById(R.id.txt_conf_no);

                txt_conf_yes.setOnClickListener(view -> {
                    dialog.dismiss();
                    ReqPhoneVerify reqPhoneVerify = new ReqPhoneVerify(txt_pin_number.getText().toString(), reqPhoneVerifyApiResponse.getData().getToken_id());
                    reqPhoneNumber(reqPhoneVerify);
                });

                txt_conf_no.setOnClickListener(view -> dialog.dismiss());


            }

            @Override
            public void failure(RetrofitError error) {
                progressDialog.dismiss();
                showAlertDialog("Error", error.getMessage());
            }
        });
    }

    private void reqPhoneNumber(ReqPhoneVerify reqPhoneVerify) {
        progressDialog.show();
        restClient.getApiService().postPhoneVerify(Utils.getToken(getActivity()).getAccessToken(), reqPhoneVerify, new Callback<ApiResponse>() {
            @Override
            public void success(ApiResponse data, Response response) {
                progressDialog.dismiss();

                String message = data.getMeta().getMessage();
                String code = String.valueOf(data.getMeta().getCode());

                new MaterialDialog.Builder(getActivity())
                        .content(message)
                        .positiveText(R.string.ok)
                        .cancelable(true)
                        .onPositive((dialog, which) -> {
                            int menuAmmount = 0;
                            for(int i = 0; i< menuList.size(); i++){
                                menuAmmount+= menuList.get(i).getAmount();
                            }
                            if (menuAmmount == 0) {
                                showAlertDialog(getResources().getString(R.string.alert), getResources().getString(R.string.order_alert_msg));
                            } else {
                                paymentMethodAct();
                            }
                        })
                        .show();

            }

            @Override
            public void failure(RetrofitError error) {
                progressDialog.dismiss();
                showAlertDialog("Error", error.getMessage());
            }
        });
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_review_order_type2;
    }


    @OnClick(R.id.btnApplyVoucher)
    void verifyVoucher() {
        showProgressDialog("Validating..");
        restClient.getApiService().validateVoucher(etVoucher.getText().toString(), restaurant.getId(), subTotal, Utils.getToken(mContext).getAccessToken(), payWithtxt, new Callback<ApiResponse<Voucher>>() {
            @Override
            public void success(ApiResponse<Voucher> voucherApiResponse, Response response) {
                progressDialog.dismiss();
                linDisc.setVisibility(View.VISIBLE);
                voucherDisc = voucherApiResponse.getData().getVoucherDetail().getValue();
                coupon = voucherApiResponse.getData().getCode();
                minTransaction = voucherApiResponse.getData().getVoucherDetail().getMinTransaction();
                maxDiscount = voucherApiResponse.getData().getVoucherDetail().getMaxDiscount();
                valueType = voucherApiResponse.getData().getVoucherDetail().getValueType();


                if (valueType.equals("percent") && voucherDisc == 100) {
                    showAlertDialog(getString(R.string.validate_voucher_success), getString(R.string.payment_managed));
                    linPaymentMethod.setOnClickListener(null);
                    paymentMethod = 0;
                    payWithtxt = "";
                    tvCardNumber.setText(R.string.payment_managed);
                    linConfirm.setEnabled(true);
                }else{
                    showMaterialDialog(R.string.validate_voucher_success);
                }

                calculateDiscount();
            }

            @Override
            public void failure(RetrofitError error) {
                progressDialog.dismiss();
                showAlertDialog("Validate voucher failed", NetworkErrorUtil.getErrorMessage(error));

            }
        });
    }


    private void calculateDiscount() {
        if (TextUtils.isEmpty(coupon)) {
            return;
        }

        calculateSubtotal();
        //validating minTransaction constraint
        if(subTotal < minTransaction && minTransaction != 0){
            linPaymentMethod.setOnClickListener(v -> {
                openPaymentMethodDialog();
            });
            showAlertDialog("Discount Removed" , "Minimum transaction for this coupon is "+ minTransaction);
            linDisc.setVisibility(View.GONE);
            if(tvCardNumber.getText().toString().equals(getString(R.string.payment_managed))){
                tvCardNumber.setText(R.string.select_payment_method);
            }
            voucherDisc = 0;
            coupon = null;
            minTransaction = 0;
            maxDiscount = 0;
            valueType = "";

        }

        if (valueType.equals("percent")) {
            voucherDiscountInPercent = Math.round(subTotal * ((float) voucherDisc / 100));
            if(maxDiscount != 0 && voucherDiscountInPercent > maxDiscount )
                voucherDiscountInPercent = maxDiscount;
            subTotal = subTotal - voucherDiscountInPercent <= 0 ? 0 : subTotal - voucherDiscountInPercent;
            tvVoucherDiscValue.setText((String.format(Locale.ENGLISH, "%,d", voucherDiscountInPercent)).replace(',', ','));
            voucherDisc = 0;
            calculateTotal(voucherDiscountInPercent);
        } else {
            tvVoucherDiscValue.setText((String.format(Locale.ENGLISH, "%,d", voucherDisc)).replace(',', ','));
            subTotal = subTotal - voucherDisc <= 0 ? 0 : subTotal - voucherDisc;
            voucherDiscountInPercent = 0;
            calculateTotal(voucherDisc);
        }
    }
}
