package com.dycode.foodgasm.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.dycode.foodgasm.R;
import com.dycode.foodgasm.activity.DealsDetailAct;
import com.dycode.foodgasm.adapters.DealsAdapter;
import com.dycode.foodgasm.constant.PrefCons;
import com.dycode.foodgasm.listener.EndlessScrollListener;
import com.dycode.foodgasm.model.ApiResponseList;
import com.dycode.foodgasm.model.Deals;
import com.dycode.foodgasm.rest.RestClient;
import com.dycode.foodgasm.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by asus pc on 19/10/2015.
 */
public class DealsFragment extends BaseFragment implements AdapterView.OnItemClickListener {

    @BindView(R.id.list)
    ListView list;
    @BindView(R.id.srlPromoList)
    SwipeRefreshLayout mSwipeRefreshLayout;

    DealsAdapter mAdapter;
    List<Deals> dealsList = new ArrayList<Deals>();
    int limit = 10;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (Utils.getListDeals(mContext, PrefCons.PREF_DEALS) != null) {
            dealsList = Utils.getListDeals(mContext, PrefCons.PREF_DEALS);
            list.setVisibility(View.VISIBLE);
        }
        mAdapter = new DealsAdapter(mContext, dealsList);
        list.setAdapter(mAdapter);
        list.setOnItemClickListener(this);
        list.setOnScrollListener(new EndlessScrollListener() {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                if (dealsList.size() >= 10) getPromoList(false);
            }
        });

        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorAccent);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getPromoList(true);
            }
        });
        mSwipeRefreshLayout.setRefreshing(true);
        getPromoList(true);

    }

    void getPromoList(final boolean isReplaceData) {
        restClient = RestClient.getInstance();

        int skip = isReplaceData ? 0 : dealsList.size();
        restClient.getApiService().getListDeals(Utils.getToken(mContext).getAccessToken(), skip, limit, new Callback<ApiResponseList<Deals>>() {
            @Override
            public void success(ApiResponseList<Deals> dealsApiResponseList, Response response) {
                if (progressDialog != null)
                    progressDialog.dismiss();
                mSwipeRefreshLayout.setRefreshing(false);
                if (isReplaceData) {
                    dealsList.clear();
                    if (dealsApiResponseList.getData().size() > 0) {
                        Utils.saveListDeals(mContext, dealsApiResponseList.getData(), PrefCons.PREF_DEALS);
                    }
                }
                if (dealsApiResponseList.getData().size() == 0 && isReplaceData) {
                    list.setVisibility(View.GONE);
                } else {
                    list.setVisibility(View.VISIBLE);
                    dealsList.addAll(dealsApiResponseList.getData());
                    mAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (progressDialog != null)
                    progressDialog.dismiss();
                mSwipeRefreshLayout.setRefreshing(false);
//                showToast("No internet connection");
//                showAlertDialog("Failed", error.getMessage());
            }
        });
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_deals;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }
}
