package com.dycode.foodgasm.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;

import com.dycode.foodgasm.FoodGasmApp;
import com.dycode.foodgasm.R;
import com.dycode.foodgasm.adapters.InboxAdapter;
import com.dycode.foodgasm.dao.DaoSession;
import com.dycode.foodgasm.model.ApiResponseList;
import com.dycode.foodgasm.model.Inbox;
import com.dycode.foodgasm.rest.RestClient;
import com.dycode.foodgasm.utils.Constants;
import com.dycode.foodgasm.utils.NetworkErrorUtil;
import com.dycode.foodgasm.utils.SPManager;
import com.dycode.foodgasm.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Okta Dwi Priatna on 02/10/2015.
 */
public class InboxFragment extends BaseFragment {

    @BindView(R.id.linContainer)
    LinearLayout linContainer;
    @BindView(R.id.srlInboxList)
    SwipeRefreshLayout mSwipeRefreshLayout;

    public static LinearLayoutManager linLayoutManager;
    public static InboxAdapter mAdapter;
    public static RecyclerView rvInbox;
    List<Inbox> inboxList = new ArrayList<Inbox>();
    int limit = 20;

    DaoSession daoSession;
    OnInboxOrderListener mListener;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        daoSession = ((FoodGasmApp) getActivity().getApplication()).getDaoSession();

        rvInbox = (RecyclerView) getActivity().findViewById(R.id.rvInbox);
        mAdapter = new InboxAdapter(getActivity(), inboxList);
        linLayoutManager = new LinearLayoutManager(mContext);
        rvInbox.setHasFixedSize(true);
        rvInbox.setLayoutManager(linLayoutManager);
        rvInbox.setAdapter(mAdapter);

        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorAccent);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getInboxList();
            }
        });

        getInboxList();
    }

    void getInboxList() {
        inboxList.clear();
        int skip = inboxList.size();
        restClient = RestClient.getInstance();
        restClient.getApiService().getListInbox(Utils.getToken(mContext).getAccessToken(), skip, limit, new Callback<ApiResponseList<Inbox>>() {
            @Override
            public void success(ApiResponseList<Inbox> inboxApiResponseList, Response response) {
                if (mSwipeRefreshLayout != null)
                    mSwipeRefreshLayout.setRefreshing(false);


                inboxList.addAll(inboxApiResponseList.getData());
                rvInbox.setVisibility(View.VISIBLE);
                mAdapter.notifyDataSetChanged();

                if (inboxList.size() == 0) {
//                    mListener.setNewInboxCount(inboxList.size());
                    SPManager.saveString(getActivity(), Constants.Inbox.STATUS_INBOX, null);
                } else {
//                    mListener.setNewInboxCount(inboxList.size());
                    SPManager.saveString(getActivity(), Constants.Inbox.STATUS_INBOX, "ada inbox");
                }
            }

            @Override
            public void failure(RetrofitError error) {
                mSwipeRefreshLayout.setRefreshing(false);
                showToast(NetworkErrorUtil.getErrorMessage(error));
            }
        });
    }


    @Override
    protected int getLayout() {
        return R.layout.fragment_inbox;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mListener = (OnInboxOrderListener) context;
    }

    public interface OnInboxOrderListener {
        void setNewInboxCount(int newOrderCount);
    }
//    private void CheckPermission() {
//        Dexter.checkPermissions(new MultiplePermissionsListener() {
//            @Override
//            public void onPermissionsChecked(MultiplePermissionsReport report) {
//
//            }
//
//            @Override
//            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
//                token.continuePermissionRequest();
//            }
//        }, Manifest.permission.READ_PHONE_STATE);
//    }
}
