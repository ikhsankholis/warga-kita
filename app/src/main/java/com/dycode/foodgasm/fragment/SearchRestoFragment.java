package com.dycode.foodgasm.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.dycode.foodgasm.FoodGasmApp;
import com.dycode.foodgasm.R;
import com.dycode.foodgasm.activity.DetailRestaurantAct;
import com.dycode.foodgasm.adapters.SearchRestaurantAdapter;
import com.dycode.foodgasm.dao.DaoSession;
import com.dycode.foodgasm.dao.SearchHistory;
import com.dycode.foodgasm.dao.SearchHistoryDao;
import com.dycode.foodgasm.model.ApiResponse;
import com.dycode.foodgasm.model.ApiResponseList;
import com.dycode.foodgasm.model.DealsDetail;
import com.dycode.foodgasm.model.Restaurant;
import com.dycode.foodgasm.rest.RestClient;
import com.dycode.foodgasm.utils.NetworkErrorUtil;
import com.dycode.foodgasm.utils.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.greenrobot.dao.async.AsyncOperation;
import de.greenrobot.dao.async.AsyncOperationListener;
import de.greenrobot.dao.async.AsyncSession;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by asus pc on 11/12/2015.
 */
public class SearchRestoFragment extends BaseFragment implements AdapterView.OnItemClickListener {
    public static final String FLAG = "FLAG";
    public static final int FLAG_SEARCH_RESTO = 0;
    public static final int FLAG_SEARCH_RESTO_DEALS = 1;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.listRestaurant)
    ListView listRestaurant;
    @BindView(R.id.tvCancel)
    TextView tvCancel;
    @BindView(R.id.etSearch)
    EditText etSearch;

    FoodGasmApp foodGasmApp;
    DaoSession daoSession;

    SearchRestaurantAdapter mAdapter;
    List<Restaurant> restaurantList = new ArrayList<Restaurant>();
    String keyword, dealsId;
    int flag = 0;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ButterKnife.bind(getActivity());

        // Obtain the shared Tracker instance.
        getTracker("Restaurant Search");

        daoSession = ((FoodGasmApp) getActivity().getApplication()).getDaoSession();
        //flag = getActivity().getIntent().getExtras().getInt(FLAG);

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etSearch.setText("");
            }
        });
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                keyword = s.toString();
                if (flag == FLAG_SEARCH_RESTO) {
                    getSearchedRestaurantList(keyword + " ");
                    mAdapter.notifyDataSetChanged();
                } else if (flag == FLAG_SEARCH_RESTO_DEALS) {
                    dealsId = getActivity().getIntent().getExtras().getString("dealsId");
                    getSearchedDealsRestaurantList(keyword + " ");
                    mAdapter.notifyDataSetChanged();
                }
            }
        });

        etSearch.setOnEditorActionListener((v, actionId, event) -> {

            if (actionId == EditorInfo.IME_ACTION_DONE) {
                if (!TextUtils.isEmpty(v.getText().toString())) {
                    //Text entered
                    if (flag == FLAG_SEARCH_RESTO) {
                        addSearchHistoryRecord(v.getText().toString());
                        getSearchedRestaurantList(v.getText().toString());
                    } else if (flag == FLAG_SEARCH_RESTO_DEALS) {
                        dealsId = getActivity().getIntent().getExtras().getString("dealsId");
                        addSearchHistoryRecord(v.getText().toString());
                        getSearchedDealsRestaurantList(v.getText().toString());
                    }

                } else {
                    //no string
                }
            }
            return true;
        });

        mAdapter = new SearchRestaurantAdapter(getActivity(), restaurantList);
        listRestaurant.setAdapter(mAdapter);
        listRestaurant.setOnItemClickListener(SearchRestoFragment.this);
        for (SearchHistory searchHistory : daoSession.getSearchHistoryDao().queryBuilder().orderDesc(SearchHistoryDao.Properties.Created_at).where(SearchHistoryDao.Properties.Created_at.notEq(0)).list()) {
            restaurantList.add(new Restaurant(searchHistory.getResto_id(), searchHistory.getKeyword(), searchHistory.getResto_city()));
        }


    }

    void getSearchedRestaurantList(final String query) {

        restClient = RestClient.getInstance();
        restClient.getApiService().getListRestaurantNoLimit(Utils.getLatString(getActivity()), Utils.getLngString(getActivity()), query, "", query, 0, new Callback<ApiResponseList<Restaurant>>() {
            @Override
            public void success(ApiResponseList<Restaurant> restaurantApiResponseList, Response response) {
                if (restaurantApiResponseList.getMeta().getCode() == 200) {
                    restaurantList.clear();
                    restaurantList.addAll(restaurantApiResponseList.getData());
                    mAdapter.notifyDataSetChanged();
                    if (restaurantList.size() > 0) {
                        if (restaurantList.get(0).getName().equals(query)) {
                            SearchHistory searchHistory = new SearchHistory();
                            Long timeStamp = System.currentTimeMillis();
                            searchHistory.setKeyword(restaurantList.get(0).getName());
                            searchHistory.setResto_city(restaurantList.get(0).getAddress().getCity());
                            searchHistory.setCreated_at(timeStamp);
                            daoSession.getSearchHistoryDao().insertOrReplace(searchHistory);

                            Intent intent = new Intent(getActivity(), DetailRestaurantAct.class);
                            intent.putExtra(Restaurant.RESTAURANT, restaurantList.get(0));
                            startActivity(intent);
                            getActivity().overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                        }
                    } else {

                    }
                } else {
                    showAlertDialog("Failed", restaurantApiResponseList.getMeta().getMessage());
                }
            }

            @Override
            public void failure(RetrofitError error) {
//                            showToast("No Internet Connection");
            }
        });
//        restClient.getApiService().getListRestaurantNoLimit(Utils.getToken(getActivity()).getAccessToken(), Utils.getLatString(getActivity()), Utils.getLngString(getActivity()), "", "", query, 0, new Callback<ApiResponseList<Restaurant>>() {
//            @Override
//            public void success(ApiResponseList<Restaurant> restaurantApiResponseList, Response response) {
//                if (restaurantApiResponseList.getMeta().getCode() == 200) {
//                    restaurantList.clear();
//                    restaurantList.addAll(restaurantApiResponseList.getData());
//                    mAdapter.notifyDataSetChanged();
//                    restClient.getApiService().getListRestaurantNoLimit(Utils.getToken(getActivity()).getAccessToken(), Utils.getLatString(getActivity()), Utils.getLngString(getActivity()), query, "", "", 0, new Callback<ApiResponseList<Restaurant>>() {
//                        @Override
//                        public void success(ApiResponseList<Restaurant> restaurantApiResponseList, Response response) {
//                            if (restaurantApiResponseList.getMeta().getCode() == 200) {
////                                restaurantList.clear();
//                                restaurantList.addAll(restaurantApiResponseList.getData());
//                                mAdapter.notifyDataSetChanged();
//                                if (restaurantList.size() > 0) {
//                                    if (restaurantList.get(0).getName().equals(query)) {
//                                        SearchHistory searchHistory = new SearchHistory();
//                                        Long timeStamp = System.currentTimeMillis();
//                                        searchHistory.setKeyword(restaurantList.get(0).getName());
//                                        searchHistory.setResto_city(restaurantList.get(0).getAddress().getCity());
//                                        searchHistory.setCreated_at(timeStamp);
//                                        daoSession.getSearchHistoryDao().insertOrReplace(searchHistory);
//
//                                        Intent intent = new Intent(getActivity(), DetailRestaurantAct.class);
//                                        intent.putExtra(Restaurant.RESTAURANT, restaurantList.get(0));
//                                        startActivity(intent);
//                                        getActivity().overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
//                                    }
//                                } else {
//
//                                }
//                            } else {
//                                showAlertDialog("Failed", restaurantApiResponseList.getMeta().getMessage());
//                            }
//                        }
//
//                        @Override
//                        public void failure(RetrofitError error) {
////                            showToast("No Internet Connection");
//                        }
//                    });
////                    mAdapter.notifyDataSetChanged();
////                    if (restaurantList.size() > 0) {
////                        if (restaurantList.get(0).getName().equals(query)) {
////                            SearchHistory searchHistory = new SearchHistory();
////                            Long timeStamp = System.currentTimeMillis();
////                            searchHistory.setKeyword(restaurantList.get(0).getName());
////                            searchHistory.setResto_city(restaurantList.get(0).getAddress().getCity());
////                            searchHistory.setCreated_at(timeStamp);
////                            daoSession.getSearchHistoryDao().insertOrReplace(searchHistory);
////
////                            Intent intent = new Intent(getActivity(), DetailRestaurantAct.class);
////                            intent.putExtra(Restaurant.RESTAURANT, restaurantList.get(0));
////                            startActivity(intent);
////                            getActivity().overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
////                        }
////                    } else {
////
////                    }
//                } else {
//                    showAlertDialog("Failed", restaurantApiResponseList.getMeta().getMessage());
//                }
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
////                showAlertDialog("Failed", error.getMessage());
////                showToast("No Internet Connection");
//            }
//        });
    }

    void getSearchedDealsRestaurantList(String query) {
        restClient.getApiService().getDetailDeals(dealsId, Utils.getLatString(getActivity()), Utils.getLngString(getActivity()), 0, 0, new Callback<ApiResponse<DealsDetail>>() {
                    @Override
                    public void success(ApiResponse<DealsDetail> dealsDetailApiResponse, Response response) {
                        restaurantList.clear();
                        restaurantList.addAll(dealsDetailApiResponse.getData().getRestaurants());
                        mAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        showAlertDialog("Failed", NetworkErrorUtil.getErrorMessage(error));
                    }
                }

        );
    }

    void addSearchHistoryRecord(String query) {
        SearchHistory searchHistory = new SearchHistory();
        Long timeStamp = System.currentTimeMillis();
        searchHistory.setCreated_at(timeStamp);
        searchHistory.setKeyword(query);
        daoSession.getSearchHistoryDao().insertOrReplace(searchHistory);

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {

        if (restaurantList.get(position).getIsHistory() != null && restaurantList.get(position).getIsHistory()) {
            etSearch.setText(restaurantList.get(position).getName());
            etSearch.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    keyword = etSearch.getText().toString();
                    getSearchedRestaurantList(keyword);
                    mAdapter.notifyDataSetChanged();
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    keyword = etSearch.getText().toString();
                    getSearchedRestaurantList(keyword);
                    mAdapter.notifyDataSetChanged();
                }

                @Override
                public void afterTextChanged(Editable s) {
                    keyword = etSearch.getText().toString();
                    getSearchedRestaurantList(keyword);
                    mAdapter.notifyDataSetChanged();

                }
            });
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(etSearch,  InputMethodManager.SHOW_IMPLICIT);

            if(!TextUtils.isEmpty(restaurantList.get(position).getId())){
                Intent intent = new Intent(getActivity(), DetailRestaurantAct.class);
                intent.putExtra(Restaurant.RESTAURANT, restaurantList.get(position));
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
            }

        } else {
            SearchHistory searchHistory = new SearchHistory();
            Long timeStamp = System.currentTimeMillis();
            Restaurant restaurant = restaurantList.get(position);
            searchHistory.setResto_id(restaurant.getId());
            searchHistory.setKeyword(restaurant.getName());
            searchHistory.setResto_city(restaurant.getAddress().getCity());
            searchHistory.setCreated_at(timeStamp);

            //insert async to prevent lag
            AsyncSession asyncSession = daoSession.startAsyncSession();
            asyncSession.setListener(operation -> {
                // do whats needed
            });
            asyncSession.insertOrReplace(searchHistory);

            Intent intent = new Intent(getActivity(), DetailRestaurantAct.class);
            intent.putExtra(Restaurant.RESTAURANT, restaurantList.get(position));
            startActivity(intent);
//            getActivity().finish();
            getActivity().overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
        }
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_searched_restaurant;
    }
}
