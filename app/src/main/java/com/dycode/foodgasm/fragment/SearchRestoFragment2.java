package com.dycode.foodgasm.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dycode.foodgasm.R;
import com.dycode.foodgasm.activity.SearchRestoAct;
import com.dycode.foodgasm.model.ApiResponseList;
import com.dycode.foodgasm.model.SearchCategory;
import com.dycode.foodgasm.rest.RestClient;
import com.dycode.foodgasm.utils.SearchCategoryPicker;
import com.dycode.foodgasm.utils.SearchLocationPicker;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by asus pc on 11/12/2015.
 */
public class SearchRestoFragment2 extends BaseFragment {

    @BindView(R.id.l_cari)
    LinearLayout lCari;
    @BindView(R.id.edit_category)
    TextView editCategory;
    @BindView(R.id.edit_keyword)
    EditText editKeyword;
    @BindView(R.id.edit_location)
    TextView editLocation;

    private List categoryList;
    private List categoryLocation;

    private String idCategory = "";

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ButterKnife.bind(getActivity());

        getTracker("Restaurant Search 2");




        categoryList = new ArrayList();
        categoryLocation = new ArrayList();
        getCategoryResto();
        getCategoryLocation();
        lCari.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SearchRestoAct.class);
                intent.putExtra(SearchRestoAct.FLAG, SearchRestoAct.FLAG_SEARCH_RESTO);
                intent.putExtra(SearchRestoAct.KEYWORD, editKeyword.getText().toString());
                intent.putExtra(SearchRestoAct.LOCATION, editLocation.getText().toString());
                intent.putExtra(SearchRestoAct.CATEGORY, idCategory);
                if (editKeyword.equals("All category")) {
                    intent.putExtra(SearchRestoAct.CATEGORY, "");
                }
                if (editLocation.getText().toString().equals("All Location")) {
                    intent.putExtra(SearchRestoAct.LOCATION, "");
                }

                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
            }
        });
        editCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final SearchCategoryPicker categoryPicker = new SearchCategoryPicker(getActivity(), categoryList, new SearchCategoryPicker.OnDatePickedListener() {
                    @Override
                    public void onDatePickCompleted(String categoryId, String categoryName) {
                        Log.d("name category", categoryName);
                        Log.d("pos category", categoryId);
                        editCategory.setText(categoryName);
                        idCategory = categoryId;

                    }
                });
                categoryPicker.dayLoopView.setNotLoop();
                categoryPicker.showPopWin(getActivity());

                InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
            }
        });


        editLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final SearchLocationPicker categoryPicker = new SearchLocationPicker(getActivity(), categoryLocation, new SearchLocationPicker.OnDatePickedListener() {
                    @Override
                    public void onDatePickCompleted(String cityName) {
                        Log.d("Cek get city picker --->", cityName);
                        editLocation.setText(cityName);
                    }
                });
                categoryPicker.dayLoopView.setNotLoop();
                categoryPicker.showPopWin(getActivity());

                InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
            }
        });
    }

    private void getCategoryResto() {
        restClient = RestClient.getInstance();
        restClient.getApiService().getListCategory(new Callback<ApiResponseList<SearchCategory>>() {
            @Override
            public void success(ApiResponseList<SearchCategory> categoryApiResponseList, Response response) {
                categoryList.addAll(categoryApiResponseList.getData());


            }

            @Override
            public void failure(RetrofitError error) {
                showToast("No Internet Connection");
            }
        });
    }

    private void getCategoryLocation() {
        restClient = RestClient.getInstance();
        restClient.getApiService().getListLocation(new Callback<ApiResponseList<String>>() {
            @Override
            public void success(ApiResponseList<String> categoryApiResponseList, Response response) {
                categoryLocation.addAll(categoryApiResponseList.getData());


            }

            @Override
            public void failure(RetrofitError error) {
                showToast("No Internet Connection");
            }
        });
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_searched_restaurant2;
    }
}
