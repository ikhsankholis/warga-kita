package com.dycode.foodgasm.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.diegocarloslima.fgelv.lib.FloatingGroupExpandableListView;
import com.diegocarloslima.fgelv.lib.WrapperExpandableListAdapter;
import com.dycode.foodgasm.FoodGasmApp;
import com.dycode.foodgasm.R;
import com.dycode.foodgasm.activity.MainAct;
import com.dycode.foodgasm.activity.OrderDetailAct;
import com.dycode.foodgasm.adapters.ExpandableOrderAdapter;
import com.dycode.foodgasm.dao.DaoSession;
import com.dycode.foodgasm.dao.OrderHistoryDao;
import com.dycode.foodgasm.model.ApiResponseList;
import com.dycode.foodgasm.model.GroupOrderHistory;
import com.dycode.foodgasm.model.Order;
import com.dycode.foodgasm.model.OrderHistory;
import com.dycode.foodgasm.rest.RestClient;
import com.dycode.foodgasm.utils.FoodgasmDateUtil;
import com.dycode.foodgasm.utils.SPManager;
import com.dycode.foodgasm.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by asus pc on 19/10/2015.
 */
public class OrderFragment extends BaseFragment implements ExpandableListView.OnChildClickListener {


    @BindView(R.id.srlOrderHistoryList)
    SwipeRefreshLayout mSwipeRefreshLayout;
    @BindView(R.id.group_list_order)
    FloatingGroupExpandableListView mGroupListView;
    @BindView(R.id.text_message)
    TextView textMessage;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    ExpandableOrderAdapter mGroupAdapter;
    WrapperExpandableListAdapter mWrapperAdapter;
    List<GroupOrderHistory> groupOrderHistoryList = new ArrayList<>();

    DaoSession daoSession;

    public static OrderFragment newInstance() {
        OrderFragment orderFragment = new OrderFragment();
        return orderFragment;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        daoSession = ((FoodGasmApp) getActivity().getApplication()).getDaoSession();
        //init data
        setData();
        mGroupAdapter = new ExpandableOrderAdapter(mContext, groupOrderHistoryList);
        mWrapperAdapter = new WrapperExpandableListAdapter(mGroupAdapter);
        mGroupListView.setAdapter(mWrapperAdapter);
        expandAll();


        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorAccent);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getListOrder();
            }
        });
        mGroupListView.setOnChildClickListener(this);
        getListOrder();


    }

    void getListOrder() {
        restClient = RestClient.getInstance();
        restClient.getApiService().getListOrder(Utils.getToken(mContext).getAccessToken(), Utils.getToken(mContext).getUid(), "true", "tu", "1", "4", new Callback<ApiResponseList<OrderHistory>>() {
            @Override
            public void success(ApiResponseList<OrderHistory> listOrderApiResponseList, Response response) {
                progressBar.setVisibility(View.GONE);
                if (mSwipeRefreshLayout != null)
                    mSwipeRefreshLayout.setRefreshing(false);


                groupOrderHistoryList.clear();

                //if no data show message
                if (listOrderApiResponseList.getData().size() == 0) {
                    textMessage.setVisibility(View.VISIBLE);
                    mGroupListView.setVisibility(View.GONE);
                    return;
                } else {
                    textMessage.setVisibility(View.GONE);
                    mGroupListView.setVisibility(View.VISIBLE);
                    for (OrderHistory orderHistory : listOrderApiResponseList.getData()) {
                        com.dycode.foodgasm.dao.OrderHistory orderHistoryDao = new com.dycode.foodgasm.dao.OrderHistory();
                        orderHistoryDao.set_id(orderHistory.getId());
                        orderHistoryDao.setNumOfPeople(orderHistory.getNumOfPeople());
                        orderHistoryDao.setOrderStatus(orderHistory.getOrderStatus());
                        orderHistoryDao.setReason(orderHistory.getReason());
                        orderHistoryDao.setRestoId(orderHistory.getResto().getId());
                        orderHistoryDao.setRestoName(orderHistory.getResto().getName());
                        orderHistoryDao.setRestoPic(orderHistory.getResto().getPic());
                        orderHistoryDao.setUserId(orderHistory.getUser().getId());
                        orderHistoryDao.setUsername(orderHistory.getUser().getUsername());
                        orderHistoryDao.setBookingDate(FoodgasmDateUtil.getDate(orderHistory.getTu()));
                        orderHistoryDao.setVisitDate(FoodgasmDateUtil.getDate(orderHistory.getVisitDate()));
                        orderHistoryDao.setIsClicked(false);

                        daoSession.insertOrReplace(orderHistoryDao);
                    }
                    setData();
                    expandAll();

                }

                mGroupAdapter.notifyDataSetChanged();
                mWrapperAdapter.notifyDataSetChanged();

            }

            @Override
            public void failure(RetrofitError error) {
                progressBar.setVisibility(View.GONE);
                mSwipeRefreshLayout.setRefreshing(false);
                if (groupOrderHistoryList.size() == 0) {
                    textMessage.setVisibility(View.VISIBLE);
                    mGroupListView.setVisibility(View.GONE);
                }
                //todo generate error message
//                showToast("error");
            }
        });
    }

    private void setData() {
        if (daoSession.getOrderHistoryDao().queryBuilder().list().size() > 0) {
            groupOrderHistoryList.add(new GroupOrderHistory(mContext.getString(R.string.new_order), daoSession.getOrderHistoryDao().queryBuilder().orderDesc(OrderHistoryDao.Properties.BookingDate).where(OrderHistoryDao.Properties.OrderStatus.eq(0), OrderHistoryDao.Properties.RestoId.isNotNull()).list()));
            groupOrderHistoryList.add(new GroupOrderHistory(mContext.getString(R.string.last_order), daoSession.getOrderHistoryDao().queryBuilder().orderDesc(OrderHistoryDao.Properties.BookingDate).where(OrderHistoryDao.Properties.OrderStatus.notEq(0), OrderHistoryDao.Properties.RestoId.isNotNull()).list()));
            mGroupListView.setVisibility(View.VISIBLE);
        }
    }

    private void expandAll() {
        for (int pos = 0; pos < groupOrderHistoryList.size(); pos++) {
            mGroupListView.expandGroup(pos);
        }

    }


    @Override
    protected int getLayout() {
        return R.layout.fragment_orders;
    }

    @Override
    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
        SPManager.saveString(mContext, "orderDetail", "order");
        groupOrderHistoryList.get(groupPosition).getOrderHistoryList().get(childPosition).setIsClicked(true);
        Intent intent = new Intent(getActivity(), OrderDetailAct.class);
        intent.putExtra(Order.ORDER, groupOrderHistoryList.get(groupPosition).getOrderHistoryList().get(childPosition).get_id());
        startActivityForResult(intent, 0);
        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }

    @Override
    public void onResume() {
        super.onResume();
        progressBar.setVisibility(View.GONE);
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();

    }


}
