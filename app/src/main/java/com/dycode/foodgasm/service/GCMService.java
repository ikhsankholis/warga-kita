package com.dycode.foodgasm.service;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.dycode.foodgasm.R;
import com.dycode.foodgasm.activity.BookOrderCompleteAct;
import com.dycode.foodgasm.activity.MainAct;
import com.dycode.foodgasm.model.ApiResponse;
import com.dycode.foodgasm.model.OrderDetail;
import com.dycode.foodgasm.rest.RestClient;
import com.dycode.foodgasm.utils.Constants;
import com.dycode.foodgasm.utils.SPManager;
import com.dycode.foodgasm.utils.Utils;
import com.google.android.gms.gcm.GcmNetworkManager;
import com.google.android.gms.gcm.GcmTaskService;
import com.google.android.gms.gcm.TaskParams;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by yogi on 3/3/17.
 */

public class GCMService extends GcmTaskService {

    protected RestClient restClient = RestClient.getInstance();
    Context mContext;
    private Bitmap icon;

    public static final int FLAG_WAIT_CONFIRMATION = 0;
    public static final int FLAG_ORDER_DECLINED = 2;
    public static final int FLAG_ORDER_ACCEPTED = 3;
    public static final int FLAG_TAKE_AWAY = 4;
    public static final int FLAG_BOOKING_ACCEPTED = 5;
    public static final int FLAG_ORDER_EXPIRED = 6;
    public static final int FLAG_ORDER_CANCELLED = 7;

    @Override
    public int onRunTask(TaskParams taskParams) {

        mContext = this.getApplicationContext();
        Handler h = new Handler(getMainLooper());
        h.post(new Runnable() {
            @Override
            public void run() {
                Log.d("get order Data > ", " running in background");
                getData();

            }
        });
        return GcmNetworkManager.RESULT_SUCCESS;
    }

    public void getData() {
        String orderId = SPManager.getString(mContext, "bookingCode");
        restClient.getApiService().getOrderDetail(orderId, Utils.getToken(mContext).getAccessToken(), new Callback<ApiResponse<OrderDetail>>() {
            @Override
            public void success(ApiResponse<OrderDetail> orderDetailApiResponse, Response response) {
                Log.d(" RESPONSE BLUE > ", response.getReason());
                cancelAll(mContext);
                if (orderDetailApiResponse.getData().getOrderStatus() != 0) {
//                    Intent intent = new Intent(GCMService.this, MainAct.class);
//                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                    intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
//                    startActivity(intent);

                    icon = BitmapFactory.decodeResource(mContext.getResources(),
                            R.mipmap.ic_launcher);

                    NotificationManager mNotificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
                    Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                    Intent i = new Intent();
                    boolean sendNotif = true;

                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

                    int type = orderDetailApiResponse.getData().getOrderStatus();

                    if (type == FLAG_ORDER_DECLINED) {
                        Log.d(" flag dd", " >  flag dec");
                        SPManager.saveString(mContext, "stack", "0");

                        i = new Intent(mContext, MainAct.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
                                Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        i.putExtra(BookOrderCompleteAct.FLAG, BookOrderCompleteAct.FLAG_ORDER_DECLINED);
                    } else if (type == FLAG_ORDER_CANCELLED) {
                        Log.d(" flag cancel", " >  flag canceled");
                        SPManager.saveString(mContext, "stack", "0");

                        i = new Intent(mContext, BookOrderCompleteAct.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
                                Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        i.putExtra(BookOrderCompleteAct.FLAG, BookOrderCompleteAct.FLAG_ORDER_CANCELLED);
                    } else if (type == FLAG_WAIT_CONFIRMATION) {
                        SPManager.saveString(mContext, "stack", "0");
                        SPManager.saveString(mContext, Constants.Inbox.STATUS_INBOX, "");
                        i = new Intent(mContext, MainAct.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
                                Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    } else if (type == FLAG_BOOKING_ACCEPTED) {
                        SPManager.saveString(mContext, Constants.Inbox.STATUS_INBOX, "booking_accepted");

                        i = new Intent(mContext, BookOrderCompleteAct.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
                                Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        i.putExtra(BookOrderCompleteAct.FLAG, BookOrderCompleteAct.FLAG_BOOKING_ACCEPTED);
                    } else if (type == FLAG_ORDER_EXPIRED) {

                        i = new Intent(mContext, BookOrderCompleteAct.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
                                Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        i.putExtra(BookOrderCompleteAct.FLAG, BookOrderCompleteAct.FLAG_ORDER_EXPIRED);
                    } else {
                        Log.d(" flag Main", " >  Activity");
                        SPManager.saveString(mContext, "stack", "0");
                        i = new Intent(mContext, MainAct.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
                                Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    }


                    Object desc = orderDetailApiResponse.getData().getReason();
                    String text = "";
                    if (desc != null) {
                        desc = orderDetailApiResponse.getData().getReason().toString();
                        text = desc.toString();
                    } else {
                        text = "Your order has been accepted";
                    }

                    PendingIntent contentIntent = PendingIntent.getActivity(mContext, 0, i, PendingIntent.FLAG_UPDATE_CURRENT);
                    NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(mContext)
                            .setContentTitle("Order " + orderDetailApiResponse.getData().getCode())
                            .setAutoCancel(true)
                            .setSmallIcon(getNotificationIcon())
                            .setLargeIcon(icon)
                            .setStyle(new NotificationCompat.BigTextStyle().bigText(text))
                            .setContentText(text)
                            .setContentIntent(contentIntent)
                            .setVibrate(new long[]{1000})
                            .setLights(Color.CYAN, 3000, 3000)
                            .setSound(uri);

                    mNotificationManager.notify(0, mBuilder.build());
                }
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });

    }

    public static void cancelAll(Context context) {
        GcmNetworkManager
                .getInstance(context)
                .cancelAllTasks(GCMService.class);
    }

    private int getNotificationIcon() {
        boolean useWhiteIcon = (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP);
        return useWhiteIcon ? R.mipmap.ic_notification_user : R.mipmap.ic_launcher;
    }
}
