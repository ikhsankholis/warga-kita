package com.dycode.foodgasm.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by asus pc on 24/11/2015.
 */
public class Open implements Parcelable {
    public static final String OPEN = "OPEN";

    @SerializedName("closetime")
    @Expose
    private String closetime;
    @SerializedName("opentime")
    @Expose
    private String opentime;
    @SerializedName("day")
    @Expose
    private List<String> day = new ArrayList<String>();

    /**
     *
     * @return
     * The closetime
     */
    public String getClosetime() {
        return closetime;
    }

    /**
     *
     * @param closetime
     * The closetime
     */
    public void setClosetime(String closetime) {
        this.closetime = closetime;
    }

    /**
     *
     * @return
     * The opentime
     */
    public String getOpentime() {
        return opentime;
    }

    /**
     *
     * @param opentime
     * The opentime
     */
    public void setOpentime(String opentime) {
        this.opentime = opentime;
    }

    /**
     *
     * @return
     * The day
     */
    public List<String> getDay() {
        return day;
    }

    /**
     *
     * @param day
     * The day
     */
    public void setDay(List<String> day) {
        this.day = day;
    }

    protected Open(Parcel in) {
        in.readList(day, Open.class.getClassLoader());
        opentime = in.readString();
        closetime = in.readString();

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(day);
        dest.writeString(opentime);
        dest.writeString(closetime);

    }

    public static final Parcelable.Creator<Open> CREATOR = new Parcelable.Creator<Open>() {
        public Open createFromParcel(Parcel in) {
            return new Open(in);
        }

        public Open[] newArray(int size) {
            return new Open[size];
        }
    };
}
