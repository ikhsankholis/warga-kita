package com.dycode.foodgasm.model;

/**
 * Created by asus pc on 28/12/2015.
 */
public class PostPayment {
    private String token;
    private String bank;

    public PostPayment(String token, String bank) {
        this.token = token;
        this.bank = bank;
    }
}
