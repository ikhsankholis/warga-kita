package com.dycode.foodgasm.model;

import com.dycode.foodgasm.dao.Menu;

import java.util.ArrayList;

/**
 * Created by faerulsalamun on 5/17/16.
 */
public class EventBusAction {
    private String name;
    private String personAmount;
    private String day;
    private String minute;
    private ArrayList<com.dycode.foodgasm.dao.Menu> menuArrayList = new ArrayList<>();

    public EventBusAction(String name, String personAmount, String day, String minute) {
        this.name = name;
        this.personAmount = personAmount;
        this.day = day;
        this.minute = minute;
    }

    public EventBusAction(String name, ArrayList<com.dycode.foodgasm.dao.Menu> menuArrayList) {
        this.name = name;
        this.menuArrayList = menuArrayList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<com.dycode.foodgasm.dao.Menu> getMenuArrayList() {
        return menuArrayList;
    }

    public void setMenuArrayList(ArrayList<Menu> menuArrayList) {
        this.menuArrayList = menuArrayList;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getMinute() {
        return minute;
    }

    public void setMinute(String minute) {
        this.minute = minute;
    }

    public String getPersonAmount() {
        return personAmount;
    }

    public void setPersonAmount(String personAmount) {
        this.personAmount = personAmount;
    }
}
