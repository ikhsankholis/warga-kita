package com.dycode.foodgasm.model;

/**
 * Created by asus pc on 29/02/2016.
 */
public class DirectPay {


    private String amount;
    private String resto;
    private String paymentWith;
    private String paymentCredential;

    public DirectPay(String amount, String resto, String paymentWith, String paymentCredential) {
        this.amount = amount;
        this.resto = resto;
        this.paymentWith = paymentWith;
        this.paymentCredential = paymentCredential;
    }
}
