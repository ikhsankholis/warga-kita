package com.dycode.foodgasm.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by asus pc on 03/11/2015.
 */
public class Menu implements Parcelable {
    public static final String MENU = "MENU";

    @SerializedName("_id")
    @Expose
    String menuID;
    @SerializedName("ts")
    @Expose
    private String ts;
    @SerializedName("name")
    @Expose
    String menuName;
    @SerializedName("price")
    @Expose
    private int menuPrice;
    @SerializedName("description")
    @Expose
    private String menuDesc;
    @SerializedName("image")
    @Expose
    private String menuPicBig;
    @SerializedName("amount")
    @Expose
    private int amount;
    @SerializedName("__v")
    @Expose
    private int V;
    @SerializedName("available")
    @Expose
    private Boolean available;
    @SerializedName("discount")
    @Expose
    private int discountPrice;
    @SerializedName("subtotalPrice")
    @Expose
    private int subtotalPrice;
    @SerializedName("taxPrice")
    @Expose
    private int taxPrice;


    public Menu(String menuID, String menuName, int menuPrice, String menuDesc, String menuPicBig,String taxPrice) {
        this.menuID = menuID;
        this.menuName = menuName;
        this.menuPrice = menuPrice;
        this.menuDesc = menuDesc;
        this.menuPicBig = menuPicBig;
        this.amount = 0;
        this.taxPrice = 0;

    }

    /**
     * @return The Id
     */
    public String getId() {
        return menuID;
    }

    /**
     * @param Id The _id
     */
    public void setId(String Id) {
        this.menuID = Id;
    }

    /**
     * @return The ts
     */
    public String getTs() {
        return ts;
    }

    /**
     * @param ts The ts
     */
    public void setTs(String ts) {
        this.ts = ts;
    }

    /**
     * @return The name
     */
    public String getName() {
        return menuName;
    }

    /**
     * @param name The name
     */
    public void setName(String name) {
        this.menuName = name;
    }

    /**
     * @return The price
     */
    public int getPrice() {
        return menuPrice;
    }

    /**
     * @param price The price
     */
    public void setPrice(int price) {
        this.menuPrice = price;
    }

    /**
     * @return The description
     */
    public String getDescription() {
        return menuDesc;
    }

    /**
     * @param description The description
     */
    public void setDescription(String description) {
        this.menuDesc = description;
    }

    /**
     * @return The image
     */
    public String getImage() {
        return menuPicBig;
    }

    /**
     * @param image The image
     */
    public void setImage(String image) {
        this.menuPicBig = image;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    /**
     * @return The V
     */
    public int getV() {
        return V;
    }

    /**
     * @param V The __v
     */
    public void setV(int V) {
        this.V = V;
    }

    /**
     * @return The available
     */
    public Boolean getAvailable() {
        return available;
    }

    /**
     * @param available The available
     */
    public void setAvailable(Boolean available) {
        this.available = available;
    }

    /**
     * @return The discountPrice
     */
    public int getDiscountPrice() {
        return discountPrice;
    }

    /**
     * @param discountPrice The discountPrice
     */
    public void setDiscountPrice(int discountPrice) {
        this.discountPrice = discountPrice;
    }

    /**
     * @return The subtotalPrice
     */
    public int getSubtotalPrice() {
        return subtotalPrice;
    }

    /**
     * @param subtotalPrice The subtotalPrice
     */
    public void setSubtotalPrice(int subtotalPrice) {
        this.subtotalPrice = subtotalPrice;
    }

    public void setTaxPrice(int taxPrice) {
        this.taxPrice = taxPrice;
    }

    public int getTaxPrice() {
        return taxPrice;
    }

    protected Menu(Parcel in) {
        menuID = in.readString();
        menuName = in.readString();
        menuPrice = in.readInt();
        menuDesc = in.readString();
        menuPicBig = in.readString();
        amount = in.readInt();
        discountPrice = in.readInt();
        subtotalPrice = in.readInt();

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(menuID);
        dest.writeString(menuName);
        dest.writeInt(menuPrice);
        dest.writeString(menuDesc);
        dest.writeString(menuPicBig);
        dest.writeInt(amount);
        dest.writeInt(discountPrice);
        dest.writeInt(subtotalPrice);
        dest.writeInt(taxPrice);

    }

    public static final Parcelable.Creator<Menu> CREATOR = new Parcelable.Creator<Menu>() {
        public Menu createFromParcel(Parcel in) {
            return new Menu(in);
        }

        public Menu[] newArray(int size) {
            return new Menu[size];
        }
    };
}
