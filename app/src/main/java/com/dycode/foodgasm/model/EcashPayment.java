package com.dycode.foodgasm.model;

/**
 * Created by yogi on 3/10/17.
 */

public class EcashPayment {

    String msisdn;
    public EcashPayment(String msisdn) {
        this.msisdn = msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getMsisdn() {
        return msisdn;
    }
}
