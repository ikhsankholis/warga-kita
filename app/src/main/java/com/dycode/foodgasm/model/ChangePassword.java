package com.dycode.foodgasm.model;

/**
 * Created by asus pc on 26/02/2016.
 */
public class ChangePassword {
    private String currentPassword;
    private String newPassword;
    private String passwordConfirmation;

    public ChangePassword(String currentPassword, String newPassword, String passwordConfirmation){
        this.currentPassword = currentPassword;
        this.newPassword = newPassword;
        this.passwordConfirmation = passwordConfirmation;
    }
}
