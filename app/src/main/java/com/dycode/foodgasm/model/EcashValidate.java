package com.dycode.foodgasm.model;

/**
 * Created by yogi on 3/10/17.
 */

public class EcashValidate {

    String mobileno;
    String status;

    public EcashValidate(String mobileno, String status) {
        this.mobileno = mobileno;
        this.status = status;
    }

    public String getMobileno() {
        return mobileno;
    }

    public void setMobileno(String mobileno) {
        this.mobileno = mobileno;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


}
