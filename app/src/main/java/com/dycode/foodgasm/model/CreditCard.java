package com.dycode.foodgasm.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by asus pc on 29/02/2016.
 */
public class CreditCard {
    @SerializedName("_id")
    @Expose
    private String Id;
    @SerializedName("ts")
    @Expose
    private String ts;
    @SerializedName("tu")
    @Expose
    private String tu;
    @SerializedName("transaction_id")
    @Expose
    private String transactionId;
    @SerializedName("saved_token_id")
    @Expose
    private String savedTokenId;
    @SerializedName("masked_card")
    @Expose
    private String maskedCard;
    @SerializedName("user")
    @Expose
    private String user;
    @SerializedName("__v")
    @Expose
    private Integer V;
    @SerializedName("state")
    @Expose
    private Boolean state;
    @SerializedName("redirect_url")
    @Expose
    private String redirect_url;
    @SerializedName("token_id")
    @Expose
    private String tokenId;
    @SerializedName("bank")
    @Expose
    private String bank;
    @SerializedName("customer_id")
    @Expose
    private String customer_id;
    @SerializedName("provider")
    @Expose
    private String provider;


    private Boolean isEditSelected = false;

    /**
     *
     * @return
     * The Id
     */
    public String getId() {
        return Id;
    }

    /**
     *
     * @param Id
     * The _id
     */
    public void setId(String Id) {
        this.Id = Id;
    }

    /**
     *
     * @return
     * The ts
     */
    public String getTs() {
        return ts;
    }

    /**
     *
     * @param ts
     * The ts
     */
    public void setTs(String ts) {
        this.ts = ts;
    }

    /**
     *
     * @return
     * The tu
     */
    public String getTu() {
        return tu;
    }

    /**
     *
     * @param tu
     * The tu
     */
    public void setTu(String tu) {
        this.tu = tu;
    }

    /**
     *
     * @return
     * The transactionId
     */
    public String getTransactionId() {
        return transactionId;
    }

    /**
     *
     * @param transactionId
     * The transaction_id
     */
    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    /**
     *
     * @return
     * The savedTokenId
     */
    public String getSavedTokenId() {
        return savedTokenId;
    }

    /**
     *
     * @param savedTokenId
     * The saved_token_id
     */
    public void setSavedTokenId(String savedTokenId) {
        this.savedTokenId = savedTokenId;
    }

    /**
     *
     * @return
     * The maskedCard
     */
    public String getMaskedCard() {
        return maskedCard;
    }

    /**
     *
     * @param maskedCard
     * The masked_card
     */
    public void setMaskedCard(String maskedCard) {
        this.maskedCard = maskedCard;
    }

    /**
     *
     * @return
     * The user
     */
    public String getUser() {
        return user;
    }

    /**
     *
     * @param user
     * The user
     */
    public void setUser(String user) {
        this.user = user;
    }

    /**
     *
     * @return
     * The V
     */
    public Integer getV() {
        return V;
    }

    /**
     *
     * @param V
     * The __v
     */
    public void setV(Integer V) {
        this.V = V;
    }

    /**
     *
     * @return
     * The state
     */
    public Boolean getState() {
        return state;
    }

    /**
     *
     * @param state
     * The state
     */
    public void setState(Boolean state) {
        this.state = state;
    }

    public Boolean getIsEditSelected() {
        return isEditSelected;
    }

    public void setIsEditSelected(Boolean isEditSelected) {
        this.isEditSelected = isEditSelected;
    }

    /**
     *
     * @return
     * The savedTokenId
     */
    public String getTokenId() {
        return tokenId;
    }

    /**
     *
     * @param tokenId
     * The saved_token_id
     */
    public void setTokenId(String tokenId) {
        this.tokenId = tokenId;
    }

    /**
     *
     * @return
     * The redirect_url
     */
    public String getRedirect_url() {
        return redirect_url;
    }

    /**
     *
     * @param redirect_url
     * The redirect_url
     */
    public void setRedirect_url(String redirect_url) {
        this.redirect_url = redirect_url;
    }

    /**
     *
     * @return
     * The bank
     */
    public String getBank() {
        return bank;
    }

    /**
     *
     * @param bank
     * The bank
     */
    public void setBank(String bank) {
        this.bank = bank;
    }

    public void setCustomer_id(String customer_id) {
        this.customer_id = customer_id;
    }

    public String getCustomer_id() {
        return customer_id;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getProvider() {
        return provider;
    }
}
