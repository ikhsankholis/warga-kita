package com.dycode.foodgasm.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by asus pc on 29/02/2016.
 */
public class DokuWalletBalanceResponse {

    //    "dokuId": "1546345120",
//            "lastBalance": 829050,
//            "clientId": "1620",
//            "responseCode": "0000",
    @SerializedName("dokuId")
    @Expose
    private String dokuId;
    @SerializedName("lastBalance")
    @Expose
    private String lastBalance;
    @SerializedName("clientId")
    @Expose
    private String clientId;
    @SerializedName("responseCode")
    @Expose
    private String responseCode;

    public String getDokuId() {
        return dokuId;
    }

    public void setDokuId(String dokuId) {
        this.dokuId = dokuId;
    }

    public String getLastBalance() {
        return lastBalance;
    }

    public void setLastBalance(String lastBalance) {
        this.lastBalance = lastBalance;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }
}
