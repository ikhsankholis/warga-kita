package com.dycode.foodgasm.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by asus pc on 01/02/2016.
 */
public class Times {
    @SerializedName("times")
    @Expose
    private List<Time> times = new ArrayList<Time>();

    /**
     *
     * @return
     * The times
     */
    public List<Time> getTimes() {
        return times;
    }

    /**
     *
     * @param times
     * The times
     */
    public void setTimes(List<Time> times) {
        this.times = times;
    }
}
