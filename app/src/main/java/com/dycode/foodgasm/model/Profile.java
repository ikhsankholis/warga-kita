package com.dycode.foodgasm.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by asus pc on 10/11/2015.
 */
public class Profile {
    public static final String PROFILE = "PROFILE";

    @SerializedName("_id")
    @Expose
    private String Id;
    @SerializedName("ts")
    @Expose
    private String ts;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("provider")
    @Expose
    private String provider;
    @SerializedName("platform")
    @Expose
    private String platform;
    @SerializedName("connected")
    @Expose
    private Connected connected;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("role")
    @Expose
    private List<String> role = new ArrayList<String>();
    @SerializedName("profilePicture")
    @Expose
    private String profilePicture;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("fullname")
    @Expose
    private String fullname;
    @SerializedName("userPhoneVerified")
    @Expose
    private String userPhoneVerified;

    /**
     *
     * @return
     * The Id
     */
    public String getId() {
        return Id;
    }

    /**
     *
     * @param Id
     * The _id
     */
    public void setId(String Id) {
        this.Id = Id;
    }

    /**
     *
     * @return
     * The ts
     */
    public String getTs() {
        return ts;
    }

    /**
     *
     * @param ts
     * The ts
     */
    public void setTs(String ts) {
        this.ts = ts;
    }

    /**
     *
     * @return
     * The email
     */
    public String getEmail() {
        return email;
    }

    /**
     *
     * @param email
     * The email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     *
     * @return
     * The username
     */
    public String getUsername() {
        return username;
    }

    /**
     *
     * @param username
     * The username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     *
     * @return
     * The provider
     */
    public String getProvider() {
        return provider;
    }

    /**
     *
     * @param provider
     * The provider
     */
    public void setProvider(String provider) {
        this.provider = provider;
    }

    /**
     *
     * @return
     * The platform
     */
    public String getPlatform() {
        return platform;
    }

    /**
     *
     * @param platform
     * The platform
     */
    public void setPlatform(String platform) {
        this.platform = platform;
    }

    /**
     *
     * @return
     * The connected
     */
    public Connected getConnected() {
        return connected;
    }

    /**
     *
     * @param connected
     * The connected
     */
    public void setConnected(Connected connected) {
        this.connected = connected;
    }

    /**
     *
     * @return
     * The state
     */
    public String getState() {
        return state;
    }

    /**
     *
     * @param state
     * The state
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     *
     * @return
     * The role
     */
    public List<String> getRole() {
        return role;
    }

    /**
     *
     * @param role
     * The role
     */
    public void setRole(List<String> role) {
        this.role = role;
    }

    /**
     *
     * @return
     * The profilePicture
     */
    public String getProfilePicture() {
        return profilePicture;
    }

    /**
     *
     * @param profilePicture
     * The profilePicture
     */
    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    /**
     *
     * @return
     * The phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     *
     * @param phone
     * The phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     *
     * @return
     * The fullname
     */
    public String getFullname() {
        return fullname;
    }

    /**
     *
     * @param fullname
     * The fullname
     */
    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public void setUserPhoneVerified(String userPhoneVerified) {
        this.userPhoneVerified = userPhoneVerified;
    }

    public String getUserPhoneVerified() {
        return userPhoneVerified;
    }
}
