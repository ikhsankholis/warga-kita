package com.dycode.foodgasm.model;

import java.util.List;

/**
 * Created by asus pc on 03/12/2015.
 */
public class PostComment {
    private String resto;
    private int rating;
    private String comment;
    private List<String> reviewPoint;

    public PostComment(String resto, int rating, String comment, List<String> reviewPoint) {
        this.resto = resto;
        this.rating = rating;
        this.comment = comment;
        this.reviewPoint = reviewPoint;
    }
}
