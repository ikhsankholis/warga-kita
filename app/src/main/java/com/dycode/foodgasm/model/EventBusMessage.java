package com.dycode.foodgasm.model;

/**
 * Created by faerulsalamun on 5/17/16.
 */
public class EventBusMessage {
    public String data;


    public EventBusMessage(String data) {
        this.data = data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getData() {
        return data;
    }
}
