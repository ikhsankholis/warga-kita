package com.dycode.foodgasm.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by asus pc on 22/12/2015.
 */
public class OrderHistory implements Parcelable {

    @SerializedName("_id")
    @Expose
    private String Id;
    @SerializedName("ts")
    @Expose
    private String ts;
    @SerializedName("tu")
    @Expose
    private String tu;
    @SerializedName("user")
    @Expose
    private User user;
    @SerializedName("resto")
    @Expose
    private Restaurant resto;
    @SerializedName("numOfPeople")
    @Expose
    private Integer numOfPeople;
    @SerializedName("__v")
    @Expose
    private Integer V;
    @SerializedName("note")
    @Expose
    private Object note;
    @SerializedName("preorder")
    @Expose
    private Preorder preorder;
    @SerializedName("booking")
    @Expose
    private Booking booking;
    @SerializedName("reason")
    @Expose
    private String reason;
    @SerializedName("orderStatus")
    @Expose
    private Integer orderStatus;
    @SerializedName("paymentStatus")
    @Expose
    private Boolean paymentStatus;
    @SerializedName("price")
    @Expose
    private Price price;
    @SerializedName("visitDate")
    @Expose
    private String visitDate;
    @SerializedName("bookingDate")
    @Expose
    private String bookingDate;
    @SerializedName("code")
    @Expose
    private String code;

    /**
     *
     * @return
     * The Id
     */
    public String getId() {
        return Id;
    }

    /**
     *
     * @param Id
     * The _id
     */
    public void setId(String Id) {
        this.Id = Id;
    }

    /**
     *
     * @return
     * The ts
     */
    public String getTs() {
        return ts;
    }

    /**
     *
     * @param ts
     * The ts
     */
    public void setTs(String ts) {
        this.ts = ts;
    }

    /**
     *
     * @return
     * The tu
     */
    public String getTu() {
        return tu;
    }

    /**
     *
     * @param tu
     * The tu
     */
    public void setTu(String tu) {
        this.tu = tu;
    }

    /**
     *
     * @return
     * The user
     */
    public User getUser() {
        return user;
    }

    /**
     *
     * @param user
     * The user
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     *
     * @return
     * The resto
     */
    public Restaurant getResto() {
        return resto;
    }

    /**
     *
     * @param resto
     * The resto
     */
    public void setResto(Restaurant resto) {
        this.resto = resto;
    }

    /**
     *
     * @return
     * The numOfPeople
     */
    public Integer getNumOfPeople() {
        return numOfPeople;
    }

    /**
     *
     * @param numOfPeople
     * The numOfPeople
     */
    public void setNumOfPeople(Integer numOfPeople) {
        this.numOfPeople = numOfPeople;
    }

    /**
     *
     * @return
     * The V
     */
    public Integer getV() {
        return V;
    }

    /**
     *
     * @param V
     * The __v
     */
    public void setV(Integer V) {
        this.V = V;
    }

    /**
     *
     * @return
     * The note
     */
    public Object getNote() {
        return note;
    }

    /**
     *
     * @param note
     * The note
     */
    public void setNote(Object note) {
        this.note = note;
    }

    /**
     *
     * @return
     * The preorder
     */
    public Preorder getPreorder() {
        return preorder;
    }

    /**
     *
     * @param preorder
     * The preorder
     */
    public void setPreorder(Preorder preorder) {
        this.preorder = preorder;
    }

    /**
     *
     * @return
     * The booking
     */
    public Booking getBooking() {
        return booking;
    }

    /**
     *
     * @param booking
     * The booking
     */
    public void setBooking(Booking booking) {
        this.booking = booking;
    }

    /**
     *
     * @return
     * The reason
     */
    public String getReason() {
        return reason;
    }

    /**
     *
     * @param reason
     * The reason
     */
    public void setReason(String reason) {
        this.reason = reason;
    }

    /**
     *
     * @return
     * The orderStatus
     */
    public Integer getOrderStatus() {
        return orderStatus;
    }

    /**
     *
     * @param orderStatus
     * The orderStatus
     */
    public void setOrderStatus(Integer orderStatus) {
        this.orderStatus = orderStatus;
    }

    /**
     *
     * @return
     * The paymentStatus
     */
    public Boolean getPaymentStatus() {
        return paymentStatus;
    }

    /**
     *
     * @param paymentStatus
     * The paymentStatus
     */
    public void setPaymentStatus(Boolean paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    /**
     *
     * @return
     * The price
     */
    public Price getPrice() {
        return price;
    }

    /**
     *
     * @param price
     * The price
     */
    public void setPrice(Price price) {
        this.price = price;
    }

    /**
     *
     * @return
     * The visitDate
     */
    public String getVisitDate() {
        return visitDate;
    }

    /**
     *
     * @param visitDate
     * The visitDate
     */
    public void setVisitDate(String visitDate) {
        this.visitDate = visitDate;
    }

    /**
     *
     * @return
     * The bookingDate
     */
    public String getBookingDate() {
        return bookingDate;
    }

    /**
     *
     * @param bookingDate
     * The bookingDate
     */
    public void setBookingDate(String bookingDate) {
        this.bookingDate = bookingDate;
    }

    /**
     *
     * @return
     * The code
     */
    public String getCode() {
        return code;
    }

    /**
     *
     * @param code
     * The code
     */
    public void setCode(String code) {
        this.code = code;
    }

    protected OrderHistory(Parcel in) {
        Id = in.readString();
        user = in.readParcelable(User.class.getClassLoader());
        resto = in.readParcelable(Restaurant.class.getClassLoader());
        numOfPeople = in.readInt();
        orderStatus = in.readInt();
        visitDate = in.readString();

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(Id);
        dest.writeParcelable(user, flags);
        dest.writeParcelable(resto, flags);
        dest.writeInt(numOfPeople);
        dest.writeInt(orderStatus);
        dest.writeString(visitDate);

    }

    public static final Parcelable.Creator<OrderHistory> CREATOR = new Parcelable.Creator<OrderHistory>() {
        public OrderHistory createFromParcel(Parcel in) {
            return new OrderHistory(in);
        }

        public OrderHistory[] newArray(int size) {
            return new OrderHistory[size];
        }
    };
}
