package com.dycode.foodgasm.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by asus pc on 05/11/2015.
 */
public class Gallery implements Parcelable {
    public static final String GALLERY = "GALLERY";

    @SerializedName("_id")
    @Expose
    String pictureId;
    @SerializedName("ts")
    @Expose
    private String ts;
    @SerializedName("pic")
    @Expose
    String pictureBig;


    public Gallery(String pictureId, String ts, String pictureBig) {
        this.pictureId = pictureId;
        this.ts = ts;
        this.pictureBig = pictureBig;
    }

    /**
     * @return The Id
     */
    public String getId() {
        return pictureId;
    }

    /**
     * @param Id The _id
     */
    public void setId(String Id) {
        this.pictureId = Id;
    }

    /**
     * @return The ts
     */
    public String getTs() {
        return ts;
    }

    /**
     * @param ts The ts
     */
    public void setTs(String ts) {
        this.ts = ts;
    }

    /**
     * @return The pic
     */
    public String getPic() {
        return pictureBig;
    }

    /**
     * @param pic The pic
     */
    public void setPic(String pic) {
        this.pictureBig = pic;
    }


    protected Gallery(Parcel in) {
        pictureId = in.readString();
        ts = in.readString();
        pictureBig = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(pictureId);
        dest.writeString(ts);
        dest.writeString(pictureBig);
    }

    public static final Parcelable.Creator<Gallery> CREATOR = new Parcelable.Creator<Gallery>() {
        public Gallery createFromParcel(Parcel in) {
            return new Gallery(in);
        }

        public Gallery[] newArray(int size) {
            return new Gallery[size];
        }
    };
}

