package com.dycode.foodgasm.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by asus pc on 29/02/2016.
 */
public class DokuWalletRegData {


    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("dokuId")
    @Expose
    private String dokuId;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDokuId() {
        return dokuId;
    }

    public void setDokuId(String dokuId) {
        this.dokuId = dokuId;
    }

//    "name": "Ali Mustofa",
//            "email": "alimustofa@dycode.com",
//            "address": "",
//            "type": "N",
//            "dokuId": "1065140972"
}
