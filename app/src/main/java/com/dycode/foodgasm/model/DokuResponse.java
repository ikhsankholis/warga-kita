package com.dycode.foodgasm.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by yogi on 10/14/16.
 */

public class DokuResponse implements Parcelable {

    @SerializedName("res_token_id")
    @Expose
    private String res_token_id;
    @SerializedName("res_pairing_code")
    @Expose
    private String res_pairing_code;
    @SerializedName("res_response_msg")
    @Expose
    private String res_response_msg;
    @SerializedName("res_device_id")
    @Expose
    private String res_device_id;
    @SerializedName("res_amount")
    @Expose
    private String res_amount;
    @SerializedName("res_token_code")
    @Expose
    private String res_token_code;
    @SerializedName("res_transaction_id")
    @Expose
    private String res_transaction_id;
    @SerializedName("res_data_email")
    @Expose
    private String res_data_email;

    private String data;


    public DokuResponse(String dataResponse) {
        this.data = dataResponse;
    }


    public DokuResponse(String res_token_id, String res_pairing_code, String res_response_msg, String res_device_id, String res_amount, String res_token_code, String res_transaction_id, String res_data_email, String res_name, String res_payment_channel, String res_data_mobile_phone) {
        this.res_token_id = res_token_id;
        this.res_pairing_code = res_pairing_code;
        this.res_response_msg = res_response_msg;
        this.res_device_id = res_device_id;
        this.res_amount = res_amount;
        this.res_token_code = res_token_code;
        this.res_transaction_id = res_transaction_id;
        this.res_data_email = res_data_email;
        this.res_name = res_name;
        this.res_payment_channel = res_payment_channel;
        this.res_data_mobile_phone = res_data_mobile_phone;
    }

    public String getRes_data_mobile_phone() {
        return res_data_mobile_phone;
    }

    public void setRes_data_mobile_phone(String res_data_mobile_phone) {
        this.res_data_mobile_phone = res_data_mobile_phone;
    }

    public String getRes_token_id() {
        return res_token_id;
    }

    public void setRes_token_id(String res_token_id) {
        this.res_token_id = res_token_id;
    }

    public String getRes_pairing_code() {
        return res_pairing_code;
    }

    public void setRes_pairing_code(String res_pairing_code) {
        this.res_pairing_code = res_pairing_code;
    }

    public String getRes_response_msg() {
        return res_response_msg;
    }

    public void setRes_response_msg(String res_response_msg) {
        this.res_response_msg = res_response_msg;
    }

    public String getRes_device_id() {
        return res_device_id;
    }

    public void setRes_device_id(String res_device_id) {
        this.res_device_id = res_device_id;
    }

    public String getRes_amount() {
        return res_amount;
    }

    public void setRes_amount(String res_amount) {
        this.res_amount = res_amount;
    }

    public String getRes_token_code() {
        return res_token_code;
    }

    public void setRes_token_code(String res_token_code) {
        this.res_token_code = res_token_code;
    }

    public String getRes_transaction_id() {
        return res_transaction_id;
    }

    public void setRes_transaction_id(String res_transaction_id) {
        this.res_transaction_id = res_transaction_id;
    }

    public String getRes_data_email() {
        return res_data_email;
    }

    public void setRes_data_email(String res_data_email) {
        this.res_data_email = res_data_email;
    }

    public String getRes_name() {
        return res_name;
    }

    public void setRes_name(String res_name) {
        this.res_name = res_name;
    }

    public String getRes_payment_channel() {
        return res_payment_channel;
    }

    public void setRes_payment_channel(String res_payment_channel) {
        this.res_payment_channel = res_payment_channel;
    }

    @SerializedName("res_name")
    @Expose
    private String res_name;
    @SerializedName("res_payment_channel")
    @Expose
    private String res_payment_channel;
    @SerializedName("res_data_mobile_phone")
    @Expose
    private String res_data_mobile_phone;

    protected DokuResponse(Parcel in) {
        res_token_id = in.readString();
        res_pairing_code = in.readString();
        res_response_msg = in.readString();
        res_device_id = in.readString();
        res_amount = in.readString();
        res_token_code = in.readString();
        res_transaction_id = in.readString();
        res_data_email = in.readString();
        res_name = in.readString();
        res_payment_channel = in.readString();
        res_data_mobile_phone = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(res_token_id);
        dest.writeString(res_pairing_code);
        dest.writeString(res_response_msg);
        dest.writeString(res_device_id);
        dest.writeString(res_amount);
        dest.writeString(res_token_code);
        dest.writeString(res_transaction_id);
        dest.writeString(res_data_email);
        dest.writeString(res_name);
        dest.writeString(res_payment_channel);
        dest.writeString(res_data_mobile_phone);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<DokuResponse> CREATOR = new Creator<DokuResponse>() {
        @Override
        public DokuResponse createFromParcel(Parcel in) {
            return new DokuResponse(in);
        }

        @Override
        public DokuResponse[] newArray(int size) {
            return new DokuResponse[size];
        }
    };
}
