package com.dycode.foodgasm.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by asus pc on 28/01/2016.
 */
public class ServiceFee {
    @SerializedName("fee")
    @Expose
    private Double fee;
    @SerializedName("name")
    @Expose
    private String name;

    /**
     *
     * @return
     * The fee
     */
    public Double getFee() {
        return fee;
    }

    /**
     *
     * @param fee
     * The fee
     */
    public void setFee(Double fee) {
        this.fee = fee;
    }

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }
}
