package com.dycode.foodgasm.model;

/**
 * Created by asus pc on 01/03/2016.
 */
public class PatchOrder {
    private String action;
    private String reason;

    public PatchOrder(String action, String reason) {
        this.action = action;
        this.reason = reason;
    }
}
