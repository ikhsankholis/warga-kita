package com.dycode.foodgasm.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by asus pc on 04/11/2015.
 */
public class Inbox implements Parcelable {
    public static final String INBOX = "INBOX";

    @SerializedName("_id")
    @Expose
    private String Id;
    @SerializedName("ts")
    @Expose
    private String ts;
    @SerializedName("tu")
    @Expose
    private String tu;
    @SerializedName("user")
    @Expose
    private User user;
    @SerializedName("resto")
    @Expose
    private Resto resto;
    @SerializedName("numOfPeople")
    @Expose
    private String numOfPeople;
    @SerializedName("reason")
    @Expose
    private String reason;
    @SerializedName("orderStatus")
    @Expose
    private String orderStatus;
    @SerializedName("visitDate")
    @Expose
    private String visitDate;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("paymentWith")
    @Expose
    private String paymentWith;
    @SerializedName("paymentCredential")
    @Expose
    private String paymentCredential;
    @SerializedName("maskedCard")
    @Expose
    private String maskedCard;


    protected Inbox(Parcel in) {
        Id = in.readString();
        ts = in.readString();
        tu = in.readString();
        user = in.readParcelable(User.class.getClassLoader());
        resto = in.readParcelable(Resto.class.getClassLoader());

        numOfPeople = in.readString();
        reason = in.readString();
        orderStatus = in.readString();
        visitDate = in.readString();
        code = in.readString();
        paymentWith = in.readString();
        paymentCredential = in.readString();
        maskedCard = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(Id);
        dest.writeString(ts);
        dest.writeString(tu);
        dest.writeParcelable(user, flags);
        dest.writeParcelable(resto, flags);
        dest.writeString(numOfPeople);
        dest.writeString(reason);
        dest.writeString(orderStatus);
        dest.writeString(visitDate);
        dest.writeString(code);
        dest.writeString(paymentWith);
        dest.writeString(paymentCredential);
        dest.writeString(maskedCard);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Inbox> CREATOR = new Creator<Inbox>() {
        @Override
        public Inbox createFromParcel(Parcel in) {
            return new Inbox(in);
        }


        @Override
        public Inbox[] newArray(int size) {
            return new Inbox[size];
        }
    };

    public static String getInbox() {
        return INBOX;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getTs() {
        return ts;
    }

    public void setTs(String ts) {
        this.ts = ts;
    }

    public String getTu() {
        return tu;
    }

    public void setTu(String tu) {
        this.tu = tu;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Resto getResto() {
        return resto;
    }

    public void setResto(Resto resto) {
        this.resto = resto;
    }

    public String getNumOfPeople() {
        return numOfPeople;
    }

    public void setNumOfPeople(String numOfPeople) {
        this.numOfPeople = numOfPeople;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getVisitDate() {
        return visitDate;
    }

    public void setVisitDate(String visitDate) {
        this.visitDate = visitDate;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setPaymentWith(String paymentWith) {
        this.paymentWith = paymentWith;
    }

    public String getPaymentWith() {
        return paymentWith;
    }

    public void setPaymentCredential(String paymentCredential) {
        this.paymentCredential = paymentCredential;
    }

    public String getPaymentCredential() {
        return paymentCredential;
    }

    public void setMaskedCard(String maskedCard) {
        this.maskedCard = maskedCard;
    }

    public String getMaskedCard() {
        return maskedCard;
    }
}