package com.dycode.foodgasm.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by asus pc on 26/02/2016.
 */
public class AppVersion {

    @SerializedName("ios")
    @Expose
    private String ios;
    @SerializedName("android")
    @Expose
    private String android;

    /**
     *
     * @return
     * The ios
     */
    public String getIos() {
        return ios;
    }

    /**
     *
     * @param ios
     * The ios
     */
    public void setIos(String ios) {
        this.ios = ios;
    }

    /**
     *
     * @return
     * The android
     */
    public String getAndroid() {
        return android;
    }

    /**
     *
     * @param android
     * The android
     */
    public void setAndroid(String android) {
        this.android = android;
    }
}
