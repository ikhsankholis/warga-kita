package com.dycode.foodgasm.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by asus pc on 03/11/2015.
 */
public class Order{
    public static final String ORDER = "ORDER";
    public static final String ORDER_ID = "ORDER_ID";

    @SerializedName("ts")
    @Expose
    private String ts;
    @SerializedName("tu")
    @Expose
    private String tu;
    @SerializedName("user")
    @Expose
    private String user;
    @SerializedName("resto")
    @Expose
    private Restaurant resto;
    @SerializedName("numOfPeople")
    @Expose
    private Integer numOfPeople;
    @SerializedName("_id")
    @Expose
    private String Id;
    @SerializedName("note")
    @Expose
    private String note;
    @SerializedName("preorder")
    @Expose
    private PreOrderb preorder;
    @SerializedName("booking")
    @Expose
    private Booking booking;
    @SerializedName("reason")
    @Expose
    private Object reason;
    @SerializedName("orderStatus")
    @Expose
    private Integer orderStatus;
    @SerializedName("paymentStatus")
    @Expose
    private Boolean paymentStatus;
    @SerializedName("price")
    @Expose
    private Price price;
    @SerializedName("visitDate")
    @Expose
    private String visitDate;
    @SerializedName("bookingDate")
    @Expose
    private String bookingDate;
    @SerializedName("code")
    @Expose
    private String code;

    /**
     *
     * @return
     * The ts
     */
    public String getTs() {
        return ts;
    }

    /**
     *
     * @param ts
     * The ts
     */
    public void setTs(String ts) {
        this.ts = ts;
    }

    /**
     *
     * @return
     * The tu
     */
    public String getTu() {
        return tu;
    }

    /**
     *
     * @param tu
     * The tu
     */
    public void setTu(String tu) {
        this.tu = tu;
    }

    /**
     *
     * @return
     * The user
     */
    public String getUser() {
        return user;
    }

    /**
     *
     * @param user
     * The user
     */
    public void setUser(String user) {
        this.user = user;
    }

    /**
     *
     * @return
     * The resto
     */
    public Restaurant getResto() {
        return resto;
    }

    /**
     *
     * @param resto
     * The resto
     */
    public void setResto(Restaurant resto) {
        this.resto = resto;
    }

    /**
     *
     * @return
     * The numOfPeople
     */
    public Integer getNumOfPeople() {
        return numOfPeople;
    }

    /**
     *
     * @param numOfPeople
     * The numOfPeople
     */
    public void setNumOfPeople(Integer numOfPeople) {
        this.numOfPeople = numOfPeople;
    }

    /**
     *
     * @return
     * The Id
     */
    public String getId() {
        return Id;
    }

    /**
     *
     * @param Id
     * The _id
     */
    public void setId(String Id) {
        this.Id = Id;
    }

    /**
     *
     * @return
     * The note
     */
    public String getNote() {
        return note;
    }

    /**
     *
     * @param note
     * The note
     */
    public void setNote(String note) {
        this.note = note;
    }

    /**
     *
     * @return
     * The preorder
     */
    public PreOrderb getPreorder() {
        return preorder;
    }

    /**
     *
     * @param preorder
     * The preorder
     */
    public void setPreorder(PreOrderb preorder) {
        this.preorder = preorder;
    }

    /**
     *
     * @return
     * The booking
     */
    public Booking getBooking() {
        return booking;
    }

    /**
     *
     * @param booking
     * The booking
     */
    public void setBooking(Booking booking) {
        this.booking = booking;
    }

    /**
     *
     * @return
     * The reason
     */
    public Object getReason() {
        return reason;
    }

    /**
     *
     * @param reason
     * The reason
     */
    public void setReason(Object reason) {
        this.reason = reason;
    }

    /**
     *
     * @return
     * The orderStatus
     */
    public Integer getOrderStatus() {
        return orderStatus;
    }

    /**
     *
     * @param orderStatus
     * The orderStatus
     */
    public void setOrderStatus(Integer orderStatus) {
        this.orderStatus = orderStatus;
    }

    /**
     *
     * @return
     * The paymentStatus
     */
    public Boolean getPaymentStatus() {
        return paymentStatus;
    }

    /**
     *
     * @param paymentStatus
     * The paymentStatus
     */
    public void setPaymentStatus(Boolean paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    /**
     *
     * @return
     * The price
     */
    public Price getPrice() {
        return price;
    }

    /**
     *
     * @param price
     * The price
     */
    public void setPrice(Price price) {
        this.price = price;
    }

    /**
     *
     * @return
     * The visitDate
     */
    public String getVisitDate() {
        return visitDate;
    }

    /**
     *
     * @param visitDate
     * The visitDate
     */
    public void setVisitDate(String visitDate) {
        this.visitDate = visitDate;
    }

    /**
     *
     * @return
     * The bookingDate
     */
    public String getBookingDate() {
        return bookingDate;
    }

    /**
     *
     * @param bookingDate
     * The bookingDate
     */
    public void setBookingDate(String bookingDate) {
        this.bookingDate = bookingDate;
    }

    /**
     *
     * @return
     * The code
     */
    public String getCode() {
        return code;
    }

    /**
     *
     * @param code
     * The code
     */
    public void setCode(String code) {
        this.code = code;
    }
}
