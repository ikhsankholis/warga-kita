package com.dycode.foodgasm.model;

/**
 * Created by asus pc on 29/02/2016.
 */
public class DokuWalletRegister {

    private String name;
    private String email;
    private String code;

    public DokuWalletRegister(String name, String email, String code) {
        this.name = name;
        this.email = email;
        this.code = code;
    }
}
