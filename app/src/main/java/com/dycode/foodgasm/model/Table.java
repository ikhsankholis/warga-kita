package com.dycode.foodgasm.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by asus pc on 03/11/2015.
 */
public class Table implements Parcelable {
    String tableId;
    String tableName;
    String status;
    String quota;


    public Table(String tableId, String tableName, String status, String quota) {
        this.tableId = tableId;
        this.tableName = tableName;
        this.status = status;
        this.quota = quota;
    }

    public String getTableId() {
        return tableId;
    }

    public String getStatus() {
        return status;
    }

    public String getQuota() {
        return quota;
    }

    public String getTableName() {
        return tableName;
    }

    protected Table(Parcel in) {
        tableId = in.readString();
        tableName = in.readString();
        status = in.readString();
        quota = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(tableId);
        dest.writeString(tableName);
        dest.writeString(status);
        dest.writeString(quota);
    }

    public static final Parcelable.Creator<Table> CREATOR = new Parcelable.Creator<Table>() {
        public Table createFromParcel(Parcel in) {
            return new Table(in);
        }

        public Table[] newArray(int size) {
            return new Table[size];
        }
    };
}
