package com.dycode.foodgasm.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by asus pc on 09/11/2015.
 */
public class Category implements Parcelable {
    public static final String CATEGORY = "CATEGORY";
    public static final String CATEGORY_ID = "CATEGORY_ID";

    @SerializedName("_id")
    @Expose
    private String categoryId;

    //Category Name from Restaurant API
    //Category Name from Deals Detail List API
    @SerializedName("categoryName")
    @Expose
    private String categoryName;
    //Category Name from Menu category API
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("position")
    @Expose
    private int position = -1;
    @SerializedName("menus")
    @Expose
    private List<Menu> menus = new ArrayList<>();

    /**
     * @return The Id
     */
    public String getId() {
        return categoryId;
    }

    /**
     * @param Id The _id
     */
    public void setId(String Id) {
        this.categoryId = Id;
    }

    /**
     * @return The categoryName
     */
    public String getCategoryName() {
        return categoryName;
    }

    /**
     * @param categoryName The categoryName
     */
    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The position
     */
    public Integer getPosition() {
        return position;
    }

    /**
     *
     * @param position
     * The position
     */
    public void setPosition(Integer position) {
        this.position = position;
    }

    /**
     *
     * @return
     * The menus
     */
    public List<Menu> getMenus() {
        return menus;
    }

    /**
     *
     * @param menus
     * The menus
     */
    public void setMenus(List<Menu> menus) {
        this.menus = menus;
    }

    public Category(){}

    public Category(String categoryId, String categoryName, String name) {
        this.categoryId = categoryId;
        this.categoryName = categoryName;
        this.name = name;
    }

    protected Category(Parcel in) {
        categoryId = in.readString();
        categoryName = in.readString();
        name = in.readString();
        position = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(categoryId);
        dest.writeString(categoryName);
        dest.writeString(name);
        dest.writeInt(position);
    }

    public static final Parcelable.Creator<Category> CREATOR = new Parcelable.Creator<Category>() {
        public Category createFromParcel(Parcel in) {
            return new Category(in);
        }

        public Category[] newArray(int size) {
            return new Category[size];
        }
    };
}
