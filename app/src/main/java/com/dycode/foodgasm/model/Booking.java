package com.dycode.foodgasm.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by asus pc on 16/12/2015.
 */
public class Booking {

    @SerializedName("table")
    @Expose
    private List<Table> table = new ArrayList<Table>();
    @SerializedName("isBooking")
    @Expose
    private Boolean isBooking;

    /**
     *
     * @return
     * The table
     */
    public List<Table> getTable() {
        return table;
    }

    /**
     *
     * @param table
     * The table
     */
    public void setTable(List<Table> table) {
        this.table = table;
    }

    /**
     *
     * @return
     * The isBooking
     */
    public Boolean getIsBooking() {
        return isBooking;
    }

    /**
     *
     * @param isBooking
     * The isBooking
     */
    public void setIsBooking(Boolean isBooking) {
        this.isBooking = isBooking;
    }
}
