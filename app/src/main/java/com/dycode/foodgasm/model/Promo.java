package com.dycode.foodgasm.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by asus pc on 09/11/2015.
 */
public class Promo {
    public static final String PROMO = "PROMO";

    @SerializedName("_id")
    @Expose
    private String Id;
    @SerializedName("ts")
    @Expose
    private String ts;
    @SerializedName("tu")
    @Expose
    private String tu;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("bankType")
    @Expose
    private String bankType;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("pic")
    @Expose
    private String pic;
    @SerializedName("category")
    @Expose
    private String category;
    @SerializedName("menus")
    @Expose
    private List<String> menus = new ArrayList<String>();
    @SerializedName("valid")
    @Expose
    private Valid valid;
    @SerializedName("discount")
    @Expose
    private Discount discount;

    /**
     *
     * @return
     * The Id
     */
    public String getId() {
        return Id;
    }

    /**
     *
     * @param Id
     * The _id
     */
    public void setId(String Id) {
        this.Id = Id;
    }

    /**
     *
     * @return
     * The ts
     */
    public String getTs() {
        return ts;
    }

    /**
     *
     * @param ts
     * The ts
     */
    public void setTs(String ts) {
        this.ts = ts;
    }

    /**
     *
     * @return
     * The tu
     */
    public String getTu() {
        return tu;
    }

    /**
     *
     * @param tu
     * The tu
     */
    public void setTu(String tu) {
        this.tu = tu;
    }

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The bankType
     */
    public String getBankType() {
        return bankType;
    }

    /**
     *
     * @param bankType
     * The bankType
     */
    public void setBankType(String bankType) {
        this.bankType = bankType;
    }

    /**
     *
     * @return
     * The description
     */
    public String getDescription() {
        return description;
    }

    /**
     *
     * @param description
     * The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     *
     * @return
     * The pic
     */
    public String getPic() {
        return pic;
    }

    /**
     *
     * @param pic
     * The pic
     */
    public void setPic(String pic) {
        this.pic = pic;
    }

    /**
     *
     * @return
     * The category
     */
    public String getCategory() {
        return category;
    }

    /**
     *
     * @param category
     * The category
     */
    public void setCategory(String category) {
        this.category = category;
    }

    /**
     *
     * @return
     * The menus
     */
    public List<String> getMenus() {
        return menus;
    }

    /**
     *
     * @param menus
     * The menus
     */
    public void setMenus(List<String> menus) {
        this.menus = menus;
    }

    /**
     *
     * @return
     * The valid
     */
    public Valid getValid() {
        return valid;
    }

    /**
     *
     * @param valid
     * The valid
     */
    public void setValid(Valid valid) {
        this.valid = valid;
    }

    /**
     *
     * @return
     * The discount
     */
    public Discount getDiscount() {
        return discount;
    }

    /**
     *
     * @param discount
     * The discount
     */
    public void setDiscount(Discount discount) {
        this.discount = discount;
    }

}
