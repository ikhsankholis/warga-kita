package com.dycode.foodgasm.model;

/**
 * Created by asus pc on 24/11/2015.
 */
public class PostLogin {
    private String username;
    private String password;
    private String grant_type = "password";

    public PostLogin(String username, String password){
        this.username = username;
        this.password = password;
    }
}
