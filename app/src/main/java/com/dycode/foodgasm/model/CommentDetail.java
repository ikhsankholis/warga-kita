package com.dycode.foodgasm.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by asus pc on 03/12/2015.
 */
public class CommentDetail {
    public static final String COMMENT_DETAIL = "COMMENT_DETAIL";

    @SerializedName("_id")
    @Expose
    private String Id;
    @SerializedName("ts")
    @Expose
    private String ts;
    @SerializedName("tu")
    @Expose
    private String tu;
    @SerializedName("user")
    @Expose
    private User user;
    @SerializedName("rating")
    @Expose
    private Integer rating;
    @SerializedName("resto")
    @Expose
    private Restaurant resto;
    @SerializedName("comment")
    @Expose
    private String comment;
    @SerializedName("publish")
    @Expose
    private Boolean publish;

    /**
     * @return The Id
     */
    public String getId() {
        return Id;
    }

    /**
     * @param Id The _id
     */
    public void setId(String Id) {
        this.Id = Id;
    }

    /**
     * @return The ts
     */
    public String getTs() {
        return ts;
    }

    /**
     * @param ts The ts
     */
    public void setTs(String ts) {
        this.ts = ts;
    }

    /**
     * @return The tu
     */
    public String getTu() {
        return tu;
    }

    /**
     * @param tu The tu
     */
    public void setTu(String tu) {
        this.tu = tu;
    }

    /**
     * @return The user
     */
    public User getUser() {
        return user;
    }

    /**
     * @param user The user
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * @return The rating
     */
    public Integer getRating() {
        return rating;
    }

    /**
     * @param rating The rating
     */
    public void setRating(Integer rating) {
        this.rating = rating;
    }

    /**
     * @return The resto
     */
    public Restaurant getResto() {
        return resto;
    }

    /**
     * @param resto The resto
     */
    public void setResto(Restaurant resto) {
        this.resto = resto;
    }

    /**
     * @return The comment
     */
    public String getComment() {
        return comment;
    }

    /**
     * @param comment The comment
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

    /**
     * @return The publish
     */
    public Boolean getPublish() {
        return publish;
    }

    /**
     * @param publish The publish
     */
    public void setPublish(Boolean publish) {
        this.publish = publish;
    }

}
