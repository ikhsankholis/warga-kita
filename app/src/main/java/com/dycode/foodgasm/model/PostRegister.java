package com.dycode.foodgasm.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by asus pc on 04/12/2015.
 */
public class PostRegister {
    private String fullname;
    private String username;
    private String email;
    private String phone;
    private String password;
    private String passwordConfirmation;
    private String platform;
    private String provider;
    private String socialId;
    private String profilePicture;
    @SerializedName("userPhoneVerified")
    @Expose
    private boolean userPhoneVerified;

    public PostRegister(String fullname, String username, String email, String phone, String password, String passwordConfirmation, String platform, String provider, String socialId,String profilePict, boolean isVerified){
        this.fullname = fullname;
        this.username = username;
        this.email = email;
        this.phone = phone;
        this.password = password;
        this.passwordConfirmation = passwordConfirmation;
        this.platform = "android";
        this.provider = provider;
        this.socialId = socialId;
        this.profilePicture = profilePict;
        this.userPhoneVerified = isVerified;
    }

    public PostRegister(String fullname, String username, String email, String phone, String platform, String provider, String socialId,String profilePict){
        this.fullname = fullname;
        this.username = username;
        this.email = email;
        this.phone = phone;
        this.platform = "android";
        this.provider = provider;
        this.socialId = socialId;
        this.profilePicture = profilePict;
    }
}
