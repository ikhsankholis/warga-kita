package com.dycode.foodgasm.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by asus pc on 04/11/2015.
 */
public class DokuSample implements Parcelable {



    @SerializedName("req_mall_id")
    @Expose
    private String req_mall_id;
    @SerializedName("req_chain_merchant")
    @Expose
    private String req_chain_merchant;
    @SerializedName("req_transaction_id")
    @Expose
    private String req_transaction_id;
    @SerializedName("req_amount")
    @Expose
    private String req_amount;
    @SerializedName("req_basket")
    @Expose
    private String req_basket;
    @SerializedName("req_currency")
    @Expose
    private String req_currency;
    @SerializedName("req_name")
    @Expose
    private String req_name;
    @SerializedName("req_email")
    @Expose
    private String req_email;
    @SerializedName("req_session_id")
    @Expose
    private String req_session_id;
    @SerializedName("req_words")
    @Expose
    private String req_words;
    @SerializedName("saved_token_id")
    @Expose
    private String saved_token_id;
    @SerializedName("masked_card")
    @Expose
    private String masked_card;
    @SerializedName("customer_id")
    @Expose
    private String customer_id;
    @SerializedName("order_id")
    @Expose
    private String order_id;




    protected DokuSample(Parcel in) {
        req_mall_id = in.readString();
        req_chain_merchant = in.readString();
        req_transaction_id = in.readString();
        req_amount = in.readString();
        req_basket = in.readString();
        req_currency = in.readString();
        req_name = in.readString();
        req_email = in.readString();
        req_session_id = in.readString();
        req_words = in.readString();
        saved_token_id = in.readString();
        masked_card = in.readString();
        customer_id = in.readString();
        order_id = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(req_mall_id);
        dest.writeString(req_chain_merchant);
        dest.writeString(req_transaction_id);
        dest.writeString(req_amount);
        dest.writeString(req_basket);
        dest.writeString(req_currency);
        dest.writeString(req_name);
        dest.writeString(req_email);
        dest.writeString(req_session_id);
        dest.writeString(req_words);
        dest.writeString(saved_token_id);
        dest.writeString(masked_card);
        dest.writeString(customer_id);
        dest.writeString(order_id);

    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<DokuSample> CREATOR = new Creator<DokuSample>() {
        @Override
        public DokuSample createFromParcel(Parcel in) {
            return new DokuSample(in);
        }

        @Override
        public DokuSample[] newArray(int size) {
            return new DokuSample[size];
        }
    };

    public String getReq_mall_id() {
        return req_mall_id;
    }

    public void setReq_mall_id(String req_mall_id) {
        this.req_mall_id = req_mall_id;
    }

    public String getReq_chain_merchant() {
        return req_chain_merchant;
    }

    public void setReq_chain_merchant(String req_chain_merchant) {
        this.req_chain_merchant = req_chain_merchant;
    }

    public String getReq_transaction_id() {
        return req_transaction_id;
    }

    public void setReq_transaction_id(String req_transaction_id) {
        this.req_transaction_id = req_transaction_id;
    }

    public String getReq_amount() {
        return req_amount;
    }

    public void setReq_amount(String req_amount) {
        this.req_amount = req_amount;
    }

    public String getReq_basket() {
        return req_basket;
    }

    public void setReq_basket(String req_basket) {
        this.req_basket = req_basket;
    }

    public String getReq_currency() {
        return req_currency;
    }

    public void setReq_currency(String req_currency) {
        this.req_currency = req_currency;
    }

    public String getReq_name() {
        return req_name;
    }

    public void setReq_name(String req_name) {
        this.req_name = req_name;
    }

    public String getReq_email() {
        return req_email;
    }

    public void setReq_email(String req_email) {
        this.req_email = req_email;
    }

    public String getReq_session_id() {
        return req_session_id;
    }

    public void setReq_session_id(String req_session_id) {
        this.req_session_id = req_session_id;
    }

    public String getReq_words() {
        return req_words;
    }

    public void setReq_words(String req_words) {
        this.req_words = req_words;
    }

    public void setCustomer_id(String customer_id) {
        this.customer_id = customer_id;
    }

    public String getCustomer_id() {
        return customer_id;
    }

    public void setMasked_card(String masked_card) {
        this.masked_card = masked_card;
    }

    public String getMasked_card() {
        return masked_card;
    }

    public void setSaved_token_id(String saved_token_id) {
        this.saved_token_id = saved_token_id;
    }

    public String getSaved_token_id() {
        return saved_token_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getOrder_id() {
        return order_id;
    }
}
