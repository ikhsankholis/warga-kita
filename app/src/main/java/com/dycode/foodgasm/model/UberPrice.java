package com.dycode.foodgasm.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by asus pc on 01/02/2016.
 */
public class UberPrice {
    @SerializedName("product_id")
    @Expose
    private String productId;
    @SerializedName("currency_code")
    @Expose
    private String currencyCode;
    @SerializedName("display_name")
    @Expose
    private String displayName;
    @SerializedName("estimate")
    @Expose
    private String estimate;
    @SerializedName("low_estimate")
    @Expose
    private Integer lowEstimate;
    @SerializedName("high_estimate")
    @Expose
    private Integer highEstimate;
    @SerializedName("surge_multiplier")
    @Expose
    private Double surgeMultiplier;
    @SerializedName("duration")
    @Expose
    private Integer duration;
    @SerializedName("distance")
    @Expose
    private Double distance;
    @SerializedName("Times")
    @Expose
    private Times times;

    /**
     *
     * @return
     * The productId
     */
    public String getProductId() {
        return productId;
    }

    /**
     *
     * @param productId
     * The product_id
     */
    public void setProductId(String productId) {
        this.productId = productId;
    }

    /**
     *
     * @return
     * The currencyCode
     */
    public String getCurrencyCode() {
        return currencyCode;
    }

    /**
     *
     * @param currencyCode
     * The currency_code
     */
    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    /**
     *
     * @return
     * The displayName
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     *
     * @param displayName
     * The display_name
     */
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    /**
     *
     * @return
     * The estimate
     */
    public String getEstimate() {
        return estimate;
    }

    /**
     *
     * @param estimate
     * The estimate
     */
    public void setEstimate(String estimate) {
        this.estimate = estimate;
    }

    /**
     *
     * @return
     * The lowEstimate
     */
    public Integer getLowEstimate() {
        return lowEstimate;
    }

    /**
     *
     * @param lowEstimate
     * The low_estimate
     */
    public void setLowEstimate(Integer lowEstimate) {
        this.lowEstimate = lowEstimate;
    }

    /**
     *
     * @return
     * The highEstimate
     */
    public Integer getHighEstimate() {
        return highEstimate;
    }

    /**
     *
     * @param highEstimate
     * The high_estimate
     */
    public void setHighEstimate(Integer highEstimate) {
        this.highEstimate = highEstimate;
    }

    /**
     *
     * @return
     * The surgeMultiplier
     */
    public Double getSurgeMultiplier() {
        return surgeMultiplier;
    }

    /**
     *
     * @param surgeMultiplier
     * The surge_multiplier
     */
    public void setSurgeMultiplier(Double surgeMultiplier) {
        this.surgeMultiplier = surgeMultiplier;
    }

    /**
     *
     * @return
     * The duration
     */
    public Integer getDuration() {
        return duration;
    }

    /**
     *
     * @param duration
     * The duration
     */
    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    /**
     *
     * @return
     * The distance
     */
    public Double getDistance() {
        return distance;
    }

    /**
     *
     * @param distance
     * The distance
     */
    public void setDistance(Double distance) {
        this.distance = distance;
    }

    /**
     *
     * @return
     * The Times
     */
    public Times getTimes() {
        return times;
    }

    /**
     *
     * @param times
     * The Times
     */
    public void setTimes(Times times) {
        this.times = times;
    }
}
