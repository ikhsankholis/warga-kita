package com.dycode.foodgasm.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by asus pc on 17/02/2016.
 */
public class ReviewPoints {
    private String reviewPoints;
    private boolean isActive = false;

    public ReviewPoints(String reviewPoints) {
        this.reviewPoints = reviewPoints;
    }

    public boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(boolean isActive){
        this.isActive = isActive;
    }

    public String getReviewPoints() {
        return reviewPoints;
    }

    public void setReviewPoints(String reviewPoints) {
        this.reviewPoints = reviewPoints;
    }
}
