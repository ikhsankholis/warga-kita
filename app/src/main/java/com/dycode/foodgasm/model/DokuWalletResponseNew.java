package com.dycode.foodgasm.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by asus pc on 29/02/2016.
 */
public class DokuWalletResponseNew {
//
//    "promotionName": "",
//            "promotionAmount": 0,
//            "clientId": "1620",
//            "responseCode": "0008",
//            "responseMessage": {
//        "id": "Balance Anda tidak cukup",
//                "en": "Your Balance is not Enough"
//    }


    @SerializedName("promotionName")
    @Expose
    private String promotionName;
    @SerializedName("clientId")
    @Expose
    private String clientId;
    @SerializedName("responseCode")
    @Expose
    private String responseCode;

    public String getPromotionName() {
        return promotionName;
    }

    public void setPromotionName(String promotionName) {
        this.promotionName = promotionName;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }
}
