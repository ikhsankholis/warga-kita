package com.dycode.foodgasm.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by asus pc on 22/01/2016.
 */
public class DealsDetail {
    @SerializedName("_id")
    @Expose
    private String Id;
    @SerializedName("ts")
    @Expose
    private String ts;
    @SerializedName("tu")
    @Expose
    private String tu;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("bankType")
    @Expose
    private String bankType;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("pic")
    @Expose
    private String pic;
    @SerializedName("discount")
    @Expose
    private Integer discount;
    @SerializedName("restaurants")
    @Expose
    private List<Restaurant> restaurants = new ArrayList<Restaurant>();
    @SerializedName("valid")
    @Expose
    private Valid valid;

    /**
     *
     * @return
     * The Id
     */
    public String getId() {
        return Id;
    }

    /**
     *
     * @param Id
     * The _id
     */
    public void setId(String Id) {
        this.Id = Id;
    }

    /**
     *
     * @return
     * The ts
     */
    public String getTs() {
        return ts;
    }

    /**
     *
     * @param ts
     * The ts
     */
    public void setTs(String ts) {
        this.ts = ts;
    }

    /**
     *
     * @return
     * The tu
     */
    public String getTu() {
        return tu;
    }

    /**
     *
     * @param tu
     * The tu
     */
    public void setTu(String tu) {
        this.tu = tu;
    }

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The bankType
     */
    public String getBankType() {
        return bankType;
    }

    /**
     *
     * @param bankType
     * The bankType
     */
    public void setBankType(String bankType) {
        this.bankType = bankType;
    }

    /**
     *
     * @return
     * The description
     */
    public String getDescription() {
        return description;
    }

    /**
     *
     * @param description
     * The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     *
     * @return
     * The pic
     */
    public String getPic() {
        return pic;
    }

    /**
     *
     * @param pic
     * The pic
     */
    public void setPic(String pic) {
        this.pic = pic;
    }

    /**
     *
     * @return
     * The discount
     */
    public Integer getDiscount() {
        return discount;
    }

    /**
     *
     * @param discount
     * The discount
     */
    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    /**
     *
     * @return
     * The restaurants
     */
    public List<Restaurant> getRestaurants() {
        return restaurants;
    }

    /**
     *
     * @param restaurants
     * The restaurants
     */
    public void setRestaurants(List<Restaurant> restaurants) {
        this.restaurants = restaurants;
    }

    /**
     *
     * @return
     * The valid
     */
    public Valid getValid() {
        return valid;
    }

    /**
     *
     * @param valid
     * The valid
     */
    public void setValid(Valid valid) {
        this.valid = valid;
    }
}
