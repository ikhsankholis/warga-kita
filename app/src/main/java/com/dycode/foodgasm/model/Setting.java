package com.dycode.foodgasm.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by asus pc on 23/01/2016.
 */
public class Setting implements Parcelable{
    @SerializedName("reviewPoints")
    @Expose
    private List<String> reviewPoints = new ArrayList<String>();
    @SerializedName("orderType")
    @Expose
    private OrderType orderType;


    /**
     *
     * @return
     * The reviewPoints
     */
    public List<String> getReviewPoints() {
        return reviewPoints;
    }

    /**
     *
     * @param reviewPoints
     * The reviewPoints
     */
    public void setReviewPoints(List<String> reviewPoints) {
        this.reviewPoints = reviewPoints;
    }

    /**
     *
     * @return
     * The orderType
     */
    public OrderType getOrderType() {
        return orderType;
    }

    /**
     *
     * @param orderType
     * The orderType
     */
    public void setOrderType(OrderType orderType) {
        this.orderType = orderType;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringList(reviewPoints);
        dest.writeParcelable(orderType, flags);
    }

    protected Setting(Parcel in) {
        reviewPoints = in.createStringArrayList();
        orderType = in.readParcelable(OrderType.class.getClassLoader());
    }

    public static final Creator<Setting> CREATOR = new Creator<Setting>() {
        @Override
        public Setting createFromParcel(Parcel in) {
            return new Setting(in);
        }

        @Override
        public Setting[] newArray(int size) {
            return new Setting[size];
        }
    };
}
