package com.dycode.foodgasm.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by asus pc on 28/01/2016.
 */
public class Trip {
    @SerializedName("distance_unit")
    @Expose
    private String distanceUnit;
    @SerializedName("duration_estimate")
    @Expose
    private Integer durationEstimate;
    @SerializedName("distance_estimate")
    @Expose
    private Double distanceEstimate;

    /**
     *
     * @return
     * The distanceUnit
     */
    public String getDistanceUnit() {
        return distanceUnit;
    }

    /**
     *
     * @param distanceUnit
     * The distance_unit
     */
    public void setDistanceUnit(String distanceUnit) {
        this.distanceUnit = distanceUnit;
    }

    /**
     *
     * @return
     * The durationEstimate
     */
    public Integer getDurationEstimate() {
        return durationEstimate;
    }

    /**
     *
     * @param durationEstimate
     * The duration_estimate
     */
    public void setDurationEstimate(Integer durationEstimate) {
        this.durationEstimate = durationEstimate;
    }

    /**
     *
     * @return
     * The distanceEstimate
     */
    public Double getDistanceEstimate() {
        return distanceEstimate;
    }

    /**
     *
     * @param distanceEstimate
     * The distance_estimate
     */
    public void setDistanceEstimate(Double distanceEstimate) {
        this.distanceEstimate = distanceEstimate;
    }
}
