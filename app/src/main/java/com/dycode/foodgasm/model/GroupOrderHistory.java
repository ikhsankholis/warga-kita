package com.dycode.foodgasm.model;

import java.util.List;

/**
 * Created by fahmi on 08/01/2016.
 */
public class GroupOrderHistory {

    String name;
    List<com.dycode.foodgasm.dao.OrderHistory> orderHistoryList;

    public GroupOrderHistory(String name, List<com.dycode.foodgasm.dao.OrderHistory> orderHistoryList) {
        this.name = name;
        this.orderHistoryList = orderHistoryList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<com.dycode.foodgasm.dao.OrderHistory> getOrderHistoryList() {
        return orderHistoryList;
    }

    public void setOrderHistoryList(List<com.dycode.foodgasm.dao.OrderHistory> orderHistoryList) {
        this.orderHistoryList = orderHistoryList;
    }
}
