package com.dycode.foodgasm.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by asus pc on 12/01/2016.
 */
public class Voucher {
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("valueType")
    @Expose
    private String valueType;
    @SerializedName("value")
    @Expose
    private float value;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("valid")
    @Expose
    private Valid valid;
    @SerializedName("voucher")
    @Expose
    private VoucherDetail voucherDetail;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getValueType() {
        return valueType;
    }

    public void setValueType(String valueType) {
        this.valueType = valueType;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Valid getValid() {
        return valid;
    }

    public void setValid(Valid valid) {
        this.valid = valid;
    }

    public VoucherDetail getVoucherDetail() {
        return voucherDetail;
    }

    public void setVoucherDetail(VoucherDetail voucherDetail) {
        this.voucherDetail = voucherDetail;
    }
}

