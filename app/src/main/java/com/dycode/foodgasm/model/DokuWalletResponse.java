package com.dycode.foodgasm.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by asus pc on 29/02/2016.
 */
public class DokuWalletResponse {



    //    "dokuId": "1546345120",
//            "dpMallId": "1201",
//            "amount": 5000,
//            "transactionId": "PR8O45QBV20161102192920",
//            "trackingId": "48442",
//            "result": "SUCCESS",
//            "approvalCode": "272404",
//            "promotionName": "",
//            "promotionAmount": 0,
//            "clientId": "1620",
//            "responseCode": "0000",
//            "responseMessage": {
//        "id": "SUKSES",
//                "en": "SUCCESS"
//    }
    @SerializedName("dokuId")
    @Expose
    private String dokuId;
    @SerializedName("dpMallId")
    @Expose
    private String dpMallId;
    @SerializedName("amount")
    @Expose
    private String amount;
    @SerializedName("transactionId")
    @Expose
    private String transactionId;
    @SerializedName("trackingId")
    @Expose
    private String trackingId;
    @SerializedName("result")
    @Expose
    private String result;
    @SerializedName("promotionName")
    @Expose
    private String promotionName;
    @SerializedName("approvalCode")
    @Expose
    private String approvalCode;
    @SerializedName("clientId")
    @Expose
    private String clientId;
    @SerializedName("responseCode")
    @Expose
    private String responseCode;


    public String getDokuId() {
        return dokuId;
    }

    public void setDokuId(String dokuId) {
        this.dokuId = dokuId;
    }

    public String getDpMallId() {
        return dpMallId;
    }

    public void setDpMallId(String dpMallId) {
        this.dpMallId = dpMallId;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getTrackingId() {
        return trackingId;
    }

    public void setTrackingId(String trackingId) {
        this.trackingId = trackingId;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getPromotionName() {
        return promotionName;
    }

    public void setPromotionName(String promotionName) {
        this.promotionName = promotionName;
    }

    public String getApprovalCode() {
        return approvalCode;
    }

    public void setApprovalCode(String approvalCode) {
        this.approvalCode = approvalCode;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }
}
