package com.dycode.foodgasm.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by asus pc on 16/12/2015.
 */
public class Price {

    @SerializedName("subtotal")
    @Expose
    private int subtotal;
    @SerializedName("discount")
    @Expose
    private float discount;
    @SerializedName("tax")
    @Expose
    private int tax;
    @SerializedName("total")
    @Expose
    private int total;
    @SerializedName("bankCharge")
    @Expose
    private int bankCharge;
    @SerializedName("convenience")
    @Expose
    private int convenience;
    @SerializedName("additionalCharge")
    @Expose
    private int additionalCharge;



    public int getBankCharge() {
        return bankCharge;
    }

    public void setBankCharge(int bankCharge) {
        this.bankCharge = bankCharge;
    }

    public int getConvenience() {
        return convenience;
    }

    public void setConvenience(int convenience) {
        this.convenience = convenience;
    }

    public float getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    public int getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(int subtotal) {
        this.subtotal = subtotal;
    }

    public int getTax() {
        return tax;
    }

    public void setTax(int tax) {
        this.tax = tax;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getAdditionalCharge() {
        return additionalCharge;
    }

    public void setAdditionalCharge(int additionalCharge) {
        this.additionalCharge = additionalCharge;
    }
}
