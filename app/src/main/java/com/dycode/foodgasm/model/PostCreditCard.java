package com.dycode.foodgasm.model;

/**
 * Created by asus pc on 29/02/2016.
 */
public class PostCreditCard {
    private String transaction_id;
    private String saved_token_id;
    private String masked_card;
    private String provider;

    public PostCreditCard(String transaction_id, String saved_token_id, String masked_card, String provider) {
        this.transaction_id = transaction_id;
        this.saved_token_id = saved_token_id;
        this.masked_card = masked_card;
        this.provider = provider;
    }
}
