package com.dycode.foodgasm.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by asus pc on 28/01/2016.
 */
public class Products {

    @SerializedName("capacity")
    @Expose
    private Integer capacity;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("price_details")
    @Expose
    private PriceDetails priceDetails;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("display_name")
    @Expose
    private String displayName;
    @SerializedName("product_id")
    @Expose
    private String productId;
    @SerializedName("Prices")
    @Expose
    private UberPrices uberPrices;
    @SerializedName("Times")
    @Expose
    private Times times;

    /**
     *
     * @return
     * The capacity
     */
    public Integer getCapacity() {
        return capacity;
    }

    /**
     *
     * @param capacity
     * The capacity
     */
    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }

    /**
     *
     * @return
     * The description
     */
    public String getDescription() {
        return description;
    }

    /**
     *
     * @param description
     * The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     *
     * @return
     * The priceDetails
     */
    public PriceDetails getPriceDetails() {
        return priceDetails;
    }

    /**
     *
     * @param priceDetails
     * The price_details
     */
    public void setPriceDetails(PriceDetails priceDetails) {
        this.priceDetails = priceDetails;
    }

    /**
     *
     * @return
     * The image
     */
    public String getImage() {
        return image;
    }

    /**
     *
     * @param image
     * The image
     */
    public void setImage(String image) {
        this.image = image;
    }

    /**
     *
     * @return
     * The displayName
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     *
     * @param displayName
     * The display_name
     */
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    /**
     *
     * @return
     * The productId
     */
    public String getProductId() {
        return productId;
    }

    /**
     *
     * @param productId
     * The product_id
     */
    public void setProductId(String productId) {
        this.productId = productId;
    }

    /**
     *
     * @return
     * The uberPrices
     */
    public UberPrices getUberPrices() {
        return uberPrices;
    }

    /**
     *
     * @param uberPrices
     * The uberPrices
     */
    public void setUberPrices(UberPrices uberPrices) {
        this.uberPrices = uberPrices;
    }

    /**
     *
     * @return
     * The Times
     */
    public Times getTimes() {
        return times;
    }

    /**
     *
     * @param times
     * The Times
     */
    public void setTimes(Times times) {
        this.times = times;
    }


}
