package com.dycode.foodgasm.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by asus pc on 01/02/2016.
 */
public class UberPrices {
    @SerializedName("prices")
    @Expose
    private List<UberPrice> prices = new ArrayList<UberPrice>();

    /**
     *
     * @return
     * The prices
     */
    public List<UberPrice> getPrices() {
        return prices;
    }

    /**
     *
     * @param prices
     * The prices
     */
    public void setPrices(List<UberPrice> prices) {
        this.prices = prices;
    }
}
