package com.dycode.foodgasm.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by asus pc on 16/12/2015.
 */
public class Preorder {

    @SerializedName("menus")
    @Expose
    private List<Menu> menus = new ArrayList<Menu>();
    @SerializedName("isOrder")
    @Expose
    private Boolean isOrder;

    /**
     *
     * @return
     * The menus
     */
    public List<Menu> getMenus() {
        return menus;
    }

    /**
     *
     * @param menus
     * The menus
     */
    public void setMenus(List<Menu> menus) {
        this.menus = menus;
    }

    /**
     *
     * @return
     * The isOrder
     */
    public Boolean getIsOrder() {
        return isOrder;
    }

    /**
     *
     * @param isOrder
     * The isOrder
     */
    public void setIsOrder(Boolean isOrder) {
        this.isOrder = isOrder;
    }

}
