package com.dycode.foodgasm.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by asus pc on 22/12/2015.
 */
public class Body {

    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("isManager")
    @Expose
    private Boolean isManager;

    /**
     *
     * @return
     * The username
     */
    public String getUsername() {
        return username;
    }

    /**
     *
     * @param username
     * The username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     *
     * @return
     * The isManager
     */
    public Boolean getIsManager() {
        return isManager;
    }

    /**
     *
     * @param isManager
     * The isManager
     */
    public void setIsManager(Boolean isManager) {
        this.isManager = isManager;
    }
}
