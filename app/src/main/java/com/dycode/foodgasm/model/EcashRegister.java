package com.dycode.foodgasm.model;

/**
 * Created by yogi on 3/10/17.
 */

public class EcashRegister {

//    phonenumber : String, required
//    idnumber : String, required. No. KTP Cenahnya mah
//    fullname : String, required
//    dateofbirth : String, required (yyyy-mm-dd)
//    city : String, required. your address.
//    email : String, required
//    pin : String, required. 6 Digit isi sendiri bebas.
//    otp : String, OTP Code dari API Request ECash OTP

    String phonenumber;
    String idnumber;
    String fullname;
    String dateofbirth;
    String city;
    String email;
    String pin;
    String otp;
    boolean isregistered;

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public String getIdnumber() {
        return idnumber;
    }

    public void setIdnumber(String idnumber) {
        this.idnumber = idnumber;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getDateofbirth() {
        return dateofbirth;
    }

    public void setDateofbirth(String dateofbirth) {
        this.dateofbirth = dateofbirth;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }


    public EcashRegister(String phonenumber, String idnumber, String fullname, String dateofbirth, String city, String email, String pin, String otp, boolean isregistered) {
        this.phonenumber = phonenumber;
        this.idnumber = idnumber;
        this.fullname = fullname;
        this.dateofbirth = dateofbirth;
        this.city = city;
        this.email = email;
        this.pin = pin;
        this.otp = otp;
        this.isregistered = isregistered;
    }

    public EcashRegister(String phonenumber, String pin, String otp, boolean isregistered) {
        this.pin = pin;
        this.otp = otp;
        this.isregistered = isregistered;
        this.phonenumber = phonenumber;
    }


}
