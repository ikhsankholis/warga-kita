package com.dycode.foodgasm.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by asus pc on 20/11/2015.
 */
public class ApiResponseList<T> {

    @SerializedName("data")
    @Expose
    private List<T> data = new ArrayList<T>();
    @SerializedName("meta")
    @Expose
    private Meta meta;

    public void setData(List<T> data) {
        this.data = data;
    }

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    public List<T> getData() {
        return data;
    }

}

