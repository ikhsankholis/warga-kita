package com.dycode.foodgasm.model;

/**
 * Created by asus pc on 28/01/2016.
 */
public class PostUber {
    private String product_id;
    private Double start_latitude;
    private Double start_longitude;
    private Double end_latitude;
    private Double end_longitude;

    public PostUber(String product_id, Double start_latitude, Double start_longitude, Double end_latitude, Double end_longitude) {
        this.product_id = product_id;
        this.start_latitude = start_latitude;
        this.start_longitude = start_longitude;
        this.end_latitude = end_latitude;
        this.end_longitude = end_longitude;
    }
}
