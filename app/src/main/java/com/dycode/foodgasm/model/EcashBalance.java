package com.dycode.foodgasm.model;

/**
 * Created by yogi on 3/10/17.
 */

public class EcashBalance {

//    phonenumber : String, required
//    idnumber : String, required. No. KTP Cenahnya mah
//    fullname : String, required
//    dateofbirth : String, required (yyyy-mm-dd)
//    city : String, required. your address.
//    email : String, required
//    pin : String, required. 6 Digit isi sendiri bebas.
//    otp : String, OTP Code dari API Request ECash OTP

    String accountID;
    String msisdn;

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getAccountID() {
        return accountID;
    }

    public void setAccountID(String accountID) {
        this.accountID = accountID;
    }



    public EcashBalance(String accountID, String msisdn) {
        this.accountID = accountID;
        this.msisdn = msisdn;
    }
}
