package com.dycode.foodgasm.model;

/**
 * Created by yogi on 3/10/17.
 */

public class EcashOTP {

//    mobileno : String, required. Ex.  081394813631
//    trxType : String, required. (Static value N01)


    String mobileno;
    String trxType;

    public EcashOTP(String mobileno, String trxType) {
        this.mobileno = mobileno;
        this.trxType = trxType;
    }

    public String getMobileno() {
        return mobileno;
    }

    public void setMobileno(String mobileno) {
        this.mobileno = mobileno;
    }

    public String getTrxType() {
        return trxType;
    }

    public void setTrxType(String trxType) {
        this.trxType = trxType;
    }

}
