package com.dycode.foodgasm.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by yogi on 3/10/17.
 */

public class EcashRegisterModel {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("partner_id")
    @Expose
    private String partner_id;
    @SerializedName("groupID")
    @Expose
    private String groupID;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("msisdn")
    @Expose
    private String msisdn;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("MID")
    @Expose
    private String MID;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPartner_id() {
        return partner_id;
    }

    public void setPartner_id(String partner_id) {
        this.partner_id = partner_id;
    }

    public String getGroupID() {
        return groupID;
    }

    public void setGroupID(String groupID) {
        this.groupID = groupID;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMID() {
        return MID;
    }

    public void setMID(String MID) {
        this.MID = MID;
    }




}
