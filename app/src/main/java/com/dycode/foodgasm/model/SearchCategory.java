package com.dycode.foodgasm.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by asus pc on 04/11/2015.
 */
public class SearchCategory implements Parcelable {

    @SerializedName("_id")
    @Expose
    private String Id;
    @SerializedName("categoryName")
    @Expose
    private String categoryName;
    @SerializedName("createAt")
    @Expose
    private String createAt;



    protected SearchCategory(Parcel in) {
        Id = in.readString();
        categoryName = in.readString();
        createAt = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(Id);
        dest.writeString(categoryName);
        dest.writeString(createAt);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<SearchCategory> CREATOR = new Creator<SearchCategory>() {
        @Override
        public SearchCategory createFromParcel(Parcel in) {
            return new SearchCategory(in);
        }

        @Override
        public SearchCategory[] newArray(int size) {
            return new SearchCategory[size];
        }
    };


    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCreateAt() {
        return createAt;
    }

    public void setCreateAt(String createAt) {
        this.createAt = createAt;
    }
}