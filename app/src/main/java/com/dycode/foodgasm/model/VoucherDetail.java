package com.dycode.foodgasm.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by USER on 6/20/2017.
 */

public class VoucherDetail {

    @SerializedName("minTransaction")
    @Expose
    private int minTransaction;
    @SerializedName("maxDiscount")
    @Expose
    private int maxDiscount;
    @SerializedName("valueType")
    @Expose
    private String valueType;
    @SerializedName("value")
    @Expose
    private int value;

    public int getMinTransaction() {
        return minTransaction;
    }

    public void setMinTransaction(int minTransaction) {
        this.minTransaction = minTransaction;
    }

    public int getMaxDiscount() {
        return maxDiscount;
    }

    public void setMaxDiscount(int maxDiscount) {
        this.maxDiscount = maxDiscount;
    }


    public String getValueType() {
        return valueType;
    }

    public int getValue() {
        return value;
    }
}
