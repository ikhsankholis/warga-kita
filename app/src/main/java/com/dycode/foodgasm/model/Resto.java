package com.dycode.foodgasm.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by yogi on 8/3/16.
 */
public class Resto implements Parcelable {
    @SerializedName("_id")
    @Expose
    private String idResto;
    @SerializedName("name")
    @Expose
    private String nameResto;
    @SerializedName("pic")
    @Expose
    private String picResto;

    protected Resto(Parcel in) {
        idResto = in.readString();
        nameResto = in.readString();
        picResto = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(idResto);
        dest.writeString(nameResto);
        dest.writeString(picResto);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Resto> CREATOR = new Creator<Resto>() {
        @Override
        public Resto createFromParcel(Parcel in) {
            return new Resto(in);
        }

        @Override
        public Resto[] newArray(int size) {
            return new Resto[size];
        }
    };

    public String getIdResto() {
        return idResto;
    }

    public void setIdResto(String idResto) {
        this.idResto = idResto;
    }

    public String getNameResto() {
        return nameResto;
    }

    public void setNameResto(String nameResto) {
        this.nameResto = nameResto;
    }

    public String getPicResto() {
        return picResto;
    }

    public void setPicResto(String picResto) {
        this.picResto = picResto;
    }
}
