package com.dycode.foodgasm.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by asus pc on 15/01/2016.
 */
public class PreOrderb {

    @SerializedName("menus")
    @Expose
    private List<String> menus = new ArrayList<String>();
    @SerializedName("isOrder")
    @Expose
    private Boolean isOrder;

    /**
     *
     * @return
     * The menus
     */
    public List<String> getMenus() {
        return menus;
    }

    /**
     *
     * @param menus
     * The menus
     */
    public void setMenus(List<String> menus) {
        this.menus = menus;
    }

    /**
     *
     * @return
     * The isOrder
     */
    public Boolean getIsOrder() {
        return isOrder;
    }

    /**
     *
     * @param isOrder
     * The isOrder
     */
    public void setIsOrder(Boolean isOrder) {
        this.isOrder = isOrder;
    }

}
