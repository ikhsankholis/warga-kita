package com.dycode.foodgasm.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by yogi on 3/10/17.
 */

public class ReqPhoneVerify {

    @SerializedName("_id")
    @Expose
    private String _id;
    @SerializedName("status")
    @Expose
    String status;
    @SerializedName("phone")
    @Expose
    String number;
    @SerializedName("code")
    @Expose
    String code;
    @SerializedName("token_id")
    @Expose
    String token_id;

    public ReqPhoneVerify(String code, String token_id) {
        this.code = code;
        this.token_id = token_id;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getToken_id() {
        return token_id;
    }

    public void setToken_id(String token_id) {
        this.token_id = token_id;
    }


}
