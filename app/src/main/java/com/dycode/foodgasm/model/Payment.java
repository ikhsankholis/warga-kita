package com.dycode.foodgasm.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by asus pc on 28/12/2015.
 */
public class Payment {

    @SerializedName("approval_code")
    @Expose
    private String approvalCode;
    @SerializedName("bank")
    @Expose
    private String bank;
    @SerializedName("eci")
    @Expose
    private String eci;
    @SerializedName("fraud_status")
    @Expose
    private String fraudStatus;
    @SerializedName("gross_amount")
    @Expose
    private String grossAmount;
    @SerializedName("masked_card")
    @Expose
    private String maskedCard;
    @SerializedName("order_id")
    @Expose
    private String orderId;
    @SerializedName("payment_type")
    @Expose
    private String paymentType;
    @SerializedName("transaction_id")
    @Expose
    private String transactionId;
    @SerializedName("transaction_status")
    @Expose
    private String transactionStatus;
    @SerializedName("transaction_time")
    @Expose
    private String transactionTime;

    /**
     *
     * @return
     * The approvalCode
     */
    public String getApprovalCode() {
        return approvalCode;
    }

    /**
     *
     * @param approvalCode
     * The approval_code
     */
    public void setApprovalCode(String approvalCode) {
        this.approvalCode = approvalCode;
    }

    /**
     *
     * @return
     * The bank
     */
    public String getBank() {
        return bank;
    }

    /**
     *
     * @param bank
     * The bank
     */
    public void setBank(String bank) {
        this.bank = bank;
    }

    /**
     *
     * @return
     * The eci
     */
    public String getEci() {
        return eci;
    }

    /**
     *
     * @param eci
     * The eci
     */
    public void setEci(String eci) {
        this.eci = eci;
    }

    /**
     *
     * @return
     * The fraudStatus
     */
    public String getFraudStatus() {
        return fraudStatus;
    }

    /**
     *
     * @param fraudStatus
     * The fraud_status
     */
    public void setFraudStatus(String fraudStatus) {
        this.fraudStatus = fraudStatus;
    }

    /**
     *
     * @return
     * The grossAmount
     */
    public String getGrossAmount() {
        return grossAmount;
    }

    /**
     *
     * @param grossAmount
     * The gross_amount
     */
    public void setGrossAmount(String grossAmount) {
        this.grossAmount = grossAmount;
    }

    /**
     *
     * @return
     * The maskedCard
     */
    public String getMaskedCard() {
        return maskedCard;
    }

    /**
     *
     * @param maskedCard
     * The masked_card
     */
    public void setMaskedCard(String maskedCard) {
        this.maskedCard = maskedCard;
    }

    /**
     *
     * @return
     * The orderId
     */
    public String getOrderId() {
        return orderId;
    }

    /**
     *
     * @param orderId
     * The order_id
     */
    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    /**
     *
     * @return
     * The paymentType
     */
    public String getPaymentType() {
        return paymentType;
    }

    /**
     *
     * @param paymentType
     * The payment_type
     */
    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    /**
     *
     * @return
     * The transactionId
     */
    public String getTransactionId() {
        return transactionId;
    }

    /**
     *
     * @param transactionId
     * The transaction_id
     */
    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    /**
     *
     * @return
     * The transactionStatus
     */
    public String getTransactionStatus() {
        return transactionStatus;
    }

    /**
     *
     * @param transactionStatus
     * The transaction_status
     */
    public void setTransactionStatus(String transactionStatus) {
        this.transactionStatus = transactionStatus;
    }

    /**
     *
     * @return
     * The transactionTime
     */
    public String getTransactionTime() {
        return transactionTime;
    }

    /**
     *
     * @param transactionTime
     * The transaction_time
     */
    public void setTransactionTime(String transactionTime) {
        this.transactionTime = transactionTime;
    }
}
