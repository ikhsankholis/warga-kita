package com.dycode.foodgasm.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by asus pc on 04/01/2016.
 */
public class OrderType implements Parcelable  {
    public static final String OrderType = "ORDERTYPE";
    @SerializedName("requestTabel")
    @Expose
    private Boolean requestTabel;
    @SerializedName("requestTabelPreorder")
    @Expose
    private Boolean requestTabelPreorder;
    @SerializedName("takeAway")
    @Expose
    private Boolean takeAway;

    /**
     *
     * @return
     * The requestTabel
     */
    public Boolean getRequestTabel() {
        return requestTabel;
    }

    /**
     *
     * @param requestTabel
     * The requestTabel
     */
    public void setRequestTabel(Boolean requestTabel) {
        this.requestTabel = requestTabel;
    }

    /**
     *
     * @return
     * The requestTabelPreorder
     */
    public Boolean getRequestTabelPreorder() {
        return requestTabelPreorder;
    }

    /**
     *
     * @param requestTabelPreorder
     * The requestTabelPreorder
     */
    public void setRequestTabelPreorder(Boolean requestTabelPreorder) {
        this.requestTabelPreorder = requestTabelPreorder;
    }

    /**
     *
     * @return
     * The takeAway
     */
    public Boolean getTakeAway() {
        return takeAway;
    }

    /**
     *
     * @param takeAway
     * The takeAway
     */
    public void setTakeAway(Boolean takeAway) {
        this.takeAway = takeAway;
    }


    protected OrderType(Parcel in) {
        requestTabel = (in.readInt()!= 0);
        requestTabelPreorder = (in.readInt()!= 0);
        takeAway = (in.readInt()!= 0);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(requestTabel ? 1 : 0);
        dest.writeInt(requestTabelPreorder ? 1 : 0);
        dest.writeInt(takeAway ? 1 : 0);

    }

    public static final Parcelable.Creator<OrderType> CREATOR = new Parcelable.Creator<OrderType>() {
        public OrderType createFromParcel(Parcel in) {
            return new OrderType(in);
        }

        public OrderType[] newArray(int size) {
            return new OrderType[size];
        }
    };
}
