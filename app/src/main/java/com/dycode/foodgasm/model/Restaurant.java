package com.dycode.foodgasm.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by asus pc on 04/11/2015.
 */
public class Restaurant implements Parcelable {
    public static final String RESTAURANT = "RESTAURANT";

    @SerializedName("_id")
    @Expose
    private String restaurantID;
    @SerializedName("name")
    @Expose
    String restaurantName;
    @SerializedName("phone")
    @Expose
    private String restaurantContact;
    @SerializedName("category")
    @Expose
    Category category;
    String restaurantType;
    @SerializedName("address")
    @Expose
    Address address;
    @SerializedName("statusCheckout")
    @Expose
    private String statusCheckout;
//    @SerializedName("owner")
//    @Expose
//    Owner owner;
    @SerializedName("__v")
    @Expose
    private int V;
    @SerializedName("pic")
    @Expose
    private String restaurantPicBig;
    @SerializedName("picThumbnail")
    @Expose
    private String picThumbnail;
    @SerializedName("websites")
    @Expose
    private String restaurantWebPage;
    @SerializedName("tax")
    @Expose
    private double tax;
    @SerializedName("open")
    @Expose
    Open open;
    @SerializedName("location")
    @Expose
    Location location;
//    @SerializedName("city")
//    @Expose
    private String city;
    @SerializedName("distance")
    @Expose
    private double distance;
    @SerializedName("promos")
    @Expose
    private List<Promo> promos = new ArrayList<Promo>();
    @SerializedName("numOfReview")
    @Expose
    private int numOfReview;
    @SerializedName("logo")
    @Expose
    private String logo;
    @SerializedName("logoThumbnail")
    @Expose
    private String logoThumbnail;
    @SerializedName("categoryName")
    @Expose
    private String categoryName;
    @SerializedName("picBig")
    @Expose
    private String picBig;
    @SerializedName("logoBig")
    @Expose
    private String logoBig;
    @SerializedName("setting")
    @Expose
    Setting setting;
    @SerializedName("menuVersion")
    @Expose
    private int menuVersion;
    @SerializedName("commission")
    @Expose
    private String commission;
    @SerializedName("closedState")
    @Expose
    private int closedState;
    @SerializedName("menuState")
    @Expose
    private int menuState;

//    @SerializedName("convenience")
//    @Expose
//    private int convenience;

    //For search purpose
    private Boolean isHistory = false;

    public Restaurant(String id, String restaurantName, String restaurantCity) {
        this.restaurantID = id;
        this.restaurantName = restaurantName;
        this.isHistory = true;
        this.city = restaurantCity;
    }

    protected Restaurant(Parcel in) {
        restaurantID = in.readString();
        restaurantName = in.readString();
        restaurantContact = in.readString();
        category = in.readParcelable(Category.class.getClassLoader());
        restaurantType = in.readString();
        address = in.readParcelable(Address.class.getClassLoader());
        statusCheckout = in.readString();
        V = in.readInt();
        restaurantPicBig = in.readString();
        picThumbnail = in.readString();
        restaurantWebPage = in.readString();
        tax = in.readDouble();
        open = in.readParcelable(Open.class.getClassLoader());
        location = in.readParcelable(Location.class.getClassLoader());
        city = in.readString();
        distance = in.readDouble();
        numOfReview = in.readInt();
        logo = in.readString();
        logoThumbnail = in.readString();
        categoryName = in.readString();
        picBig = in.readString();
        logoBig = in.readString();
        setting = in.readParcelable(Setting.class.getClassLoader());
        menuVersion = in.readInt();
        commission = in.readString();
        closedState = in.readInt();
        menuState = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(restaurantID);
        dest.writeString(restaurantName);
        dest.writeString(restaurantContact);
        dest.writeParcelable(category, flags);
        dest.writeString(restaurantType);
        dest.writeParcelable(address, flags);
        dest.writeString(statusCheckout);
        dest.writeInt(V);
        dest.writeString(restaurantPicBig);
        dest.writeString(picThumbnail);
        dest.writeString(restaurantWebPage);
        dest.writeDouble(tax);
        dest.writeParcelable(open, flags);
        dest.writeParcelable(location, flags);
        dest.writeString(city);
        dest.writeDouble(distance);
        dest.writeInt(numOfReview);
        dest.writeString(logo);
        dest.writeString(logoThumbnail);
        dest.writeString(categoryName);
        dest.writeString(picBig);
        dest.writeString(logoBig);
        dest.writeParcelable(setting, flags);
        dest.writeInt(menuVersion);
        dest.writeString(commission);
        dest.writeInt(closedState);
        dest.writeInt(menuState);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Restaurant> CREATOR = new Creator<Restaurant>() {
        @Override
        public Restaurant createFromParcel(Parcel in) {
            return new Restaurant(in);
        }

        @Override
        public Restaurant[] newArray(int size) {
            return new Restaurant[size];
        }
    };

    public Boolean getIsHistory() {
        return isHistory;
    }

    /**
     * @return The Id
     */
    public String getId() {
        return restaurantID;
    }

    /**
     * @param Id The _id
     */
    public void setId(String Id) {
        this.restaurantID = Id;
    }

    /**
     * @return The name
     */
    public String getName() {
        return restaurantName;
    }

    /**
     * @param name The name
     */
    public void setName(String name) {
        this.restaurantName = name;
    }

    /**
     * @return The phone
     */
    public String getPhone() {
        return restaurantContact;
    }

    /**
     * @param phone The phone
     */
    public void setPhone(String phone) {
        this.restaurantContact = phone;
    }

    /**
     * @return The category
     */
    public Category getCategory() {
        return category;
    }

    /**
     * @param category The category
     */
    public void setCategory(Category category) {
        this.category = category;
    }

    /**
     * @return The address
     */
    public Address getAddress() {
        return address;
    }

    /**
     * @param address The address
     */
    public void setAddress(Address address) {
        this.address = address;
    }

    /**
     * @return The statusCheckout
     */
    public String getStatusCheckout() {
        return statusCheckout;
    }

    /**
     * @param statusCheckout The statusCheckout
     */
    public void setStatusCheckout(String statusCheckout) {
        this.statusCheckout = statusCheckout;
    }

    /**
     * @return The owner
     */
//    public Owner getOwner() {
//        return owner;
//    }
//
//    /**
//     * @param owner The owner
//     */
//    public void setOwner(Owner owner) {
//        this.owner = owner;
//    }

    /**
     * @return The V
     */
    public int getV() {
        return V;
    }

    /**
     * @param V The __v
     */
    public void setV(int V) {
        this.V = V;
    }

    /**
     * @return The pic
     */
    public String getPic() {
        return restaurantPicBig;
    }

    /**
     * @param pic The pic
     */
    public void setPic(String pic) {
        this.restaurantPicBig = pic;
    }

    /**
     * @return The websites
     */
    public String getWebsites() {
        return restaurantWebPage;
    }

    /**
     * @param websites The websites
     */
    public void setWebsites(String websites) {
        this.restaurantWebPage = websites;
    }

    /**
     * @return The tax
     */
    public Double getTax() {
        return tax;
    }

    /**
     * @param tax The tax
     */
    public void setTax(Double tax) {
        this.tax = tax;
    }

    /**
     * @return The open
     */
    public Open getOpen() {
        return open;
    }

    /**
     * @param open The open
     */
    public void setOpen(Open open) {
        this.open = open;
    }


    /**
     * @return The city
     */
    public String getCity() {
        return city;
    }

    /**
     * @param city The city
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * @return The location
     */
    public Location getLocation() {
        return location;
    }

    /**
     * @param location The location
     */
    public void setLocation(Location location) {
        this.location = location;
    }

    /**
     * @return The distance
     */
    public Double getDistance() {
        return distance;
    }

    /**
     * @param distance The distance
     */
    public void setDistance(Double distance) {
        this.distance = distance;
    }

    /**
     * @return The promos
     */
    public List<Promo> getPromos() {
        return promos;
    }

    /**
     * @param promos The promos
     */
    public void setPromos(List<Promo> promos) {
        this.promos = promos;
    }

    /**
     * @return The numOfReview
     */
    public int getNumOfReview() {
        return numOfReview;
    }

    /**
     * @param numOfReview The numOfReview
     */
    public void setNumOfReview(int numOfReview) {
        this.numOfReview = numOfReview;
    }

    /**
     * @return The logo
     */
    public String getLogo() {
        return logo;
    }

    /**
     * @param logo The logo
     */
    public void setLogo(String logo) {
        this.logo = logo;
    }


    /**
     * @return The categoryName
     */
    public String getCategoryName() {
        return categoryName;
    }

    /**
     * @param categoryName The categoryName
     */
    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    /**
     * @return The picBig
     */
    public String getPicBig() {
        return picBig;
    }

    /**
     * @param picBig The picBig
     */
    public void setPicBig(String picBig) {
        this.picBig = picBig;
    }

    /**
     * @return The logoBig
     */
    public String getLogoBig() {
        return logoBig;
    }

    /**
     * @param logoBig The logoBig
     */
    public void setLogoBig(String logoBig) {
        this.logoBig = logoBig;
    }

    /**
     * @return The logoThumbnail
     */
    public String getLogoThumbnail() {
        return logoThumbnail;
    }

    /**
     * @param logoThumbnail The logoThumbnail
     */
    public void setLogoThumbnail(String logoThumbnail) {
        this.logoThumbnail = logoThumbnail;
    }


    /**
     * @return The setting
     */
    public Setting getSetting() {
        return setting;
    }

    /**
     * @param setting The setting
     */
    public void setSetting(Setting setting) {
        this.setting = setting;
    }

    /**
     * @return The menuVersion
     */
    public int getMenuVersion() {
        return menuVersion;
    }

    public int getMenuState() {
        return menuState;
    }

    public void setMenuState(int menuState) {
        this.menuState = menuState;
    }

    /**
     * @param menuVersion The menuVersion
     */
    public void setMenuVersion(int menuVersion) {
        this.menuVersion = menuVersion;
    }

//    public Integer getConvenience() {
//        return convenience;
//    }
//
//    public void setConvenience(Integer convenience) {
//        this.convenience = convenience;
//    }


    public void setCommission(String commission) {
        this.commission = commission;
    }

    public String getCommission() {
        return commission;
    }

    public int getClosedState() {
        return closedState;
    }

    public void setClosedState(int closedState) {
        this.closedState = closedState;
    }
}