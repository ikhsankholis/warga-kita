package com.dycode.foodgasm.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by asus pc on 01/02/2016.
 */
public class Time {
    @SerializedName("product_id")
    @Expose
    private String productId;
    @SerializedName("display_name")
    @Expose
    private String displayName;
    @SerializedName("estimate")
    @Expose
    private Integer estimate;

    /**
     *
     * @return
     * The productId
     */
    public String getProductId() {
        return productId;
    }

    /**
     *
     * @param productId
     * The product_id
     */
    public void setProductId(String productId) {
        this.productId = productId;
    }

    /**
     *
     * @return
     * The displayName
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     *
     * @param displayName
     * The display_name
     */
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    /**
     *
     * @return
     * The estimate
     */
    public Integer getEstimate() {
        return estimate;
    }

    /**
     *
     * @param estimate
     * The estimate
     */
    public void setEstimate(Integer estimate) {
        this.estimate = estimate;
    }
}
