package com.dycode.foodgasm.model;

import java.util.List;

/**
 * Created by asus pc on 16/12/2015.
 */
public class PostOrder {
    private String user;
    private String resto;
    private String visitDate;
    private Boolean isBooking;
    private int numOfPeople;
    List<com.dycode.foodgasm.dao.Menu> menus;
    private String note;
    private String coupon;
    Price price;
    private String maskedCard;
    private String paymentWith;
    private String paymentCredential;


    public PostOrder(String user, String resto, String visitDate, Boolean isBooking, int numOfPeople, List<com.dycode.foodgasm.dao.Menu> menus, String note, Price price, String coupon, String maskedCard, String paymentWith) {
        this.user = user;
        this.resto = resto;
        this.visitDate = visitDate;
        this.isBooking = isBooking;
        this.numOfPeople = numOfPeople;
        this.menus = menus;
        this.note = note;
        this.price = price;
        this.coupon = coupon;
        this.maskedCard = maskedCard;
        this.paymentWith = paymentWith;
    }

    public PostOrder(String user, String resto, String visitDate, Boolean isBooking, int numOfPeople, List<com.dycode.foodgasm.dao.Menu> menus, String note, Price price, String coupon, String maskedCard, String paymentWith, String paymentCredential) {
        this.user = user;
        this.resto = resto;
        this.visitDate = visitDate;
        this.isBooking = isBooking;
        this.numOfPeople = numOfPeople;
        this.menus = menus;
        this.note = note;
        this.price = price;
        this.coupon = coupon;
        this.maskedCard = maskedCard;
        this.paymentWith = paymentWith;
        this.paymentCredential = paymentCredential;
    }

    public PostOrder(String user, String resto, String visitDate, Boolean isBooking, int numOfPeople, List<com.dycode.foodgasm.dao.Menu> menus, String note, Price price, String coupon) {
        this.user = user;
        this.resto = resto;
        this.visitDate = visitDate;
        this.isBooking = isBooking;
        this.numOfPeople = numOfPeople;
        this.menus = menus;
        this.note = note;
        this.price = price;
        this.coupon = coupon;
    }
}
