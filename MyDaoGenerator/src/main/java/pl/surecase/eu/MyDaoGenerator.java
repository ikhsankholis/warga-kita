package pl.surecase.eu;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Schema;

public class MyDaoGenerator {

    public static void main(String args[]) throws Exception {
        Schema schema = new Schema(29, "com.dycode.foodgasm.dao");

        addProfile(schema);
        addRestaurant(schema);
        addPromo(schema);
        addMenu(schema);
        addGallery(schema);
        addDeal(schema);
        addComment(schema);
        addCategory(schema);
        addOrderHistory(schema);
        addOrderMenu(schema);
        addSearchHistory(schema);
        addCreditCard(schema);
        addRestoAddress(schema);
        addMenuVersion(schema);

        new DaoGenerator().generateAll(schema, args[0]);
//        new DaoGenerator().generateAll(schema, "/Users/faerulsalamun/Documents/Kerjaan/Dycode/Android/foodgasm-user/app/src/main/java");
    }


    private static void addProfile(Schema schema) {
        Entity profile = schema.addEntity("Profile");
        profile.setHasKeepSections(true);
        profile.setTableName("Profile");
        profile.addStringProperty("_id").primaryKey();
        profile.addStringProperty("email");
        profile.addStringProperty("fullname");
        profile.addStringProperty("phone");
        profile.addStringProperty("role");
        profile.addStringProperty("username");
    }

    private static void addRestaurant(Schema schema) {
        Entity restaurant = schema.addEntity("Restaurant");
        restaurant.setHasKeepSections(true);
        restaurant.setTableName("Restaurant");
        restaurant.addStringProperty("_id").primaryKey();
        restaurant.addStringProperty("name");
        restaurant.addStringProperty("phone");
        restaurant.addStringProperty("category");
        restaurant.addStringProperty("address");
        restaurant.addStringProperty("city");
        restaurant.addIntProperty("statusCheckout");
        restaurant.addStringProperty("ownerEmail");
        restaurant.addStringProperty("ownerId");
        restaurant.addStringProperty("ownerUsername");
        restaurant.addIntProperty("__v");
        restaurant.addStringProperty("pic");
        restaurant.addStringProperty("websites");
        restaurant.addIntProperty("tax");
        restaurant.addStringProperty("opentime");
        restaurant.addStringProperty("closetime");
        restaurant.addDoubleProperty("latitude");
        restaurant.addDoubleProperty("longitude");
        restaurant.addDoubleProperty("distance");
        restaurant.addIntProperty("numOfReview");
        restaurant.addIntProperty("convenience");
    }

    private static void addPromo(Schema schema) {
        Entity promo = schema.addEntity("Promo");
        promo.setHasKeepSections(true);
        promo.setTableName("Promo");
        promo.addStringProperty("_id").primaryKey();
        promo.addDateProperty("createdAt");
        promo.addDateProperty("updatedAt");
        promo.addStringProperty("name");
        promo.addStringProperty("description");
        promo.addStringProperty("category");
        promo.addStringProperty("pic");
        promo.addStringProperty("restoId");
        promo.addDateProperty("validFrom");
        promo.addDateProperty("validTo");
        promo.addStringProperty("discountKind");
        promo.addIntProperty("discountValue");
    }

    private static void addMenu(Schema schema) {
        Entity menu = schema.addEntity("Menu");
        menu.setHasKeepSections(true);
        menu.setTableName("Menu");
        menu.addStringProperty("_id").primaryKey();
        menu.addDateProperty("createdAt");
        menu.addDateProperty("updatedAt");
        menu.addStringProperty("name");
        menu.addIntProperty("price");
        menu.addIntProperty("discount");
        menu.addIntProperty("discountPrice");
        menu.addStringProperty("phone");
        menu.addStringProperty("categoryId");
        menu.addStringProperty("categoryName");
        menu.addStringProperty("description");
        menu.addStringProperty("pic");
        menu.addStringProperty("restoId");
        menu.addIntProperty("amount");
        menu.addIntProperty("subtotalPrice");
        menu.addStringProperty("note");
    }

    private static void addGallery(Schema schema) {
        Entity doctor = schema.addEntity("Gallery");
        doctor.setHasKeepSections(true);
        doctor.setTableName("Gallery");
        doctor.addStringProperty("_id").primaryKey();
        doctor.addStringProperty("pic");
        doctor.addStringProperty("restoId");
    }

    private static void addDeal(Schema schema) {
        Entity deal = schema.addEntity("Deal");
        deal.setHasKeepSections(true);
        deal.setTableName("Deal");
        deal.addStringProperty("_id").primaryKey();
        deal.addDateProperty("createdAt");
        deal.addDateProperty("updatedAt");
        deal.addDateProperty("validFrom");
        deal.addDateProperty("validTo");
        deal.addStringProperty("name");
        deal.addStringProperty("pic");
        deal.addStringProperty("desc");
        deal.addStringProperty("bank");
        deal.addIntProperty("discount");
    }

    private static void addComment(Schema schema) {
        Entity comment = schema.addEntity("Comment");
        comment.setHasKeepSections(true);
        comment.setTableName("Comment");
        comment.addStringProperty("_id").primaryKey();
        comment.addStringProperty("comment");
        comment.addStringProperty("username");
        comment.addStringProperty("userId");
        comment.addStringProperty("restoId");
        comment.addIntProperty("rating");
        comment.addBooleanProperty("publish");
    }

    private static void addCategory(Schema schema) {
        Entity category = schema.addEntity("Category");
        category.setHasKeepSections(true);
        category.setTableName("Category");
        category.addStringProperty("_id").primaryKey();
        category.addStringProperty("restoId");
        category.addStringProperty("name");
        category.addIntProperty("position");
    }

    private static void addOrderHistory(Schema schema) {
        Entity orderHistory = schema.addEntity("OrderHistory");
        orderHistory.setHasKeepSections(true);
        orderHistory.addStringProperty("_id").primaryKey();
        orderHistory.addStringProperty("userId");
        orderHistory.addStringProperty("username");
        orderHistory.addStringProperty("restoId");
        orderHistory.addStringProperty("restoName");
        orderHistory.addStringProperty("restoPic");
        orderHistory.addIntProperty("numOfPeople");
        orderHistory.addStringProperty("reason");
        orderHistory.addStringProperty("note");
        orderHistory.addStringProperty("subTotal");
        orderHistory.addStringProperty("tax");
        orderHistory.addStringProperty("total");
        orderHistory.addBooleanProperty("isBooking");
        orderHistory.addBooleanProperty("isOrder");
        orderHistory.addIntProperty("orderStatus");
        orderHistory.addDateProperty("bookingDate");
        orderHistory.addDateProperty("visitDate");
        orderHistory.addBooleanProperty("isClicked");
    }

    private static void addOrderMenu(Schema schema) {
        Entity orderMenu = schema.addEntity("OrderMenu");
        orderMenu.setHasKeepSections(true);
        orderMenu.addStringProperty("_id").primaryKey();
        orderMenu.addStringProperty("orderId");
        orderMenu.addStringProperty("name");
        orderMenu.addIntProperty("amount");
        orderMenu.addStringProperty("price");
        orderMenu.addStringProperty("subtotalPrice");
        orderMenu.addStringProperty("discountPrice");
        orderMenu.addBooleanProperty("available");
    }

    private static void addSearchHistory(Schema schema) {
        Entity searchHistory = schema.addEntity("SearchHistory");
        searchHistory.setHasKeepSections(true);
        searchHistory.addStringProperty("keyword").primaryKey();
        searchHistory.addStringProperty("resto_city");
        searchHistory.addLongProperty("created_at");
        searchHistory.addStringProperty("resto_id");
    }

    private static void addCreditCard(Schema schema){
        Entity creditCard = schema.addEntity("CreditCard");
        creditCard.setHasKeepSections(true);
        creditCard.addStringProperty("_id").primaryKey();
        creditCard.addStringProperty("transaction_id");
        creditCard.addStringProperty("saved_token_id");
        creditCard.addStringProperty("masked_card");
        creditCard.addBooleanProperty("state");
        creditCard.addBooleanProperty("isEditSelected");
    }

    private static void addRestoAddress(Schema schema){
        Entity restoAddress = schema.addEntity("RestoAdress");
        restoAddress.setHasKeepSections(true);
        restoAddress.addStringProperty("_id").primaryKey();
        restoAddress.addStringProperty("street");
        restoAddress.addStringProperty("district");
        restoAddress.addStringProperty("city");
        restoAddress.addStringProperty("province");
        restoAddress.addStringProperty("postalCode");
    }

    private static void addMenuVersion(Schema schema){
        Entity restoAddress = schema.addEntity("MenuVersion");
        restoAddress.setHasKeepSections(true);
        restoAddress.addStringProperty("restoId").primaryKey();
        restoAddress.addIntProperty("menuVersion");
    }


}